//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This file is part of the C4 Engine and is provided under the
// terms of the license agreement entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef MGBase_h
#define MGBase_h


#include "C4Node.h"
#include "C4Application.h"


extern "C"
{
	C4MODULEEXPORT C4::Application *ConstructApplication(void);
}


namespace C4
{
	enum
	{
		kInputXInverted			= 1 << 0,
		kInputYInverted			= 1 << 1
	};


	enum
	{
		kPlayerColorCount		= 11
	};


	enum
	{
		kLocatorSpawn			= 'spwn',
		kLocatorSpectator		= 'spec',
		kLocatorBenchmark		= 'bnch'
	};


	enum
	{
		kSoundGroupEffects		= 'efct',
		kSoundGroupMusic		= 'musi',
		kSoundGroupVoice		= 'voic'
	};


	enum
	{
		kCollisionCorpse		= kCollisionBaseKind << 0,
		kCollisionRemains		= kCollisionBaseKind << 1,
		kCollisionPlayer		= kCollisionBaseKind << 2,
		kCollisionShot			= kCollisionBaseKind << 3,
		kCollisionPlasma		= kCollisionBaseKind << 4,
		kCollisionFireball		= kCollisionBaseKind << 5,
		kCollisionBlackCat		= kCollisionBaseKind << 6,
		kCollisionVenom			= kCollisionBaseKind << 7
	};


	class GameCharacterController;
	class GameRigidBodyController;


	struct SplashDamageData
	{
		Array<GameCharacterController *, 32>	characterArray;
		Array<GameRigidBodyController *, 32>	rigidBodyArray;
	};


	extern const char kConnectorKeyTeleporter[];
	extern const char kConnectorKeyJump[];
}


#endif

// ZYUTNLM
