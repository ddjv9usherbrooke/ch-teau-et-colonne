#include "CastleController.h"

#include "AIController.h"
#include "TriggerController.h"

using namespace C4;

CastleController::CastleController() :
Controller(kControllerCastle), heightMax_(0.0f)
{

}

CastleController::CastleController(const CastleController& castleController) : Controller(castleController)
{

}

CastleController::~CastleController()
{
}

Controller *CastleController::Replicate(void) const
{
	return (new CastleController(*this));
}

bool CastleController::ValidNode(const Node *node)
{
	return (node->GetNodeType() == kNodeTrigger);
}

void CastleController::Preprocess(void)
{

}

void CastleController::Activate(Node *initiator, Node *trigger)
{
	Node* flag = trigger->GetConnectedNode("target");
	if (heightMax_ < 15.0f)
	{
		flag->SetNodePosition(flag->GetNodePosition() + C4::Point3D(0.0f, 0.0f, 0.01f));
		heightMax_ += 0.01f;
		flag->Invalidate();
	}
}

