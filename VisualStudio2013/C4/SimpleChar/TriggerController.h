#pragma once

#include "../../../GameCode/SimpleChar.h"

namespace C4
{
	class TriggerController : public Controller
	{

	private:

		TriggerController(const TriggerController& triggerController);

		Controller *Replicate(void) const override;

	public:

		TriggerController();
		~TriggerController();

		static bool ValidNode(const Node *node);

		void Preprocess(void) override;

		void Activate(Node *initiator, Node *trigger) override;

	};
}