#include "AIController.h"

#include "TriggerController.h"
#include "CastleController.h"
#include "Machine.h"

using namespace C4;

AIController::AIController() : RigidBodyController(kControllerAI), machine_(this)
{
}

AIController::AIController(const AIController& aiController) : RigidBodyController(aiController), machine_(aiController.getMachine())
{

}

AIController::~AIController()
{
}

Controller *AIController::Replicate(void) const
{
	return (new AIController(*this));
}


bool AIController::ValidNode(const Node *node)
{
	return (node->GetNodeType() == kNodeGeometry);
}

void AIController::Preprocess(void)
{
	RigidBodyController::Preprocess();

	SetRigidBodyFlags(kRigidBodyKeepAwake | kRigidBodyFixedOrientation);
	SetFrictionCoefficient(0.0F);

}

void AIController::Move(void)
{
	machine_.execute();
}

void AIController::Activate(Node *initiator, Node *trigger)
{

}
RigidBodyStatus AIController::HandleNewRigidBodyContact(const RigidBodyContact *contact, RigidBodyController *contactBody)
{
	return (kRigidBodyUnchanged);
}