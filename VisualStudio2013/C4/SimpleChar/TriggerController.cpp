#include "TriggerController.h"

#include "AIController.h"
#include "CastleController.h"

using namespace C4;

TriggerController::TriggerController() : Controller(kControllerTrigger)
{
	
}

TriggerController::TriggerController(const TriggerController& triggerController) : Controller(triggerController)
{
	
}

TriggerController::~TriggerController()
{
}

Controller *TriggerController::Replicate(void) const
{
	return (new TriggerController(*this));
}

bool TriggerController::ValidNode(const Node *node)
{
	return (node->GetNodeType() == kNodeTrigger);
}

void TriggerController::Preprocess(void)
{

}


void TriggerController::Activate(Node *initiator, Node *trigger)
{
	Model *model = Model::Get(kModelAI);
	AIController *controller = new AIController();
	model->SetController(controller); // ?
	TheWorldMgr->GetWorld()->AddNewNode(model);
	model->SetNodePosition(C4::Point3D(10, 10, 10));
	Transform4D t = Transform4D();
	/*t.SetIdentity();
	t.SetRotationAboutZ(-0.5f);
	model->SetWorldTransform(t);*/
}