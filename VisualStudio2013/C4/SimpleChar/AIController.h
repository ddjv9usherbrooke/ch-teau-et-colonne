#pragma once

#include "../../../GameCode/SimpleChar.h"
#include "Machine.h"

namespace C4
{
	class AIController : public RigidBodyController
	{
	private:
		Machine machine_;

		AIController(const AIController& aiController);

		Controller *Replicate(void) const override;

	public:

		AIController();
		~AIController();

		static bool ValidNode(const Node *node);

		void Preprocess(void) override;

		RigidBodyStatus HandleNewRigidBodyContact(const RigidBodyContact *contact, RigidBodyController *contactBody);
		void Move(void) override;

		void Activate(Node *initiator, Node *trigger) override;

		Machine getMachine() const
		{
			return machine_;
		}
	};
}