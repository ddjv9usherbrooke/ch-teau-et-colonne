Entry (id = 'BTTN')
{
	Entry (id = 'OKAY') {string {"OK"}}
	Entry (id = 'CANC') {string {"Cancel"}}
}
Entry (id = 'QSAV') {string {"Game saved."}}
Entry (id = 'LOAD') {string {"Load Saved Game"}}
Entry (id = 'SAVE') {string {"Save Current Game"}}
Entry (id = 'MULT')
{
	Entry (id = 'CONN') {string {" [#8F8]connected."}}
	Entry (id = 'DSCN') {string {" [#8F8]disconnected."}}
	Entry (id = 'TIME') {string {" [#8F8]timed out."}}
	Entry (id = 'LOST') {string {"[#8F8]Connection lost."}}
	Entry (id = 'DIED') {string {" [#8F8]died."}}
	Entry (id = 'KILL') {string {" [#8F8]killed [#FFF]"}}
	Entry (id = 'SUIC') {string {" [#8F8]suicided."}}
	Entry (id = 'STOP') {string {"[#8F8]."}}
	Entry (id = 'CHAT') {string {"[#8F8]: "}}
	Entry (id = 'NAME') {string {" [#8F8]renamed to [#FFF]"}}
	Entry (id = 'ACPT') {string {"Connection accepted"}}
	Entry (id = 'FAIL')
	{
		Entry (id = 'TIME') {string {"No response from server"}}
		Entry (id = 'PROT') {string {"Protocol mismatch with server"}}
		Entry (id = 'NSRV') {string {"Connection refused by server"}}
		Entry (id = 'FULL') {string {"Server full"}}
	}
}
Entry (id = 'MISC')
{
	Entry (id = 'USE ') {string {"Use"}}
	Entry (id = 'DFGN') {string {"Game"}}
	Entry (id = 'DFPN') {string {"Player"}}
}
Entry (id = 'CTRL')
{
	Entry (id = 'body')
	{
		string {"Game Rigid Body"}
		Entry (id = 'GAME') {string {"Game Rigid Body Settings"}}
		Entry (id = 'GTYP')
		{
			string {"Special rigid body type"}
			Entry (id = 'NONE') {string {"<none>"}}
			Entry (id = 'pump') {string {"Pumpkin"}}
			Entry (id = 'wmln') {string {"Watermelon"}}
			Entry (id = 'cbox') {string {"Cardboard box"}}
			Entry (id = 'prop') {string {"Propane tank"}}
			Entry (id = 'hrnt') {string {"Hornet nest"}}
		}
		Entry (id = 'IMMV') {string {"Rigid body is immovable"}}
		Entry (id = 'SBOX') {string {"Sleep box multiplier"}}
		Entry (id = 'brak') {string {"Break Apart"}}
	}
	Entry (id = 'expl')
	{
		string {"Explosive Object"}
		Entry (id = 'SETT') {string {"Explosive Settings"}}
		Entry (id = 'HLTH') {string {"Damage required to explode"}}
		Entry (id = 'DAMG') {string {"Damage done when exploding"}}
		Entry (id = 'SHKI') {string {"Camera shake intensity"}}
		Entry (id = 'SHKD') {string {"Camera shake duration (s)"}}
		Entry (id = 'expl') {string {"Explode"}}
	}
	Entry (id = 'crmb')
	{
		string {"Crumble Object"}
		Entry (id = 'SETT') {string {"Crumble Settings"}}
		Entry (id = 'HLTH') {string {"Damage required to crumble"}}
		Entry (id = 'crmb') {string {"Crumble"}}
	}
	Entry (id = 'scrt')
	{
		string {"Secret Script"}
		Entry (id = 'SETT') {string {"Secret Script Settings"}}
		Entry (id = 'MESS')
		{
			string {"Message"}
			Entry (id = 'NONE') {string {"<none>"}}
			Entry (id = 'MINI') {string {"Mini-Secret Found!"}}
			Entry (id = 'AREA') {string {"Secret Area Found!"}}
			Entry (id = 'ROOM') {string {"Secret Room Found!"}}
			Entry (id = 'PASS') {string {"Secret Passageway Found!"}}
			Entry (id = 'TRES') {string {"Secret Treasure Found!"}}
			Entry (id = 'ITEM') {string {"Secret Item Found!"}}
		}
	}
	Entry (id = 'clct')
	{
		string {"Collectable"}
		Entry (id = 'RSPN') {string {"Item respawns"}}
		Entry (id = 'RTIM') {string {"Respawn time (s)"}}
	}
	Entry (id = 'wndf')
	{
		string {"Wind"}
		Entry (id = 'XMIN') {string {"X min speed (m/s)"}}
		Entry (id = 'XMAX') {string {"X max speed (m/s)"}}
		Entry (id = 'YMIN') {string {"Y min speed (m/s)"}}
		Entry (id = 'YMAX') {string {"Y max speed (m/s)"}}
		Entry (id = 'ZMIN') {string {"Z min speed (m/s)"}}
		Entry (id = 'ZMAX') {string {"Z max speed (m/s)"}}
		Entry (id = 'BMIN') {string {"Minimum blow time (s)"}}
		Entry (id = 'BMAX') {string {"Maximum blow time (s)"}}
		Entry (id = 'RMIN') {string {"Minimum rest time (s)"}}
		Entry (id = 'RMAX') {string {"Maximum rest time (s)"}}
	}
	Entry (id = 'ltng') {string {"Lightning"}}
	Entry (id = 'spid')
	{
		string {"Spider"}
		Entry (id = 'VRNT')
		{
			string {"Spider variant"}
			Entry (id = 'NRML') {string {"Normal"}}
			Entry (id = 'VENM') {string {"Venomous"}}
		}
	}
	Entry (id = 'bat ') {string {"Bat"}}
	Entry (id = 'cuda') {string {"Barracuda"}}
	Entry (id = 'skel')
	{
		string {"Skeleton"}
		Entry (id = 'POSE')
		{
			string {"Initial pose"}
			Entry (id = 'NONE') {string {"<none>"}}
			Entry (id = 'WAKE') {string {"Wakeup"}}
			Entry (id = 'RST1') {string {"Rest1"}}
			Entry (id = 'RST2') {string {"Rest2"}}
			Entry (id = 'RST3') {string {"Rest3"}}
			Entry (id = 'RST4') {string {"Rest4"}}
			Entry (id = 'RST5') {string {"Rest5"}}
		}
		Entry (id = 'RISE') {string {"Rise from ground"}}
		Entry (id = 'SHLD') {string {"Carry shield"}}
		Entry (id = 'PTCH') {string {"Show eye patch"}}
		Entry (id = 'HOOK') {string {"Show hook"}}
		Entry (id = 'HELM')
		{
			string {"Helmet"}
			Entry (id = 'NONE') {string {"<none>"}}
			Entry (id = 'RAND') {string {"<random>"}}
			Entry (id = 'ROMN') {string {"Roman"}}
			Entry (id = 'VIKG') {string {"Viking"}}
		}
		Entry (id = 'WEAP')
		{
			string {"Weapon"}
			Entry (id = 'NONE') {string {"<none>"}}
			Entry (id = 'RAND') {string {"<random>"}}
			Entry (id = 'SWRD') {string {"Sword"}}
			Entry (id = 'PIPE') {string {"Pipe"}}
			Entry (id = 'BONE') {string {"Bone"}}
			Entry (id = 'BRCH') {string {"Branch"}}
			Entry (id = 'SARM') {string {"Arm"}}
		}
	}
	Entry (id = 'harm') {string {"Haunted Arm"}}
	Entry (id = 'gbln')
	{
		string {"Goblin"}
		Entry (id = 'SKUL')
		{
			string {"Skull"}
			Entry (id = 'NONE') {string {"<none>"}}
			Entry (id = 'RAND') {string {"<random>"}}
			Entry (id = 'GOAT') {string {"Goat"}}
			Entry (id = 'DEER') {string {"Deer"}}
		}
	}
	Entry (id = 'zomb')
	{
		string {"Zombie"}
		Entry (id = 'VRNT')
		{
			string {"Zombie variant"}
			Entry (id = 'NRML') {string {"Normal"}}
			Entry (id = 'TECH') {string {"Technician"}}
			Entry (id = 'SCNT') {string {"Scientist"}}
		}
		Entry (id = 'POSE')
		{
			string {"Initial pose"}
			Entry (id = 'NONE') {string {"<none>"}}
			Entry (id = 'WAKE') {string {"Wakeup"}}
			Entry (id = 'RST1') {string {"Rest1"}}
			Entry (id = 'RST2') {string {"Rest2"}}
			Entry (id = 'RST3') {string {"Rest3"}}
			Entry (id = 'RST4') {string {"Rest4"}}
		}
		Entry (id = 'RISE') {string {"Rise from grave"}}
		Entry (id = 'DIRT') {string {"Do not show dirt effect"}}
		Entry (id = 'FAST') {string {"Use fast chase animation"}}
		Entry (id = 'HAT ') {string {"Wear hard hat"}}
	}
	Entry (id = 'wtch')
	{
		string {"Witch"}
		Entry (id = 'VRNT')
		{
			string {"Witch variant"}
			Entry (id = 'NRML') {string {"Normal"}}
			Entry (id = 'MAST') {string {"Master"}}
		}
		Entry (id = 'FLYB') {string {"Fly on broom"}}
		Entry (id = 'HAT ') {string {"Wear hat"}}
		Entry (id = 'ATCK')
		{
			string {"Enabled attacks"}
			Entry (id = 'CATS') {string {"Throw cats"}}
			Entry (id = 'AXES') {string {"Throw axes"}}
			Entry (id = 'KNIF') {string {"Throw knives"}}
			Entry (id = 'POTN') {string {"Throw potions"}}
		}
	}
	Entry (id = 'brom') {string {"Magic Broom"}}
	Entry (id = 'golm')
	{
		string {"Golem"}
		Entry (id = 'VRNT')
		{
			string {"Golem variant"}
			Entry (id = 'TAR ') {string {"Tar"}}
			Entry (id = 'MUD ') {string {"Mud"}}
			Entry (id = 'SWMP') {string {"Swamp"}}
			Entry (id = 'TOXC') {string {"Toxic"}}
			Entry (id = 'LAVA') {string {"Lava"}}
		}
	}
	Entry (id = 'pmkn')
	{
		string {"Pumpkinhead"}
		Entry (id = 'THRW') {string {"Throw fireballs with both hands"}}
		Entry (id = 'RISE') {string {"Rise from ground"}}
		Entry (id = 'HEAD')
		{
			string {"Head"}
			Entry (id = 'PMKN') {string {"Pumpkin"}}
			Entry (id = 'WMLN') {string {"Watermelon"}}
			Entry (id = 'CBOX') {string {"Cardboard box"}}
			Entry (id = 'PROP') {string {"Propane tank"}}
		}
	}
	Entry (id = 'reap') {string {"Grim Reaper"}}
	Entry (id = 'fish') {string {"Fish"}}
	Entry (id = 'chik')
	{
		string {"Chicken"}
		Entry (id = 'VRNT')
		{
			string {"Chicken variant"}
			Entry (id = 'WHIT') {string {"White"}}
			Entry (id = 'BRWN') {string {"Brown"}}
			Entry (id = 'BLCK') {string {"Black"}}
		}
	}
}
Entry (id = 'MTHD')
{
	Entry (id = 'mess')
	{
		string {"Display Message"}
		Entry (id = 'TEXT') {string {"Message text"}}
	}
	Entry (id = 'actn')
	{
		string {"Display Action"}
		Entry (id = 'TYPE') {string {"Action type"}}
		Entry (id = 'TIME') {string {"Display time (s)"}}
	}
	Entry (id = 'lit+') {string {"Activate Flashlight"}}
	Entry (id = 'lit-') {string {"Deactivate Flashlight"}}
	Entry (id = 'glit') {string {"Get Flashlight State"}}
	Entry (id = 'weap')
	{
		string {"Give Weapon"}
		Entry (id = 'TYPE') {string {"Weapon type"}}
	}
	Entry (id = 'ammo')
	{
		string {"Give Ammo"}
		Entry (id = 'TYPE') {string {"Weapon type"}}
		Entry (id = 'AMMO') {string {"Ammo amount"}}
		Entry (id = 'SCND') {string {"Secondary ammo"}}
	}
	Entry (id = 'powr')
	{
		string {"Give Power"}
		Entry (id = 'TYPE')
		{
			string {"Power type"}
			Entry (id = 'cndp') {string {"Speed (Candy)"}}
			Entry (id = 'clkp') {string {"Time (Clock)"}}
			Entry (id = 'frgp') {string {"Jump (Frog)"}}
			Entry (id = 'sksp') {string {"Damage (Sword)"}}
			Entry (id = 'daxp') {string {"Double Gun (Axes)"}}
			Entry (id = 'potp') {string {"Invisible (Potion)"}}
		}
	}
	Entry (id = 'tres')
	{
		string {"Give Treasure"}
		Entry (id = 'TYPE')
		{
			string {"Treasure type"}
			Entry (id = 'gdct') {string {"Coin (Gold)"}}
			Entry (id = 'svct') {string {"Coin (Silver)"}}
			Entry (id = 'bzct') {string {"Coin (Bronze)"}}
			Entry (id = 'cnct') {string {"Candy Corn"}}
			Entry (id = 'gskt') {string {"Gold Skull"}}
			Entry (id = 'rdjt') {string {"Jewel (Red)"}}
			Entry (id = 'prjt') {string {"Jewel (Purple)"}}
			Entry (id = 'grjt') {string {"Jewel (Green)"}}
			Entry (id = 'yljt') {string {"Jewel (Yellow)"}}
			Entry (id = 'cryt') {string {"Crystal"}}
		}
		Entry (id = 'QUAN') {string {"Quantity"}}
	}
	Entry (id = 'hlth')
	{
		string {"Give Health"}
		Entry (id = 'HLTH') {string {"Health amount"}}
		Entry (id = 'OVER') {string {"Allow overflow"}}
	}
	Entry (id = 'scor')
	{
		string {"Give Score"}
		Entry (id = 'SCOR') {string {"Score amount"}}
	}
	Entry (id = 'spid') {string {"Generate Spider"}}
	Entry (id = 'bat ') {string {"Generate Bat"}}
	Entry (id = 'skel') {string {"Generate Skeleton"}}
	Entry (id = 'harm') {string {"Generate Haunted Arm"}}
	Entry (id = 'gbln') {string {"Generate Goblin"}}
	Entry (id = 'zomb') {string {"Generate Zombie"}}
	Entry (id = 'wtch') {string {"Generate Witch"}}
	Entry (id = 'brom') {string {"Generate Magic Broom"}}
	Entry (id = 'golm') {string {"Generate Golem"}}
	Entry (id = 'pmkn') {string {"Generate Pumpkinhead"}}
	Entry (id = 'reap') {string {"Generate Grim Reaper"}}
	Entry (id = 'spdc') {string {"Get Spider Count"}}
	Entry (id = 'batc') {string {"Get Bat Count"}}
	Entry (id = 'sklc') {string {"Get Skeleton Count"}}
	Entry (id = 'armc') {string {"Get Haunted Arm Count"}}
	Entry (id = 'gblc') {string {"Get Goblin Count"}}
	Entry (id = 'zmbc') {string {"Get Zombie Count"}}
	Entry (id = 'wchc') {string {"Get Witch Count"}}
	Entry (id = 'brmc') {string {"Get Magic Broom Count"}}
	Entry (id = 'golc') {string {"Get Golem Count"}}
	Entry (id = 'pmkc') {string {"Get Pumpkinhead Count"}}
	Entry (id = 'repc') {string {"Get Grim Reaper Count"}}
	Entry (id = 'damg')
	{
		string {"Damage Character"}
		Entry (id = 'DAMG') {string {"Damage amount"}}
		Entry (id = 'BLOD') {string {"Do not show blood"}}
	}
	Entry (id = 'kill') {string {"Kill Character"}}
	Entry (id = 'kmon') {string {"Kill All Monsters"}}
	Entry (id = 'shak')
	{
		string {"Shake Camera"}
		Entry (id = 'INTS') {string {"Camera shake intensity"}}
		Entry (id = 'DURA') {string {"Camera shake duration (s)"}}
	}
	Entry (id = 'endw')
	{
		string {"End World"}
		Entry (id = 'WNAM') {string {"Next world name"}}
		Entry (id = 'PICK') {string {"Next World Name"}}
	}
}
Entry (id = 'PROP')
{
	Entry (id = 'clct')
	{
		string {"Collectable"}
		Entry (id = 'TYPE')
		{
			string {"Collectable type"}
			Entry (id = 'weap') {string {"Weapon"}}
			Entry (id = 'ammo') {string {"Ammo"}}
			Entry (id = 'hlth') {string {"Health"}}
			Entry (id = 'powr') {string {"Power"}}
			Entry (id = 'tres') {string {"Treasure"}}
		}
		Entry (id = 'COLR') {string {"Respawn color"}}
		Entry (id = 'PICK') {string {"Respawn Color"}}
	}
	Entry (id = 'ctnr')
	{
		string {"Container"}
		Entry (id = 'NONE') {string {"<none>"}}
		Entry (id = 'TRES') {string {"Treasure"}}
		Entry (id = 'HLTH') {string {"Health"}}
		Entry (id = 'POWR') {string {"Power"}}
		Entry (id = 'WEAP') {string {"Weapon"}}
		Entry (id = 'AMMO') {string {"Ammo"}}
		Entry (id = 'MNST') {string {"Monster"}}
		Entry (id = 'HZRD') {string {"Hazard"}}
	}
	Entry (id = 'isnd')
	{
		string {"Impact sound"}
		Entry (id = 'PICK') {string {"Impact Sound"}}
		Entry (id = 'HRD0') {string {"Hard impact sound 1"}}
		Entry (id = 'HRD1') {string {"Hard impact sound 2"}}
		Entry (id = 'SFT0') {string {"Soft impact sound 1"}}
		Entry (id = 'SFT1') {string {"Soft impact sound 2"}}
	}
	Entry (id = 'shot') {string {"Shootable"}}
	Entry (id = 'brak')
	{
		string {"Breakable"}
		Entry (id = 'HSET') {string {"Breakable Settings"}}
		Entry (id = 'KALL') {string {"Keep all pieces when breaking"}}
		Entry (id = 'COLL') {string {"Characters can collide with pieces"}}
		Entry (id = 'IFRC') {string {"Pieces ignore force fields"}}
		Entry (id = 'CKEY') {string {"Activation connector key"}}
		Entry (id = 'HSND') {string {"Breakable Sounds"}}
		Entry (id = 'PICK') {string {"Breakable Sound"}}
		Entry (id = 'SND0') {string {"Breaking sound 1"}}
		Entry (id = 'SND1') {string {"Breaking sound 2"}}
		Entry (id = 'SND2') {string {"Breaking sound 3"}}
	}
	Entry (id = 'leak')
	{
		string {"Leaking goo"}
		Entry (id = 'LEAK') {string {"Maximum number of leaks"}}
		Entry (id = 'MATL')
		{
			string {"Leak material type"}
			Entry (id = 'wine') {string {"Wine"}}
			Entry (id = 'toxc') {string {"Toxic goo"}}
			Entry (id = 'sgut') {string {"Spider guts"}}
		}
	}
	Entry (id = 'pant')
	{
		string {"Paint can"}
		Entry (id = 'COLR') {string {"Paint color"}}
		Entry (id = 'PICK') {string {"Paint Color"}}
	}
	Entry (id = 'tprt') {string {"Teleporter"}}
	Entry (id = 'jump')
	{
		string {"Jump pad"}
		Entry (id = 'HITE') {string {"Jump height"}}
	}
}
Entry (id = 'MODL')
{
	Entry (id = 'MNST')
	{
		Entry (id = 'spid') {string {"Spider"}}
		Entry (id = 'bat ') {string {"Bat"}}
		Entry (id = 'cuda') {string {"Barracuda"}}
		Entry (id = 'skel') {string {"Skeleton"}}
		Entry (id = 'harm') {string {"Haunted Arm"}}
		Entry (id = 'gbln') {string {"Goblin"}}
		Entry (id = 'zomb') {string {"Zombie"}}
		Entry (id = 'wtch') {string {"Witch"}}
		Entry (id = 'brom') {string {"Magic Broom"}}
		Entry (id = 'golm') {string {"Golem"}}
		Entry (id = 'pmkn') {string {"Pumpkinhead"}}
		Entry (id = 'reap') {string {"Grim Reaper"}}
	}
	Entry (id = 'ANML')
	{
		Entry (id = 'fish') {string {"Fish"}}
		Entry (id = 'chik') {string {"Chicken"}}
	}
	Entry (id = 'WEAP')
	{
		Entry (id = 'faxe') {string {"Axe"}}
		Entry (id = 'pist') {string {"Pistol"}}
		Entry (id = 'shgn') {string {"Shotgun"}}
		Entry (id = 'cbow') {string {"Crossbow"}}
		Entry (id = 'spsh') {string {"Spike Shooter"}}
		Entry (id = 'glch') {string {"Grenade Launcher"}}
		Entry (id = 'qchg') {string {"Quantum Charger"}}
		Entry (id = 'rlch') {string {"Rocket Launcher"}}
		Entry (id = 'pgun') {string {"Plasma Gun"}}
		Entry (id = 'pcan') {string {"Proton Cannon"}}
		Entry (id = 'catf') {string {"Cat Flinger"}}
		Entry (id = 'chbl') {string {"Chicken Blaster"}}
		Entry (id = 'brcn') {string {"Beer Cannon"}}
		Entry (id = 'hngn') {string {"Hornet Gun"}}
	}
	Entry (id = 'AMMO')
	{
		Entry (id = 'blam') {string {"Bullet Ammo"}}
		Entry (id = 'sham') {string {"Shell Ammo"}}
		Entry (id = 'aram') {string {"Arrow Ammo"}}
		Entry (id = 'exam') {string {"Arrow Ammo (Explosive)"}}
		Entry (id = 'spam') {string {"Spike Ammo"}}
		Entry (id = 'rsam') {string {"Spike Ammo (Rail)"}}
		Entry (id = 'csam') {string {"Spike Ammo (Combo)"}}
		Entry (id = 'gram') {string {"Grenade Ammo"}}
		Entry (id = 'cham') {string {"Charge Ammo"}}
		Entry (id = 'rkam') {string {"Rocket Ammo"}}
		Entry (id = 'plam') {string {"Plasma Ammo"}}
		Entry (id = 'pram') {string {"Proton Ammo"}}
	}
	Entry (id = 'HLTH')
	{
		Entry (id = 'appl') {string {"Health 10 (Apple)"}}
		Entry (id = 'bana') {string {"Health 10 (Banana)"}}
		Entry (id = 'grap') {string {"Health 10 (Grapes)"}}
		Entry (id = 'orng') {string {"Health 10 (Orange)"}}
		Entry (id = 'burg') {string {"Health 25 (Burger)"}}
		Entry (id = 'pizz') {string {"Health 25 (Pizza)"}}
		Entry (id = 'sub ') {string {"Health 25 (Sub)"}}
		Entry (id = 'taco') {string {"Health 25 (Taco)"}}
		Entry (id = 'ham ') {string {"Health 50 (Ham)"}}
		Entry (id = 'rost') {string {"Health 50 (Roast Beef)"}}
		Entry (id = 'spag') {string {"Health 50 (Spaghetti)"}}
		Entry (id = 'turk') {string {"Health 50 (Turkey)"}}
		Entry (id = 'gblt') {string {"Health Full (Goblet)"}}
	}
	Entry (id = 'POWR')
	{
		Entry (id = 'cndp') {string {"Power Speed (Candy)"}}
		Entry (id = 'clkp') {string {"Power Time (Clock)"}}
		Entry (id = 'frgp') {string {"Power Jump (Frog)"}}
		Entry (id = 'sksp') {string {"Power Damage (Sword)"}}
		Entry (id = 'daxp') {string {"Power Double Gun (Axes)"}}
		Entry (id = 'potp') {string {"Power Invisible (Potion)"}}
	}
	Entry (id = 'TRES')
	{
		Entry (id = 'gdct') {string {"Coin (Gold)"}}
		Entry (id = 'svct') {string {"Coin (Silver)"}}
		Entry (id = 'bzct') {string {"Coin (Bronze)"}}
		Entry (id = 'cnct') {string {"Candy Corn"}}
		Entry (id = 'gskt') {string {"Gold Skull"}}
		Entry (id = 'rdjt') {string {"Jewel (Red)"}}
		Entry (id = 'prjt') {string {"Jewel (Purple)"}}
		Entry (id = 'grjt') {string {"Jewel (Green)"}}
		Entry (id = 'yljt') {string {"Jewel (Yellow)"}}
		Entry (id = 'cryt') {string {"Crystal"}}
	}
	Entry (id = 'MASK')
	{
		Entry (id = 'mgob') {string {"Mask Goblin"}}
		Entry (id = 'mwch') {string {"Mask Witch"}}
		Entry (id = 'mpkn') {string {"Mask Pumpkin"}}
	}
}
Entry (id = 'PART')
{
	Entry (id = 'rain')
	{
		string {"Rain"}
		Entry (id = 'SETT') {string {"Rain Settings"}}
		Entry (id = 'INTS') {string {"Raindrops per square meter per second"}}
		Entry (id = 'RMIN') {string {"Minimum raindrop radius"}}
		Entry (id = 'RMAX') {string {"Maximum raindrop radius"}}
		Entry (id = 'CMIN') {string {"Color range begin"}}
		Entry (id = 'CMAX') {string {"Color range end"}}
		Entry (id = 'PICK') {string {"Rain Color"}}
	}
	Entry (id = 'bran')
	{
		string {"Burning Rain"}
		Entry (id = 'SETT') {string {"Fire Settings"}}
		Entry (id = 'BURN') {string {"Burning rain percentage"}}
		Entry (id = 'FIRE') {string {"Enable fireballs"}}
		Entry (id = 'TMIN') {string {"Minimum time between fireballs (s)"}}
		Entry (id = 'TMAX') {string {"Maximum time between fireballs (s)"}}
	}
	Entry (id = 'gfog')
	{
		string {"Ground Fog"}
		Entry (id = 'FSET') {string {"Fog Settings"}}
		Entry (id = 'COLR') {string {"Fog color"}}
		Entry (id = 'PICK') {string {"Fog Color"}}
		Entry (id = 'SOFT') {string {"Render with soft depth"}}
		Entry (id = 'DSCL') {string {"Soft depth scale"}}
		Entry (id = 'RAMP') {string {"Render with depth ramp"}}
		Entry (id = 'DMIN') {string {"Depth ramp minimum distance (m)"}}
		Entry (id = 'DMAX') {string {"Depth ramp maximum distance (m)"}}
		Entry (id = 'ESET') {string {"Emission Settings"}}
		Entry (id = 'EMIT') {string {"Particle emission rate (num/m²•s)"}}
		Entry (id = 'LMIN') {string {"Minimum particle lifetime (s)"}}
		Entry (id = 'LMAX') {string {"Maximum particle lifetime (s)"}}
		Entry (id = 'FADI') {string {"Particle fade-in time (s)"}}
		Entry (id = 'FADO') {string {"Particle fade-out time (s)"}}
		Entry (id = 'WARM') {string {"Initialize in warm state"}}
		Entry (id = 'PSET') {string {"Particle Settings"}}
		Entry (id = 'RMIN') {string {"Minimum particle radius (m)"}}
		Entry (id = 'RMAX') {string {"Maximum particle radius (m)"}}
		Entry (id = 'EMIN') {string {"Minimum expansion rate (m/s)"}}
		Entry (id = 'EMAX') {string {"Maximum expansion rate (m/s)"}}
		Entry (id = 'WMIN') {string {"Minimum rotation rate (rev/s)"}}
		Entry (id = 'WMAX') {string {"Maximum rotation rate (rev/s)"}}
		Entry (id = 'VSET') {string {"Velocity Settings"}}
		Entry (id = 'ZMIN') {string {"Minimum vertical velocity (m/s)"}}
		Entry (id = 'ZMAX') {string {"Maximum vertical velocity (m/s)"}}
		Entry (id = 'XMIN') {string {"Minimum horizontal velocity (m/s)"}}
		Entry (id = 'XMAX') {string {"Maximum horizontal velocity (m/s)"}}
	}
	Entry (id = 'pabm') {string {"Proton Ammo Beam"}}
	Entry (id = 'smok') {string {"Torch Smoke"}}
	Entry (id = 'chim') {string {"Chimney Smoke"}}
	Entry (id = 'hwav') {string {"Heat Waves"}}
	Entry (id = 'bstm')
	{
		string {"Bubbling Steam"}
		Entry (id = 'SETT') {string {"Bubbling Steam Settings"}}
		Entry (id = 'COLR') {string {"Steam color"}}
		Entry (id = 'PICK') {string {"Steam Color"}}
	}
	Entry (id = 'fgoo')
	{
		string {"Flowing Goo"}
		Entry (id = 'SETT') {string {"Flowing Goo Settings"}}
		Entry (id = 'GRAV') {string {"Gravity multiplier"}}
		Entry (id = 'LIFE') {string {"Particle life time (s)"}}
	}
	Entry (id = 'trbl') {string {"Tarball"}}
	Entry (id = 'venm') {string {"Venom"}}
}
Entry (id = 'LOCA')
{
	Entry (id = 'spwn') {string {"Spawn"}}
	Entry (id = 'spec') {string {"Spectator"}}
	Entry (id = 'bnch') {string {"Benchmark"}}
	Entry (id = 'cent') {string {"Center Position"}}
	Entry (id = 'open') {string {"Open Position"}}
	Entry (id = 'clos') {string {"Closed Position"}}
	Entry (id = 'shft') {string {"Shifted Position"}}
	Entry (id = 'fire') {string {"Fire Position"}}
}
Entry (id = 'SBST')
{
	Entry (id = 'dirt') {string {"Dirt"}}
	Entry (id = 'ston') {string {"Stone"}}
	Entry (id = 'clay') {string {"Clay"}}
	Entry (id = 'wood') {string {"Wood"}}
	Entry (id = 'metl') {string {"Metal"}}
	Entry (id = 'glas') {string {"Glass"}}
	Entry (id = 'rbbr') {string {"Rubber"}}
	Entry (id = 'cbrd') {string {"Cardboard"}}
	Entry (id = 'strw') {string {"Straw"}}
	Entry (id = 'fenc') {string {"Fence"}}
}
