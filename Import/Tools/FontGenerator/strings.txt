Entry (id = 'MCMD') {string {"Generate Font..."}}
Entry (id = 'FGEN')
{
	Entry (id = 'FSET') {string {"Font Settings"}}
	Entry (id = 'NAME') {string {"Font name, Data/"}}
	Entry (id = 'SPAC') {string {"Glyph spacing"}}
	Entry (id = 'LEAD') {string {"Vertical leading"}}
	Entry (id = 'SSET') {string {"Shadow Settings"}}
	Entry (id = 'SHAD') {string {"Render shadow"}}
	Entry (id = 'STYP')
	{
		string {"Shadow type"}
		Entry (id = 'SHRP') {string {"Sharp"}}
		Entry (id = 'SOFT') {string {"Soft"}}
		Entry (id = 'SFTR') {string {"Softer"}}
	}
	Entry (id = 'DARK') {string {"Shadow darkness"}}
	Entry (id = 'SHDX') {string {"Shadow offset X"}}
	Entry (id = 'SHDY') {string {"Shadow offset Y"}}
}
Entry (id = 'PICK')
{
	string {"Select Font"}
	Entry (id = 'FONT') {string {"Font"}}
	Entry (id = 'SIZE') {string {"Size:"}}
	Entry (id = 'BOLD') {string {"Boldface"}}
	Entry (id = 'ITAL') {string {"Italic"}}
}
Entry (id = 'LPCK') {string {"Load Ranges"}}
Entry (id = 'SPCK') {string {"Save Ranges"}}
