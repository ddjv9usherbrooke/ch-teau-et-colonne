Entry (id = 'WRLD')
{
	Entry (id = 'MOPN') {string {"Open World..."}}
	Entry (id = 'OPEN') {string {"Open World"}}
	Entry (id = 'MNEW') {string {"New World"}}
	Entry (id = 'MROP') {string {"Reopen Played World"}}
	Entry (id = 'NRES') {string {"[#FF8]World not found: [#FFF]"}}
}
Entry (id = 'BTTN')
{
	Entry (id = 'OKAY') {string {"OK"}}
	Entry (id = 'CANC') {string {"Cancel"}}
	Entry (id = 'SAVE') {string {"Save"}}
	Entry (id = 'DSCD') {string {"Discard"}}
	Entry (id = 'DSAV') {string {"Don’t Save"}}
	Entry (id = 'DELT') {string {"Delete"}}
}
Entry (id = 'CFRM') {string {"Save "}}
Entry (id = 'SAVE') {string {"Save World"}}
Entry (id = 'VPRT')
{
	Entry (id = 'TOP ') {string {"Top"}}
	Entry (id = 'BTTM') {string {"Bottom"}}
	Entry (id = 'FRNT') {string {"Front"}}
	Entry (id = 'BACK') {string {"Back"}}
	Entry (id = 'RGHT') {string {"Right"}}
	Entry (id = 'LEFT') {string {"Left"}}
	Entry (id = 'PERS') {string {"Perspective"}}
	Entry (id = 'GRPH') {string {"Scene Graph"}}
}
Entry (id = 'MENU')
{
	Entry (id = 'WRLD')
	{
		string {"World"}
		Entry (id = 'CLOS') {string {"Close"}}
		Entry (id = 'SAVW') {string {"Save World"}}
		Entry (id = 'SVAS') {string {"Save World As..."}}
		Entry (id = 'PLAY') {string {"Save and Play World"}}
		Entry (id = 'ISCN') {string {"Import Scene..."}}
		Entry (id = 'ESCN') {string {"Export Scene..."}}
		Entry (id = 'IMDL') {string {"Load from Model Resource..."}}
		Entry (id = 'EMDL') {string {"Save to Model Resource..."}}
	}
	Entry (id = 'EDIT')
	{
		string {"Edit"}
		Entry (id = 'UNDO') {string {"Undo"}}
		Entry (id = 'CUT ') {string {"Cut"}}
		Entry (id = 'COPY') {string {"Copy"}}
		Entry (id = 'PAST') {string {"Paste"}}
		Entry (id = 'PSUB') {string {"Paste Subnodes"}}
		Entry (id = 'CLER') {string {"Clear"}}
		Entry (id = 'SALL') {string {"Select All"}}
		Entry (id = 'SZON') {string {"Select All in Zone"}}
		Entry (id = 'SMSK') {string {"Select All by Mask"}}
		Entry (id = 'SMAT') {string {"Select All with Material"}}
		Entry (id = 'TREE') {string {"Select Subtree"}}
		Entry (id = 'SUPR') {string {"Select Super Node"}}
		Entry (id = 'LOCK') {string {"Lock Selection"}}
		Entry (id = 'ULCK') {string {"Unlock Selection"}}
		Entry (id = 'DUPL') {string {"Duplicate"}}
		Entry (id = 'CLON') {string {"Clone"}}
		Entry (id = 'CXFM') {string {"Copy Transform"}}
		Entry (id = 'PXFM') {string {"Paste Transform"}}
	}
	Entry (id = 'NODE')
	{
		string {"Node"}
		Entry (id = 'INFO') {string {"Get Info..."}}
		Entry (id = 'EDIT') {string {"Edit Script or Panel"}}
		Entry (id = 'GRUP') {string {"Group Nodes"}}
		Entry (id = 'RXFM') {string {"Reset Transform to Identity"}}
		Entry (id = 'AGRD') {string {"Align Position to Grid"}}
		Entry (id = 'TARG') {string {"Set Target Zone"}}
		Entry (id = 'ITRG') {string {"Set Infinite Target Zone"}}
		Entry (id = 'MZON') {string {"Move to Target Zone"}}
		Entry (id = 'ITAR') {string {"Set Infinite Target Zone"}}
		Entry (id = 'CONN') {string {"Connect Node"}}
		Entry (id = 'UCON') {string {"Unconnect Node"}}
		Entry (id = 'CIZN') {string {"Connect to Infinite Zone"}}
		Entry (id = 'SCON') {string {"Select Connected Node"}}
		Entry (id = 'SICN') {string {"Select Incoming Connecting Nodes"}}
		Entry (id = 'MCAM') {string {"Move Viewport Camera to Node"}}
		Entry (id = 'OINS') {string {"Open Instanced World"}}
	}
	Entry (id = 'GEOM')
	{
		string {"Geometry"}
		Entry (id = 'RBLD') {string {"Rebuild Geometry"}}
		Entry (id = 'RBPA') {string {"Rebuild with New Path"}}
		Entry (id = 'NORM') {string {"Recalculate Normals"}}
		Entry (id = 'BAKE') {string {"Bake Transform into Vertices"}}
		Entry (id = 'ORIG') {string {"Reposition Mesh Origin"}}
		Entry (id = 'SMAT') {string {"Set Material"}}
		Entry (id = 'RMAT') {string {"Remove Material"}}
		Entry (id = 'CBLV') {string {"Combine Detail Levels"}}
		Entry (id = 'SPLV') {string {"Separate Detail Levels"}}
		Entry (id = 'CONV') {string {"Convert to Generic Geometry"}}
		Entry (id = 'MERG') {string {"Merge Geometry"}}
		Entry (id = 'IVRT') {string {"Invert Geometry"}}
		Entry (id = 'SECT') {string {"Intersect Geometry"}}
		Entry (id = 'UNON') {string {"Union Geometry"}}
		Entry (id = 'GOCC') {string {"Generate Ambient Occlusion Data"}}
		Entry (id = 'ROCC') {string {"Remove Ambient Occlusion Data"}}
	}
	Entry (id = 'VIEW')
	{
		string {"View"}
		Entry (id = 'HIDE') {string {"Hide Selection"}}
		Entry (id = 'UHID') {string {"Unhide All"}}
		Entry (id = 'UHTZ') {string {"Unhide All in Target Zone"}}
		Entry (id = 'HNTG') {string {"Hide Non-Target Zones"}}
	}
	Entry (id = 'LAYO')
	{
		string {"Layout"}
		Entry (id = 'BFAC') {string {"Show Backfaces"}}
		Entry (id = 'EXWD') {string {"Show Instanced Worlds"}}
		Entry (id = 'EXMD') {string {"Show Models"}}
		Entry (id = 'LGHT') {string {"Render Lighting"}}
		Entry (id = 'CENT') {string {"Draw from Center"}}
		Entry (id = 'CAPS') {string {"Cap Geometry"}}
		Entry (id = 'PRT1') {string {"Show Viewport 1"}}
		Entry (id = 'PRT2') {string {"Show Viewport 2"}}
		Entry (id = 'PRT3') {string {"Show Viewport 3"}}
		Entry (id = 'PRT4') {string {"Show Viewport 4"}}
		Entry (id = 'PRT5') {string {"Show Viewport 5"}}
		Entry (id = 'PRT6') {string {"Show Viewport 6"}}
		Entry (id = 'PRT7') {string {"Show Viewport 7"}}
		Entry (id = 'PRT8') {string {"Show Viewport 8"}}
		Entry (id = 'SETT') {string {"Editor Settings..."}}
	}
	Entry (id = 'PAGE')
	{
		string {"Page"}
		Entry (id = 'SHWA') {string {"Show All Pages"}}
	}
	Entry (id = 'VPRT')
	{
		Entry (id = 'TOP ') {string {"Top (+z)"}}
		Entry (id = 'BTTM') {string {"Bottom (−z)"}}
		Entry (id = 'FRNT') {string {"Front (+x)"}}
		Entry (id = 'BACK') {string {"Back (−x)"}}
		Entry (id = 'RGHT') {string {"Right (−y)"}}
		Entry (id = 'LEFT') {string {"Left (+y)"}}
		Entry (id = 'PERS') {string {"Perspective"}}
		Entry (id = 'GRPH') {string {"Scene Graph"}}
		Entry (id = 'FALL') {string {"Frame All"}}
		Entry (id = 'FSEL') {string {"Frame Selection"}}
	}
}
Entry (id = 'NAME')
{
	Entry (id = 'NODE') {string {"Node"}}
	Entry (id = 'GRUP') {string {"Group"}}
	Entry (id = 'INST') {string {"Instance"}}
	Entry (id = 'MODL') {string {"Model"}}
	Entry (id = 'BONE') {string {"Bone"}}
	Entry (id = 'WRLD') {string {"World"}}
	Entry (id = 'GEOM')
	{
		string {"Geometry"}
		Entry (id = 'PLAT') {string {"Plate"}}
		Entry (id = 'DISK') {string {"Disk"}}
		Entry (id = 'HOLE') {string {"Hole"}}
		Entry (id = 'ANNU') {string {"Annulus"}}
		Entry (id = 'BOX ') {string {"Box"}}
		Entry (id = 'PYRA') {string {"Pyramid"}}
		Entry (id = 'CYLD') {string {"Cylinder"}}
		Entry (id = 'CONE') {string {"Cone"}}
		Entry (id = 'SPHR') {string {"Sphere"}}
		Entry (id = 'DOME') {string {"Dome"}}
		Entry (id = 'TORS') {string {"Torus"}}
		Entry (id = 'TCON') {string {"Truncated Cone"}}
		Entry (id = 'FLUD') {string {"Fluid"}}
		Entry (id = 'ROPE') {string {"Rope"}}
		Entry (id = 'CLTH') {string {"Cloth"}}
		Entry (id = 'TUBE') {string {"Path Tube"}}
		Entry (id = 'EXTR') {string {"Path Extrusion"}}
		Entry (id = 'REVO') {string {"Path Revolution"}}
	}
	Entry (id = 'CAMR')
	{
		Entry (id = 'FRUS') {string {"Frustum Camera"}}
	}
	Entry (id = 'LITE')
	{
		Entry (id = 'PONT') {string {"Point Light"}}
		Entry (id = 'CUBE') {string {"Cube Light"}}
		Entry (id = 'SPOT') {string {"Spot Light"}}
		Entry (id = 'INFT') {string {"Infinite Light"}}
		Entry (id = 'DPTH') {string {"Depth Light"}}
		Entry (id = 'LAND') {string {"Landscape Light"}}
	}
	Entry (id = 'SORC')
	{
		Entry (id = 'AMBT') {string {"Ambient Source"}}
		Entry (id = 'OMNI') {string {"Omnidirectional Source"}}
		Entry (id = 'DRCT') {string {"Directed Source"}}
	}
	Entry (id = 'SPAC')
	{
		Entry (id = 'FOG ') {string {"Fog Space"}}
		Entry (id = 'SHAD') {string {"Shadow Space"}}
		Entry (id = 'AMBT') {string {"Ambient Space"}}
		Entry (id = 'RDSY') {string {"Radiosity Space"}}
		Entry (id = 'ACST') {string {"Acoustics Space"}}
		Entry (id = 'OCCL') {string {"Occlusion Space"}}
		Entry (id = 'PANT') {string {"Paint Space"}}
		Entry (id = 'PHYS') {string {"Physics Space"}}
	}
	Entry (id = 'ZONE')
	{
		Entry (id = 'INFT') {string {"Infinite Zone"}}
		Entry (id = 'BOX ') {string {"Box Zone"}}
		Entry (id = 'CYLD') {string {"Cylinder Zone"}}
		Entry (id = 'POLY') {string {"Polygon Zone"}}
	}
	Entry (id = 'PORT')
	{
		Entry (id = 'INFT') {string {"Infinite Portal"}}
		Entry (id = 'DRCT') {string {"Direct Portal"}}
		Entry (id = 'REMO') {string {"Remote Portal"}}
		Entry (id = 'OCCL') {string {"Occlusion Portal"}}
	}
	Entry (id = 'MARK')
	{
		Entry (id = 'LOCA') {string {"Locator Marker"}}
		Entry (id = 'IPST') {string {"Impostor Marker"}}
		Entry (id = 'CONN') {string {"Connection Marker"}}
		Entry (id = 'CUBE') {string {"Cube Map Marker"}}
	}
	Entry (id = 'TRIG')
	{
		Entry (id = 'BOX ') {string {"Box Trigger"}}
		Entry (id = 'CYLD') {string {"Cylinder Trigger"}}
		Entry (id = 'SPHR') {string {"Sphere Trigger"}}
	}
	Entry (id = 'EFCT')
	{
		Entry (id = 'QUAD') {string {"Quad Effect"}}
		Entry (id = 'FLAR') {string {"Flare Effect"}}
		Entry (id = 'BEAM') {string {"Beam Effect"}}
		Entry (id = 'TUBE') {string {"Tube Effect"}}
		Entry (id = 'FIRE') {string {"Fire Effect"}}
		Entry (id = 'PANL') {string {"Panel Effect"}}
		Entry (id = 'BOX ') {string {"Box Shaft"}}
		Entry (id = 'CYLD') {string {"Cylinder Shaft"}}
		Entry (id = 'TPYR') {string {"Truncated Pyramid Shaft"}}
		Entry (id = 'TCON') {string {"Truncated Cone Shaft"}}
	}
	Entry (id = 'PART')
	{
		string {"Particle System"}
		Entry (id = 'BOX ') {string {"Box Emitter"}}
		Entry (id = 'CYLD') {string {"Cylinder Emitter"}}
		Entry (id = 'SPHR') {string {"Sphere Emitter"}}
	}
	Entry (id = 'PHYS')
	{
		Entry (id = 'PHYS') {string {"Physics Node"}}
		Entry (id = 'BOX ') {string {"Box Shape"}}
		Entry (id = 'PYRA') {string {"Pyramid Shape"}}
		Entry (id = 'CYLD') {string {"Cylinder Shape"}}
		Entry (id = 'CONE') {string {"Cone Shape"}}
		Entry (id = 'SPHR') {string {"Sphere Shape"}}
		Entry (id = 'DOME') {string {"Dome Shape"}}
		Entry (id = 'CPSL') {string {"Capsule Shape"}}
		Entry (id = 'TPYR') {string {"Truncated Pyramid Shape"}}
		Entry (id = 'TCON') {string {"Truncated Cone Shape"}}
		Entry (id = 'TDOM') {string {"Truncated Dome Shape"}}
		Entry (id = 'SPHJ') {string {"Spherical Joint"}}
		Entry (id = 'UNIJ') {string {"Universal Joint"}}
		Entry (id = 'DISJ') {string {"Discal Joint"}}
		Entry (id = 'REVJ') {string {"Revolute Joint"}}
		Entry (id = 'CYLJ') {string {"Cylindrical Joint"}}
		Entry (id = 'PRSJ') {string {"Prismatic Joint"}}
		Entry (id = 'BOXF') {string {"Box Field"}}
		Entry (id = 'CYLF') {string {"Cylinder Field"}}
		Entry (id = 'SPHF') {string {"Sphere Field"}}
		Entry (id = 'PLTB') {string {"Plate Blocker"}}
		Entry (id = 'BOXB') {string {"Box Blocker"}}
		Entry (id = 'CYLB') {string {"Cylinder Blocker"}}
		Entry (id = 'SPHB') {string {"Sphere Blocker"}}
		Entry (id = 'CPSB') {string {"Capsule Blocker"}}
	}
	Entry (id = 'SKYB')
	{
		Entry (id = 'SKYB') {string {"Skybox"}}
	}
	Entry (id = 'IPST')
	{
		Entry (id = 'IPST') {string {"Impostor"}}
	}
	Entry (id = 'PATH') {string {"Path"}}
	Entry (id = 'TBLK') {string {"Terrain Block"}}
	Entry (id = 'TERR') {string {"Terrain Geometry"}}
	Entry (id = 'WBLK') {string {"Water Block"}}
	Entry (id = 'WATR') {string {"Water Geometry"}}
	Entry (id = 'HWAT') {string {"Horizon Water"}}
}
Entry (id = 'PAGE')
{
	Entry (id = 'WRLD')
	{
		Entry (id = 'CLEN') {string {"Cleanup"}}
		Entry (id = 'SALL') {string {"Select All"}}
		Entry (id = 'SSOM') {string {"Select Some..."}}
		Entry (id = 'EXAL') {string {"Expand All in Scene Graph"}}
		Entry (id = 'COAL') {string {"Collapse All in Scene Graph"}}
		Entry (id = 'RPLI') {string {"Replace Instances"}}
		Entry (id = 'RPLM') {string {"Replace Modifiers"}}
		Entry (id = 'NWMD') {string {"New Modifier Preset..."}}
		Entry (id = 'DLMD') {string {"Delete Modifier Preset"}}
		Entry (id = 'RNMD') {string {"Rename Modifier Preset..."}}
		Entry (id = 'NONE') {string {"<none>"}}
	}
	Entry (id = 'MODL')
	{
		Entry (id = 'SALL') {string {"Select All"}}
		Entry (id = 'SSOM') {string {"Select Some..."}}
		Entry (id = 'EXAL') {string {"Expand All in Scene Graph"}}
		Entry (id = 'COAL') {string {"Collapse All in Scene Graph"}}
	}
	Entry (id = 'PANT')
	{
		Entry (id = 'ASSC') {string {"Associate Paint Space"}}
		Entry (id = 'DSSC') {string {"Dissociate Paint Space"}}
		Entry (id = 'SPNT') {string {"Select Associated Paint Spaces"}}
		Entry (id = 'SGEO') {string {"Select Associated Geometries"}}
	}
	Entry (id = 'TERR')
	{
		Entry (id = 'OGEO') {string {"Optimize Terrain Geometry"}}
		Entry (id = 'RBLD') {string {"Rebuild Terrain Block..."}}
	}
	Entry (id = 'WATR')
	{
		Entry (id = 'GLND') {string {"Generate Land Elevation Data"}}
		Entry (id = 'RLND') {string {"Remove Land Elevation Data"}}
		Entry (id = 'RBLD') {string {"Rebuild Water Block..."}}
		Entry (id = 'IWAV') {string {"Import Wave Data..."}}
		Entry (id = 'RWAV') {string {"Remove Wave Data"}}
		Entry (id = 'PICK') {string {"Import Wave Data"}}
	}
	Entry (id = 'LAND')
	{
		Entry (id = 'NWLB') {string {"New Landscaping Brush..."}}
		Entry (id = 'DLLB') {string {"Delete Landscaping Brush"}}
		Entry (id = 'RNLB') {string {"Rename Landscaping Brush..."}}
		Entry (id = 'IMLB') {string {"Import Landscaping Brush..."}}
		Entry (id = 'EXLB') {string {"Export Landscaping Brush..."}}
	}
	Entry (id = 'LITE')
	{
		Entry (id = 'SALA') {string {"Select All Ambient Spaces"}}
		Entry (id = 'SALR') {string {"Select All Radiosity Spaces"}}
		Entry (id = 'GAMB') {string {"Generate Ambient Space Data"}}
		Entry (id = 'GRAD') {string {"Generate Radiosity Space Data"}}
	}
	Entry (id = 'PART')
	{
		Entry (id = 'SALL') {string {"Select All"}}
		Entry (id = 'GHIT') {string {"Generate Emitter Height Map"}}
	}
	Entry (id = 'INFO')
	{
		Entry (id = 'NONE') {string {"<none>"}}
		Entry (id = 'GNRC') {string {"<generic>"}}
	}
	Entry (id = 'FIND')
	{
		Entry (id = 'GRUP') {string {"Group"}}
		Entry (id = 'TBLK') {string {"Block (Terrain)"}}
		Entry (id = 'WBLK') {string {"Block (Water)"}}
		Entry (id = 'BLKR') {string {"Blocker"}}
		Entry (id = 'BONE') {string {"Bone"}}
		Entry (id = 'CAMR') {string {"Camera"}}
		Entry (id = 'EFCT') {string {"Effect"}}
		Entry (id = 'EMIT') {string {"Emitter"}}
		Entry (id = 'FELD') {string {"Field"}}
		Entry (id = 'GEOM') {string {"Geometry"}}
		Entry (id = 'IPST') {string {"Impostor"}}
		Entry (id = 'INST') {string {"Instance"}}
		Entry (id = 'JONT') {string {"Joint"}}
		Entry (id = 'LITE') {string {"Light"}}
		Entry (id = 'MARK') {string {"Marker"}}
		Entry (id = 'MODL') {string {"Model"}}
		Entry (id = 'PHYS') {string {"Physics"}}
		Entry (id = 'PORT') {string {"Portal"}}
		Entry (id = 'SHAP') {string {"Shape"}}
		Entry (id = 'SKYB') {string {"Skybox"}}
		Entry (id = 'SORC') {string {"Source"}}
		Entry (id = 'SPAC') {string {"Space"}}
		Entry (id = 'TRIG') {string {"Trigger"}}
		Entry (id = 'ZONE') {string {"Zone"}}
	}
}
Entry (id = 'AJST')
{
	Entry (id = 'RROT') {string {"Randomize rotation"}}
	Entry (id = 'TANG') {string {"Align to tangent plane"}}
	Entry (id = 'SINK') {string {"Sink into ground"}}
	Entry (id = 'SRAD') {string {"Sink radius"}}
	Entry (id = 'OMIN') {string {"Random Z offset min"}}
	Entry (id = 'OMAX') {string {"Random Z offset max"}}
}
Entry (id = 'INFO')
{
	string {"Node Info"}
	Entry (id = 'PANE')
	{
		Entry (id = 'PROP') {string {"Properties"}}
		Entry (id = 'MDFR') {string {"Modifiers"}}
		Entry (id = 'CONN') {string {"Connectors"}}
		Entry (id = 'CTRL') {string {"Controller"}}
		Entry (id = 'FORC') {string {"Force"}}
	}
	Entry (id = 'CONN')
	{
		Entry (id = 'WILD') {string {"Any node"}}
		Entry (id = 'GEOM') {string {"Geometry"}}
		Entry (id = 'CAMR') {string {"Camera"}}
		Entry (id = 'LITE') {string {"Light"}}
		Entry (id = 'SORC') {string {"Source"}}
		Entry (id = 'SPAC') {string {"Space"}}
		Entry (id = 'ZONE') {string {"Zone"}}
		Entry (id = 'PORT') {string {"Portal"}}
		Entry (id = 'MARK') {string {"Marker"}}
		Entry (id = 'TRIG') {string {"Trigger"}}
		Entry (id = 'EFCT') {string {"Effect"}}
		Entry (id = 'SHAP') {string {"Shape"}}
		Entry (id = 'MODL') {string {"Model"}}
	}
	Entry (id = 'NONE') {string {"<none>"}}
}
Entry (id = 'MATL')
{
	string {"Material Manager"}
	Entry (id = 'DFLT') {string {"default"}}
	Entry (id = 'DIFF')
	{
		Entry (id = 'HDIF') {string {"Diffuse Reflection Settings"}}
		Entry (id = 'DIFF') {string {"Diffuse color"}}
		Entry (id = 'HTXT') {string {"Texture Map Settings"}}
		Entry (id = 'TX1M') {string {"Texture map (primary)"}}
		Entry (id = 'TX2M') {string {"Texture map (secondary)"}}
		Entry (id = 'TX1I') {string {"Texcoord input (primary)"}}
		Entry (id = 'TX2I') {string {"Texcoord input (secondary)"}}
		Entry (id = 'BLND')
		{
			string {"Texture blend mode"}
			Entry (id = 'ADD ') {string {"Add"}}
			Entry (id = 'AVG ') {string {"Average"}}
			Entry (id = 'MULT') {string {"Multiply"}}
			Entry (id = 'VTXA') {string {"Vertex alpha"}}
			Entry (id = 'PRMA') {string {"1st tex alpha"}}
			Entry (id = 'SCDA') {string {"2nd tex alpha"}}
			Entry (id = 'PRIA') {string {"1st tex inv alpha"}}
			Entry (id = 'SCIA') {string {"2nd tex inv alpha"}}
		}
		Entry (id = 'HNRM') {string {"Normal Map Settings"}}
		Entry (id = 'NM1M') {string {"Normal map (primary)"}}
		Entry (id = 'NM2M') {string {"Normal map (secondary)"}}
		Entry (id = 'NM1I') {string {"Texcoord input (primary)"}}
		Entry (id = 'NM2I') {string {"Texcoord input (secondary)"}}
		Entry (id = 'HHZN') {string {"Horizon Map Settings"}}
		Entry (id = 'HRZN')
		{
			string {"Enable horizon mapping"}
			Entry (id = 'XINF') {string {"Exclude infinite lights"}}
			Entry (id = 'XPNT') {string {"Exclude point lights"}}
		}
	}
	Entry (id = 'SPEC')
	{
		Entry (id = 'HSPC') {string {"Specular Reflection Settings"}}
		Entry (id = 'SPEC') {string {"Specular color"}}
		Entry (id = 'EXPN') {string {"Specular exponent"}}
		Entry (id = 'GLSM') {string {"Gloss map"}}
		Entry (id = 'GLSI') {string {"Texcoord input"}}
		Entry (id = 'HMFT') {string {"Microfacet Reflection Settings"}}
		Entry (id = 'MFCT') {string {"Microfacet color"}}
		Entry (id = 'MRFL') {string {"Microfacet reflectivity"}}
		Entry (id = 'SLPX') {string {"Microfacet slope x"}}
		Entry (id = 'SLPY') {string {"Microfacet slope y"}}
	}
	Entry (id = 'AMBT')
	{
		Entry (id = 'HEMS') {string {"Emission Settings"}}
		Entry (id = 'EMIS') {string {"Emission color"}}
		Entry (id = 'EMSM') {string {"Emission map"}}
		Entry (id = 'EMSI') {string {"Texcoord input"}}
		Entry (id = 'HENV') {string {"Environment Map Settings"}}
		Entry (id = 'ENVC') {string {"Environment color"}}
		Entry (id = 'ENVM') {string {"Environment override map"}}
		Entry (id = 'HRFL') {string {"Reflection Buffer Settings"}}
		Entry (id = 'REFL') {string {"Reflection color"}}
		Entry (id = 'NINC') {string {"Normal incidence reflectivity"}}
		Entry (id = 'RFLO') {string {"Reflection offset scale"}}
		Entry (id = 'HRFR') {string {"Refraction Buffer Settings"}}
		Entry (id = 'REFR') {string {"Refraction color"}}
		Entry (id = 'RFRO') {string {"Refraction offset scale"}}
		Entry (id = 'OPCM') {string {"Opacity map"}}
		Entry (id = 'OPCI') {string {"Texcoord input"}}
	}
	Entry (id = 'FLAG')
	{
		Entry (id = 'HFLG') {string {"Rendering Flags"}}
		Entry (id = 'TSID') {string {"Render two-sided"}}
		Entry (id = 'ATST') {string {"Render with alpha test"}}
		Entry (id = 'ACOV') {string {"Use alpha multisample coverage"}}
		Entry (id = 'SSHD') {string {"Enable per-sample shading"}}
		Entry (id = 'GLOW') {string {"Apply emission glow"}}
		Entry (id = 'BLOM') {string {"Apply specular bloom"}}
		Entry (id = 'VOCC') {string {"Render vertex occlusion"}}
		Entry (id = 'SSAO') {string {"Render screen-space occlusion"}}
		Entry (id = 'HMUT') {string {"Color Mutability Flags"}}
		Entry (id = 'DIFF') {string {"Diffuse color mutable"}}
		Entry (id = 'SPEC') {string {"Specular color mutable"}}
		Entry (id = 'MFCT') {string {"Microfacet color mutable"}}
		Entry (id = 'EMIS') {string {"Emission color mutable"}}
		Entry (id = 'ENVC') {string {"Environment color mutable"}}
		Entry (id = 'REFL') {string {"Reflection color mutable"}}
		Entry (id = 'REFR') {string {"Refraction color mutable"}}
		Entry (id = 'HSUB') {string {"Substance Data"}}
		Entry (id = 'SBST')
		{
			string {"Substance type"}
			Entry (id = 'NONE') {string {"<none>"}}
		}
	}
	Entry (id = 'TEXC')
	{
		Entry (id = 'HTX1') {string {"Texcoord 1 Settings"}}
		Entry (id = 'HTX2') {string {"Texcoord 2 Settings"}}
		Entry (id = 'SSCL') {string {"S scale"}}
		Entry (id = 'TSCL') {string {"T scale"}}
		Entry (id = 'SOFF') {string {"S offset"}}
		Entry (id = 'TOFF') {string {"T offset"}}
		Entry (id = 'ANIM') {string {"Enable animation"}}
		Entry (id = 'SPED') {string {"Animation speed"}}
		Entry (id = 'DRCT') {string {"Animation direction (deg)"}}
		Entry (id = 'HTWT') {string {"Terrain/Water Texcoord Settings"}}
		Entry (id = 'TWSC') {string {"Texcoord scale"}}
	}
	Entry (id = 'ITEX')
	{
		Entry (id = 'TEX1') {string {"Texcoord 1"}}
		Entry (id = 'TEX2') {string {"Texcoord 2"}}
	}
	Entry (id = 'PICK')
	{
		Entry (id = 'DIFF') {string {"Diffuse Color"}}
		Entry (id = 'TX1M') {string {"Texture Map (Primary)"}}
		Entry (id = 'TX2M') {string {"Texture Map (Secondary)"}}
		Entry (id = 'NM1M') {string {"Normal Map (Primary)"}}
		Entry (id = 'NM2M') {string {"Normal Map (Secondary)"}}
		Entry (id = 'SPEC') {string {"Specular Color"}}
		Entry (id = 'GLSM') {string {"Gloss Map"}}
		Entry (id = 'MFCT') {string {"Microfacet Color"}}
		Entry (id = 'EMIS') {string {"Emission Color"}}
		Entry (id = 'EMSM') {string {"Emission Map"}}
		Entry (id = 'ENVC') {string {"Environment Color"}}
		Entry (id = 'ENVM') {string {"Environment Map"}}
		Entry (id = 'REFL') {string {"Reflection Color"}}
		Entry (id = 'REFR') {string {"Refraction Color"}}
		Entry (id = 'OPCM') {string {"Opacity Map"}}
	}
	Entry (id = 'IMPT') {string {"Import Material"}}
	Entry (id = 'EXPT') {string {"Export Material"}}
}
Entry (id = 'SHDR')
{
	string {"Shader Editor"}
	Entry (id = 'CFRM') {string {"Save changes to shader?"}}
	Entry (id = 'SAVE') {string {"Save Shader"}}
	Entry (id = 'MESS')
	{
		Entry (id = 'ICMP') {string {"Graph incomplete"}}
		Entry (id = 'TEMP') {string {"Graph too complex"}}
		Entry (id = 'LITR') {string {"Too many literal constants"}}
		Entry (id = 'TEXC') {string {"Too many interpolants"}}
		Entry (id = 'TXTR') {string {"Too many textures"}}
	}
	Entry (id = 'MENU')
	{
		Entry (id = 'SHDR')
		{
			string {"Shader"}
			Entry (id = 'CLOS') {string {"Close"}}
			Entry (id = 'SAVE') {string {"Save Shader"}}
		}
		Entry (id = 'EDIT')
		{
			string {"Edit"}
			Entry (id = 'UNDO') {string {"Undo"}}
			Entry (id = 'CUT ') {string {"Cut"}}
			Entry (id = 'COPY') {string {"Copy"}}
			Entry (id = 'PAST') {string {"Paste"}}
			Entry (id = 'CLER') {string {"Clear"}}
			Entry (id = 'SALL') {string {"Select All"}}
			Entry (id = 'DUPL') {string {"Duplicate"}}
		}
		Entry (id = 'PROC')
		{
			string {"Process"}
			Entry (id = 'INFO') {string {"Get Info..."}}
			Entry (id = 'TDET') {string {"Toggle Detail Level"}}
		}
	}
}
Entry (id = 'SCPT')
{
	string {"Script Editor"}
	Entry (id = 'CFRM') {string {"Save changes to script?"}}
	Entry (id = 'SAVE') {string {"Save Script"}}
	Entry (id = 'DCFM') {string {"Are you sure you want to delete the selected script?\n\nThis cannot be undone."}}
	Entry (id = 'DELT') {string {"Delete Script"}}
	Entry (id = 'MENU')
	{
		Entry (id = 'SCPT')
		{
			string {"Script"}
			Entry (id = 'CLOS') {string {"Close"}}
			Entry (id = 'SAVE') {string {"Save Script"}}
		}
		Entry (id = 'EDIT')
		{
			string {"Edit"}
			Entry (id = 'UNDO') {string {"Undo"}}
			Entry (id = 'CUT ') {string {"Cut"}}
			Entry (id = 'COPY') {string {"Copy"}}
			Entry (id = 'PAST') {string {"Paste"}}
			Entry (id = 'CLER') {string {"Clear"}}
			Entry (id = 'SALL') {string {"Select All"}}
			Entry (id = 'DUPL') {string {"Duplicate"}}
		}
		Entry (id = 'MTHD')
		{
			string {"Method"}
			Entry (id = 'INFO') {string {"Get Info..."}}
			Entry (id = 'FCON') {string {"Cycle Fiber Condition"}}
		}
	}
	Entry (id = 'TARG')
	{
		string {"Target node"}
		Entry (id = 'CTRL') {string {"Controller target"}}
		Entry (id = 'INTR') {string {"Initiator node"}}
		Entry (id = 'TRIG') {string {"Trigger node"}}
		Entry (id = 'CONN') {string {"Connector "}}
	}
	Entry (id = 'EVNT')
	{
		Entry (id = 'CTAC') {string {"Controller Activate\nEvent"}}
		Entry (id = 'CTDA') {string {"Controller Deactivate\nEvent"}}
		Entry (id = 'WGAC') {string {"Widget Activate\nEvent"}}
		Entry (id = 'WGCH') {string {"Widget Change\nEvent"}}
		Entry (id = 'WGBH') {string {"Widget Begin Hover\nEvent"}}
		Entry (id = 'WGEH') {string {"Widget End Hover\nEvent"}}
	}
	Entry (id = 'UNAM') {string {"<unnamed>"}}
	Entry (id = 'SCPT')
	{
		Entry (id = 'NAME') {string {"Script name"}}
		Entry (id = 'CTAC') {string {"Handles controller activate event"}}
		Entry (id = 'CTDA') {string {"Handles controller deactivate event"}}
		Entry (id = 'WGAC') {string {"Handles widget activate event"}}
		Entry (id = 'WGCH') {string {"Handles widget change event"}}
		Entry (id = 'WGBH') {string {"Handles widget begin hover event"}}
		Entry (id = 'WGEH') {string {"Handles widget end hover event"}}
	}
}
Entry (id = 'PANL')
{
	string {"Panel Editor"}
	Entry (id = 'MNEW') {string {"New Panel"}}
	Entry (id = 'MOPN') {string {"Open Panel..."}}
	Entry (id = 'OPEN') {string {"Open Panel"}}
	Entry (id = 'CFRM') {string {"Save changes to "}}
	Entry (id = 'PANL') {string {"panel"}}
	Entry (id = 'SAVE') {string {"Save Panel"}}
	Entry (id = 'MENU')
	{
		Entry (id = 'PANL')
		{
			string {"Panel"}
			Entry (id = 'CLOS') {string {"Close"}}
			Entry (id = 'SAVE') {string {"Save Panel"}}
			Entry (id = 'SVAS') {string {"Save Panel As..."}}
			Entry (id = 'SETT') {string {"Window Settings..."}}
		}
		Entry (id = 'EDIT')
		{
			string {"Edit"}
			Entry (id = 'UNDO') {string {"Undo"}}
			Entry (id = 'CUT ') {string {"Cut"}}
			Entry (id = 'COPY') {string {"Copy"}}
			Entry (id = 'PAST') {string {"Paste"}}
			Entry (id = 'CLER') {string {"Clear"}}
			Entry (id = 'SALL') {string {"Select All"}}
			Entry (id = 'DUPL') {string {"Duplicate"}}
		}
		Entry (id = 'WDGT')
		{
			string {"Widget"}
			Entry (id = 'FRWD') {string {"Bring Forward"}}
			Entry (id = 'FRNT') {string {"Bring to Front"}}
			Entry (id = 'BKWD') {string {"Send Backward"}}
			Entry (id = 'BACK') {string {"Send to Back"}}
			Entry (id = 'HIDE') {string {"Hide Selection"}}
			Entry (id = 'UHID') {string {"Unhide All"}}
			Entry (id = 'LOCK') {string {"Lock Selection"}}
			Entry (id = 'ULCK') {string {"Unlock All"}}
			Entry (id = 'GRUP') {string {"Group Selection"}}
			Entry (id = 'UGRP') {string {"Ungroup Selection"}}
			Entry (id = 'RROT') {string {"Reset Rotation"}}
			Entry (id = 'RTEX') {string {"Reset Texcoords"}}
			Entry (id = 'REPL') {string {"Replace Texture"}}
			Entry (id = 'ASCL') {string {"Auto-scale Texture"}}
			Entry (id = 'ESCR') {string {"Edit Script"}}
			Entry (id = 'DSCR') {string {"Delete Script"}}
		}
		Entry (id = 'ARNG')
		{
			string {"Arrange"}
			Entry (id = 'NUGL') {string {"Nudge Left"}}
			Entry (id = 'NUGR') {string {"Nudge Right"}}
			Entry (id = 'NUGU') {string {"Nudge Up"}}
			Entry (id = 'NUGD') {string {"Nudge Down"}}
			Entry (id = 'ALNL') {string {"Align Left Sides"}}
			Entry (id = 'ALNR') {string {"Align Right Sides"}}
			Entry (id = 'ALNT') {string {"Align Top Sides"}}
			Entry (id = 'ALNB') {string {"Align Bottom Sides"}}
			Entry (id = 'ALNH') {string {"Align Horizontal Centers"}}
			Entry (id = 'ALNV') {string {"Align Vertical Centers"}}
		}
	}
	Entry (id = 'ERRR')
	{
		string {"Panel Editor Error"}
		Entry (id = 'LOCK') {string {"The panel could not be saved because the file is read-only."}}
		Entry (id = 'ACES') {string {"The panel could not be saved because access was denied. The file might be read-only."}}
		Entry (id = 'PROT') {string {"The panel could not be saved because the disk is write-protected."}}
		Entry (id = 'DFUL') {string {"The panel could not be saved because the disk is full."}}
		Entry (id = 'NSAV') {string {"The panel could not be saved because an error occurred."}}
	}
	Entry (id = 'NRES') {string {"Panel not found -- "}}
}
Entry (id = 'LAND')
{
	string {"Landscaping Editor"}
	Entry (id = 'CFRM') {string {"Save changes to brush?"}}
	Entry (id = 'SAVE') {string {"Save Brush"}}
	Entry (id = 'MENU')
	{
		Entry (id = 'LAND')
		{
			string {"Landscaping"}
			Entry (id = 'CLOS') {string {"Close"}}
			Entry (id = 'SAVE') {string {"Save Brush"}}
			Entry (id = 'RGEN') {string {"Regenerate Preview"}}
		}
	}
	Entry (id = 'PICK') {string {"Add World"}}
	Entry (id = 'INST') {string {"Instance Settings"}}
	Entry (id = 'DENS') {string {"Density (count per cell)"}}
	Entry (id = 'OGRP') {string {"Overlap within group (%)"}}
	Entry (id = 'OOUT') {string {"Overlap outside group (%)"}}
	Entry (id = 'AJST') {string {"Placement Adjustments"}}
}
Entry (id = 'IMDL') {string {"Import Model"}}
Entry (id = 'EMDL') {string {"Export Model"}}
Entry (id = 'SETT')
{
	Entry (id = 'DRAW') {string {"Drawing Settings"}}
	Entry (id = 'ASNP')
	{
		string {"Angle snap"}
		Entry (id = '15DG') {string {"15 degrees"}}
		Entry (id = '30DG') {string {"30 degrees"}}
		Entry (id = '45DG') {string {"45 degrees"}}
	}
	Entry (id = 'PAGE')
	{
		string {"Tool Page Settings"}
		Entry (id = 'NONE') {string {"Not shown"}}
		Entry (id = 'LEFT') {string {"Left side"}}
		Entry (id = 'RGHT') {string {"Right side"}}
	}
	Entry (id = 'PAG1') {string {"Tool column 1"}}
	Entry (id = 'PAG2') {string {"Tool column 2"}}
	Entry (id = 'PAG3') {string {"Tool column 3"}}
	Entry (id = 'PAG4') {string {"Tool column 4"}}
}
Entry (id = 'TBLD')
{
	Entry (id = 'PLAN')
	{
		string {"Create Flat Plane"}
		Entry (id = 'PARM') {string {"Terrain Parameters"}}
		Entry (id = 'XSIZ') {string {"X terrain size (voxels)"}}
		Entry (id = 'YSIZ') {string {"Y terrain size (voxels)"}}
		Entry (id = 'POSI') {string {"Empty space height (voxels)"}}
		Entry (id = 'NEGA') {string {"Solid space depth (voxels)"}}
	}
	Entry (id = 'TGAH')
	{
		string {"Import TGA Height Field"}
		Entry (id = 'FILE') {string {"TGA File Specification"}}
		Entry (id = 'IMAG') {string {"Height field image file"}}
		Entry (id = 'PICK') {string {"Height Field Image"}}
		Entry (id = 'PARM') {string {"Terrain Parameters"}}
		Entry (id = 'SCAL') {string {"Height scale (distance)"}}
		Entry (id = 'POSI') {string {"Empty space height (voxels)"}}
		Entry (id = 'NEGA') {string {"Solid space depth (voxels)"}}
	}
	Entry (id = 'RAWH')
	{
		string {"Import RAW Height Field"}
		Entry (id = 'FILE') {string {"RAW File Specification"}}
		Entry (id = 'IMAG') {string {"Height field image file"}}
		Entry (id = 'PICK') {string {"Height Field Image"}}
		Entry (id = 'WIDE') {string {"Image width (pixels)"}}
		Entry (id = 'HIGH') {string {"Image height (pixels)"}}
		Entry (id = 'CHAN')
		{
			string {"Channel count"}
			Entry (id = '1111') {string {"1 channel"}}
			Entry (id = '2222') {string {"2 channels"}}
			Entry (id = '3333') {string {"3 channels"}}
			Entry (id = '4444') {string {"4 channels"}}
		}
		Entry (id = 'DPTH')
		{
			string {"Bits per channel"}
			Entry (id = '8888') {string {"8 bits"}}
			Entry (id = '1616') {string {"16 bits"}}
		}
		Entry (id = 'PARM') {string {"Terrain Parameters"}}
		Entry (id = 'SCAL') {string {"Height scale (distance)"}}
		Entry (id = 'POSI') {string {"Empty space height (voxels)"}}
		Entry (id = 'NEGA') {string {"Solid space depth (voxels)"}}
	}
	Entry (id = 'RAWV')
	{
		string {"Import RAW Voxel Map"}
		Entry (id = 'FILE') {string {"RAW File Specification"}}
		Entry (id = 'VOXL') {string {"Voxel map file"}}
		Entry (id = 'PICK') {string {"Voxel Map"}}
		Entry (id = 'WIDE') {string {"Voxel map width (voxels)"}}
		Entry (id = 'HIGH') {string {"Voxel map height (voxels)"}}
		Entry (id = 'DEEP') {string {"Voxel map depth (voxels)"}}
		Entry (id = 'UDIR')
		{
			string {"Up direction"}
			Entry (id = 'YYYY') {string {"Y axis"}}
			Entry (id = 'ZZZZ') {string {"Z axis"}}
		}
	}
}
Entry (id = 'WBLD')
{
	Entry (id = 'GEOM')
	{
		string {"Water Geometry Parameters"}
		Entry (id = 'XSIZ') {string {"X water size (quads)"}}
		Entry (id = 'YSIZ') {string {"Y water size (quads)"}}
		Entry (id = 'HRZN') {string {"Horizon distance"}}
	}
	Entry (id = 'HFLG')
	{
		string {"Horizon Extension Directions"}
		Entry (id = 'EAST') {string {"Extend positive X (east)"}}
		Entry (id = 'WEST') {string {"Extend negative X (west)"}}
		Entry (id = 'NRTH') {string {"Extend positive Y (north)"}}
		Entry (id = 'SOTH') {string {"Extend negative Y (south)"}}
	}
}
Entry (id = 'MODL')
{
	Entry (id = 'MOPN') {string {"Open Model..."}}
	Entry (id = 'OPEN') {string {"Open Model"}}
	Entry (id = 'MENU')
	{
		Entry (id = 'MODL')
		{
			string {"Model"}
			Entry (id = 'CLOS') {string {"Close"}}
			Entry (id = 'SANM') {string {"Save Animation"}}
			Entry (id = 'SMDL') {string {"Save Model in Pose"}}
			Entry (id = 'ENVR') {string {"Select Environment..."}}
		}
		Entry (id = 'CUE ')
		{
			string {"Cue"}
			Entry (id = 'INSC') {string {"Insert Cue..."}}
			Entry (id = 'DELC') {string {"Delete Cue"}}
			Entry (id = 'DELA') {string {"Delete All Cues"}}
			Entry (id = 'INFO') {string {"Get Cue Info..."}}
		}
	}
	Entry (id = 'ENVR') {string {"Select Environment"}}
	Entry (id = 'CTYP') {string {"Cue type identifier"}}
	Entry (id = 'NRES') {string {"Model not found -- "}}
}
Entry (id = 'ERRR')
{
	string {"World Editor Error"}
	Entry (id = 'LOCK') {string {"The world could not be saved because the file is read-only."}}
	Entry (id = 'ACES') {string {"The world could not be saved because access was denied. The file might be read-only."}}
	Entry (id = 'PROT') {string {"The world could not be saved because the disk is write-protected."}}
	Entry (id = 'DFUL') {string {"The world could not be saved because the disk is full."}}
	Entry (id = 'NSAV') {string {"The world could not be saved because an error occurred."}}
	Entry (id = 'COMB') {string {"The detail levels could not be combined because the the skinning data does not match among the selected geometries."}}
	Entry (id = 'MERG') {string {"The geometries could not be merged because the resulting mesh would have too many vertices."}}
}
