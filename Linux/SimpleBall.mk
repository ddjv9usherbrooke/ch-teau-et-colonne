#--------------------------------------------------------
#
# SimpleBall
#
#--------------------------------------------------------

TARGETNAME			:= SimpleBall

include Include.mk

INCDIR				:= -I../EngineCode -I../GameCode $(COMMON_INCS)
CFLAGS				:= $(INCDIR) $(COMMON_DEFINES) $(COMMON_CFLAGS) $(PLUGIN_CFLAGS) $(WARNINGS)
LFLAGS				:= $(COMMON_LFLAGS) $(PLUGIN_LFLAGS)
LIBS				:= $(COMMON_LIBS)

#--------------------------------------------------------

SIMPLEBALL			:= SimpleBall.cpp

#--------------------------------------------------------

SRCS				:= $(addprefix GameCode/,$(SIMPLEBALL))
DEBUG_OBJS			:= $(patsubst %.cpp,Debug/%.o,$(SRCS))
OPTIMIZED_OBJS		:= $(patsubst %.cpp,Optimized/%.o,$(SRCS))

#--------------------------------------------------------

debug: debug_dir Debug/$(TARGETNAME).so
	cp Debug/$(TARGETNAME).so ../$(TARGETNAME).so

optimized: optimized_dir Optimized/$(TARGETNAME).so
	cp Optimized/$(TARGETNAME).so ../$(TARGETNAME).so

debug_dir:
	mkdir -p Debug/GameCode

optimized_dir:
	mkdir -p Optimized/GameCode

Debug/$(TARGETNAME).so: $(DEBUG_OBJS)
	gcc $(LFLAGS) -o $@ $(DEBUG_OBJS) $(LIBS)

Optimized/$(TARGETNAME).so: $(OPTIMIZED_OBJS)
	gcc $(LFLAGS) -o $@ $(OPTIMIZED_OBJS) $(LIBS)

Debug/%.o: ../%.cpp
	gcc -IDebug $(CFLAGS) $(DEBUG_CFLAGS) -c $< -o $@

Optimized/%.o: ../%.cpp
	gcc -IOptimized $(CFLAGS) $(OPTIMIZED_CFLAGS) -c $< -o $@
