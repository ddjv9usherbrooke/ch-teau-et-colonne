#--------------------------------------------------------
#
# Tool_WorldEditor
#
#--------------------------------------------------------

TARGETNAME			:= WorldEditor

include Include.mk

INCDIR				:= -I../EngineCode -I../PluginCode $(COMMON_INCS)
CFLAGS				:= $(INCDIR) $(COMMON_DEFINES) -DC4EDITOR $(COMMON_CFLAGS) $(PLUGIN_CFLAGS) $(WARNINGS)
LFLAGS				:= $(COMMON_LFLAGS) $(PLUGIN_LFLAGS)
LIBS				:= $(COMMON_LIBS)

#--------------------------------------------------------

LANDSCAPING			:= C4Landscaping.cpp
MANIPULATORS		:= C4CameraManipulators.cpp C4EditorManipulators.cpp C4EffectManipulators.cpp C4EmitterManipulators.cpp \
					   C4GeometryManipulators.cpp C4InstanceManipulators.cpp C4LightManipulators.cpp C4MarkerManipulators.cpp \
					   C4ModelManipulators.cpp C4PhysicsManipulators.cpp C4PortalManipulators.cpp C4SourceManipulators.cpp \
					   C4SpaceManipulators.cpp C4TriggerManipulators.cpp C4VolumeManipulators.cpp C4ZoneManipulators.cpp
MODELVIEWER			:= C4ModelViewer.cpp
PANELEDITOR			:= C4PanelEditor.cpp
SCRIPTEDITOR		:= C4ScriptEditor.cpp
SHADEREDITOR		:= C4ShaderEditor.cpp
TERRAINWATER		:= C4TerrainBuilders.cpp C4TerrainTools.cpp C4WaterTools.cpp
WORLDEDITOR			:= C4EditorBase.cpp C4EditorCommands.cpp C4EditorConnectors.cpp C4EditorGizmo.cpp C4EditorPages.cpp \
					   C4EditorPlugins.cpp C4EditorSupport.cpp C4EditorTools.cpp C4EditorOperations.cpp C4EditorViewports.cpp \
					   C4MaterialContainer.cpp C4MaterialManager.cpp C4NodeInfo.cpp C4WorldEditor.cpp

#--------------------------------------------------------

SRCS				:= $(addprefix PluginCode/,$(LANDSCAPING) $(MANIPULATORS) $(MODELVIEWER) $(PANELEDITOR) $(SCRIPTEDITOR) $(SHADEREDITOR) $(TERRAINWATER) $(WORLDEDITOR))
DEBUG_OBJS			:= $(patsubst %.cpp,Debug/%.o,$(SRCS))
OPTIMIZED_OBJS		:= $(patsubst %.cpp,Optimized/%.o,$(SRCS))

#--------------------------------------------------------

debug: debug_dir output_dir Debug/$(TARGETNAME).so
	cp Debug/$(TARGETNAME).so ../Plugins/Tools/$(TARGETNAME).so

optimized: optimized_dir output_dir Optimized/$(TARGETNAME).so
	cp Optimized/$(TARGETNAME).so ../Plugins/Tools/$(TARGETNAME).so

debug_dir:
	mkdir -p Debug/PluginCode

optimized_dir:
	mkdir -p Optimized/PluginCode

output_dir:
	mkdir -p ../Plugins/Tools

Debug/$(TARGETNAME).so: $(DEBUG_OBJS)
	gcc $(LFLAGS) -o $@ $(DEBUG_OBJS) $(LIBS)

Optimized/$(TARGETNAME).so: $(OPTIMIZED_OBJS)
	gcc $(LFLAGS) -o $@ $(OPTIMIZED_OBJS) $(LIBS)

Debug/%.o: ../%.cpp
	gcc -IDebug $(CFLAGS) $(DEBUG_CFLAGS) -c $< -o $@

Optimized/%.o: ../%.cpp
	gcc -IOptimized $(CFLAGS) $(OPTIMIZED_CFLAGS) -c $< -o $@
