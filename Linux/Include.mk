COMMON_INCS			:= -I/usr/include -I/usr/lib/gcc/x86_64-linux-gnu/4.8.1/include

COMMON_DEFINES		:= -DC4LINUX

COMMON_CFLAGS		:= -m64 -msse -msse2 -std=c++11 -pthread -ffast-math -fno-strict-aliasing -fno-exceptions -fno-rtti
PLUGIN_CFLAGS		:= -fPIC

WARNINGS			:= -Wall -Wno-unused-function -Wno-switch -Wno-multichar -Wno-char-subscripts \
					   -Wno-uninitialized -Wno-non-virtual-dtor -Wno-delete-non-virtual-dtor -Wno-sign-compare \
					   -Wno-invalid-offsetof -Wno-deprecated-declarations -Wno-unused-result -Wno-array-bounds

DEBUG_CFLAGS		:= -DC4DEBUG -g -Og
OPTIMIZED_CFLAGS	:= -DC4OPTIMIZED -O3

COMMON_LFLAGS		:= -fno-exceptions -fno-rtti
PLUGIN_LFLAGS		:= -shared -fPIC

COMMON_LIBS			:= -lc -lstdc++ -lm -lpthread -ldl
