#--------------------------------------------------------
#
# Game
#
#--------------------------------------------------------

TARGETNAME			:= Game

include Include.mk

INCDIR				:= -I../EngineCode -I../PluginCode -I../GameCode $(COMMON_INCS)
CFLAGS				:= $(INCDIR) $(COMMON_DEFINES) $(COMMON_CFLAGS) $(PLUGIN_CFLAGS) $(WARNINGS)
LFLAGS				:= $(COMMON_LFLAGS) $(PLUGIN_LFLAGS)
LIBS				:= $(COMMON_LIBS)

#--------------------------------------------------------

MGANIMALS			:= MGChicken.cpp MGFish.cpp
MGEFFECTS			:= MGEffects.cpp MGWeather.cpp
MGGENERAL			:= MGBase.cpp MGCameras.cpp MGCharacter.cpp MGControllers.cpp MGGame.cpp MGInput.cpp MGMaterials.cpp \
					   MGMultiplayer.cpp MGProperties.cpp MGRigidBody.cpp MGScripts.cpp
MGINTERFACE			:= MGConfiguration.cpp MGCrosshairs.cpp MGInterface.cpp
MGMONSTERS			:= MGBat.cpp MGBlackCat.cpp MGFireball.cpp MGGoblin.cpp MGGolem.cpp MGGrimReaper.cpp MGHauntedArm.cpp \
					   MGMagicBroom.cpp MGMonster.cpp MGPumpkinhead.cpp MGSkeleton.cpp MGSpider.cpp MGTarball.cpp \
					   MGVenom.cpp MGWitch.cpp MGZombie.cpp
MGPLAYER			:= MGFighter.cpp MGGusGraves.cpp MGPowers.cpp MGSoldier.cpp MGTreasure.cpp
MGWEAPONS			:= MGAxe.cpp MGBeerCannon.cpp MGCatFlinger.cpp MGChickenBlaster.cpp MGCrossbow.cpp MGGrenadeLauncher.cpp \
					   MGHornetGun.cpp MGPistol.cpp MGPlasmaGun.cpp MGProtonCannon.cpp MGQuantumCharger.cpp \
					   MGRocketLauncher.cpp MGShotgun.cpp MGSpikeShooter.cpp MGWeapons.cpp

#--------------------------------------------------------

SRCS				:= $(addprefix GameCode/,$(MGANIMALS) $(MGEFFECTS) $(MGGENERAL) $(MGINTERFACE) $(MGMONSTERS) $(MGPLAYER) $(MGWEAPONS))
DEBUG_OBJS			:= $(patsubst %.cpp,Debug/%.o,$(SRCS))
OPTIMIZED_OBJS		:= $(patsubst %.cpp,Optimized/%.o,$(SRCS))

#--------------------------------------------------------

debug: debug_dir Debug/$(TARGETNAME).so
	cp Debug/$(TARGETNAME).so ../$(TARGETNAME).so

optimized: optimized_dir Optimized/$(TARGETNAME).so
	cp Optimized/$(TARGETNAME).so ../$(TARGETNAME).so

debug_dir:
	mkdir -p Debug/GameCode

optimized_dir:
	mkdir -p Optimized/GameCode

Debug/$(TARGETNAME).so: $(DEBUG_OBJS)
	gcc $(LFLAGS) -o $@ $(DEBUG_OBJS) $(LIBS)

Optimized/$(TARGETNAME).so: $(OPTIMIZED_OBJS)
	gcc $(LFLAGS) -o $@ $(OPTIMIZED_OBJS) $(LIBS)

Debug/%.o: ../%.cpp
	gcc -IDebug $(CFLAGS) $(DEBUG_CFLAGS) -c $< -o $@

Optimized/%.o: ../%.cpp
	gcc -IOptimized $(CFLAGS) $(OPTIMIZED_CFLAGS) -c $< -o $@
