#--------------------------------------------------------
#
# Tool_OpenGexImporter
#
#--------------------------------------------------------

TARGETNAME			:= OpenGexImporter

include Include.mk

INCDIR				:= -I../EngineCode -I../PluginCode $(COMMON_INCS)
CFLAGS				:= $(INCDIR) $(COMMON_DEFINES) $(COMMON_CFLAGS) $(PLUGIN_CFLAGS) $(WARNINGS)
LFLAGS				:= $(COMMON_LFLAGS) $(PLUGIN_LFLAGS)
LIBS				:= $(COMMON_LIBS)

#--------------------------------------------------------

OPENGEXIMPORTER		:= C4OpenGexImporter.cpp

#--------------------------------------------------------

SRCS				:= $(addprefix PluginCode/,$(OPENGEXIMPORTER))
DEBUG_OBJS			:= $(patsubst %.cpp,Debug/%.o,$(SRCS))
OPTIMIZED_OBJS		:= $(patsubst %.cpp,Optimized/%.o,$(SRCS))

#--------------------------------------------------------

debug: debug_dir output_dir Debug/$(TARGETNAME).so
	cp Debug/$(TARGETNAME).so ../Plugins/Tools/Editor/$(TARGETNAME).so

optimized: optimized_dir output_dir Optimized/$(TARGETNAME).so
	cp Optimized/$(TARGETNAME).so ../Plugins/Tools/Editor/$(TARGETNAME).so

debug_dir:
	mkdir -p Debug/PluginCode

optimized_dir:
	mkdir -p Optimized/PluginCode

output_dir:
	mkdir -p ../Plugins/Tools/Editor

Debug/$(TARGETNAME).so: $(DEBUG_OBJS)
	gcc $(LFLAGS) -o $@ $(DEBUG_OBJS) $(LIBS)

Optimized/$(TARGETNAME).so: $(OPTIMIZED_OBJS)
	gcc $(LFLAGS) -o $@ $(OPTIMIZED_OBJS) $(LIBS)

Debug/%.o: ../%.cpp
	gcc -IDebug $(CFLAGS) $(DEBUG_CFLAGS) -c $< -o $@

Optimized/%.o: ../%.cpp
	gcc -IOptimized $(CFLAGS) $(OPTIMIZED_CFLAGS) -c $< -o $@
