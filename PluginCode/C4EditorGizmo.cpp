//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This file is part of the C4 Engine and is provided under the
// terms of the license agreement entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4EditorGizmo.h"
#include "C4EditorManipulators.h"
#include "C4EditorViewports.h"


using namespace C4;


namespace
{
	const float kGizmoThreshold = 0.95F;


	const ConstPoint3D gizmoPosition[32] =
	{
		{0.0F, 0.0F, 0.0F}, {0.0F, 0.0F, 0.0F}, {40.0F, 0.0F, 0.0F}, {40.0F, 0.0F, 0.0F},
		{40.0F, 0.0F, 0.0F}, {40.0F, 0.0F, 0.0F}, {56.0F, 0.0F, 0.0F}, {56.0F, 0.0F, 0.0F},
		{0.0F, 0.0F, 0.0F}, {0.0F, 0.0F, 0.0F}, {0.0F, 40.0F, 0.0F}, {0.0F, 40.0F, 0.0F},
		{0.0F, 40.0F, 0.0F}, {0.0F, 40.0F, 0.0F}, {0.0F, 56.0F, 0.0F}, {0.0F, 56.0F, 0.0F},
		{0.0F, 0.0F, 0.0F}, {0.0F, 0.0F, 0.0F}, {0.0F, 0.0F, 40.0F}, {0.0F, 0.0F, 40.0F},
		{0.0F, 0.0F, 40.0F}, {0.0F, 0.0F, 40.0F}, {0.0F, 0.0F, 56.0F}, {0.0F, 0.0F, 56.0F},
		{0.0F, 0.0F, 0.0F}, {0.0F, 0.0F, 0.0F}, {40.0F, 0.0F, 0.0F}, {40.0F, 0.0F, 0.0F},
		{40.0F, 0.0F, 0.0F}, {40.0F, 0.0F, 0.0F}, {56.0F, 0.0F, 0.0F}, {56.0F, 0.0F, 0.0F}
	};

	const ConstColor4C gizmoColor[32] =
	{
		{255, 0, 0, 255}, {255, 0, 0, 255}, {255, 0, 0, 255}, {255, 0, 0, 255}, {255, 0, 0, 255}, {255, 0, 0, 255}, {255, 0, 0, 255}, {255, 0, 0, 255},
		{0, 255, 0, 255}, {0, 255, 0, 255}, {0, 255, 0, 255}, {0, 255, 0, 255}, {0, 255, 0, 255}, {0, 255, 0, 255}, {0, 255, 0, 255}, {0, 255, 0, 255},
		{0, 96, 255, 255}, {0, 96, 255, 255}, {0, 96, 255, 255}, {0, 96, 255, 255}, {0, 96, 255, 255}, {0, 96, 255, 255}, {0, 96, 255, 255}, {0, 96, 255, 255},
		{255, 0, 0, 255}, {255, 0, 0, 255}, {255, 0, 0, 255}, {255, 0, 0, 255}, {255, 0, 0, 255}, {255, 0, 0, 255}, {255, 0, 0, 255}, {255, 0, 0, 255}
	};

	const ConstVector4D gizmoTangent[32] =
	{
		{1.0F, 0.0F, 0.0F, -10.0F}, {1.0F, 0.0F, 0.0F, 10.0F}, {1.0F, 0.0F, 0.0F, 10.0F}, {1.0F, 0.0F, 0.0F, -10.0F},
		{1.0F, 0.0F, 0.0F, -10.0F}, {1.0F, 0.0F, 0.0F, 10.0F}, {1.0F, 0.0F, 0.0F, 10.0F}, {1.0F, 0.0F, 0.0F, -10.0F},
		{0.0F, 1.0F, 0.0F, -10.0F}, {0.0F, 1.0F, 0.0F, 10.0F}, {0.0F, 1.0F, 0.0F, 10.0F}, {0.0F, 1.0F, 0.0F, -10.0F},
		{0.0F, 1.0F, 0.0F, -10.0F}, {0.0F, 1.0F, 0.0F, 10.0F}, {0.0F, 1.0F, 0.0F, 10.0F}, {0.0F, 1.0F, 0.0F, -10.0F},
		{0.0F, 0.0F, 1.0F, -10.0F}, {0.0F, 0.0F, 1.0F, 10.0F}, {0.0F, 0.0F, 1.0F, 10.0F}, {0.0F, 0.0F, 1.0F, -10.0F},
		{0.0F, 0.0F, 1.0F, -10.0F}, {0.0F, 0.0F, 1.0F, 10.0F}, {0.0F, 0.0F, 1.0F, 10.0F}, {0.0F, 0.0F, 1.0F, -10.0F},
		{1.0F, 0.0F, 0.0F, -10.0F}, {1.0F, 0.0F, 0.0F, 10.0F}, {1.0F, 0.0F, 0.0F, 10.0F}, {1.0F, 0.0F, 0.0F, -10.0F},
		{1.0F, 0.0F, 0.0F, -10.0F}, {1.0F, 0.0F, 0.0F, 10.0F}, {1.0F, 0.0F, 0.0F, 10.0F}, {1.0F, 0.0F, 0.0F, -10.0F}
	};

	const ConstPoint2D gizmoTexcoord[32] =
	{
		{0.0F, 0.0F}, {0.0F, 1.0F}, {0.625F, 1.0F}, {0.625F, 0.0F},
		{0.625F, 0.0F}, {0.625F, 1.0F}, {0.9921875F, 1.0F}, {0.9921875F, 0.0F},
		{0.0F, 0.0F}, {0.0F, 1.0F}, {0.625F, 1.0F}, {0.625F, 0.0F},
		{0.625F, 0.0F}, {0.625F, 1.0F}, {0.9921875F, 1.0F}, {0.9921875F, 0.0F},
		{0.0F, 0.0F}, {0.0F, 1.0F}, {0.625F, 1.0F}, {0.625F, 0.0F},
		{0.625F, 0.0F}, {0.625F, 1.0F}, {0.9921875F, 1.0F}, {0.9921875F, 0.0F},
		{0.0F, 0.0F}, {0.0F, 1.0F}, {0.625F, 1.0F}, {0.625F, 0.0F},
		{0.625F, 0.0F}, {0.625F, 1.0F}, {0.9921875F, 1.0F}, {0.9921875F, 0.0F}
	};


	const TextureHeader boxEdgeTextureHeader =
	{
		kTexture2D,
		kTextureForceHighQuality,
		kTextureSemanticDiffuse,
		kTextureSemanticTransparency,
		kTextureLA8,
		8, 1, 1,
		{kTextureClamp, kTextureRepeat, kTextureClamp},
		1
	};


	const TextureHeader boxHiliteTextureHeader =
	{
		kTexture2D,
		kTextureForceHighQuality,
		kTextureSemanticDiffuse,
		kTextureSemanticTransparency,
		kTextureL8,
		8, 1, 1,
		{kTextureClamp, kTextureRepeat, kTextureClamp},
		1
	};


	const unsigned_int8 boxEdgeTextureImage[16] =
	{
		0xFF, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00
	};


	const unsigned_int8 boxHiliteTextureImage[8] =
	{
		0x00, 0x55, 0xAA, 0xFF, 0xFF, 0xAA, 0x55, 0x00
	};
}


EditorGizmo::EditorGizmo(const EditorManipulator *manipulator) :
		gizmoVertexBuffer(kVertexBufferAttribute | kVertexBufferDynamic),
		gizmoTextureMap("WorldEditor/arrow"),
		gizmoRenderable(kRenderQuads), 
		boxVertexBuffer(kVertexBufferAttribute | kVertexBufferDynamic),
		boxDiffuseColor(ColorRGBA(1.0F, 1.0F, 0.5F, 1.0F)),
		boxTextureMap(&boxEdgeTextureHeader, boxEdgeTextureImage), 
		boxRenderable(kRenderQuads),
		faceVertexBuffer(kVertexBufferAttribute | kVertexBufferDynamic), 
		faceRenderable(kRenderQuads), 
		edgeVertexBuffer(kVertexBufferAttribute | kVertexBufferDynamic),
		edgeDiffuseColor(kAttributeMutable),
		edgeTextureMap(&boxHiliteTextureHeader, boxHiliteTextureImage),
		edgeRenderable(kRenderQuads), 
		handleVertexBuffer(kVertexBufferAttribute | kVertexBufferDynamic),
		handleRenderable(kRenderQuads)
{
	gizmoManipulator = manipulator;
	hiliteEdgeIndex = -1; 

	gizmoRenderable.SetAmbientBlendState(kBlendInterpolate);
	gizmoRenderable.SetShaderFlags(kShaderAmbientEffect | kShaderVertexPolyboard | kShaderLinearPolyboard | kShaderOrthoPolyboard | kShaderScaleVertex);
	gizmoRenderable.SetRenderParameterPointer(&gizmoScaleVector);
	gizmoRenderable.SetTransformable(manipulator->GetTargetNode());

	gizmoRenderable.SetVertexBuffer(kVertexBufferAttributeArray, &gizmoVertexBuffer, sizeof(GizmoVertex));
	gizmoVertexBuffer.Establish(sizeof(GizmoVertex) * 32);

	gizmoAttributeList.Append(&gizmoTextureMap);
	gizmoRenderable.SetMaterialAttributeList(&gizmoAttributeList);

	for (machine a = 0; a < 32; a++)
	{
		gizmoVertex[a].position = gizmoPosition[a];
		gizmoVertex[a].color = gizmoColor[a];
		gizmoVertex[a].tangent = gizmoTangent[a];
		gizmoVertex[a].texcoord = gizmoTexcoord[a];
	}

	boxRenderable.SetAmbientBlendState(kBlendInterpolate);
	boxRenderable.SetShaderFlags(kShaderAmbientEffect | kShaderVertexPolyboard | kShaderLinearPolyboard);
	boxRenderable.SetTransformable(manipulator->GetTargetNode());

	boxRenderable.SetVertexCount(48);
	boxRenderable.SetVertexBuffer(kVertexBufferAttributeArray, &boxVertexBuffer, sizeof(BoxVertex));
	boxRenderable.SetVertexAttributeArray(kArrayPosition, 0, 3);
	boxRenderable.SetVertexAttributeArray(kArrayTangent, sizeof(Point3D), 4);
	boxRenderable.SetVertexAttributeArray(kArrayTexcoord, sizeof(Point3D) + sizeof(Vector4D), 2);
	boxVertexBuffer.Establish(sizeof(BoxVertex) * 48);

	boxAttributeList.Append(&boxDiffuseColor);
	boxAttributeList.Append(&boxTextureMap);
	boxRenderable.SetMaterialAttributeList(&boxAttributeList);

	for (machine a = 0; a < 48; a += 4)
	{
		boxVertex[a].texcoord.Set(0.0F, 0.0F);
		boxVertex[a + 1].texcoord.Set(1.0F, 0.0F);
		boxVertex[a + 2].texcoord.Set(1.0F, 1.0F);
		boxVertex[a + 3].texcoord.Set(0.0F, 1.0F);
	}

	faceRenderable.SetAmbientBlendState(kBlendAccumulate);
	faceRenderable.SetShaderFlags(kShaderAmbientEffect);
	faceRenderable.SetTransformable(manipulator->GetTargetNode());

	faceRenderable.SetVertexCount(24);
	faceRenderable.SetVertexBuffer(kVertexBufferAttributeArray, &faceVertexBuffer, sizeof(FaceVertex));
	faceRenderable.SetVertexAttributeArray(kArrayPosition, 0, 3);
	faceRenderable.SetVertexAttributeArray(kArrayColor, sizeof(Point3D), 1);
	faceVertexBuffer.Establish(sizeof(FaceVertex) * 24);

	for (machine a = 0; a < 24; a++)
	{
		faceVertex[a].color.Set(16, 16, 16, 255);
	}

	edgeRenderable.SetAmbientBlendState(kBlendAccumulate);
	edgeRenderable.SetShaderFlags(kShaderAmbientEffect | kShaderVertexPolyboard | kShaderLinearPolyboard);
	edgeRenderable.SetTransformable(manipulator->GetTargetNode());

	edgeRenderable.SetVertexCount(4);
	edgeRenderable.SetVertexBuffer(kVertexBufferAttributeArray, &edgeVertexBuffer, sizeof(BoxVertex));
	edgeRenderable.SetVertexAttributeArray(kArrayPosition, 0, 3);
	edgeRenderable.SetVertexAttributeArray(kArrayTangent, sizeof(Point3D), 4);
	edgeRenderable.SetVertexAttributeArray(kArrayTexcoord, sizeof(Point3D) + sizeof(Vector4D), 2);
	edgeVertexBuffer.Establish(sizeof(BoxVertex) * 4);

	edgeAttributeList.Append(&edgeDiffuseColor);
	edgeAttributeList.Append(&edgeTextureMap);
	edgeRenderable.SetMaterialAttributeList(&edgeAttributeList);

	edgeVertex[0].texcoord.Set(0.0F, 0.0F);
	edgeVertex[1].texcoord.Set(1.0F, 0.0F);
	edgeVertex[2].texcoord.Set(1.0F, 1.0F);
	edgeVertex[3].texcoord.Set(0.0F, 1.0F);

	handleRenderable.SetAmbientBlendState(kBlendInterpolate);
	handleRenderable.SetShaderFlags(kShaderAmbientEffect | kShaderVertexBillboard | kShaderScaleVertex);
	handleRenderable.SetRenderParameterPointer(&gizmoScaleVector);
	handleRenderable.SetTransformable(manipulator->GetTargetNode());

	handleRenderable.SetVertexCount(12);
	handleRenderable.SetVertexBuffer(kVertexBufferAttributeArray, &handleVertexBuffer, sizeof(HandleVertex));
	handleRenderable.SetVertexAttributeArray(kArrayPosition, 0, 3);
	handleRenderable.SetVertexAttributeArray(kArrayColor, sizeof(Point3D), 1);
	handleRenderable.SetVertexAttributeArray(kArrayBillboard, sizeof(Point3D) + sizeof(Color4C), 2);
	handleVertexBuffer.Establish(sizeof(HandleVertex) * 12);

	for (machine a = 0; a < 4; a++)
	{
		handleVertex[a].color.Set(255, 0, 0, 255);
		handleVertex[a + 4].color.Set(0, 255, 0, 255);
		handleVertex[a + 8].color.Set(0, 96, 255, 255);
	}

	for (machine a = 0; a < 12; a += 4)
	{
		handleVertex[a].billboard.Set(-3.0F, -3.0F);
		handleVertex[a + 1].billboard.Set(-3.0F, 3.0F);
		handleVertex[a + 2].billboard.Set(3.0F, 3.0F);
		handleVertex[a + 3].billboard.Set(3.0F, -3.0F);
	}
}

EditorGizmo::~EditorGizmo()
{
}

void EditorGizmo::HiliteMovers(unsigned_int32 mask)
{
	if (mask & 1)
	{
		for (machine a = 4; a < 8; a++)
		{
			gizmoVertex[a].color.Set(255, 255, 255, 255);
		}

		for (machine a = 28; a < 32; a++)
		{
			gizmoVertex[a].color.Set(255, 255, 255, 255);
		}
	}
	else
	{
		for (machine a = 4; a < 8; a++)
		{
			gizmoVertex[a].color.Set(255, 0, 0, 255);
		}

		for (machine a = 28; a < 32; a++)
		{
			gizmoVertex[a].color.Set(255, 0, 0, 255);
		}
	}

	if (mask & 2)
	{
		for (machine a = 12; a < 16; a++)
		{
			gizmoVertex[a].color.Set(255, 255, 255, 255);
		}
	}
	else
	{
		for (machine a = 12; a < 16; a++)
		{
			gizmoVertex[a].color.Set(0, 255, 0, 255);
		}
	}

	if (mask & 4)
	{
		for (machine a = 20; a < 24; a++)
		{
			gizmoVertex[a].color.Set(255, 255, 255, 255);
		}
	}
	else
	{
		for (machine a = 20; a < 24; a++)
		{
			gizmoVertex[a].color.Set(0, 96, 255, 255);
		}
	}
}

void EditorGizmo::HiliteRotators(unsigned_int32 mask)
{
	if (mask & 1)
	{
		for (machine a = 0; a < 4; a++)
		{
			handleVertex[a].color.Set(255, 255, 255, 255);
		}
	}
	else
	{
		for (machine a = 0; a < 4; a++)
		{
			handleVertex[a].color.Set(255, 0, 0, 255);
		}
	}

	if (mask & 2)
	{
		for (machine a = 4; a < 8; a++)
		{
			handleVertex[a].color.Set(255, 255, 255, 255);
		}
	}
	else
	{
		for (machine a = 4; a < 8; a++)
		{
			handleVertex[a].color.Set(0, 255, 0, 255);
		}
	}

	if (mask & 4)
	{
		for (machine a = 8; a < 12; a++)
		{
			handleVertex[a].color.Set(255, 255, 255, 255);
		}
	}
	else
	{
		for (machine a = 8; a < 12; a++)
		{
			handleVertex[a].color.Set(0, 96, 255, 255);
		}
	}
}

int32 EditorGizmo::PickMover(const ManipulatorViewportData *viewportData, const Ray *ray) const
{
	Vector3D viewDirection = GetTransformable()->GetInverseWorldTransform() * viewportData->viewportCamera->GetNodeTransform()[2];

	const Transform4D& transform = gizmoRenderable.GetTransformable()->GetInverseWorldTransform();
	Point3D p = transform * ray->origin;
	Vector3D v = transform * ray->direction;

	float scale = viewportData->viewportScale;
	float handleRadius = scale * 10.0F;
	float handlePosition = scale * 48.0F;

	float r2 = handleRadius * handleRadius;
	float x = p.x - handlePosition;
	float y = p.y - handlePosition;
	float z = p.z - handlePosition;

	float a = v * v;
	float b0 = x * v.x + p.y * v.y + p.z * v.z;
	float c0 = x * x + p.y * p.y + p.z * p.z - r2;
	float b1 = p.x * v.x + y * v.y + p.z * v.z;
	float c1 = p.x * p.x + y * y + p.z * p.z - r2;
	float b2 = p.x * v.x + p.y * v.y + z * v.z;
	float c2 = p.x * p.x + p.y * p.y + z * z - r2;

	float d0 = b0 * b0 - a * c0;
	float d1 = b1 * b1 - a * c1;
	float d2 = b2 * b2 - a * c2;

	int32 index = -1;
	float t = K::infinity;

	if ((d0 > K::min_float) && (Fabs(viewDirection.x) <= kGizmoThreshold))
	{
		float u = (-b0 - Sqrt(d0)) / a;
		if (u < t)
		{
			t = u;
			index = 0;
		}
	}

	if ((d1 > K::min_float) && (Fabs(viewDirection.y) <= kGizmoThreshold))
	{
		float u = (-b1 - Sqrt(d1)) / a;
		if (u < t)
		{
			t = u;
			index = 1;
		}
	}

	if ((d2 > K::min_float) && (Fabs(viewDirection.z) <= kGizmoThreshold))
	{
		float u = (-b2 - Sqrt(d2)) / a;
		if (u < t)
		{
			index = 2;
		}
	}

	return (index);
}

int32 EditorGizmo::PickRotator(const ManipulatorViewportData *viewportData, const Ray *ray) const
{
	const Transform4D& transform = gizmoRenderable.GetTransformable()->GetInverseWorldTransform();
	Point3D p = transform * ray->origin;
	Vector3D v = transform * ray->direction;

	float scale = viewportData->viewportScale;
	float handleRadius = scale * 3.0F;
	float handlePosition = scale * 24.0F;

	float r2 = handleRadius * handleRadius;
	float x = p.x - handlePosition;
	float y = p.y - handlePosition;
	float z = p.z - handlePosition;

	float a = v * v;
	float b0 = p.x * v.x + y * v.y + z * v.z;
	float c0 = p.x * p.x + y * y + z * z - r2;
	float b1 = x * v.x + p.y * v.y + z * v.z;
	float c1 = x * x + p.y * p.y + z * z - r2;
	float b2 = x * v.x + y * v.y + p.z * v.z;
	float c2 = x * x + y * y + p.z * p.z - r2;

	float d0 = b0 * b0 - a * c0;
	float d1 = b1 * b1 - a * c1;
	float d2 = b2 * b2 - a * c2;

	int32 index = -1;
	float t = K::infinity;

	if (d0 > K::min_float)
	{
		float u = (-b0 - Sqrt(d0)) / a;
		if (u < t)
		{
			t = u;
			index = 0;
		}
	}

	if (d1 > K::min_float)
	{
		float u = (-b1 - Sqrt(d1)) / a;
		if (u < t)
		{
			t = u;
			index = 1;
		}
	}

	if (d2 > K::min_float)
	{
		float u = (-b2 - Sqrt(d2)) / a;
		if (u < t)
		{
			index = 2;
		}
	}

	return (index);
}

void EditorGizmo::HiliteFace(int32 face, float intensity)
{
	for (machine a = 0; a < 24; a++)
	{
		faceVertex[a].color.Set(16, 16, 16, 255);
	}

	if (face >= 0)
	{
		unsigned_int32 value = (unsigned_int32) (intensity * 32.0F);
		FaceVertex *vertex = faceVertex + face * 4;

		for (machine a = 0; a < 4; a++)
		{
			vertex[a].color.Set(value, value, value, 255);
		}
	}
}

void EditorGizmo::HiliteEdge(int32 edge, float intensity)
{
	hiliteEdgeIndex = edge;
	if (edge >= 0)
	{
		intensity *= 0.25F;
		edgeDiffuseColor.SetDiffuseColor(ColorRGBA(intensity, intensity, intensity, 1.0F));
	}
}

int32 EditorGizmo::PickFace(const Ray *ray, Point3D *point) const
{
	const Transform4D& transform = GetTransformable()->GetInverseWorldTransform();
	const Point3D& position = transform * ray->origin;
	const Vector3D& direction = transform * ray->direction;
	const Box3D& box = gizmoBox;

	if ((position.x > box.max.x) && (direction.x < 0.0F))
	{
		float t = (box.max.x - position.x) / direction.x;
		float y = position.y + t * direction.y;
		float z = position.z + t * direction.z;

		if ((y > box.min.y) && (y < box.max.y) && (z > box.min.z) && (z < box.max.z))
		{
			if (point)
			{
				*point = ray->origin + ray->direction * t;
			}

			return (0);
		}
	}

	if ((position.x < box.min.x) && (direction.x > 0.0F))
	{
		float t = (box.min.x - position.x) / direction.x;
		float y = position.y + t * direction.y;
		float z = position.z + t * direction.z;

		if ((y > box.min.y) && (y < box.max.y) && (z > box.min.z) && (z < box.max.z))
		{
			if (point)
			{
				*point = ray->origin + ray->direction * t;
			}

			return (1);
		}
	}

	if ((position.y > box.max.y) && (direction.y < 0.0F))
	{
		float t = (box.max.y - position.y) / direction.y;
		float x = position.x + t * direction.x;
		float z = position.z + t * direction.z;

		if ((x > box.min.x) && (x < box.max.x) && (z > box.min.z) && (z < box.max.z))
		{
			if (point)
			{
				*point = ray->origin + ray->direction * t;
			}

			return (2);
		}
	}

	if ((position.y < box.min.y) && (direction.y > 0.0F))
	{
		float t = (box.min.y - position.y) / direction.y;
		float x = position.x + t * direction.x;
		float z = position.z + t * direction.z;

		if ((x > box.min.x) && (x < box.max.x) && (z > box.min.z) && (z < box.max.z))
		{
			if (point)
			{
				*point = ray->origin + ray->direction * t;
			}

			return (3);
		}
	}

	if ((position.z > box.max.z) && (direction.z < 0.0F))
	{
		float t = (box.max.z - position.z) / direction.z;
		float x = position.x + t * direction.x;
		float y = position.y + t * direction.y;

		if ((x > box.min.x) && (x < box.max.x) && (y > box.min.y) && (y < box.max.y))
		{
			if (point)
			{
				*point = ray->origin + ray->direction * t;
			}

			return (4);
		}
	}

	if ((position.z < box.min.z) && (direction.z > 0.0F))
	{
		float t = (box.min.z - position.z) / direction.z;
		float x = position.x + t * direction.x;
		float y = position.y + t * direction.y;

		if ((x > box.min.x) && (x < box.max.x) && (y > box.min.y) && (y < box.max.y))
		{
			if (point)
			{
				*point = ray->origin + ray->direction * t;
			}

			return (5);
		}
	}

	return (-1);
}

int32 EditorGizmo::PickEdge(const Ray *ray, Point3D *point) const
{
	const Transform4D& transform = GetTransformable()->GetInverseWorldTransform();
	Bivector4D rayLine(transform * ray->origin, transform * ray->direction);

	const Box3D& box = gizmoBox;
	Vector3D size = box.GetSize();
	float width = Fmin(size.x, size.y, size.z) * 0.0625F;

	for (machine a = 0; a < 4; a++)
	{
		float y = box[a & 1].y;
		float z = box[(a >> 1) & 1].z;

		Bivector4D edgeLine(Point3D(box.min.x, y, z), Point3D(box.max.x, y, z));
		Antivector3D normal = rayLine.GetTangent() % edgeLine.GetTangent();

		float t = SquaredMag(normal);
		if (t > K::min_float)
		{
			float d = Fabs((rayLine ^ edgeLine) * InverseSqrt(t));
			if (d < width)
			{
				Antivector4D plane = edgeLine ^ normal;
				Vector4D p = rayLine ^ plane;
				if (Fabs(p.w) > K::min_float)
				{
					float w = 1.0F / p.w;
					float x = p.x * w;
					if ((x > box.min.x) && (x < box.max.x))
					{
						if (point)
						{
							*point = GetTransformable()->GetWorldTransform() * Point3D(x, p.y * w, p.z * w);
						}

						return (a);
					}
				}
			}
		}
	}

	for (machine a = 0; a < 4; a++)
	{
		float x = box[a & 1].x;
		float z = box[(a >> 1) & 1].z;

		Bivector4D edgeLine(Point3D(x, box.min.y, z), Point3D(x, box.max.y, z));
		Antivector3D normal = rayLine.GetTangent() % edgeLine.GetTangent();

		float t = SquaredMag(normal);
		if (t > K::min_float)
		{
			float d = Fabs((rayLine ^ edgeLine) * InverseSqrt(t));
			if (d < width)
			{
				Antivector4D plane = edgeLine ^ normal;
				Vector4D p = rayLine ^ plane;
				if (Fabs(p.w) > K::min_float)
				{
					float w = 1.0F / p.w;
					float y = p.y * w;
					if ((y > box.min.y) && (y < box.max.y))
					{
						if (point)
						{
							*point = GetTransformable()->GetWorldTransform() * Point3D(p.x * w, y, p.z * w);
						}

						return (a + 4);
					}
				}
			}
		}
	}

	for (machine a = 0; a < 4; a++)
	{
		float x = box[a & 1].x;
		float y = box[(a >> 1) & 1].y;

		Bivector4D edgeLine(Point3D(x, y, box.min.z), Point3D(x, y, box.max.z));
		Antivector3D normal = rayLine.GetTangent() % edgeLine.GetTangent();

		float t = SquaredMag(normal);
		if (t > K::min_float)
		{
			float d = Fabs((rayLine ^ edgeLine) * InverseSqrt(t));
			if (d < width)
			{
				Antivector4D plane = edgeLine ^ normal;
				Vector4D p = rayLine ^ plane;
				if (Fabs(p.w) > K::min_float)
				{
					float w = 1.0F / p.w;
					float z = p.z * w;
					if ((z > box.min.z) && (z < box.max.z))
					{
						if (point)
						{
							*point = GetTransformable()->GetWorldTransform() * Point3D(p.x * w, p.y * w, z);
						}

						return (a + 8);
					}
				}
			}
		}
	}

	return (-1);
}

void EditorGizmo::Render(const ManipulatorRenderData *renderData)
{
	float scale = renderData->viewportScale;
	gizmoScaleVector.Set(scale, scale, scale, scale);

	Vector3D viewDirection = GetTransformable()->GetInverseWorldTransform() * renderData->viewportCamera->GetNodeTransform()[2];

	List<Renderable> *renderList = renderData->gizmoList;
	if (renderList)
	{
		if (Fabs(viewDirection.x) > kGizmoThreshold)
		{
			gizmoRenderable.SetVertexCount(16);
			gizmoRenderable.SetVertexAttributeArray(kArrayPosition, sizeof(GizmoVertex) * 8, 3);
			gizmoRenderable.SetVertexAttributeArray(kArrayColor, sizeof(GizmoVertex) * 8 + sizeof(Point3D), 1);
			gizmoRenderable.SetVertexAttributeArray(kArrayTangent, sizeof(GizmoVertex) * 8 + sizeof(Point3D) + sizeof(Color4C), 4);
			gizmoRenderable.SetVertexAttributeArray(kArrayTexcoord, sizeof(GizmoVertex) * 8 + sizeof(Point3D) + sizeof(Color4C) + sizeof(Vector4D), 2);
		}
		else if (Fabs(viewDirection.y) > kGizmoThreshold)
		{
			gizmoRenderable.SetVertexCount(16);
			gizmoRenderable.SetVertexAttributeArray(kArrayPosition, sizeof(GizmoVertex) * 16, 3);
			gizmoRenderable.SetVertexAttributeArray(kArrayColor, sizeof(GizmoVertex) * 16 + sizeof(Point3D), 1);
			gizmoRenderable.SetVertexAttributeArray(kArrayTangent, sizeof(GizmoVertex) * 16 + sizeof(Point3D) + sizeof(Color4C), 4);
			gizmoRenderable.SetVertexAttributeArray(kArrayTexcoord, sizeof(GizmoVertex) * 16 + sizeof(Point3D) + sizeof(Color4C) + sizeof(Vector4D), 2);
		}
		else
		{
			gizmoRenderable.SetVertexCount((Fabs(viewDirection.z) > kGizmoThreshold) ? 16 : 24);
			gizmoRenderable.SetVertexAttributeArray(kArrayPosition, 0, 3);
			gizmoRenderable.SetVertexAttributeArray(kArrayColor, sizeof(Point3D), 1);
			gizmoRenderable.SetVertexAttributeArray(kArrayTangent, sizeof(Point3D) + sizeof(Color4C), 4);
			gizmoRenderable.SetVertexAttributeArray(kArrayTexcoord, sizeof(Point3D) + sizeof(Color4C) + sizeof(Vector4D), 2);
		}

		gizmoVertexBuffer.UpdateBuffer(0, sizeof(GizmoVertex) * 32, gizmoVertex);
		renderList->Append(&gizmoRenderable);

		if (renderData->viewportType == kEditorViewportFrustum)
		{
			RenderBox(renderData);
		}
	}

	renderList = renderData->handleList;
	if (renderList)
	{
		scale *= 24.0F;

		handleVertex[0].position.Set(0.0F, scale, scale);
		handleVertex[1].position.Set(0.0F, scale, scale);
		handleVertex[2].position.Set(0.0F, scale, scale);
		handleVertex[3].position.Set(0.0F, scale, scale);
		handleVertex[4].position.Set(scale, 0.0F, scale);
		handleVertex[5].position.Set(scale, 0.0F, scale);
		handleVertex[6].position.Set(scale, 0.0F, scale);
		handleVertex[7].position.Set(scale, 0.0F, scale);
		handleVertex[8].position.Set(scale, scale, 0.0F);
		handleVertex[9].position.Set(scale, scale, 0.0F);
		handleVertex[10].position.Set(scale, scale, 0.0F);
		handleVertex[11].position.Set(scale, scale, 0.0F);

		handleVertexBuffer.UpdateBuffer(0, sizeof(HandleVertex) * 12, handleVertex);
		renderList->Append(&handleRenderable);
	}
}

void EditorGizmo::RenderBox(const ManipulatorRenderData *renderData)
{
	float	scale[8];

	Box3D& box = gizmoBox;
	box = gizmoManipulator->CalculateNodeBoundingBox();
	EditorManipulator::AdjustBoundingBox(&box);

	Transform4D transform = renderData->viewportCamera->GetInverseWorldTransform() * gizmoManipulator->GetTargetNode()->GetWorldTransform();
	const MatrixRow4D& row = transform.GetRow(2);

	scale[0] = row ^ box.min;
	scale[1] = row ^ Point3D(box.max.x, box.min.y, box.min.z);
	scale[2] = row ^ Point3D(box.min.x, box.max.y, box.min.z);
	scale[3] = row ^ Point3D(box.max.x, box.max.y, box.min.z);
	scale[4] = row ^ Point3D(box.min.x, box.min.y, box.max.z);
	scale[5] = row ^ Point3D(box.max.x, box.min.y, box.max.z);
	scale[6] = row ^ Point3D(box.min.x, box.max.y, box.max.z);
	scale[7] = row ^ box.max;

	float t = renderData->viewportScale * 0.125F;
	for (machine a = 0; a < 8; a++)
	{
		scale[a] = Fmax(scale[a], 0.5F) * t;
	}

	boxVertex[0].position.Set(box.min.x, box.min.y, box.min.z);
	boxVertex[1].position.Set(box.min.x, box.min.y, box.min.z);
	boxVertex[2].position.Set(box.max.x, box.min.y, box.min.z);
	boxVertex[3].position.Set(box.max.x, box.min.y, box.min.z);

	boxVertex[4].position.Set(box.min.x, box.max.y, box.min.z);
	boxVertex[5].position.Set(box.min.x, box.max.y, box.min.z);
	boxVertex[6].position.Set(box.max.x, box.max.y, box.min.z);
	boxVertex[7].position.Set(box.max.x, box.max.y, box.min.z);

	boxVertex[8].position.Set(box.min.x, box.min.y, box.max.z);
	boxVertex[9].position.Set(box.min.x, box.min.y, box.max.z);
	boxVertex[10].position.Set(box.max.x, box.min.y, box.max.z);
	boxVertex[11].position.Set(box.max.x, box.min.y, box.max.z);

	boxVertex[12].position.Set(box.min.x, box.max.y, box.max.z);
	boxVertex[13].position.Set(box.min.x, box.max.y, box.max.z);
	boxVertex[14].position.Set(box.max.x, box.max.y, box.max.z);
	boxVertex[15].position.Set(box.max.x, box.max.y, box.max.z);

	boxVertex[16].position.Set(box.min.x, box.min.y, box.min.z);
	boxVertex[17].position.Set(box.min.x, box.min.y, box.min.z);
	boxVertex[18].position.Set(box.min.x, box.max.y, box.min.z);
	boxVertex[19].position.Set(box.min.x, box.max.y, box.min.z);

	boxVertex[20].position.Set(box.max.x, box.min.y, box.min.z);
	boxVertex[21].position.Set(box.max.x, box.min.y, box.min.z);
	boxVertex[22].position.Set(box.max.x, box.max.y, box.min.z);
	boxVertex[23].position.Set(box.max.x, box.max.y, box.min.z);

	boxVertex[24].position.Set(box.min.x, box.min.y, box.max.z);
	boxVertex[25].position.Set(box.min.x, box.min.y, box.max.z);
	boxVertex[26].position.Set(box.min.x, box.max.y, box.max.z);
	boxVertex[27].position.Set(box.min.x, box.max.y, box.max.z);

	boxVertex[28].position.Set(box.max.x, box.min.y, box.max.z);
	boxVertex[29].position.Set(box.max.x, box.min.y, box.max.z);
	boxVertex[30].position.Set(box.max.x, box.max.y, box.max.z);
	boxVertex[31].position.Set(box.max.x, box.max.y, box.max.z);

	boxVertex[32].position.Set(box.min.x, box.min.y, box.min.z);
	boxVertex[33].position.Set(box.min.x, box.min.y, box.min.z);
	boxVertex[34].position.Set(box.min.x, box.min.y, box.max.z);
	boxVertex[35].position.Set(box.min.x, box.min.y, box.max.z);

	boxVertex[36].position.Set(box.max.x, box.min.y, box.min.z);
	boxVertex[37].position.Set(box.max.x, box.min.y, box.min.z);
	boxVertex[38].position.Set(box.max.x, box.min.y, box.max.z);
	boxVertex[39].position.Set(box.max.x, box.min.y, box.max.z);

	boxVertex[40].position.Set(box.min.x, box.max.y, box.min.z);
	boxVertex[41].position.Set(box.min.x, box.max.y, box.min.z);
	boxVertex[42].position.Set(box.min.x, box.max.y, box.max.z);
	boxVertex[43].position.Set(box.min.x, box.max.y, box.max.z);

	boxVertex[44].position.Set(box.max.x, box.max.y, box.min.z);
	boxVertex[45].position.Set(box.max.x, box.max.y, box.min.z);
	boxVertex[46].position.Set(box.max.x, box.max.y, box.max.z);
	boxVertex[47].position.Set(box.max.x, box.max.y, box.max.z);

	boxVertex[0].tangent.Set(1.0F, 0.0F, 0.0F, -scale[0]);
	boxVertex[1].tangent.Set(1.0F, 0.0F, 0.0F, scale[0]);
	boxVertex[2].tangent.Set(1.0F, 0.0F, 0.0F, scale[1]);
	boxVertex[3].tangent.Set(1.0F, 0.0F, 0.0F, -scale[1]);

	boxVertex[4].tangent.Set(1.0F, 0.0F, 0.0F, -scale[2]);
	boxVertex[5].tangent.Set(1.0F, 0.0F, 0.0F, scale[2]);
	boxVertex[6].tangent.Set(1.0F, 0.0F, 0.0F, scale[3]);
	boxVertex[7].tangent.Set(1.0F, 0.0F, 0.0F, -scale[3]);

	boxVertex[8].tangent.Set(1.0F, 0.0F, 0.0F, -scale[4]);
	boxVertex[9].tangent.Set(1.0F, 0.0F, 0.0F, scale[4]);
	boxVertex[10].tangent.Set(1.0F, 0.0F, 0.0F, scale[5]);
	boxVertex[11].tangent.Set(1.0F, 0.0F, 0.0F, -scale[5]);

	boxVertex[12].tangent.Set(1.0F, 0.0F, 0.0F, -scale[6]);
	boxVertex[13].tangent.Set(1.0F, 0.0F, 0.0F, scale[6]);
	boxVertex[14].tangent.Set(1.0F, 0.0F, 0.0F, scale[7]);
	boxVertex[15].tangent.Set(1.0F, 0.0F, 0.0F, -scale[7]);

	boxVertex[16].tangent.Set(0.0F, 1.0F, 0.0F, -scale[0]);
	boxVertex[17].tangent.Set(0.0F, 1.0F, 0.0F, scale[0]);
	boxVertex[18].tangent.Set(0.0F, 1.0F, 0.0F, scale[2]);
	boxVertex[19].tangent.Set(0.0F, 1.0F, 0.0F, -scale[2]);

	boxVertex[20].tangent.Set(0.0F, 1.0F, 0.0F, -scale[1]);
	boxVertex[21].tangent.Set(0.0F, 1.0F, 0.0F, scale[1]);
	boxVertex[22].tangent.Set(0.0F, 1.0F, 0.0F, scale[3]);
	boxVertex[23].tangent.Set(0.0F, 1.0F, 0.0F, -scale[3]);

	boxVertex[24].tangent.Set(0.0F, 1.0F, 0.0F, -scale[4]);
	boxVertex[25].tangent.Set(0.0F, 1.0F, 0.0F, scale[4]);
	boxVertex[26].tangent.Set(0.0F, 1.0F, 0.0F, scale[6]);
	boxVertex[27].tangent.Set(0.0F, 1.0F, 0.0F, -scale[6]);

	boxVertex[28].tangent.Set(0.0F, 1.0F, 0.0F, -scale[5]);
	boxVertex[29].tangent.Set(0.0F, 1.0F, 0.0F, scale[5]);
	boxVertex[30].tangent.Set(0.0F, 1.0F, 0.0F, scale[7]);
	boxVertex[31].tangent.Set(0.0F, 1.0F, 0.0F, -scale[7]);

	boxVertex[32].tangent.Set(0.0F, 0.0F, 1.0F, -scale[0]);
	boxVertex[33].tangent.Set(0.0F, 0.0F, 1.0F, scale[0]);
	boxVertex[34].tangent.Set(0.0F, 0.0F, 1.0F, scale[4]);
	boxVertex[35].tangent.Set(0.0F, 0.0F, 1.0F, -scale[4]);

	boxVertex[36].tangent.Set(0.0F, 0.0F, 1.0F, -scale[1]);
	boxVertex[37].tangent.Set(0.0F, 0.0F, 1.0F, scale[1]);
	boxVertex[38].tangent.Set(0.0F, 0.0F, 1.0F, scale[5]);
	boxVertex[39].tangent.Set(0.0F, 0.0F, 1.0F, -scale[5]);

	boxVertex[40].tangent.Set(0.0F, 0.0F, 1.0F, -scale[2]);
	boxVertex[41].tangent.Set(0.0F, 0.0F, 1.0F, scale[2]);
	boxVertex[42].tangent.Set(0.0F, 0.0F, 1.0F, scale[6]);
	boxVertex[43].tangent.Set(0.0F, 0.0F, 1.0F, -scale[6]);

	boxVertex[44].tangent.Set(0.0F, 0.0F, 1.0F, -scale[3]);
	boxVertex[45].tangent.Set(0.0F, 0.0F, 1.0F, scale[3]);
	boxVertex[46].tangent.Set(0.0F, 0.0F, 1.0F, scale[7]);
	boxVertex[47].tangent.Set(0.0F, 0.0F, 1.0F, -scale[7]);

	boxVertexBuffer.UpdateBuffer(0, sizeof(BoxVertex) * 48, boxVertex);

	faceVertex[0].position.Set(box.max.x, box.min.y, box.min.z);
	faceVertex[1].position.Set(box.max.x, box.max.y, box.min.z);
	faceVertex[2].position.Set(box.max.x, box.max.y, box.max.z);
	faceVertex[3].position.Set(box.max.x, box.min.y, box.max.z);

	faceVertex[4].position.Set(box.min.x, box.max.y, box.min.z);
	faceVertex[5].position.Set(box.min.x, box.min.y, box.min.z);
	faceVertex[6].position.Set(box.min.x, box.min.y, box.max.z);
	faceVertex[7].position.Set(box.min.x, box.max.y, box.max.z);

	faceVertex[8].position.Set(box.max.x, box.max.y, box.min.z);
	faceVertex[9].position.Set(box.min.x, box.max.y, box.min.z);
	faceVertex[10].position.Set(box.min.x, box.max.y, box.max.z);
	faceVertex[11].position.Set(box.max.x, box.max.y, box.max.z);

	faceVertex[12].position.Set(box.min.x, box.min.y, box.min.z);
	faceVertex[13].position.Set(box.max.x, box.min.y, box.min.z);
	faceVertex[14].position.Set(box.max.x, box.min.y, box.max.z);
	faceVertex[15].position.Set(box.min.x, box.min.y, box.max.z);

	faceVertex[16].position.Set(box.min.x, box.min.y, box.max.z);
	faceVertex[17].position.Set(box.max.x, box.min.y, box.max.z);
	faceVertex[18].position.Set(box.max.x, box.max.y, box.max.z);
	faceVertex[19].position.Set(box.min.x, box.max.y, box.max.z);

	faceVertex[20].position.Set(box.min.x, box.max.y, box.min.z);
	faceVertex[21].position.Set(box.max.x, box.max.y, box.min.z);
	faceVertex[22].position.Set(box.max.x, box.min.y, box.min.z);
	faceVertex[23].position.Set(box.min.x, box.min.y, box.min.z);

	faceVertexBuffer.UpdateBuffer(0, sizeof(FaceVertex) * 24, faceVertex);

	renderData->gizmoList->Append(&boxRenderable);
	renderData->gizmoList->Append(&faceRenderable);

	int32 index = hiliteEdgeIndex;
	if (index >= 0)
	{
		if (index < 4)
		{
			float y = box[index & 1].y;
			float z = box[(index >> 1) & 1].z;
			edgeVertex[0].position.Set(box.min.x, y, z);
			edgeVertex[1].position.Set(box.min.x, y, z);
			edgeVertex[2].position.Set(box.max.x, y, z);
			edgeVertex[3].position.Set(box.max.x, y, z);

			index *= 2;
			float s1 = scale[index];
			float s2 = scale[index + 1];
			edgeVertex[0].tangent.Set(1.0F, 0.0F, 0.0F, -s1 * 5.0F);
			edgeVertex[1].tangent.Set(1.0F, 0.0F, 0.0F, s1 * 5.0F);
			edgeVertex[2].tangent.Set(1.0F, 0.0F, 0.0F, s2 * 5.0F);
			edgeVertex[3].tangent.Set(1.0F, 0.0F, 0.0F, -s2 * 5.0F);
		}
		else if (index < 8)
		{
			index -= 4;
			float x = box[index & 1].x;
			float z = box[(index >> 1) & 1].z;
			edgeVertex[0].position.Set(x, box.min.y, z);
			edgeVertex[1].position.Set(x, box.min.y, z);
			edgeVertex[2].position.Set(x, box.max.y, z);
			edgeVertex[3].position.Set(x, box.max.y, z);

			index += index & 2;
			float s1 = scale[index];
			float s2 = scale[index + 2];
			edgeVertex[0].tangent.Set(0.0F, 1.0F, 0.0F, -s1 * 5.0F);
			edgeVertex[1].tangent.Set(0.0F, 1.0F, 0.0F, s1 * 5.0F);
			edgeVertex[2].tangent.Set(0.0F, 1.0F, 0.0F, s2 * 5.0F);
			edgeVertex[3].tangent.Set(0.0F, 1.0F, 0.0F, -s2 * 5.0F);
		}
		else
		{
			index -= 8;
			float x = box[index & 1].x;
			float y = box[(index >> 1) & 1].y;
			edgeVertex[0].position.Set(x, y, box.min.z);
			edgeVertex[1].position.Set(x, y, box.min.z);
			edgeVertex[2].position.Set(x, y, box.max.z);
			edgeVertex[3].position.Set(x, y, box.max.z);

			float s1 = scale[index];
			float s2 = scale[index + 4];
			edgeVertex[0].tangent.Set(0.0F, 0.0F, 1.0F, -s1 * 5.0F);
			edgeVertex[1].tangent.Set(0.0F, 0.0F, 1.0F, s1 * 5.0F);
			edgeVertex[2].tangent.Set(0.0F, 0.0F, 1.0F, s2 * 5.0F);
			edgeVertex[3].tangent.Set(0.0F, 0.0F, 1.0F, -s2 * 5.0F);
		}

		edgeVertexBuffer.UpdateBuffer(0, sizeof(BoxVertex) * 4, edgeVertex);
		renderData->gizmoList->Append(&edgeRenderable);
	}
}

// ZYUTNLM
