//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This file is part of the C4 Engine and is provided under the
// terms of the license agreement entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4EditorGizmo_h
#define C4EditorGizmo_h


#include "C4EditorBase.h"


namespace C4
{
	class EditorManipulator;


	class EditorGizmo
	{
		private:

			struct GizmoVertex
			{
				Point3D		position;
				Color4C		color;
				Vector4D	tangent;
				Point2D		texcoord;
			};

			struct BoxVertex
			{
				Point3D		position;
				Vector4D	tangent;
				Point2D		texcoord;
			};

			struct FaceVertex
			{
				Point3D		position;
				Color4C		color;
			};

			struct HandleVertex
			{
				Point3D		position;
				Color4C		color;
				Vector2D	billboard;
			};

			const EditorManipulator			*gizmoManipulator;

			Box3D							gizmoBox;
			Vector4D						gizmoScaleVector;

			int32							hiliteEdgeIndex;

			VertexBuffer					gizmoVertexBuffer;
			List<Attribute>					gizmoAttributeList;
			TextureMapAttribute				gizmoTextureMap;
			Renderable						gizmoRenderable;
			GizmoVertex						gizmoVertex[32];

			VertexBuffer					boxVertexBuffer;
			List<Attribute>					boxAttributeList;
			DiffuseAttribute				boxDiffuseColor;
			TextureMapAttribute				boxTextureMap;
			Renderable						boxRenderable;
			BoxVertex						boxVertex[48];

			VertexBuffer					faceVertexBuffer;
			Renderable						faceRenderable;
			FaceVertex						faceVertex[24];

			VertexBuffer					edgeVertexBuffer;
			List<Attribute>					edgeAttributeList;
			DiffuseAttribute				edgeDiffuseColor;
			TextureMapAttribute				edgeTextureMap;
			Renderable						edgeRenderable;
			BoxVertex						edgeVertex[4];

			VertexBuffer					handleVertexBuffer;
			Renderable						handleRenderable;
			HandleVertex					handleVertex[12];

			void UpdateColor(void);

			void RenderBox(const ManipulatorRenderData *renderData);

		public:

			EditorGizmo(const EditorManipulator *manipulator);
			~EditorGizmo();

			const Box3D& GetGizmoBox(void) const
			{
				return (gizmoBox);
			}

			const Transformable *GetTransformable(void) const
			{
				return (gizmoRenderable.GetTransformable());
			}

			C4EDITORAPI void HiliteMovers(unsigned_int32 mask);
			C4EDITORAPI void HiliteRotators(unsigned_int32 mask);
 
			C4EDITORAPI int32 PickMover(const ManipulatorViewportData *viewportData, const Ray *ray) const;
			C4EDITORAPI int32 PickRotator(const ManipulatorViewportData *viewportData, const Ray *ray) const;
 
			C4EDITORAPI void HiliteFace(int32 face, float intensity = 1.0F);
			C4EDITORAPI void HiliteEdge(int32 edge, float intensity = 1.0F); 
			C4EDITORAPI int32 PickFace(const Ray *ray, Point3D *point = nullptr) const; 
			C4EDITORAPI int32 PickEdge(const Ray *ray, Point3D *point = nullptr) const;

			C4EDITORAPI void Render(const ManipulatorRenderData *renderData);
	}; 
}


#endif

// ZYUTNLM
