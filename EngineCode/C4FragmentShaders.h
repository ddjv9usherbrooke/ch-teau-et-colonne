//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Universit� de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4FragmentShaders_h
#define C4FragmentShaders_h


#include "C4Programs.h"


namespace C4
{
	enum
	{
		kFragmentParamConstant0						= 0,
		kFragmentParamConstant1						= 1,
		kFragmentParamConstant2						= 2,
		kFragmentParamConstant3						= 3,
		kFragmentParamConstant4						= 4,
		kFragmentParamConstant5						= 5,
		kFragmentParamConstant6						= 6,
		kFragmentParamConstant7						= 7,

		kFragmentParamLightColor					= 8,
		kFragmentParamFogColor						= 9,
		kFragmentParamFogParams						= 10,		// Fog params (F ∧ C, -(F ∧ C)², F ∧ C <= 0, density / ln 2)

		kFragmentParamShaderTime					= 11,		// (normalized absolute time, 0.0, 0.0, 0.0)
		kFragmentParamDetailLevel					= 12,		// (detail level parameter, 0.0, 0.0, 0.0)
		kFragmentParamParallaxScale					= 13,		// Parallax scale (s scale, t scale, 0.0, 0.0)

		kFragmentParamVelocityScale					= 14,		// Velocity scale (x scale, y scale, 0.0, 0.0)
		kFragmentParamDistortionScale				= 15,		// Distortion scale (x scale, y scale, 0.0, 0.0)

		kFragmentParamShadowSample1					= 16,		// Shadow sample offsets (ds1, dt1, ds2, dt2)
		kFragmentParamShadowSample2					= 17,		// Shadow sample offsets (ds3, dt3, ds4, dt4)
		kFragmentParamShadowMapScale1				= 18,		// Shadow map (s, t, p) texcoord scale, section 1
		kFragmentParamShadowMapScale2				= 19,		// Shadow map (s, t, p) texcoord scale, section 2
		kFragmentParamShadowMapScale3				= 20,		// Shadow map (s, t, p) texcoord scale, section 3
		kFragmentParamShadowMapOffset1				= 21,		// Shadow map (s, t, p) texcoord offset, section 1
		kFragmentParamShadowMapOffset2				= 22,		// Shadow map (s, t, p) texcoord offset, section 2
		kFragmentParamShadowMapOffset3				= 23,		// Shadow map (s, t, p) texcoord offset, section 3
		kFragmentParamShadowViewDirection			= 24,		// Shadow-space scaled view direction, section 0

		kFragmentParamImpostorShadowBlend			= 25,		// Impostor shadow map elevation blend
		kFragmentParamImpostorShadowScale			= 26,		// Impostor shadow map elevation scales
		kFragmentParamImpostorDistance				= 27		// (impostor distance, 0.0, 0.0, 0.0)

		// Render::kMaxFragmentParamCount
	};


	#define FRAGMENT_PARAM_CONSTANT0				"0"
	#define FRAGMENT_PARAM_CONSTANT1				"1"
	#define FRAGMENT_PARAM_CONSTANT2				"2"
	#define FRAGMENT_PARAM_CONSTANT3				"3"
	#define FRAGMENT_PARAM_CONSTANT4				"4"
	#define FRAGMENT_PARAM_CONSTANT5				"5"
	#define FRAGMENT_PARAM_CONSTANT6				"6"
	#define FRAGMENT_PARAM_CONSTANT7				"7"

	#define FRAGMENT_PARAM_LIGHT_COLOR				"8"
	#define FRAGMENT_PARAM_FOG_COLOR				"9"
	#define FRAGMENT_PARAM_FOG_PARAMS				"10"

	#define FRAGMENT_PARAM_SHADER_TIME				"11"
	#define FRAGMENT_PARAM_DETAIL_LEVEL				"12"
	#define FRAGMENT_PARAM_PARALLAX_SCALE			"13"

	#define FRAGMENT_PARAM_VELOCITY_SCALE			"14"
	#define FRAGMENT_PARAM_DISTORTION_SCALE			"15"

	#define FRAGMENT_PARAM_SHADOW_SAMPLE1			"16"
	#define FRAGMENT_PARAM_SHADOW_SAMPLE2			"17"
	#define FRAGMENT_PARAM_SHADOW_MAP_SCALE1		"18"
	#define FRAGMENT_PARAM_SHADOW_MAP_SCALE2		"19"
	#define FRAGMENT_PARAM_SHADOW_MAP_SCALE3		"20"
	#define FRAGMENT_PARAM_SHADOW_MAP_OFFSET1		"21"
	#define FRAGMENT_PARAM_SHADOW_MAP_OFFSET2		"22"
	#define FRAGMENT_PARAM_SHADOW_MAP_OFFSET3		"23"
	#define FRAGMENT_PARAM_SHADOW_VIEW_DIRECTION	"24"

	#define FRAGMENT_PARAM_IMPOSTOR_SHADOW_BLEND	"25"
	#define FRAGMENT_PARAM_IMPOSTOR_SHADOW_SCALE	"26"
	#define FRAGMENT_PARAM_IMPOSTOR_DISTANCE		"27"

	#define FRAGMENT_PARAM_COUNT					"28"


	class FragmentShader : public Render::FragmentShaderObject, public Shared, public HashTableElement<FragmentShader>
	{
		public:

			typedef ShaderSignature KeyType;

		private:

			static Storage<HashTable<FragmentShader>>		hashTable;

			#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

			#endif //] 

			unsigned_int32		shaderSignature[1];
 
			FragmentShader(const char *source, unsigned_int32 size, const unsigned_int32 *signature);
 
			#if C4ORBIS || C4PS3 //[  

			// -- PS3 code hidden --

			#endif //] 

			~FragmentShader();

		public:
 
			static const char		prolog1Text[];
			static const char		prolog2Text[];
			static const char		epilogText[];

			static const char		copyZero[];
			static const char		copyConstant[];

			KeyType GetKey(void) const
			{
				return (ShaderSignature(shaderSignature));
			}

			static void Initialize(void);
			static void Terminate(void);

			static unsigned_int32 Hash(const KeyType& key);

			static FragmentShader *Find(const unsigned_int32 *signature);
			static FragmentShader *New(const char *source, unsigned_int32 size, const unsigned_int32 *signature);

			static void ReleaseCache(void);

			#if C4ORBIS //[ 

			// -- Orbis code hidden --

			#elif C4PS3 //[ 

			// -- PS3 code hidden --

			#endif //]
	};
}


#endif

// ZYUTNLM
