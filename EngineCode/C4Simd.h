//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4Simd_h
#define C4Simd_h


#if C4WINDOWS && C4FASTBUILD

	typedef union __declspec(intrin_type) __declspec(align(16)) __m128
	{
		float				m128_f32[4];
		unsigned __int64	m128_u64[2];
		__int8				m128_i8[16];
		__int16				m128_i16[8];
		__int32				m128_i32[4];
		__int64				m128_i64[2];
		unsigned __int8		m128_u8[16];
		unsigned __int16	m128_u16[8];
		unsigned __int32	m128_u32[4];
	} __m128;

	typedef union __declspec(intrin_type) __declspec(align(16)) __m128i
	{
		__int8				m128i_i8[16];
		__int16				m128i_i16[8];
		__int32				m128i_i32[4];
		__int64				m128i_i64[2];
		unsigned __int8		m128i_u8[16];
		unsigned __int16	m128i_u16[8];
		unsigned __int32	m128i_u32[4];
		unsigned __int64	m128i_u64[2];
	} __m128i;

	extern "C"
	{
		extern __m128 _mm_add_ss(__m128, __m128);
		extern __m128 _mm_add_ps(__m128, __m128);
		extern __m128 _mm_sub_ss(__m128, __m128);
		extern __m128 _mm_sub_ps(__m128, __m128);
		extern __m128 _mm_mul_ss(__m128, __m128);
		extern __m128 _mm_mul_ps(__m128, __m128);
		extern __m128 _mm_div_ss(__m128, __m128);
		extern __m128 _mm_div_ps(__m128, __m128);
		extern __m128 _mm_rsqrt_ss(__m128);
		extern __m128 _mm_rsqrt_ps(__m128);
		extern __m128 _mm_min_ss(__m128, __m128);
		extern __m128 _mm_min_ps(__m128, __m128);
		extern __m128 _mm_max_ss(__m128, __m128);
		extern __m128 _mm_max_ps(__m128, __m128);
		extern __m128 _mm_and_ps(__m128, __m128);
		extern __m128 _mm_andnot_ps(__m128, __m128);
		extern __m128 _mm_or_ps(__m128, __m128);
		extern __m128 _mm_xor_ps(__m128, __m128);
		extern __m128 _mm_cmpeq_ps(__m128, __m128);
		extern __m128 _mm_cmplt_ps(__m128, __m128);
		extern int _mm_comieq_ss(__m128, __m128);
		extern int _mm_comilt_ss(__m128, __m128);
		extern int _mm_comigt_ss(__m128, __m128);
		extern int _mm_cvtt_ss2si(__m128);
		extern __m128 _mm_shuffle_ps(__m128, __m128, unsigned int);
		extern __m128 _mm_setzero_ps(void);
		extern __m128 _mm_load_ss(const float *);
		extern __m128 _mm_load_ps(const float *);
		extern __m128 _mm_loadu_ps(const float *);
		extern void _mm_store_ss(float *, __m128);
		extern void _mm_store_ps(float *, __m128);
		extern unsigned int _mm_getcsr(void);
		extern void _mm_setcsr(unsigned int);

		extern __m128i _mm_setzero_si128(void);
		extern __m128i _mm_load_si128(const __m128i *);
		extern __m128i _mm_loadu_si128(const __m128i *);
		extern int _mm_cvtsi128_si32(__m128i);
		extern __m128i _mm_shuffle_epi32(__m128i, int);
		extern __m128i _mm_shufflehi_epi16(__m128i, int);
		extern __m128i _mm_shufflelo_epi16(__m128i, int);
		extern __m128i _mm_packs_epi16(__m128i, __m128i);
		extern __m128i _mm_packs_epi32(__m128i, __m128i);
		extern __m128i _mm_packus_epi16(__m128i, __m128i);
		extern __m128i _mm_unpacklo_epi8(__m128i, __m128i);
		extern __m128i _mm_unpackhi_epi8(__m128i, __m128i);
		extern __m128i _mm_unpacklo_epi16(__m128i, __m128i);
		extern __m128i _mm_unpackhi_epi16(__m128i, __m128i);
		extern __m128i _mm_cmplt_epi8(__m128i, __m128i);
		extern __m128i _mm_cmplt_epi16(__m128i, __m128i);
		extern __m128 _mm_cvtepi32_ps(__m128i);
		extern __m128i _mm_cvtps_epi32(__m128);
		extern __m128i _mm_add_epi32(__m128i, __m128i);
		extern __m128i _mm_sub_epi32(__m128i, __m128i);
	}

	#define _MM_SHUFFLE(fp3,fp2,fp1,fp0) (((fp3) << 6) | ((fp2) << 4) | ((fp1) << 2) | ((fp0)))

#endif


namespace C4
{
	#if C4INTEL

		typedef __m128 vec_float;
		typedef __m128i vec_int8;
		typedef __m128i vec_int16; 
		typedef __m128i vec_int32;
		typedef __m128i vec_unsigned_int8;
		typedef __m128i vec_unsigned_int16; 
		typedef __m128i vec_unsigned_int32;
 
	#elif C4POWERPC 

		typedef vector float vec_float;
		typedef vector signed char vec_int8;
		typedef vector signed short vec_int16; 
		typedef vector signed int vec_int32;
		typedef vector unsigned char vec_unsigned_int8;
		typedef vector unsigned short vec_unsigned_int16;
		typedef vector unsigned int vec_unsigned_int32;
 
	#endif


	inline vec_float SimdGetFloatZero(void)
	{
		#if C4INTEL

			return (_mm_setzero_ps());

		#elif C4POWERPC

			return ((vec_float) vec_splat_s32(0));

		#endif
	}

	inline vec_float SimdGetNegativeZero(void)
	{
		#if C4INTEL

			alignas(16) static const unsigned_int32 float_80000000[4] = {0x80000000, 0x80000000, 0x80000000, 0x80000000};
			return (_mm_load_ps(reinterpret_cast<const float *>(float_80000000)));

		#elif C4POWERPC

			return ((vec_float) vec_rl(vec_splat_u32(1), vec_splat_u32(-1)));

		#endif
	}

	template <unsigned int value> inline vec_float SimdLoadConstant(void)
	{
		alignas(16) static const unsigned_int32 k[4] = {value, value, value, value};

		#if C4INTEL

			return (_mm_load_ps(reinterpret_cast<const float *>(k)));

		#elif C4POWERPC

			return (vec_ld(0, reinterpret_cast<const float *>(k)));

		#endif
	}

	inline vec_float SimdSmearX(const vec_float& v)
	{
		#if C4INTEL

			return (_mm_shuffle_ps(v, v, _MM_SHUFFLE(0, 0, 0, 0)));

		#elif C4POWERPC

			return (vec_splat(v, 0));

		#endif
	}

	inline vec_float SimdSmearY(const vec_float& v)
	{
		#if C4INTEL

			return (_mm_shuffle_ps(v, v, _MM_SHUFFLE(1, 1, 1, 1)));

		#elif C4POWERPC

			return (vec_splat(v, 1));

		#endif
	}

	inline vec_float SimdSmearZ(const vec_float& v)
	{
		#if C4INTEL

			return (_mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 2, 2, 2)));

		#elif C4POWERPC

			return (vec_splat(v, 2));

		#endif
	}

	inline vec_float SimdSmearW(const vec_float& v)
	{
		#if C4INTEL

			return (_mm_shuffle_ps(v, v, _MM_SHUFFLE(3, 3, 3, 3)));

		#elif C4POWERPC

			return (vec_splat(v, 3));

		#endif
	}

	inline vec_float SimdLoad(const float *ptr, machine offset = 0)
	{
		#if C4INTEL

			return (_mm_load_ps(&ptr[offset]));

		#elif C4POWERPC

			return (vec_ld(offset << 2, ptr));

		#endif
	}

	inline vec_float SimdLoadUnaligned(const float *ptr)
	{
		#if C4INTEL

			return (_mm_loadu_ps(ptr));

		#elif C4POWERPC

			return (vec_perm(vec_ld(0, ptr), vec_ld(16, ptr), vec_lvsl(0, ptr)));

		#endif
	}

	inline vec_float SimdLoadScalar(const float *ptr, machine offset = 0)
	{
		#if C4INTEL

			return (_mm_load_ss(&ptr[offset]));

		#elif C4POWERPC

			offset <<= 2;
			vec_float v = vec_ld(offset, ptr);
			return (vec_perm(v, v, vec_lvsl(offset, ptr)));

		#endif
	}

	inline vec_float SimdLoadSmearScalar(const float *ptr, machine offset = 0)
	{
		#if C4INTEL

			vec_float v = _mm_load_ss(&ptr[offset]);
			return (_mm_shuffle_ps(v, v, _MM_SHUFFLE(0, 0, 0, 0)));

		#elif C4POWERPC

			offset <<= 2;
			vec_float v = vec_ld(offset, ptr);
			return (vec_perm(v, v, (vector unsigned char) vec_splat((vector unsigned int) vec_lvsl(offset, ptr), 0)));

		#endif
	}

	inline void SimdStore(const vec_float& v, float *ptr, machine offset = 0)
	{
		#if C4INTEL

			_mm_store_ps(&ptr[offset], v);

		#elif C4POWERPC

			vec_st(v, offset << 2, ptr);

		#endif
	}

	inline void SimdStoreUnaligned(const vec_float& v, float *ptr)
	{
		#if C4INTEL

			_mm_store_ss(&ptr[0], v);
			_mm_store_ss(&ptr[1], SimdSmearY(v));
			_mm_store_ss(&ptr[2], SimdSmearZ(v));
			_mm_store_ss(&ptr[3], SimdSmearW(v));

		#elif C4POWERPC

			vec_float u = vec_perm(v, v, vec_lvsr(0, ptr));
			vec_ste(u, 0, ptr);
			vec_ste(u, 4, ptr);
			vec_ste(u, 8, ptr);
			vec_ste(u, 12, ptr);

		#endif
	}

	inline void SimdStoreX(const vec_float& v, float *ptr, machine offset = 0)
	{
		#if C4INTEL

			_mm_store_ss(&ptr[offset], v);

		#elif C4POWERPC

			vec_ste(vec_splat(v, 0), offset << 2, ptr);

		#endif
	}

	inline void SimdStoreY(const vec_float& v, float *ptr, machine offset = 0)
	{
		#if C4INTEL

			_mm_store_ss(&ptr[offset], _mm_shuffle_ps(v, v, _MM_SHUFFLE(1, 1, 1, 1)));

		#elif C4POWERPC

			vec_ste(vec_splat(v, 1), offset << 2, ptr);

		#endif
	}

	inline void SimdStoreZ(const vec_float& v, float *ptr, machine offset = 0)
	{
		#if C4INTEL

			_mm_store_ss(&ptr[offset], _mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 2, 2, 2)));

		#elif C4POWERPC

			vec_ste(vec_splat(v, 2), offset << 2, ptr);

		#endif
	}

	inline void SimdStoreW(const vec_float& v, float *ptr, machine offset = 0)
	{
		#if C4INTEL

			_mm_store_ss(&ptr[offset], _mm_shuffle_ps(v, v, _MM_SHUFFLE(3, 3, 3, 3)));

		#elif C4POWERPC

			vec_ste(vec_splat(v, 3), offset << 2, ptr);

		#endif
	}

	inline void SimdStore3D(const vec_float& v, float *ptr)
	{
		#if C4INTEL

			_mm_store_ss(&ptr[0], v);
			_mm_store_ss(&ptr[1], _mm_shuffle_ps(v, v, _MM_SHUFFLE(1, 1, 1, 1)));
			_mm_store_ss(&ptr[2], _mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 2, 2, 2)));

		#elif C4POWERPC

			vec_float u = vec_perm(v, v, vec_lvsr(0, ptr));
			vec_ste(u, 0, ptr);
			vec_ste(u, 4, ptr);
			vec_ste(u, 8, ptr);

		#endif
	}

	inline int32 SimdTruncateConvert(const vec_float& v)
	{
		#if C4INTEL

			return (_mm_cvtt_ss2si(v));

		#elif C4POWERPC

			union
			{
				vector signed int	v;
				int32				i[4];
			} u;

			u.v = vec_cts(v, 0);
			return (u.i[0]);

		#endif
	}

	inline vec_float SimdNegate(const vec_float& v)
	{
		#if C4INTEL

			return (_mm_sub_ps(_mm_setzero_ps(), v));

		#elif C4POWERPC

			return (vec_sub((vec_float) vec_splat_s32(0), v));

		#endif
	}

	inline vec_float SimdMin(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_min_ps(v1, v2));

		#elif C4POWERPC

			return (vec_min(v1, v2));

		#endif
	}

	inline vec_float SimdMinScalar(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_min_ss(v1, v2));

		#elif C4POWERPC

			return (vec_min(v1, v2));

		#endif
	}

	inline vec_float SimdMax(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_max_ps(v1, v2));

		#elif C4POWERPC

			return (vec_max(v1, v2));

		#endif
	}

	inline vec_float SimdMaxScalar(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_max_ss(v1, v2));

		#elif C4POWERPC

			return (vec_max(v1, v2));

		#endif
	}

	inline vec_float SimdAdd(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_add_ps(v1, v2));

		#elif C4POWERPC

			return (vec_add(v1, v2));

		#endif
	}

	inline vec_float SimdAddScalar(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_add_ss(v1, v2));

		#elif C4POWERPC

			return (vec_add(v1, v2));

		#endif
	}

	inline vec_float SimdSub(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_sub_ps(v1, v2));

		#elif C4POWERPC

			return (vec_sub(v1, v2));

		#endif
	}

	inline vec_float SimdSubScalar(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_sub_ss(v1, v2));

		#elif C4POWERPC

			return (vec_sub(v1, v2));

		#endif
	}

	inline vec_float SimdMul(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_mul_ps(v1, v2));

		#elif C4POWERPC

			return (vec_madd(v1, v2, SimdGetFloatZero()));

		#endif
	}

	inline vec_float SimdMulScalar(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_mul_ss(v1, v2));

		#elif C4POWERPC

			return (vec_madd(v1, v2, SimdGetFloatZero()));

		#endif
	}

	inline vec_float SimdMadd(const vec_float& v1, const vec_float& v2, const vec_float& v3)
	{
		#if C4INTEL

			return (_mm_add_ps(_mm_mul_ps(v1, v2), v3));

		#elif C4POWERPC

			return (vec_madd(v1, v2, v3));

		#endif
	}

	inline vec_float SimdMaddScalar(const vec_float& v1, const vec_float& v2, const vec_float& v3)
	{
		#if C4INTEL

			return (_mm_add_ss(_mm_mul_ss(v1, v2), v3));

		#elif C4POWERPC

			return (vec_madd(v1, v2, v3));

		#endif
	}

	inline vec_float SimdNmsub(const vec_float& v1, const vec_float& v2, const vec_float& v3)
	{
		#if C4INTEL

			return (_mm_sub_ps(v3, _mm_mul_ps(v1, v2)));

		#elif C4POWERPC

			return (vec_nmsub(v1, v2, v3));

		#endif
	}

	inline vec_float SimdNmsubScalar(const vec_float& v1, const vec_float& v2, const vec_float& v3)
	{
		#if C4INTEL

			return (_mm_sub_ss(v3, _mm_mul_ss(v1, v2)));

		#elif C4POWERPC

			return (vec_nmsub(v1, v2, v3));

		#endif
	}

	inline vec_float SimdDiv(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_div_ps(v1, v2));

		#elif C4POWERPC

			const vec_float zero = SimdGetFloatZero();
			const vec_float one = SimdLoadConstant<0x3F800000>();

			vec_float f = vec_re(v2);
			f = vec_madd(vec_nmsub(f, v2, one), f, f);
			return (vec_madd(v1, f, zero));

		#endif
	}

	inline vec_float SimdDivScalar(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_div_ss(v1, v2));

		#elif C4POWERPC

			const vec_float zero = SimdGetFloatZero();
			const vec_float one = SimdLoadConstant<0x3F800000>();

			vec_float f = vec_re(v2);
			f = vec_madd(vec_nmsub(f, v2, one), f, f);
			return (vec_madd(v1, f, zero));

		#endif
	}

	inline vec_float SimdAnd(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_and_ps(v1, v2));

		#elif C4POWERPC

			return (vec_and(v1, v2));

		#endif
	}

	inline vec_float SimdAndc(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_andnot_ps(v2, v1));

		#elif C4POWERPC

			return (vec_andc(v1, v2));

		#endif
	}

	inline vec_float SimdOr(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_or_ps(v1, v2));

		#elif C4POWERPC

			return (vec_or(v1, v2));

		#endif
	}

	inline vec_float SimdXor(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_xor_ps(v1, v2));

		#elif C4POWERPC

			return (vec_xor(v1, v2));

		#endif
	}

	inline vec_float SimdSelect(const vec_float& v1, const vec_float& v2, const vec_float& mask)
	{
		#if C4INTEL

			return (_mm_or_ps(_mm_andnot_ps(mask, v1), _mm_and_ps(mask, v2)));

		#elif C4POWERPC

			return (vec_sel(v1, v2, (vector unsigned int) mask));

		#endif
	}

	inline vec_float SimdMaskCmpeq(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_cmpeq_ps(v1, v2));

		#elif C4POWERPC

			return ((vector float) vec_cmpeq(v1, v2));

		#endif
	}

	inline vec_float SimdMaskCmplt(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_cmplt_ps(v1, v2));

		#elif C4POWERPC

			return ((vector float) vec_cmpgt(v2, v1));

		#endif
	}

	inline vec_float SimdMaskCmpgt(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_cmplt_ps(v2, v1));

		#elif C4POWERPC

			return ((vector float) vec_cmpgt(v1, v2));

		#endif
	}

	inline bool SimdCmpeqScalar(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_comieq_ss(v1, v2));

		#elif C4POWERPC

			return (vec_all_eq(vec_splat(v1, 0), vec_splat(v2, 0)));

		#endif
	}

	inline bool SimdCmpltScalar(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_comilt_ss(v1, v2));

		#elif C4POWERPC

			return (vec_all_lt(vec_splat(v1, 0), vec_splat(v2, 0)));

		#endif
	}

	inline bool SimdCmpgtScalar(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (_mm_comigt_ss(v1, v2));

		#elif C4POWERPC

			return (vec_all_gt(vec_splat(v1, 0), vec_splat(v2, 0)));

		#endif
	}

	inline bool SimdCmpltAny3D(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (((_mm_comilt_ss(v1, v2)) | (_mm_comilt_ss(SimdSmearY(v1), SimdSmearY(v2))) | (_mm_comilt_ss(SimdSmearZ(v1), SimdSmearZ(v2)))) != 0);

		#elif C4POWERPC

			const vector unsigned char perm = vec_lvsl(12, (const float *) 0);
			vec_float u1 = vec_perm(vec_sld(v1, v1, 4), v1, perm);
			vec_float u2 = vec_perm(vec_sld(v2, v2, 4), v2, perm);
			return (vec_any_gt(u2, u1));

		#endif
	}

	inline bool SimdCmpgtAny3D(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			return (((_mm_comigt_ss(v1, v2)) | (_mm_comigt_ss(SimdSmearY(v1), SimdSmearY(v2))) | (_mm_comigt_ss(SimdSmearZ(v1), SimdSmearZ(v2)))) != 0);

		#elif C4POWERPC

			const vector unsigned char perm = vec_lvsl(12, (const float *) 0);
			vec_float u1 = vec_perm(vec_sld(v1, v1, 4), v1, perm);
			vec_float u2 = vec_perm(vec_sld(v2, v2, 4), v2, perm);
			return (vec_any_gt(u1, u2));

		#endif
	}

	inline vec_float SimdInverseSqrt(const vec_float& v)
	{
		#if C4INTEL

			const vec_float three = SimdLoadConstant<0x40400000>();
			const vec_float half = SimdLoadConstant<0x3F000000>();

			vec_float f = _mm_rsqrt_ps(v);
			return (_mm_mul_ps(_mm_mul_ps(_mm_sub_ps(three, _mm_mul_ps(v, _mm_mul_ps(f, f))), f), half));

		#elif C4POWERPC

			const vec_float zero = SimdGetFloatZero();
			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float half = SimdLoadConstant<0x3F000000>();

			vec_float f = vec_rsqrte(v);
			return (vec_madd(vec_nmsub(v, vec_madd(f, f, zero), one), vec_madd(f, half, zero), f));

		#endif
	}

	inline vec_float SimdInverseSqrtScalar(const vec_float& v)
	{
		#if C4INTEL

			const vec_float three = SimdLoadConstant<0x40400000>();
			const vec_float half = SimdLoadConstant<0x3F000000>();

			vec_float f = _mm_rsqrt_ss(v);
			return (_mm_mul_ss(_mm_mul_ss(_mm_sub_ss(three, _mm_mul_ss(v, _mm_mul_ss(f, f))), f), half));

		#elif C4POWERPC

			const vec_float zero = SimdGetFloatZero();
			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float half = SimdLoadConstant<0x3F000000>();

			vec_float f = vec_rsqrte(v);
			return (vec_madd(vec_nmsub(v, vec_madd(f, f, zero), one), vec_madd(f, half, zero), f));

		#endif
	}

	inline vec_float SimdSqrt(const vec_float& v)
	{
		vec_float mask = SimdMaskCmpeq(v, SimdGetFloatZero());
		return (SimdAndc(SimdMul(SimdInverseSqrt(v), v), mask));
	}

	inline vec_float SimdSqrtScalar(const vec_float& v)
	{
		vec_float mask = SimdMaskCmpeq(v, SimdGetFloatZero());
		return (SimdAndc(SimdMulScalar(SimdInverseSqrtScalar(v), v), mask));
	}

	inline vec_float SimdFloor(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_sub_ps(_mm_add_ps(_mm_add_ps(_mm_sub_ps(v, two23), two23), two23), two23);
			result = _mm_sub_ps(result, _mm_and_ps(one, _mm_cmplt_ps(v, result)));

			vec_float mask = _mm_cmplt_ps(two23, _mm_andnot_ps(SimdGetNegativeZero(), v));
			return (_mm_or_ps(_mm_andnot_ps(mask, result), _mm_and_ps(mask, v)));

		#elif C4POWERPC

			return (vec_floor(v));

		#endif
	}

	inline vec_float SimdFloorScalar(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_sub_ss(_mm_add_ss(_mm_add_ss(_mm_sub_ss(v, two23), two23), two23), two23);
			result = _mm_sub_ss(result, _mm_and_ps(one, _mm_cmplt_ps(v, result)));

			vec_float mask = _mm_cmplt_ps(two23, _mm_andnot_ps(SimdGetNegativeZero(), v));
			return (_mm_or_ps(_mm_andnot_ps(mask, result), _mm_and_ps(mask, v)));

		#elif C4POWERPC

			return (vec_floor(v));

		#endif
	}

	inline vec_float SimdPositiveFloor(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_sub_ps(_mm_add_ps(v, two23), two23);
			return (_mm_sub_ps(result, _mm_and_ps(one, _mm_cmplt_ps(v, result))));

		#elif C4POWERPC

			return (vec_floor(v));

		#endif
	}

	inline vec_float SimdPositiveFloorScalar(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_sub_ss(_mm_add_ss(v, two23), two23);
			return (_mm_sub_ss(result, _mm_and_ps(one, _mm_cmplt_ps(v, result))));

		#elif C4POWERPC

			return (vec_floor(v));

		#endif
	}

	inline vec_float SimdNegativeFloor(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_add_ps(_mm_sub_ps(v, two23), two23);
			return (_mm_sub_ps(result, _mm_and_ps(one, _mm_cmplt_ps(v, result))));

		#elif C4POWERPC

			return (vec_floor(v));

		#endif
	}

	inline vec_float SimdNegativeFloorScalar(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_add_ss(_mm_sub_ss(v, two23), two23);
			return (_mm_sub_ss(result, _mm_and_ps(one, _mm_cmplt_ps(v, result))));

		#elif C4POWERPC

			return (vec_floor(v));

		#endif
	}

	inline vec_float SimdCeil(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_sub_ps(_mm_add_ps(_mm_add_ps(_mm_sub_ps(v, two23), two23), two23), two23);
			result = _mm_add_ps(result, _mm_and_ps(one, _mm_cmplt_ps(result, v)));

			vec_float mask = _mm_cmplt_ps(two23, _mm_andnot_ps(SimdGetNegativeZero(), v));
			return (_mm_or_ps(_mm_andnot_ps(mask, result), _mm_and_ps(mask, v)));

		#elif C4POWERPC

			return (vec_ceil(v));

		#endif
	}

	inline vec_float SimdCeilScalar(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_sub_ss(_mm_add_ss(_mm_add_ss(_mm_sub_ss(v, two23), two23), two23), two23);
			result = _mm_add_ss(result, _mm_and_ps(one, _mm_cmplt_ps(result, v)));

			vec_float mask = _mm_cmplt_ps(two23, _mm_andnot_ps(SimdGetNegativeZero(), v));
			return (_mm_or_ps(_mm_andnot_ps(mask, result), _mm_and_ps(mask, v)));

		#elif C4POWERPC

			return (vec_ceil(v));

		#endif
	}

	inline vec_float SimdPositiveCeil(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_sub_ps(_mm_add_ps(v, two23), two23);
			return (_mm_add_ps(result, _mm_and_ps(one, _mm_cmplt_ps(result, v))));

		#elif C4POWERPC

			return (vec_ceil(v));

		#endif
	}

	inline vec_float SimdPositiveCeilScalar(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_sub_ss(_mm_add_ss(v, two23), two23);
			return (_mm_add_ss(result, _mm_and_ps(one, _mm_cmplt_ps(result, v))));

		#elif C4POWERPC

			return (vec_ceil(v));

		#endif
	}

	inline vec_float SimdNegativeCeil(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_add_ps(_mm_sub_ps(v, two23), two23);
			return (_mm_add_ps(result, _mm_and_ps(one, _mm_cmplt_ps(result, v))));

		#elif C4POWERPC

			return (vec_ceil(v));

		#endif
	}

	inline vec_float SimdNegativeCeilScalar(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_add_ss(_mm_sub_ss(v, two23), two23);
			return (_mm_add_ss(result, _mm_and_ps(one, _mm_cmplt_ps(result, v))));

		#elif C4POWERPC

			return (vec_ceil(v));

		#endif
	}

	inline void SimdFloorCeil(const vec_float& v, vec_float *f, vec_float *c)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_sub_ps(_mm_add_ps(_mm_add_ps(_mm_sub_ps(v, two23), two23), two23), two23);
			*f = _mm_sub_ps(result, _mm_and_ps(one, _mm_cmplt_ps(v, result)));
			*c = _mm_add_ps(result, _mm_and_ps(one, _mm_cmplt_ps(result, v)));

		#elif C4POWERPC

			*f = vec_floor(v);
			*c = vec_ceil(v);

		#endif
	}

	inline void SimdFloorCeilScalar(const vec_float& v, vec_float *f, vec_float *c)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_sub_ss(_mm_add_ss(_mm_add_ss(_mm_sub_ss(v, two23), two23), two23), two23);
			*f = _mm_sub_ss(result, _mm_and_ps(one, _mm_cmplt_ps(v, result)));
			*c = _mm_add_ss(result, _mm_and_ps(one, _mm_cmplt_ps(result, v)));

		#elif C4POWERPC

			*f = vec_floor(v);
			*c = vec_ceil(v);

		#endif
	}

	inline void SimdPositiveFloorCeil(const vec_float& v, vec_float *f, vec_float *c)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_sub_ps(_mm_add_ps(v, two23), two23);
			*f = _mm_sub_ps(result, _mm_and_ps(one, _mm_cmplt_ps(v, result)));
			*c = _mm_add_ps(result, _mm_and_ps(one, _mm_cmplt_ps(result, v)));

		#elif C4POWERPC

			*f = vec_floor(v);
			*c = vec_ceil(v);

		#endif
	}

	inline void SimdPositiveFloorCeilScalar(const vec_float& v, vec_float *f, vec_float *c)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_sub_ss(_mm_add_ss(v, two23), two23);
			*f = _mm_sub_ss(result, _mm_and_ps(one, _mm_cmplt_ps(v, result)));
			*c = _mm_add_ss(result, _mm_and_ps(one, _mm_cmplt_ps(result, v)));

		#elif C4POWERPC

			*f = vec_floor(v);
			*c = vec_ceil(v);

		#endif
	}

	inline void SimdNegativeFloorCeil(const vec_float& v, vec_float *f, vec_float *c)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_add_ps(_mm_sub_ps(v, two23), two23);
			*f = _mm_sub_ps(result, _mm_and_ps(one, _mm_cmplt_ps(v, result)));
			*c = _mm_add_ps(result, _mm_and_ps(one, _mm_cmplt_ps(result, v)));

		#elif C4POWERPC

			*f = vec_floor(v);
			*c = vec_ceil(v);

		#endif
	}

	inline void SimdNegativeFloorCeilScalar(const vec_float& v, vec_float *f, vec_float *c)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();

			vec_float result = _mm_add_ss(_mm_sub_ss(v, two23), two23);
			*f = _mm_sub_ss(result, _mm_and_ps(one, _mm_cmplt_ps(v, result)));
			*c = _mm_add_ss(result, _mm_and_ps(one, _mm_cmplt_ps(result, v)));

		#elif C4POWERPC

			*f = vec_floor(v);
			*c = vec_ceil(v);

		#endif
	}

	inline vec_float SimdTrunc(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();
			const vec_float negzero = SimdGetNegativeZero();

			vec_float w = _mm_andnot_ps(negzero, v);
			vec_float result = _mm_sub_ps(_mm_add_ps(w, two23), two23);
			result = _mm_sub_ps(result, _mm_and_ps(one, _mm_cmplt_ps(w, result)));
			result = _mm_or_ps(result, _mm_and_ps(negzero, v));

			vec_float mask = _mm_cmplt_ps(two23, w);
			return (_mm_or_ps(_mm_andnot_ps(mask, result), _mm_and_ps(mask, v)));

		#elif C4POWERPC

			return (vec_trunc(v));

		#endif
	}

	inline vec_float SimdTruncScalar(const vec_float& v)
	{
		#if C4INTEL

			const vec_float one = SimdLoadConstant<0x3F800000>();
			const vec_float two23 = SimdLoadConstant<0x4B000000>();
			const vec_float negzero = SimdGetNegativeZero();

			vec_float w = _mm_andnot_ps(negzero, v);
			vec_float result = _mm_sub_ss(_mm_add_ss(w, two23), two23);
			result = _mm_sub_ss(result, _mm_and_ps(one, _mm_cmplt_ps(w, result)));
			result = _mm_or_ps(result, _mm_and_ps(negzero, v));

			vec_float mask = _mm_cmplt_ps(two23, w);
			return (_mm_or_ps(_mm_andnot_ps(mask, result), _mm_and_ps(mask, v)));

		#elif C4POWERPC

			return (vec_trunc(v));

		#endif
	}

	inline vec_float SimdFsgn(const vec_float& v)
	{
		vec_float result = SimdLoadConstant<0x3F800000>();
		result = SimdOr(result, SimdAnd(v, SimdGetNegativeZero()));
		return (SimdAndc(result, SimdMaskCmpeq(v, SimdGetFloatZero())));
	}

	inline vec_float SimdNonzeroFsgn(const vec_float& v)
	{
		vec_float result = SimdLoadConstant<0x3F800000>();
		return (SimdOr(result, SimdAnd(v, SimdGetNegativeZero())));
	}

	inline vec_float SimdDot3D(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			vec_float d = _mm_mul_ps(v1, v2);
			return (_mm_add_ss(_mm_add_ss(d, SimdSmearY(d)), SimdSmearZ(d)));

		#elif C4POWERPC

			vec_float d = vec_madd(v1, v2, SimdGetFloatZero());
			return (vec_add(vec_add(d, SimdSmearY(d)), SimdSmearZ(d)));

		#endif
	}

	inline vec_float SimdPlaneWedgePoint3D(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			vec_float d = _mm_mul_ps(v1, v2);
			return (_mm_add_ss(_mm_add_ss(_mm_add_ss(d, SimdSmearY(d)), SimdSmearZ(d)), SimdSmearW(v1)));

		#elif C4POWERPC

			vec_float d = vec_madd(v1, v2, SimdGetFloatZero());
			return (vec_add(vec_add(vec_add(d, SimdSmearY(d)), SimdSmearZ(d)), SimdSmearW(v1)));

		#endif
	}

	inline vec_float SimdProjectOnto3D(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			vec_float d = SimdDot3D(v1, v2);
			return (_mm_mul_ps(v2, SimdSmearX(d)));

		#elif C4POWERPC

			vec_float d = SimdDot3D(v1, v2);
			return (vec_madd(v2, SimdSmearX(d), SimdGetFloatZero()));

		#endif
	}

	inline vec_float SimdCross3D(const vec_float& v1, const vec_float& v2)
	{
		#if C4INTEL

			vec_float a = _mm_shuffle_ps(v1, v1, _MM_SHUFFLE(0, 0, 2, 1));
			vec_float b = _mm_shuffle_ps(v2, v2, _MM_SHUFFLE(0, 1, 0, 2));
			vec_float c = _mm_mul_ps(a, b);

			a = _mm_shuffle_ps(v1, v1, _MM_SHUFFLE(0, 1, 0, 2));
			b = _mm_shuffle_ps(v2, v2, _MM_SHUFFLE(0, 0, 2, 1));
			return (_mm_sub_ps(c, _mm_mul_ps(a, b)));

		#elif C4POWERPC

			const vector unsigned char d1 = vec_lvsl(12, (const float *) 0);
			const vector unsigned char d2 = vec_lvsl(0, (const float *) 0);
			const vector unsigned char perm1 = vec_sld(d1, d2, 8);				// 14151617 18191A1B 00010203 04050607
			const vector unsigned char perm2 = vec_sld(d1, d2, 12);				// 18191A1B 00010203 04050607 08090A0B

			vec_float c = vec_madd(vec_perm(v1, v1, perm1), vec_perm(v2, v2, perm2), SimdGetFloatZero());
			return (vec_nmsub(vec_perm(v1, v1, perm2), vec_perm(v2, v2, perm1), c));

		#endif
	}

	inline vec_float SimdTransformVector3D(const vec_float& c1, const vec_float& c2, const vec_float& c3, const vec_float& v)
	{
		#if C4INTEL

			vec_float result = _mm_mul_ps(c1, SimdSmearX(v));
			result = _mm_add_ps(result, _mm_mul_ps(c2, SimdSmearY(v)));
			return (_mm_add_ps(result, _mm_mul_ps(c3, SimdSmearZ(v))));

		#elif C4POWERPC

			vec_float result = vec_madd(c1, SimdSmearX(v), SimdGetFloatZero());
			result = vec_madd(c2, SimdSmearY(v), result);
			return (vec_madd(c3, SimdSmearZ(v), result));

		#endif
	}

	inline vec_float SimdTransformPoint3D(const vec_float& c1, const vec_float& c2, const vec_float& c3, const vec_float& c4, const vec_float& p)
	{
		#if C4INTEL

			vec_float result = _mm_mul_ps(c1, SimdSmearX(p));
			result = _mm_add_ps(result, _mm_mul_ps(c2, SimdSmearY(p)));
			result = _mm_add_ps(result, _mm_mul_ps(c3, SimdSmearZ(p)));
			return (_mm_add_ps(result, c4));

		#elif C4POWERPC

			vec_float result = vec_madd(c1, SimdSmearX(p), SimdGetFloatZero());
			result = vec_madd(c2, SimdSmearY(p), result);
			result = vec_madd(c3, SimdSmearZ(p), result);
			return (vec_add(result, c4));

		#endif
	}

	inline vec_int8 SimdInt8GetZero(void)
	{
		#if C4INTEL

			return (_mm_setzero_si128());

		#elif C4POWERPC

			return (vec_splat_s8(0));

		#endif
	}

	inline vec_int32 SimdInt32GetZero(void)
	{
		#if C4INTEL

			return (_mm_setzero_si128());

		#elif C4POWERPC

			return (vec_splat_s32(0));

		#endif
	}

	inline vec_int8 SimdInt8GetInfinity(void)
	{
		#if C4INTEL

			alignas(16) static const unsigned_int8 int_80[16] = {0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80};
			return (_mm_load_si128(reinterpret_cast<const __m128i *>(int_80)));

		#elif C4POWERPC

			return (vec_rl(vec_splat_s8(1), vec_splat_u8(7)));

		#endif
	}

	inline vec_int8 SimdInt8Load(const int8 *ptr, machine offset = 0)
	{
		#if C4INTEL

			return (_mm_load_si128(reinterpret_cast<const __m128i *>(&ptr[offset])));

		#elif C4POWERPC

			return (vec_ld(offset, ptr));

		#endif
	}

	inline vec_int8 SimdInt8LoadUnaligned(const int8 *ptr)
	{
		#if C4INTEL

			return (_mm_loadu_si128(reinterpret_cast<const __m128i *>(ptr)));

		#elif C4POWERPC

			return (vec_perm(vec_ld(0, ptr), vec_ld(16, ptr), vec_lvsl(0, ptr)));

		#endif
	}

	inline vec_unsigned_int8 SimdUnsignedInt8Load(const unsigned_int8 *ptr, machine offset = 0)
	{
		#if C4INTEL

			return (_mm_load_si128(reinterpret_cast<const __m128i *>(&ptr[offset])));

		#elif C4POWERPC

			return (vec_ld(offset, ptr));

		#endif
	}

	inline vec_unsigned_int8 SimdUnsignedInt8LoadUnaligned(const unsigned_int8 *ptr)
	{
		#if C4INTEL

			return (_mm_loadu_si128(reinterpret_cast<const __m128i *>(ptr)));

		#elif C4POWERPC

			return (vec_perm(vec_ld(0, ptr), vec_ld(16, ptr), vec_lvsl(0, ptr)));

		#endif
	}

	inline void SimdInt32StoreX(const vec_int32& v, int32 *ptr, machine offset = 0)
	{
		#if C4INTEL

			ptr[offset] = _mm_cvtsi128_si32(v);

		#elif C4POWERPC

			vec_ste(vec_splat(v, 0), offset << 2, ptr);

		#endif
	}

	inline void SimdInt32StoreY(const vec_int32& v, int32 *ptr, machine offset = 0)
	{
		#if C4INTEL

			ptr[offset] = _mm_cvtsi128_si32(_mm_shuffle_epi32(v, _MM_SHUFFLE(1, 1, 1, 1)));

		#elif C4POWERPC

			vec_ste(vec_splat(v, 1), offset << 2, ptr);

		#endif
	}

	inline void SimdInt32StoreZ(const vec_int32& v, int32 *ptr, machine offset = 0)
	{
		#if C4INTEL

			ptr[offset] = _mm_cvtsi128_si32(_mm_shuffle_epi32(v, _MM_SHUFFLE(2, 2, 2, 2)));

		#elif C4POWERPC

			vec_ste(vec_splat(v, 2), offset << 2, ptr);

		#endif
	}

	inline void SimdInt32StoreW(const vec_int32& v, int32 *ptr, machine offset = 0)
	{
		#if C4INTEL

			ptr[offset] = _mm_cvtsi128_si32(_mm_shuffle_epi32(v, _MM_SHUFFLE(3, 3, 3, 3)));

		#elif C4POWERPC

			vec_ste(vec_splat(v, 3), offset << 2, ptr);

		#endif
	}

	inline vec_int16 SimdInt32PackSaturate(const vec_int32& v)
	{
		#if C4INTEL

			return (_mm_packs_epi32(v, v));

		#elif C4POWERPC

			return (vec_vpkswss(v, v));

		#endif
	}

	inline vec_int16 SimdInt32PackSaturate(const vec_int32& v1, const vec_int32& v2)
	{
		#if C4INTEL

			return (_mm_packs_epi32(v1, v2));

		#elif C4POWERPC

			return (vec_vpkswss(v1, v2));

		#endif
	}

	inline vec_int8 SimdInt16PackSaturate(const vec_int16& v)
	{
		#if C4INTEL

			return (_mm_packs_epi16(v, v));

		#elif C4POWERPC

			return (vec_packs(v, v));

		#endif
	}

	inline vec_unsigned_int8 SimdInt16PackUnsignedSaturate(const vec_int16& v)
	{
		#if C4INTEL

			return (_mm_packus_epi16(v, v));

		#elif C4POWERPC

			return (vec_vpkshus(v, v));

		#endif
	}

	inline vec_int16 SimdInt8UnpackA(const vec_int8& v)
	{
		#if C4INTEL

			return (_mm_unpacklo_epi8(v, _mm_cmplt_epi8(v, _mm_setzero_si128())));

		#elif C4POWERPC

			return (vec_unpackh(v));

		#endif
	}

	inline vec_int16 SimdInt8UnpackB(const vec_int8& v)
	{
		#if C4INTEL

			return (_mm_unpackhi_epi8(v, _mm_cmplt_epi8(v, _mm_setzero_si128())));

		#elif C4POWERPC

			return (vec_unpackl(v));

		#endif
	}

	inline vec_unsigned_int16 SimdUnsignedInt8UnpackA(const vec_unsigned_int8& v)
	{
		#if C4INTEL

			return (_mm_unpacklo_epi8(v, _mm_setzero_si128()));

		#elif C4POWERPC

			return ((vec_unsigned_int16) vec_mergeh(vec_splat_u8(0), v));

		#endif
	}

	inline vec_unsigned_int16 SimdUnsignedInt8UnpackB(const vec_unsigned_int8& v)
	{
		#if C4INTEL

			return (_mm_unpackhi_epi8(v, _mm_setzero_si128()));

		#elif C4POWERPC

			return ((vec_unsigned_int16) vec_mergel(vec_splat_u8(0), v));

		#endif
	}

	inline vec_int32 SimdInt16UnpackA(const vec_int16& v)
	{
		#if C4INTEL

			return (_mm_unpacklo_epi16(v, _mm_cmplt_epi16(v, _mm_setzero_si128())));

		#elif C4POWERPC

			return (vec_unpackh(v));

		#endif
	}

	inline vec_int32 SimdInt16UnpackB(const vec_int16& v)
	{
		#if C4INTEL

			return (_mm_unpackhi_epi16(v, _mm_cmplt_epi16(v, _mm_setzero_si128())));

		#elif C4POWERPC

			return (vec_unpackl(v));

		#endif
	}

	inline vec_unsigned_int32 SimdUnsignedInt16UnpackA(const vec_unsigned_int16& v)
	{
		#if C4INTEL

			return (_mm_unpacklo_epi16(v, _mm_setzero_si128()));

		#elif C4POWERPC

			return ((vec_unsigned_int32) vec_mergeh(vec_splat_u16(0), v));

		#endif
	}

	inline vec_unsigned_int32 SimdUnsignedInt16UnpackB(const vec_unsigned_int16& v)
	{
		#if C4INTEL

			return (_mm_unpackhi_epi16(v, _mm_setzero_si128()));

		#elif C4POWERPC

			return ((vec_unsigned_int32) vec_mergel(vec_splat_u16(0), v));

		#endif
	}

	inline vec_int8 SimdInt8MergeA(const vec_int8& v1, const vec_int8& v2)
	{
		#if C4INTEL

			return (_mm_unpacklo_epi8(v1, v2));

		#elif C4POWERPC

			return (vec_mergeh(v1, v2));

		#endif
	}

	inline vec_int8 SimdInt8MergeB(const vec_int8& v1, const vec_int8& v2)
	{
		#if C4INTEL

			return (_mm_unpackhi_epi8(v1, v2));

		#elif C4POWERPC

			return (vec_mergel(v1, v2));

		#endif
	}

	inline vec_unsigned_int8 SimdUnsignedInt8MergeA(const vec_unsigned_int8& v1, const vec_unsigned_int8& v2)
	{
		#if C4INTEL

			return (_mm_unpacklo_epi8(v1, v2));

		#elif C4POWERPC

			return (vec_mergeh(v1, v2));

		#endif
	}

	inline vec_unsigned_int8 SimdUnsignedInt8MergeB(const vec_unsigned_int8& v1, const vec_unsigned_int8& v2)
	{
		#if C4INTEL

			return (_mm_unpackhi_epi8(v1, v2));

		#elif C4POWERPC

			return (vec_mergel(v1, v2));

		#endif
	}

	inline vec_int16 SimdInt16Deinterleave(const vec_int16& v)
	{
		#if C4INTEL

			vec_int16 i = _mm_shufflelo_epi16(v, _MM_SHUFFLE(3, 1, 2, 0));
			i = _mm_shufflehi_epi16(i, _MM_SHUFFLE(3, 1, 2, 0));
			return (_mm_shuffle_epi32(i, _MM_SHUFFLE(3, 1, 2, 0)));

		#elif C4POWERPC

			const vector unsigned int w = (vector unsigned int) vec_lvsl(0, (const unsigned int *) 0);
			const vector unsigned char shift = vec_splat_u8(8);
			const vector unsigned char perm = (vector unsigned char) vec_pack(vec_sro(w, vec_add(shift, shift)), w);	// 0001 0405 0809 0C0D 0203 0607 0A0B 0E0F
			return (vec_perm(v, v, perm));

		#endif
	}

	inline vec_float SimdInt32ConvertFloat(const vec_int32& v)
	{
		#if C4INTEL

			return (_mm_cvtepi32_ps(v));

		#elif C4POWERPC

			return (vec_ctf(v, 0));

		#endif
	}

	inline vec_int32 SimdConvertInt32(const vec_float& v)
	{
		#if C4INTEL

			return (_mm_cvtps_epi32(v));

		#elif C4POWERPC

			return (vec_cts(v, 0));

		#endif
	}

	inline vec_int32 SimdInt32Add(const vec_int32& v1, const vec_int32& v2)
	{
		#if C4INTEL

			return (_mm_add_epi32(v1, v2));

		#elif C4POWERPC

			return (vec_add(v1, v2));

		#endif
	}

	inline vec_int32 SimdInt32Sub(const vec_int32& v1, const vec_int32& v2)
	{
		#if C4INTEL

			return (_mm_sub_epi32(v1, v2));

		#elif C4POWERPC

			return (vec_sub(v1, v2));

		#endif
	}
}


#endif

// ZYUTNLM
