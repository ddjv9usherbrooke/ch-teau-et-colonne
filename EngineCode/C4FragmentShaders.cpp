//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4FragmentShaders.h"
#include "C4Engine.h"


#define C4LOG_FRAGMENT_SHADERS		0


using namespace C4;


Storage<HashTable<FragmentShader>> FragmentShader::hashTable;


#if C4OPENGL

	const char FragmentShader::prolog1Text[] =
	{
		"#version 150\n"

		"uniform vec4 fparam[" FRAGMENT_PARAM_COUNT "];\n"

		"in vresult\n"
		"{\n"
			"vec4 vcolor[2];\n"
			"vec4 texcoord[8];\n"
		"};\n"

		"out vec4 fcolor;\n"
	};

	const char FragmentShader::prolog2Text[] =
	{
		"void main()\n"
		"{\n"
			"vec4 temp"
	};

	const char FragmentShader::epilogText[] =
	{
		"}\n"
	};

#elif C4PSSL //[ 

			// -- Orbis code hidden --

#elif C4CG //[ 

			// -- PS3 code hidden --

#endif //]


const char FragmentShader::copyZero[] =
{
	#if C4OPENGL

		"#version 150\n"

		"out vec4 fcolor;\n"

		"void main()\n"
		"{\n"
			"fcolor = vec4(0.0, 0.0, 0.0, 0.0);\n"
		"}\n"

	#elif C4PSSL //[ 

			// -- Orbis code hidden --

	#elif C4CG //[ 

			// -- PS3 code hidden --

	#endif //]
};

const char FragmentShader::copyConstant[] =
{
	#if C4OPENGL

		"#version 150\n"

		"uniform vec4 fparam[" FRAGMENT_PARAM_COUNT "];\n"

		"out vec4 fcolor;\n"

		"void main()\n"
		"{\n"
			"fcolor = fparam[" FRAGMENT_PARAM_CONSTANT7 "];\n"
		"}\n"

	#elif C4PSSL //[ 

			// -- Orbis code hidden --

	#elif C4CG //[ 

			// -- PS3 code hidden --

	#endif //] 
};

 
FragmentShader::FragmentShader(const char *source, unsigned_int32 size, const unsigned_int32 *signature) : FragmentShaderObject(source, size)
{ 
	MemoryMgr::CopyMemory(signature, shaderSignature, signature[0] * 4 + 4); 

	#if C4LOG_FRAGMENT_SHADERS

		Engine::LogSource(source); 

	#endif

	#if C4ORBIS //[ 
 
			// -- Orbis code hidden --

	#endif //]

	#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]
}

#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

#endif //]

FragmentShader::~FragmentShader()
{
	#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]
}

void FragmentShader::Initialize(void)
{
	new(hashTable) HashTable<FragmentShader>(16, 16);

	#if C4ORBIS //[ 

			// -- Orbis code hidden --

	#endif //]
}

void FragmentShader::Terminate(void)
{
	hashTable->~HashTable();
}

unsigned_int32 FragmentShader::Hash(const KeyType& key)
{
	unsigned_int32 hash = 0;

	int32 count = key[0];
	for (machine a = 1; a <= count; a++)
	{
		hash += key[a];
		hash = (hash << 5) | (hash >> 27);
	}

	return (hash);
}

FragmentShader *FragmentShader::Find(const unsigned_int32 *signature)
{
	FragmentShader *shader = hashTable->Find(ShaderSignature(signature));
	if (shader)
	{
		shader->Retain();
	}

	return (shader);
}

FragmentShader *FragmentShader::New(const char *source, unsigned_int32 size, const unsigned_int32 *signature)
{
	FragmentShader *shader = MemoryMgr::GetMainHeap()->New<FragmentShader>(sizeof(FragmentShader) + signature[0] * 4);
	new(shader) FragmentShader(source, size, signature);
	hashTable->Insert(shader);

	shader->Retain();
	return (shader);
}

void FragmentShader::ReleaseCache(void)
{
	int32 bucketCount = hashTable->GetBucketCount();
	for (machine a = 0; a < bucketCount; a++)
	{
		FragmentShader *shader = hashTable->GetFirstBucketElement(a);
		while (shader)
		{
			FragmentShader *next = shader->Next();

			if (shader->GetReferenceCount() == 1)
			{
				shader->Release();
			}

			shader = next;
		}
	}
}

#if C4ORBIS //[ 

			// -- Orbis code hidden --

#elif C4PS3 //[ 

			// -- PS3 code hidden --

#endif //]

// ZYUTNLM
