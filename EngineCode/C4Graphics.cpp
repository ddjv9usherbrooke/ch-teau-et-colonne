//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4FragmentShaders.h"
#include "C4GeometryShaders.h"
#include "C4World.h"
#include "C4Movies.h"

#if C4WINDOWS

	#include "C4Nvidia.h"

	extern "C"
	{
		__declspec(dllexport) DWORD NvOptimusEnablement = 1;
	}

#endif

using namespace C4;


namespace
{
	const float kFrustumEpsilon			= 2.0e-6F;
	const float kVelocityMultiplier		= 0.142857F;
	const float kMotionBlurBoxExpand	= 8.0F;


	enum
	{
		kGraphicsDepthTestLess			= 1 << 0,
		kGraphicsAmbientLessEqual		= 1 << 1,
		kGraphicsCullFaceBack			= 1 << 2,
		kGraphicsFrontFaceCCW			= 1 << 3,
		kGraphicsClipEnabled			= 1 << 4,
		kGraphicsLightScissor			= 1 << 5,
		kGraphicsShadowScissor			= 1 << 6,
		kGraphicsUpdateShadowScissor	= 1 << 7,
		kGraphicsDepthBoundsAvail		= 1 << 8,
		kGraphicsObliqueFrustum			= 1 << 9,
		kGraphicsLightDepthBounds		= 1 << 10,
		kGraphicsShadowDepthBounds		= 1 << 11,
		kGraphicsStencilClear			= 1 << 12,
		kGraphicsStencilValid			= 1 << 13,
		kGraphicsRenderLight			= 1 << 14,
		kGraphicsRenderStencil			= 1 << 15,
		kGraphicsRenderShadowMap		= 1 << 16,
		kGraphicsReactivateTextures		= 1 << 17,
		kGraphicsMotionBlurAvail		= 1 << 18,
		kGraphicsDistortionAvail		= 1 << 19,
		kGraphicsGlowBloomAvail			= 1 << 20,

		kGraphicsScissorMask			= kGraphicsLightScissor | kGraphicsShadowScissor,
		kGraphicsDepthBoundsMask		= kGraphicsLightDepthBounds | kGraphicsShadowDepthBounds
	};


	enum
	{
		kPostColorMatrix				= 1 << 0,
		kPostMotionBlur					= 1 << 1,
		kPostMotionBlurGradient			= 1 << 2,
		kPostDistortion					= 1 << 3,
		kPostGlowBloom					= 1 << 4,
		kPostShaderCount				= 1 << 5
	};


	enum
	{
		kLocalFragmentShaderCopyZero,
		kLocalFragmentShaderCopyConstant,
		kLocalFragmentShaderCount
	};


	enum
	{
		kLocalGeometryShaderExtrudeNormalLine,
		kLocalGeometryShaderCount
	};


	const TextureHeader nullTextureHeader =
	{
		kTexture2D,
		kTextureForceHighQuality,
		kTextureSemanticNone,
		kTextureSemanticNone,
		kTextureI8,
		4, 4, 1,
		{kTextureRepeat, kTextureRepeat, kTextureRepeat},
		3
	};

	alignas(32) const unsigned_int8 nullTextureImage[21] =
	{
		0xFF, 0x00, 0xFF, 0x00, 0x00, 0xFF, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0x00, 0x00, 0xFF, 0x00, 0xFF,
		0xFF, 0x00, 0xFF, 0x00,
		0xFF
	};
}
 

GraphicsMgr *C4::TheGraphicsMgr = nullptr;
 

namespace C4 
{ 
	template <> GraphicsMgr Manager<GraphicsMgr>::managerObject(0);
	template <> GraphicsMgr **Manager<GraphicsMgr>::managerPointer = &TheGraphicsMgr;

	template <> const char *const Manager<GraphicsMgr>::resultString[] = 
	{
		nullptr,
		"Graphics context format failed",
		"Graphics context initialization failed",
		"Graphics hardware insufficient" 
	};

	template <> const unsigned_int32 Manager<GraphicsMgr>::resultIdentifier[] =
	{
		0, 'FORM', 'CTXT', 'HARD'
	};

	template class Manager<GraphicsMgr>;
}


GraphicsExtensionData GraphicsMgr::extensionData[kGraphicsExtensionCount] =
{
	{"GL_ARB_color_buffer_float",				nullptr,							0x0300,		false,	true},
	{"GL_ARB_conservative_depth",				nullptr,							0x0420,		false,	true},
	{"GL_ARB_debug_output",						nullptr,							0xFFFF,		false,	true},
	{"GL_ARB_depth_clamp",						"GL_NV_depth_clamp",				0x0320,		false,	true},
	{"GL_ARB_framebuffer_object",				"GL_EXT_framebuffer_object",		0x0300,		true,	true},
	{"GL_EXT_framebuffer_blit",					nullptr,							0x0300,		false,	true},
	{"GL_EXT_framebuffer_multisample",			nullptr,							0x0300,		false,	true},
	{"GL_ARB_framebuffer_sRGB",					"GL_EXT_framebuffer_sRGB",			0x0300,		false,	true},
	{"GL_ARB_geometry_shader4",					nullptr,							0x0320,		false,	true},
	{"GL_ARB_get_program_binary",				nullptr,							0x0410,		false,	true},
	{"GL_ARB_half_float_pixel",					nullptr,							0x0300,		false,	true},
	{"GL_ARB_instanced_arrays",					nullptr,							0x0330,		false,	true},
	{"GL_ARB_invalidate_subdata",				nullptr,							0xFFFF,		false,	true},
	{"GL_ARB_map_buffer_range",					nullptr,							0x0300,		false,	true},
	{"GL_ARB_packed_depth_stencil",				"GL_EXT_packed_depth_stencil",		0x0300,		true,	true},
	{"GL_ARB_sample_shading",					nullptr,							0x0400,		false,	true},
	{"GL_ARB_seamless_cube_map",				nullptr,							0x0320,		false,	true},
	{"GL_ARB_shader_texture_lod",				"GL_ATI_shader_texture_lod",		0xFFFF,		false,	true},
	{"GL_ARB_tessellation_shader",				nullptr,							0x0400,		false,	true},
	{"GL_ARB_texture_array",					"GL_EXT_texture_array",				0x0300,		false,	true},
	{"GL_ARB_texture_compression_rgtc",			"GL_EXT_texture_compression_rgtc",	0x0300,		false,	true},
	{"GL_ARB_texture_float",					"GL_ATI_texture_float",				0x0300,		false,	true},
	{"GL_ARB_texture_rectangle",				"GL_EXT_texture_rectangle",			0x0310,		true,	true},
	{"GL_ARB_texture_rg",						nullptr,							0x0300,		true,	true},

	#if !C4MACOS

		{"GL_ARB_texture_swizzle",					"GL_EXT_texture_swizzle",			0x0330,		true,	true},

	#else

		{"GL_ARB_texture_swizzle",					"GL_EXT_texture_swizzle",			0x0330,		false,	true},

	#endif

	{"GL_ARB_timer_query",						nullptr,							0x0330,		false,	true},
	{"GL_ARB_uniform_buffer_object",			nullptr,							0x0310,		false,	true},
	{"GL_ARB_vertex_array_object",				"GL_APPLE_vertex_array_object",		0x0300,		false,	true},
	{"GL_EXT_depth_bounds_test",				nullptr,							0xFFFF,		false,	true},
	{"GL_EXT_direct_state_access",				nullptr,							0xFFFF,		false,	true},
	{"GL_EXT_texture_compression_s3tc",			nullptr,							0xFFFF,		true,	true},
	{"GL_EXT_texture_filter_anisotropic",		nullptr,							0xFFFF,		false,	true},
	{"GL_EXT_texture_mirror_clamp",				"GL_ATI_texture_mirror_once",		0xFFFF,		false,	true},
	{"GL_NV_conditional_render",				nullptr,							0x0300,		false,	true},
	{"GL_NV_explicit_multisample",				nullptr,							0xFFFF,		false,	true},
	{"GL_NV_framebuffer_multisample_coverage",	nullptr,							0xFFFF,		false,	true},
	{"GL_NV_shader_buffer_load",				nullptr,							0xFFFF,		false,	true},
	{"GL_NV_transform_feedback",				nullptr,							0xFFFF,		false,	true},
	{"GL_NV_vertex_buffer_unified_memory",		nullptr,							0xFFFF,		false,	true}
};


#if C4WINDOWS

	WindowSystemExtensionData GraphicsMgr::windowSystemExtensionData[kWindowSystemExtensionCount] =
	{
		{"WGL_ARB_create_context", true},
		{"WGL_ARB_pixel_format", true},
		{"WGL_EXT_swap_control", true},
		{"WGL_EXT_swap_control_tear", true}
	};

#elif C4LINUX

	WindowSystemExtensionData GraphicsMgr::windowSystemExtensionData[kWindowSystemExtensionCount] =
	{
		{"GLX_ARB_create_context_profile", true},
		{"GLX_EXT_swap_control", true},
		{"GLX_EXT_swap_control_tear", true}
	};

#endif


GraphicsCapabilities::GraphicsCapabilities()
{
	programBinaryFormatArray = nullptr;
}

GraphicsCapabilities::~GraphicsCapabilities()
{
	delete[] programBinaryFormatArray;
}


#if C4PS3 //[ 

			// -- PS3 code hidden --

#endif //]


NormalFrameBuffer::NormalFrameBuffer(int32 width, int32 height, unsigned_int32 mask)
{
	static const unsigned_int32 targetFormat[kRenderTargetCount] =
	{
		Render::kTextureRenderBufferRGBA8,			// kRenderTargetDisplay
		Render::kTextureRenderBufferRGBA8,			// kRenderTargetPrimary
		Render::kTextureRenderBufferRGBA8,			// kRenderTargetReflection
		Render::kTextureRenderBufferRGBA8,			// kRenderTargetRefraction
		Render::kTextureRenderBufferRGBA16F,		// kRenderTargetStructure
		Render::kTextureRenderBufferR8,				// kRenderTargetOcclusion1
		Render::kTextureRenderBufferR8				// kRenderTargetOcclusion2
	};

	Render::FrameBufferObject::Construct();

	renderTargetMask = mask;
	for (machine a = 0; a < kRenderTargetCount; a++)
	{
		if (mask & 1)
		{
			Render::TextureAllocationData	allocationData;

			Render::TextureObject *texture = &textureObject[a];
			texture->Construct(Render::kTextureTargetRectangle);

			allocationData.memorySize = nullptr;
			allocationData.format = targetFormat[a];
			allocationData.encoding = Render::kTextureEncodingLinear;
			allocationData.width = width;
			allocationData.height = height;
			allocationData.renderBuffer = true;

			texture->AllocateStorageRect(&allocationData);

			if (a == kRenderTargetDisplay)
			{
				texture->SetSWrapMode(Render::kWrapClampToBorder);
				texture->SetTWrapMode(Render::kWrapClampToBorder);
				texture->SetMaxAnisotropy(4.0F);
			}
			else if (a == kRenderTargetStructure)
			{
				texture->SetMinFilterMode(Render::kFilterNearest);
				texture->SetMagFilterMode(Render::kFilterNearest);
			}
		}

		mask >>= 1;
	}

	depthRenderBuffer.Construct();
	depthRenderBuffer.AllocateStorage(width, height, Render::kRenderBufferDepthStencil);

	SetDepthStencilRenderBuffer(&depthRenderBuffer);
	Render::ResetFrameBuffer();
}

NormalFrameBuffer::~NormalFrameBuffer()
{
	depthRenderBuffer.Destruct();

	unsigned_int32 mask = renderTargetMask;
	for (machine a = kRenderTargetCount - 1; a >= 0; a--)
	{
		if (mask & (1 << a))
		{
			textureObject[a].Destruct();
		}
	}

	Render::FrameBufferObject::Destruct();
}

void NormalFrameBuffer::Invalidate(void)
{
	unsigned_int32 mask = renderTargetMask;
	for (machine a = 0; a < kRenderTargetCount; a++)
	{
		if (mask & 1)
		{
			textureObject[a].InvalidateImage();
		}

		mask >>= 1;
	}
}


ShadowFrameBuffer::ShadowFrameBuffer(int32 width, int32 height)
{
	Render::TextureAllocationData	allocationData;

	Render::FrameBufferObject::Construct();
	textureObject.Construct(Render::kTextureTarget2D);

	allocationData.memorySize = nullptr;
	allocationData.format = Render::kTextureDepth;
	allocationData.encoding = Render::kTextureEncodingLinear;
	allocationData.width = width;
	allocationData.height = height;
	allocationData.renderBuffer = true;

	textureObject.AllocateStorage2D(&allocationData);
	textureObject.SetCompareFunc(Render::kShadowLessEqual);
	textureObject.SetBorderColor(Render::kTextureBorderWhite);

	SetTextureAnisotropy(TheGraphicsMgr->GetTextureFilterAnisotropy());

	textureObject.SetSWrapMode(Render::kWrapClampToBorder);
	textureObject.SetTWrapMode(Render::kWrapClampToBorder);

	SetDepthRenderTexture(&textureObject);
	Render::ResetFrameBuffer();
}

ShadowFrameBuffer::~ShadowFrameBuffer()
{
	textureObject.Destruct();
	Render::FrameBufferObject::Destruct();
}

void ShadowFrameBuffer::SetTextureAnisotropy(int32 anisotropy)
{
	const GraphicsCapabilities *capabilities = TheGraphicsMgr->GetCapabilities();
	if (capabilities->extensionFlag[kExtensionTextureFilterAnisotropic])
	{
		textureObject.SetMaxAnisotropy(Fmin((float) anisotropy, capabilities->maxTextureAnisotropy));
	}
}

void ShadowFrameBuffer::Invalidate(void)
{
	textureObject.InvalidateImage();
}


MultisampleFrameBuffer::MultisampleFrameBuffer(int32 width, int32 height, int32 sampleCount)
{
	Render::FrameBufferObject::Construct();

	colorRenderBuffer.Construct();
	unsigned_int32 format = Render::kRenderBufferRGBA8;
	colorRenderBuffer.AllocateMultisampleStorage(width, height, sampleCount, format);

	depthRenderBuffer.Construct();
	depthRenderBuffer.AllocateMultisampleStorage(width, height, sampleCount, Render::kRenderBufferDepthStencil);

	SetColorRenderBuffer(&colorRenderBuffer);
	SetDepthStencilRenderBuffer(&depthRenderBuffer);

	#if C4OPENGL

		GLint	samples;

		glGetIntegerv(GL_SAMPLES, &samples);
		sampleDivider = 1.0F / (float) samples;

	#else

		sampleDivider = 1.0F / (float) sampleCount;

	#endif

	Render::ResetFrameBuffer();
}

MultisampleFrameBuffer::~MultisampleFrameBuffer()
{
	depthRenderBuffer.Destruct();
	colorRenderBuffer.Destruct();
	Render::FrameBufferObject::Destruct();
}


GraphicsMgr::GraphicsMgr(int) :
		fullscreenVertexBuffer(kVertexBufferAttribute | kVertexBufferStatic),
		glowBloomVertexBuffer(kVertexBufferAttribute | kVertexBufferDynamic),
		processGridVertexBuffer(kVertexBufferAttribute | kVertexBufferStatic),
		processGridIndexBuffer(kVertexBufferIndex | kVertexBufferDynamic),
		textureDetailLevelObserver(this, &GraphicsMgr::HandleTextureDetailLevelEvent),
		paletteDetailLevelObserver(this, &GraphicsMgr::HandlePaletteDetailLevelEvent),
		textureAnisotropyObserver(this, &GraphicsMgr::HandleTextureAnisotropyEvent),
		renderNormalizeBumpsObserver(this, &GraphicsMgr::HandleRenderNormalizeBumpsEvent),
		renderParallaxMappingObserver(this, &GraphicsMgr::HandleRenderParallaxMappingEvent),
		renderHorizonMappingObserver(this, &GraphicsMgr::HandleRenderHorizonMappingEvent),
		renderTerrainBumpsObserver(this, &GraphicsMgr::HandleRenderTerrainBumpsEvent),
		renderStructureEffectsObserver(this, &GraphicsMgr::HandleRenderStructureEffectsEvent),
		renderAmbientOcclusionObserver(this, &GraphicsMgr::HandleRenderAmbientOcclusionEvent),
		postBrightnessObserver(this, &GraphicsMgr::HandlePostBrightnessEvent),
		postMotionBlurObserver(this, &GraphicsMgr::HandlePostMotionBlurEvent),
		postDistortionObserver(this, &GraphicsMgr::HandlePostDistortionEvent),
		postGlowBloomObserver(this, &GraphicsMgr::HandlePostGlowBloomEvent)
{
	driverVersion = 0;

	cameraObject = nullptr;
	cameraTransformable = nullptr;

	cameraSpaceTransform(3,0) = cameraSpaceTransform(3,1) = cameraSpaceTransform(3,2) = 0.0F;
	cameraSpaceTransform(3,3) = 1.0F;

	#if C4WINDOWS

		if (NvAPI_Initialize() == NVAPI_OK)
		{
			NV_DISPLAY_DRIVER_VERSION	version;

			version.version = NV_DISPLAY_DRIVER_VERSION_VER;
			if (NvAPI_GetDisplayDriverVersion(NVAPI_DEFAULT_HANDLE, &version) == NVAPI_OK)
			{
				driverVersion = version.drvVersion;
				if (driverVersion < 32018)
				{
					// GL_EXT_direct_state_access is broken in Nvidia drivers before version 320.18.

					GraphicsMgr::extensionData[kExtensionDirectStateAccess].enabled = false;
				}
			}

			NvAPI_Unload();
		}

	#endif
}

GraphicsMgr::~GraphicsMgr()
{
}

EngineResult GraphicsMgr::Construct(void)
{
	static const ConstPoint2D fullscreenVertex[3] = {{-1.0F, -3.0F}, {3.0F, 1.0F}, {-1.0F, 1.0F}};

	new(capabilities) GraphicsCapabilities;
	new(syncRenderSignal) Signal;

	int32 fullFrameWidth = TheDisplayMgr->GetFullFrameWidth();
	int32 fullFrameHeight = TheDisplayMgr->GetFullFrameHeight();
	int32 displayWidth = TheDisplayMgr->GetDisplayWidth();
	int32 displayHeight = TheDisplayMgr->GetDisplayHeight();
	int32 displaySamples = TheDisplayMgr->GetDisplaySamples();
	unsigned_int32 displayFlags = TheDisplayMgr->GetDisplayFlags();

	GraphicsResult result = InitializeGraphicsContext(fullFrameWidth, fullFrameHeight, displayWidth, displayHeight, displayFlags);
	if (result != kGraphicsOkay)
	{
		return (result);
	}

	bool extensionsAvailable = InitializeOpenglExtensions();
	UpdateLog();

	if (!extensionsAvailable)
	{
		TerminateGraphicsContext();
		return (kGraphicsNoHardware);
	}

	#if C4OPENGL

		Render::InitializeCoreOpenGL();

	#endif

	ShaderProgram::Initialize();
	MicrofacetAttribute::Initialize();

	dynamicShadowMapSize = Min(Min(capabilities->maxTextureSize, capabilities->maxRenderbufferSize) / kMaxShadowCascadeCount, 1024);
	targetDisableMask = 0;

	syncRenderObject = nullptr;
	syncLoadFlag = false;

	fogSpaceObject = nullptr;
	fogSpaceTransformable = nullptr;
	lightObject = nullptr;
	lightTransformable = nullptr;
	geometryTransformable = nullptr;
	geometryLightPosition.Set(0.0F, 0.0F, 0.0F, 1.0F);

	currentBlendState = kBlendReplace;
	currentStencilMode = kStencilNone;
	currentGraphicsState = kGraphicsDepthTestLess | kGraphicsCullFaceBack | kGraphicsFrontFaceCCW | kGraphicsReactivateTextures;
	currentRenderState = 0;
	currentMaterialState = 0;
	currentShaderType = kShaderAmbient;
	currentShaderVariant = kShaderVariantNormal;
	currentAmbientMode = kAmbientNormal;
	currentOcclusionQuery = nullptr;
	currentArrayState = 0;

	colorTransformFlags = 0;
	finalColorScale[0].Set(1.0F, 1.0F, 1.0F, 1.0F);
	finalColorBias.Set(0.0F, 0.0F, 0.0F, 0.0F);

	diagnosticFlags = 0;

	#if C4OPENGL

		glEnable(GL_SCISSOR_TEST);

		glActiveTexture(GL_TEXTURE0);
		glEnableVertexAttribArray(0);

		glPointParameteri(GL_POINT_SPRITE_COORD_ORIGIN, GL_LOWER_LEFT);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	#endif

	Render::EnableBlend();
	Render::EnableCullFace();
	Render::SetBlendFunc(Render::kBlendOne, Render::kBlendZero, Render::kBlendZero, Render::kBlendZero);
	Render::SetDepthFunc(Render::kDepthLess);
	Render::SetStencilFunc(Render::kStencilAlways, 0, ~0);
	Render::SetCullFace(Render::kCullBack);

	#if C4OPENGL && C4DEBUG && C4CREATE_DEBUG_CONTEXT

		if (capabilities->extensionFlag[kExtensionDebugOutput])
		{
			glDebugMessageCallbackARB(&DebugCallback, this);
		}

	#endif

	if (capabilities->extensionFlag[kExtensionTimerQuery])
	{
		for (machine a = 0; a < kGraphicsTimeIndexCount; a++)
		{
			timerQuery[a].Construct();
		}
	}

	if (capabilities->extensionFlag[kExtensionSampleShading])
	{
		Render::SetMinSampleShading(1.0F);
	}

	InitializeVariables();

	currentRenderTargetType = kRenderTargetNone;
	renderTargetHeight = displayHeight;

	genericFrameBuffer.Construct();

	unsigned_int32 mask = (1 << kRenderTargetPrimary) | (1 << kRenderTargetReflection) | (1 << kRenderTargetRefraction);

	#if C4OCULUS

		if (displayFlags & kDisplayOculus)
		{
			mask |= 1 << kRenderTargetDisplay;
		}

	#endif

	if (capabilities->capabilityFlag[kCapabilityFramebufferFloat])
	{
		if (renderOptionFlags & (kRenderOptionStructureEffects | kRenderOptionMotionBlur))
		{
			mask |= 1 << kRenderTargetStructure;
		}

		if (renderOptionFlags & kRenderOptionAmbientOcclusion)
		{
			mask |= (1 << kRenderTargetStructure) | (1 << kRenderTargetOcclusion1) | (1 << kRenderTargetOcclusion2);
		}
	}

	normalFrameBuffer = new NormalFrameBuffer(displayWidth, displayHeight, mask);

	if (!capabilities->extensionFlag[kExtensionFramebufferMultisample])
	{
		displaySamples = 1;
	}

	if (displaySamples > 1)
	{
		int32 samples = Min(displaySamples, capabilities->maxMultisampleSamples);
		multisampleFrameBuffer = new MultisampleFrameBuffer(displayWidth, displayHeight, samples);
	}
	else
	{
		multisampleFrameBuffer = nullptr;
	}

	shadowFrameBuffer = new ShadowFrameBuffer(dynamicShadowMapSize, dynamicShadowMapSize * kMaxShadowCascadeCount);

	float f = kMotionBlurBoxExpand / (float) displayWidth;
	motionBlurBoxLeftOffset = 0.5F - f;
	motionBlurBoxRightOffset = 0.5F + f;

	f = kMotionBlurBoxExpand / (float) displayHeight;
	motionBlurBoxBottomOffset = 0.5F - f;
	motionBlurBoxTopOffset = 0.5F + f;

	float scale = (float) displayWidth * 0.03125F;

	#if C4OPENGL

		Render::SetFragmentShaderParameter(kFragmentParamDistortionScale, scale, scale, 0.0F, 0.0F);

	#elif C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]

	nullTexture = Texture::Get(&nullTextureHeader, nullTextureImage);

	AmbientOutputProcess::Initialize();
	OcclusionPostProcess::Initialize();
	HorizonProcess::Initialize();

	fullscreenVertexBuffer.Establish(sizeof(Point2D) * 3, fullscreenVertex);

	glowBloomVertexBuffer.Establish(sizeof(Point2D) * 4);
	processGridVertexBuffer.Establish(sizeof(Point2D) * (kProcessGridWidth + 1) * (kProcessGridHeight + 1));
	processGridIndexBuffer.Establish(sizeof(Triangle) * kProcessGridWidth * kProcessGridHeight * 2);

	InitializeProcessGrid();
	InitializeActiveFlags();

	VertexBuffer::ReactivateAll();
	OcclusionQuery::ReactivateAll();

	if (cameraObject)
	{
		SetCamera(cameraObject, cameraTransformable);
	}

	return (kEngineOkay);
}

void GraphicsMgr::Destruct(void)
{
	if (TheMovieMgr)
	{
		TheMovieMgr->StopRecording();
	}

	if (TheWorldMgr)
	{
		VertexBuffer::SaveAll();
	}

	VertexBuffer::DeactivateAll();
	OcclusionQuery::DeactivateAll();
	Texture::DeactivateAll();
	ShaderData::Purge();

	processGridIndexBuffer.Establish(0);
	processGridVertexBuffer.Establish(0);
	glowBloomVertexBuffer.Establish(0);
	fullscreenVertexBuffer.Establish(0);

	HorizonProcess::Terminate();
	OcclusionPostProcess::Terminate();
	AmbientOutputProcess::Terminate();

	nullTexture->Release();

	delete shadowFrameBuffer;
	delete multisampleFrameBuffer;
	delete normalFrameBuffer;
	genericFrameBuffer.Destruct();

	if (capabilities->extensionFlag[kExtensionTimerQuery])
	{
		for (machine a = kGraphicsTimeIndexCount - 1; a >= 0; a--)
		{
			timerQuery[a].Destruct();
		}
	}

	MicrofacetAttribute::Terminate();
	ShaderProgram::Terminate();

	#if C4OPENGL

		extensionsString.Clear();
		Render::TerminateCoreOpenGL();

	#endif

	TerminateGraphicsContext();

	syncRenderSignal->~Signal();
	capabilities->~GraphicsCapabilities();
}

#if C4OPENGL && C4DEBUG && C4CREATE_DEBUG_CONTEXT

	void GraphicsMgr::DebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, void *userParam)
	{
		Engine::Report(String<>(message) += "<br/>\r\n", kReportLog);
	}

#endif

void GraphicsMgr::LogExtensions(const char *string)
{
	if (string)
	{
		while (*string != 0)
		{
			String<63>	line;

			const char *s = string;
			if (*s < 33)
			{
				string++;
				continue;
			}

			while (*s > 32)
			{
				s++;
			}

			int32 len = Min((int32) (s - string), 63);

			for (machine b = 0; b < len; b++)
			{
				line[b] = string[b];
			}

			line[len] = 0;
			string = s;

			Engine::Report(line, kReportLog);
			Engine::Report("<br/>\r\n", kReportLog);
		}
	}
}

void GraphicsMgr::UpdateLog(void) const
{
	static bool logUpdated = false;
	if (!logUpdated)
	{
		logUpdated = true;

		Engine::Report("Graphics Manager", kReportLog | kReportHeading);

		#if C4OPENGL

			Engine::Report("<table class=\"data\" cellspacing=\"0\" cellpadding=\"0\">\r\n", kReportLog);

			Engine::Report("<tr><th>GL_VENDOR</th><td>", kReportLog);
			Engine::Report(GetOpenGLVendor(), kReportLog);
			Engine::Report("</td></tr>\r\n", kReportLog);

			Engine::Report("<tr><th>GL_RENDERER</th><td>", kReportLog);
			Engine::Report(GetOpenGLRenderer(), kReportLog);
			Engine::Report("</td></tr>\r\n", kReportLog);

			Engine::Report("<tr><th>GL_VERSION</th><td>", kReportLog);
			Engine::Report(GetOpenGLVersion(), kReportLog);
			Engine::Report("</td></tr>\r\n", kReportLog);

			Engine::Report("<tr><th>GLSL Version</th><td>", kReportLog);
			Engine::Report(GetGLSLVersion(), kReportLog);
			Engine::Report("</td></tr>\r\n", kReportLog);

			if (driverVersion != 0)
			{
				Engine::Report("<tr><th>Driver version</th><td>", kReportLog);
				Engine::Report((String<63>(driverVersion / 100) += '.') += driverVersion % 100, kReportLog);
				Engine::Report("</td></tr>\r\n", kReportLog);
			}

			Engine::Report("<tr><th>GL_EXTENSIONS</th><td><div style=\"height: 128px; overflow: auto;\">\r\n", kReportLog);
			LogExtensions(extensionsString);

			#if C4WINDOWS

				Engine::Report("</div></td></tr>\r\n<tr><th>WGL_EXTENSIONS</th><td><div style=\"height: 128px; overflow: auto;\">\r\n", kReportLog);
				LogExtensions(windowSystemExtensionsString);

			#elif C4LINUX

				Engine::Report("</div></td></tr>\r\n<tr><th>GLX_EXTENSIONS</th><td><div style=\"height: 128px; overflow: auto;\">\r\n", kReportLog);
				LogExtensions(windowSystemExtensionsString);

			#endif

			Engine::Report("</div></td></tr>\r\n", kReportLog);

			Engine::Report("<tr><th>Texture limits</th><td>", kReportLog);
			Engine::Report("Max texture 2D size: ", kReportLog);
			Engine::Report(Text::IntegerToString(capabilities->maxTextureSize), kReportLog);
			Engine::Report("<br/>Max texture 3D size: ", kReportLog);
			Engine::Report(Text::IntegerToString(capabilities->max3DTextureSize), kReportLog);
			Engine::Report("<br/>Max texture cube size: ", kReportLog);
			Engine::Report(Text::IntegerToString(capabilities->maxCubeTextureSize), kReportLog);

			if (capabilities->extensionFlag[kExtensionTextureArray])
			{
				Engine::Report("<br/>Max array texture layers: ", kReportLog);
				Engine::Report(Text::IntegerToString(capabilities->maxArrayTextureLayers), kReportLog);
			}

			Engine::Report("<br/>Max texture lod bias: ", kReportLog);
			Engine::Report(Text::FloatToString(capabilities->maxTextureLodBias), kReportLog);

			if (capabilities->extensionFlag[kExtensionTextureFilterAnisotropic])
			{
				Engine::Report("<br/>Max texture anisotropy: ", kReportLog);
				Engine::Report(Text::FloatToString(capabilities->maxTextureAnisotropy), kReportLog);
			}

			Engine::Report("<br/>Max combined texture image units: ", kReportLog);
			Engine::Report(Text::IntegerToString(capabilities->maxCombinedTextureImageUnits), kReportLog);
			Engine::Report("</td></tr>\r\n", kReportLog);

			Engine::Report("<tr><th>Query object limits</th><td>", kReportLog);
			Engine::Report("Query counter bits: ", kReportLog);
			Engine::Report(Text::IntegerToString(capabilities->queryCounterBits), kReportLog);
			Engine::Report("</td></tr>\r\n", kReportLog);

			if (capabilities->extensionFlag[kExtensionFramebufferObject])
			{
				Engine::Report("<tr><th>Framebuffer object limits</th><td>", kReportLog);
				Engine::Report("Max framebuffer color attachments: ", kReportLog);
				Engine::Report(Text::IntegerToString(capabilities->maxColorAttachments), kReportLog);
				Engine::Report("<br/>Max framebuffer render buffer size: ", kReportLog);
				Engine::Report(Text::IntegerToString(capabilities->maxRenderbufferSize), kReportLog);

				if (capabilities->extensionFlag[kExtensionFramebufferMultisample])
				{
					Engine::Report("<br/>Max framebuffer samples: ", kReportLog);
					Engine::Report(Text::IntegerToString(capabilities->maxMultisampleSamples), kReportLog);
				}

				Engine::Report("</td></tr>\r\n", kReportLog);
			}

			Engine::Report("<tr><th>Vertex shader limits</th><td>", kReportLog);
			Engine::Report("Max vertex attribs: ", kReportLog);
			Engine::Report(Text::IntegerToString(capabilities->maxVertexAttribs), kReportLog);
			Engine::Report("<br/>Max varying components: ", kReportLog);
			Engine::Report(Text::IntegerToString(capabilities->maxVaryingComponents), kReportLog);
			Engine::Report("<br/>Max vertex uniform components: ", kReportLog);
			Engine::Report(Text::IntegerToString(capabilities->maxVertexUniformComponents), kReportLog);
			Engine::Report("<br/>Max vertex texture image units: ", kReportLog);
			Engine::Report(Text::IntegerToString(capabilities->maxVertexTextureImageUnits), kReportLog);
			Engine::Report("</td></tr>\r\n", kReportLog);

			Engine::Report("<tr><th>Fragment shader limits</th><td>", kReportLog);
			Engine::Report("Max fragment uniform components: ", kReportLog);
			Engine::Report(Text::IntegerToString(capabilities->maxFragmentUniformComponents), kReportLog);
			Engine::Report("<br/>Max fragment texture image units: ", kReportLog);
			Engine::Report(Text::IntegerToString(capabilities->maxFragmentTextureImageUnits), kReportLog);
			Engine::Report("</td></tr>\r\n", kReportLog);

			if (capabilities->extensionFlag[kExtensionGeometryShader4])
			{
				Engine::Report("<tr><th>Geometry shader limits</th><td>", kReportLog);
				Engine::Report("Max vertex varying components: ", kReportLog);
				Engine::Report(Text::IntegerToString(capabilities->maxVertexVaryingComponents), kReportLog);
				Engine::Report("<br/>Max geometry varying components: ", kReportLog);
				Engine::Report(Text::IntegerToString(capabilities->maxGeometryVaryingComponents), kReportLog);
				Engine::Report("<br/>Max geometry uniform components: ", kReportLog);
				Engine::Report(Text::IntegerToString(capabilities->maxGeometryUniformComponents), kReportLog);
				Engine::Report("<br/>Max geometry output vertices: ", kReportLog);
				Engine::Report(Text::IntegerToString(capabilities->maxGeometryOutputVertices), kReportLog);
				Engine::Report("<br/>Max geometry total output components: ", kReportLog);
				Engine::Report(Text::IntegerToString(capabilities->maxGeometryTotalOutputComponents), kReportLog);
				Engine::Report("<br/>Max geometry texture image units: ", kReportLog);
				Engine::Report(Text::IntegerToString(capabilities->maxGeometryTextureImageUnits), kReportLog);
				Engine::Report("</td></tr>\r\n", kReportLog);
			}

			if (capabilities->extensionFlag[kExtensionGetProgramBinary])
			{
				Engine::Report("<tr><th>Program binary formats</th><td>", kReportLog);

				int32 count = capabilities->programBinaryFormats;
				for (machine a = 0; a < count; a++)
				{
					Engine::Report((String<31>("0x") += Text::IntegerToHexString8(capabilities->programBinaryFormatArray[a])) += "<br/>", kReportLog);
				}

				Engine::Report("</td></tr>\r\n", kReportLog);
			}

			if (capabilities->extensionFlag[kExtensionTransformFeedback])
			{
				Engine::Report("<tr><th>Transform feedback limits</th><td>", kReportLog);
				Engine::Report("Max feedback interleaved components: ", kReportLog);
				Engine::Report(Text::IntegerToString(capabilities->maxTransformFeedbackInterleavedComponents), kReportLog);
				Engine::Report("<br/>Max feedback separate components: ", kReportLog);
				Engine::Report(Text::IntegerToString(capabilities->maxTransformFeedbackSeparateComponents), kReportLog);
				Engine::Report("<br/>Max feedback separate attributes: ", kReportLog);
				Engine::Report(Text::IntegerToString(capabilities->maxTransformFeedbackSeparateAttribs), kReportLog);
				Engine::Report("</td></tr>\r\n", kReportLog);
			}

			Engine::Report("</table>\r\n", kReportLog);

		#endif
	}
}

GraphicsResult GraphicsMgr::InitializeGraphicsContext(int32 frameWidth, int32 frameHeight, int32 displayWidth, int32 displayHeight, unsigned_int32 displayFlags)
{
	Render::Initialize();

	#if C4WINDOWS

		static const int formatAttributes[] =
		{
			WGL_SUPPORT_OPENGL_ARB, true,
			WGL_DRAW_TO_WINDOW_ARB, true,
			WGL_DOUBLE_BUFFER_ARB, true,
			WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
			WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
			WGL_COLOR_BITS_ARB, 24,
			WGL_ALPHA_BITS_ARB, 8,
			WGL_DEPTH_BITS_ARB, 0,
			WGL_STENCIL_BITS_ARB, 0,
			0, 0
		};

		static const int contextAttributes[] =
		{
			WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
			WGL_CONTEXT_MINOR_VERSION_ARB, 3,
			WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,

			#if C4DEBUG && C4CREATE_DEBUG_CONTEXT

				WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_DEBUG_BIT_ARB,

			#endif

			0, 0
		};

		PIXELFORMATDESCRIPTOR	formatDescriptor;
		UINT					formatCount;
		int						pixelFormat;

		MemoryMgr::ClearMemory(&formatDescriptor, sizeof(PIXELFORMATDESCRIPTOR));
		formatDescriptor.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		formatDescriptor.nVersion = 1;

		formatDescriptor.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_GENERIC_ACCELERATED | PFD_DOUBLEBUFFER | PFD_SWAP_EXCHANGE;
		if (!(displayFlags & kDisplayFullscreen))
		{
			formatDescriptor.dwFlags |= PFD_SUPPORT_COMPOSITION;
		}

		formatDescriptor.iPixelType = PFD_TYPE_RGBA;
		formatDescriptor.cColorBits = 24;
		formatDescriptor.cAlphaBits = 8;
		formatDescriptor.cDepthBits = 0;
		formatDescriptor.cStencilBits = 0;
		formatDescriptor.iLayerType = PFD_MAIN_PLANE;

		InitializeWglExtensions(&formatDescriptor);

		if ((!capabilities->windowSystemExtensionFlag[kWindowSystemExtensionCreateContext]) || (!capabilities->windowSystemExtensionFlag[kWindowSystemExtensionPixelFormat]))
		{
			return (kGraphicsContextFailed);
		}

		deviceContext = GetDC(TheDisplayMgr->GetDisplayWindow());

		if ((!wglChoosePixelFormatARB(deviceContext, formatAttributes, nullptr, 1, &pixelFormat, &formatCount)) || (formatCount == 0))
		{
			ReleaseDC(TheDisplayMgr->GetDisplayWindow(), deviceContext);
			return (kGraphicsNoHardware);
		}

		if (!SetPixelFormat(deviceContext, pixelFormat, &formatDescriptor))
		{
			ReleaseDC(TheDisplayMgr->GetDisplayWindow(), deviceContext);
			return (kGraphicsFormatFailed);
		}

		openglContext = wglCreateContextAttribsARB(deviceContext, nullptr, contextAttributes);
		if (!openglContext)
		{
			ReleaseDC(TheDisplayMgr->GetDisplayWindow(), deviceContext);
			return (kGraphicsContextFailed);
		}

		if (!wglMakeCurrent(deviceContext, openglContext))
		{
			wglDeleteContext(openglContext);
			ReleaseDC(TheDisplayMgr->GetDisplayWindow(), deviceContext);
			return (kGraphicsContextFailed);
		}

		if (wglGetExtensionsStringARB)
		{
			windowSystemExtensionsString = wglGetExtensionsStringARB(deviceContext);
		}

	#elif C4MACOS

		static NSOpenGLPixelFormatAttribute formatAttributes[] =
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core,
			NSOpenGLPFAMinimumPolicy,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFADoubleBuffer,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADepthSize, 0,
			NSOpenGLPFAStencilSize, 0,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes: formatAttributes];
		if (!pixelFormat)
		{
			return (kGraphicsNoHardware);
		}

		openglContext = [[NSOpenGLContext alloc] initWithFormat: pixelFormat shareContext: nil];
		[pixelFormat release];

		if (!openglContext)
		{
			return (kGraphicsContextFailed);
		}

		if (displayFlags & kDisplayFullscreen)
		{
			CGLContextObj contextObject = (CGLContextObj) [openglContext CGLContextObj];
			GLint size[2] = {frameWidth, frameHeight};
			CGLSetParameter(contextObject, kCGLCPSurfaceBackingSize, size);
			CGLEnable(contextObject, kCGLCESurfaceBackingSize);
		}

		[openglContext makeCurrentContext];

		NSOpenGLView *windowView = TheDisplayMgr->GetWindowView();
		[windowView setOpenGLContext: openglContext];
		[openglContext setView: windowView];

		openglBundle = TheEngine->GetOpenGLBundle();

	#elif C4LINUX

		static const int formatAttributes[] =
		{
			GLX_DOUBLEBUFFER, True,
			GLX_RED_SIZE, 8,
			GLX_GREEN_SIZE, 8,
			GLX_BLUE_SIZE, 8,
			GLX_ALPHA_SIZE, 8,
			GLX_DEPTH_SIZE, 0,
			GLX_STENCIL_SIZE, 0,
			GLX_RENDER_TYPE, GLX_RGBA_BIT,
			GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
			GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
			None
		};

		static const int contextAttributes[] =
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
			GLX_CONTEXT_MINOR_VERSION_ARB, 3,
			GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,

			#if C4DEBUG && C4CREATE_DEBUG_CONTEXT

				GLX_CONTEXT_FLAGS_ARB, GLX_CONTEXT_DEBUG_BIT_ARB,

			#endif

			0, 0
		};

		int						configCount;
		XSetWindowAttributes	windowAttributes;

		openglDisplay = TheEngine->GetEngineDisplay();

		InitializeGlxExtensions();
		if (!capabilities->windowSystemExtensionFlag[kWindowSystemExtensionCreateContextProfile])
		{
			return (kGraphicsContextFailed);
		}

		GLXFBConfig *configList = glXChooseFBConfig(openglDisplay, DefaultScreen(openglDisplay), formatAttributes, &configCount);
		if ((!configList) || (configCount < 1))
		{
			return (kGraphicsFormatFailed);
		}

		XVisualInfo *visualInfo = glXGetVisualFromFBConfig(openglDisplay, configList[0]);
		if (!visualInfo)
		{
			return (kGraphicsFormatFailed);
		}

		::Window engineWindow = TheEngine->GetEngineWindow();
		openglColormap = XCreateColormap(openglDisplay, engineWindow, visualInfo->visual, AllocNone);

		windowAttributes.override_redirect = true;
		windowAttributes.colormap = openglColormap;
		openglWindow = XCreateWindow(openglDisplay, engineWindow, 0, 0, frameWidth, frameHeight, 0, visualInfo->depth, InputOutput, visualInfo->visual, CWOverrideRedirect | CWColormap, &windowAttributes);

		openglContext = glXCreateContextAttribsARB(openglDisplay, configList[0], nullptr, true, contextAttributes);

		XFree(visualInfo);
		XFree(configList);

		if (!openglContext)
		{
			XDestroyWindow(openglDisplay, openglWindow);
			XFreeColormap(openglDisplay, openglColormap);
			return (kGraphicsContextFailed);
		}

		if (!glXMakeCurrent(openglDisplay, openglWindow, openglContext))
		{
			XDestroyWindow(openglDisplay, openglWindow);
			XFreeColormap(openglDisplay, openglColormap);
			return (kGraphicsContextFailed);
		}

		XMapWindow(openglDisplay, openglWindow);

	#elif C4PS3 //[ 

			// -- PS3 code hidden --

	#elif C4ORBIS //[ 

			// -- Orbis code hidden --

	#endif //]

	SetDisplaySyncMode(&displayFlags);
	DetermineHardwareSpeed();

	return (kEngineOkay);
}

void GraphicsMgr::TerminateGraphicsContext(void)
{
	#if C4WINDOWS

		wglMakeCurrent(nullptr, nullptr);
		wglDeleteContext(openglContext);
		ReleaseDC(TheDisplayMgr->GetDisplayWindow(), deviceContext);

	#elif C4MACOS

		[NSOpenGLContext clearCurrentContext];
		[openglContext release];

	#elif C4LINUX

		glXMakeCurrent(openglDisplay, None, nullptr);
		glXDestroyContext(openglDisplay, openglContext);

		XDestroyWindow(openglDisplay, openglWindow);
		XFreeColormap(openglDisplay, openglColormap);

	#elif C4ORBIS //[ 

			// -- Orbis code hidden --

	#elif C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]

	Render::Terminate();
}

void GraphicsMgr::SetDisplaySyncMode(const unsigned_int32 *displayFlags)
{
	unsigned_int32 flags = *displayFlags;

	#if C4WINDOWS

		if (capabilities->windowSystemExtensionFlag[kWindowSystemExtensionSwapControl])
		{
			int32 swapInterval = ((flags & kDisplayRefreshSync) != 0);
			if ((capabilities->windowSystemExtensionFlag[kWindowSystemExtensionSwapControlTear]) && (flags & kDisplaySyncTear))
			{
				swapInterval = -swapInterval;
			}

			wglSwapIntervalEXT(swapInterval);
		}

	#elif C4MACOS

		GLint swapInterval = ((flags & kDisplayRefreshSync) != 0);
		[openglContext setValues: &swapInterval forParameter: NSOpenGLCPSwapInterval];

	#elif C4LINUX

		if (capabilities->windowSystemExtensionFlag[kWindowSystemExtensionSwapControl])
		{
			int32 swapInterval = ((flags & kDisplayRefreshSync) != 0);
			if ((capabilities->windowSystemExtensionFlag[kWindowSystemExtensionSwapControlTear]) && (flags & kDisplaySyncTear))
			{
				swapInterval = -swapInterval;
			}

			glXSwapIntervalEXT(openglDisplay, openglWindow, swapInterval);
		}

	#elif C4ORBIS //[ 

			// -- Orbis code hidden --

	#elif C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]
}

void GraphicsMgr::DetermineHardwareSpeed(void)
{
	#if C4ORBIS //[ 

			// -- Orbis code hidden --

	#elif C4PS3 //[ 

			// -- PS3 code hidden --

	#else //]

		capabilities->hardwareSpeed = 0;

		const char *renderer = GetOpenGLRenderer();
		for (machine a = 0;; a++)
		{
			unsigned_int32 c = renderer[a];
			if (c == 0)
			{
				break;
			}

			if (c - '0' < 10U)
			{
				c = renderer[a + 1];
				if ((c - '0' < 10U) && ((unsigned_int32) renderer[a + 2] - '0' < 10U))
				{
					if (c >= '5')
					{
						capabilities->hardwareSpeed = (c < '8') ? 1 : 2;
					}

					break;
				}
			}
		}

	#endif
}

#if C4WINDOWS

	void GraphicsMgr::InitializeWglExtensions(PIXELFORMATDESCRIPTOR *formatDescriptor)
	{
		static const wchar_t name[] = L"wgl";

		WNDCLASSEXW		windowClass;

		HINSTANCE instance = TheEngine->GetInstance();

		windowClass.cbSize = sizeof(WNDCLASSEXW);
		windowClass.style = CS_NOCLOSE | CS_OWNDC;
		windowClass.lpfnWndProc = &WglWindowProc;
		windowClass.cbClsExtra = 0;
		windowClass.cbWndExtra = 0;
		windowClass.hInstance = instance;
		windowClass.hIcon = nullptr;
		windowClass.hCursor = nullptr;
		windowClass.hbrBackground = nullptr;
		windowClass.lpszMenuName = nullptr;
		windowClass.lpszClassName = name;
		windowClass.hIconSm = nullptr;

		RegisterClassExW(&windowClass);
		HWND window = CreateWindowExW(0, name, name, WS_POPUP, 0, 0, 32, 32, nullptr, nullptr, instance, formatDescriptor);
		DestroyWindow(window);
		UnregisterClassW(name, instance);
	}

	LRESULT CALLBACK GraphicsMgr::WglWindowProc(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
	{
		if (message == WM_CREATE)
		{
			TheGraphicsMgr->windowSystemExtensionsString = nullptr;
			bool *windowSystemExtensionFlag = TheGraphicsMgr->capabilities->windowSystemExtensionFlag;
			for (machine a = 0; a < kWindowSystemExtensionCount; a++)
			{
				windowSystemExtensionFlag[a] = false;
			}

			const CREATESTRUCTW *createStruct = (CREATESTRUCTW *) lparam;
			const PIXELFORMATDESCRIPTOR *formatDescriptor = (PIXELFORMATDESCRIPTOR *) createStruct->lpCreateParams;

			HDC deviceContext = GetDC(window);
			int pixelFormat = ChoosePixelFormat(deviceContext, formatDescriptor);
			if (pixelFormat != 0)
			{
				if (SetPixelFormat(deviceContext, pixelFormat, formatDescriptor))
				{
					HGLRC openglContext = wglCreateContext(deviceContext);
					if (wglMakeCurrent(deviceContext, openglContext))
					{
						GLGETEXTFUNC(wglGetExtensionsStringARB);
						if (wglGetExtensionsStringARB)
						{
							const char *string = wglGetExtensionsStringARB(deviceContext);

							const WindowSystemExtensionData *data = windowSystemExtensionData;
							for (machine a = 0; a < kWindowSystemExtensionCount; a++)
							{
								windowSystemExtensionFlag[a] = ((data->enabled) && (Text::FindText(string, data->name) >= 0));
								data++;
							}

							if (windowSystemExtensionFlag[kWindowSystemExtensionCreateContext])
							{
								GLGETEXTFUNC(wglCreateContextAttribsARB);
							}

							if (windowSystemExtensionFlag[kWindowSystemExtensionPixelFormat])
							{
								GLGETEXTFUNC(wglChoosePixelFormatARB);
							}

							if (windowSystemExtensionFlag[kWindowSystemExtensionSwapControl])
							{
								GLGETEXTFUNC(wglSwapIntervalEXT);
							}
						}

						wglMakeCurrent(nullptr, nullptr);
					}

					wglDeleteContext(openglContext);
				}
			}

			ReleaseDC(window, deviceContext);
			return (0);
		}

		return (DefWindowProcW(window, message, wparam, lparam));
	}

#elif C4LINUX

	void GraphicsMgr::InitializeGlxExtensions(void)
	{
		const char *string = glXQueryExtensionsString(openglDisplay, DefaultScreen(openglDisplay));
		windowSystemExtensionsString = string;

		bool *windowSystemExtensionFlag = capabilities->windowSystemExtensionFlag;
		const WindowSystemExtensionData *data = windowSystemExtensionData;
		for (machine a = 0; a < kWindowSystemExtensionCount; a++)
		{
			windowSystemExtensionFlag[a] = ((data->enabled) && (Text::FindText(string, data->name) >= 0));
			data++;
		}

		if (windowSystemExtensionFlag[kWindowSystemExtensionCreateContextProfile])
		{
			GLGETEXTFUNC(glXCreateContextAttribsARB);
		}

		if (windowSystemExtensionFlag[kWindowSystemExtensionSwapControl])
		{
			GLGETEXTFUNC(glXSwapIntervalEXT);
		}
	}

#endif

bool GraphicsMgr::InitializeOpenglExtensions(void)
{
	#if C4OPENGL

		GLint	majorVersion;
		GLint	minorVersion;
		GLint	extensionCount;

		glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
		glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

		unsigned_int32 version = (majorVersion << 8) | (minorVersion << 4);
		capabilities->openglVersion = version;

		if (version < 0x0320)
		{
			return (false);
		}

		bool *extensionFlag = capabilities->extensionFlag;
		for (machine a = 0; a < kGraphicsExtensionCount; a++)
		{
			extensionFlag[a] = false;
		}

		bool *capabilityFlag = capabilities->capabilityFlag;
		for (machine a = 0; a < kGraphicsCapabilityCount; a++)
		{
			capabilityFlag[a] = false;
		}

		const GraphicsExtensionData *data = extensionData;
		for (machine a = 0; a < kGraphicsExtensionCount; a++)
		{
			if ((version >= data->version) && (data->enabled))
			{
				extensionFlag[a] = true;
			}

			data++;
		}

		glGetIntegerv(GL_MAX_SAMPLES, reinterpret_cast<GLint *>(&capabilities->maxMultisampleSamples));
		glGetIntegerv(GL_MAX_TEXTURE_SIZE, reinterpret_cast<GLint *>(&capabilities->maxTextureSize));
		glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE, reinterpret_cast<GLint *>(&capabilities->max3DTextureSize));
		glGetIntegerv(GL_MAX_CUBE_MAP_TEXTURE_SIZE, reinterpret_cast<GLint *>(&capabilities->maxCubeTextureSize));
		glGetFloatv(GL_MAX_TEXTURE_LOD_BIAS, &capabilities->maxTextureLodBias);
		glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, reinterpret_cast<GLint *>(&capabilities->maxCombinedTextureImageUnits));

		GLGETCOREFUNC(glGetStringi);

		glGetIntegerv(GL_NUM_EXTENSIONS, &extensionCount);
		for (machine a = 0; a < extensionCount; a++)
		{
			const char *string = reinterpret_cast<const char *>(glGetStringi(GL_EXTENSIONS, (GLuint) a));
			(extensionsString += string) += ' ';

			data = extensionData;
			for (machine b = 0; b < kGraphicsExtensionCount; b++)
			{
				if ((!extensionFlag[b]) && (data->enabled))
				{
					const char *name1 = data->name1;
					const char *name2 = data->name2;

					if ((Text::CompareText(string, name1)) || ((name2) && (Text::CompareText(string, name2))))
					{
						extensionFlag[b] = true;
						break;
					}
				}

				data++;
			}
		}

		data = extensionData;
		for (machine a = 0; a < kGraphicsExtensionCount; a++)
		{
			if ((data->required) && (!extensionFlag[a]))
			{
				return (false);
			}

			data++;
		}

		#if C4WINDOWS || C4LINUX

			C4::InitializeOpenglExtensions(capabilities);

		#elif C4MACOS || C4IOS

			C4::InitializeOpenglExtensions(capabilities, openglBundle);

		#endif

		glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, reinterpret_cast<GLint *>(&capabilities->maxColorAttachments));
		glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE, reinterpret_cast<GLint *>(&capabilities->maxRenderbufferSize));
		glGetQueryiv(GL_SAMPLES_PASSED, GL_QUERY_COUNTER_BITS, reinterpret_cast<GLint *>(&capabilities->queryCounterBits));

		glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, reinterpret_cast<GLint *>(&capabilities->maxVertexAttribs));
		glGetIntegerv(GL_MAX_VARYING_COMPONENTS, reinterpret_cast<GLint *>(&capabilities->maxVaryingComponents));
		glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS, reinterpret_cast<GLint *>(&capabilities->maxVertexUniformComponents));
		glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, reinterpret_cast<GLint *>(&capabilities->maxVertexTextureImageUnits));

		glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS, reinterpret_cast<GLint *>(&capabilities->maxFragmentUniformComponents));
		glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, reinterpret_cast<GLint *>(&capabilities->maxFragmentTextureImageUnits));

		if (extensionFlag[kExtensionTextureFilterAnisotropic])
		{
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &capabilities->maxTextureAnisotropy);
		}

		if ((extensionFlag[kExtensionColorBufferFloat]) && (extensionFlag[kExtensionHalfFloatPixel]))
		{
			capabilityFlag[kCapabilityFramebufferFloat] = true;
		}

		if (extensionFlag[kExtensionTextureArray])
		{
			glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, reinterpret_cast<GLint *>(&capabilities->maxArrayTextureLayers));
		}

		if ((extensionFlag[kExtensionTextureRG]) && (extensionFlag[kExtensionTextureSwizzle]))
		{
			capabilityFlag[kCapabilityTextureSwizzle] = true;
		}

		if ((extensionFlag[kExtensionShaderBufferLoad]) && (extensionFlag[kExtensionVertexBufferUnifiedMemory]))
		{
			capabilityFlag[kCapabilityUnifiedMemory] = true;
		}

		if (capabilities->extensionFlag[kExtensionGeometryShader4])
		{
			glGetIntegerv(GL_MAX_VERTEX_VARYING_COMPONENTS, reinterpret_cast<GLint *>(&capabilities->maxVertexVaryingComponents));
			glGetIntegerv(GL_MAX_GEOMETRY_VARYING_COMPONENTS, reinterpret_cast<GLint *>(&capabilities->maxGeometryVaryingComponents));
			glGetIntegerv(GL_MAX_GEOMETRY_UNIFORM_COMPONENTS, reinterpret_cast<GLint *>(&capabilities->maxGeometryUniformComponents));
			glGetIntegerv(GL_MAX_GEOMETRY_OUTPUT_VERTICES, reinterpret_cast<GLint *>(&capabilities->maxGeometryOutputVertices));
			glGetIntegerv(GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS, reinterpret_cast<GLint *>(&capabilities->maxGeometryTotalOutputComponents));
			glGetIntegerv(GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS, reinterpret_cast<GLint *>(&capabilities->maxGeometryTextureImageUnits));
		}

		if (extensionFlag[kExtensionGetProgramBinary])
		{
			glGetIntegerv(GL_NUM_PROGRAM_BINARY_FORMATS, reinterpret_cast<GLint *>(&capabilities->programBinaryFormats));
			capabilities->programBinaryFormatArray = new int32[capabilities->programBinaryFormats];
			glGetIntegerv(GL_PROGRAM_BINARY_FORMATS, reinterpret_cast<GLint *>(capabilities->programBinaryFormatArray));
		}

		if (extensionFlag[kExtensionTransformFeedback])
		{
			glGetIntegerv(GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS, reinterpret_cast<GLint *>(&capabilities->maxTransformFeedbackInterleavedComponents));
			glGetIntegerv(GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS, reinterpret_cast<GLint *>(&capabilities->maxTransformFeedbackSeparateComponents));
			glGetIntegerv(GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS, reinterpret_cast<GLint *>(&capabilities->maxTransformFeedbackSeparateAttribs));
		}

	#elif C4ORBIS //[ 

			// -- Orbis code hidden --

	#elif C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]

	return (true);
}

void GraphicsMgr::InitializeVariables(void)
{
	unsigned_int32 flags = 0;
	unsigned_int32 disableFlags = kRenderOptionMotionBlur;

	unsigned_int32 speed = capabilities->hardwareSpeed;
	if (speed < 2)
	{
		disableFlags = kRenderOptionHorizonMapping | kRenderOptionStructureEffects | kRenderOptionAmbientOcclusion | kRenderOptionDistortion | kRenderOptionGlowBloom;
		if (speed < 1)
		{
			disableFlags |= kRenderOptionNormalizeBumps | kRenderOptionParallaxMapping | kRenderOptionTerrainBumps;
		}
	}

	Variable *normalize = TheEngine->InitVariable("renderNormalizeBumps", (disableFlags & kRenderOptionNormalizeBumps) ? "0" : "1", kVariablePermanent);
	normalize->AddObserver(&renderNormalizeBumpsObserver);
	if (normalize->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionNormalizeBumps;
	}

	Variable *parallax = TheEngine->InitVariable("renderParallaxMapping", (disableFlags & kRenderOptionParallaxMapping) ? "0" : "1", kVariablePermanent);
	parallax->AddObserver(&renderParallaxMappingObserver);
	if (parallax->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionParallaxMapping;
	}

	Variable *horizon = TheEngine->InitVariable("renderHorizonMapping", (disableFlags & kRenderOptionHorizonMapping) ? "0" : "1", kVariablePermanent);
	horizon->AddObserver(&renderHorizonMappingObserver);
	if (horizon->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionHorizonMapping;
	}

	Variable *terrain = TheEngine->InitVariable("renderTerrainBumps", (disableFlags & kRenderOptionTerrainBumps) ? "0" : "1", kVariablePermanent);
	terrain->AddObserver(&renderTerrainBumpsObserver);
	if (terrain->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionTerrainBumps;
	}

	TheEngine->InitVariable("textureDetailLevel", "1", kVariablePermanent, &textureDetailLevelObserver);
	TheEngine->InitVariable("paletteDetailLevel", "0", kVariablePermanent, &paletteDetailLevelObserver);

	Variable *anisotropy = TheEngine->InitVariable("textureAnisotropy", (speed >= 1) ? "4" : "1", kVariablePermanent);
	textureFilterAnisotropy = anisotropy->GetIntegerValue();
	anisotropy->AddObserver(&textureAnisotropyObserver);

	Variable *structure = TheEngine->InitVariable("renderStructureEffects", (disableFlags & kRenderOptionStructureEffects) ? "0" : "1", kVariablePermanent);
	structure->AddObserver(&renderStructureEffectsObserver);
	if (structure->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionStructureEffects;
	}

	Variable *occlusion = TheEngine->InitVariable("renderAmbientOcclusion", (disableFlags & kRenderOptionAmbientOcclusion) ? "0" : "1", kVariablePermanent);
	occlusion->AddObserver(&renderAmbientOcclusionObserver);
	if (occlusion->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionAmbientOcclusion;
	}

	TheEngine->InitVariable("postBrightness", "1.0", kVariablePermanent, &postBrightnessObserver);

	Variable *motionBlur = TheEngine->InitVariable("postMotionBlur", "0", kVariablePermanent);
	motionBlur->AddObserver(&postMotionBlurObserver);
	if (motionBlur->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionMotionBlur;
	}

	Variable *distortion = TheEngine->InitVariable("postDistortion", (disableFlags & kRenderOptionDistortion) ? "0" : "1", kVariablePermanent);
	distortion->AddObserver(&postDistortionObserver);
	if (distortion->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionDistortion;
	}

	Variable *glowBloom = TheEngine->InitVariable("postGlowBloom", (disableFlags & kRenderOptionGlowBloom) ? "0" : "1", kVariablePermanent);
	glowBloom->AddObserver(&postGlowBloomObserver);
	if (glowBloom->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionGlowBloom;
	}

	renderOptionFlags = flags;
}

void GraphicsMgr::HandleTextureDetailLevelEvent(Variable *variable)
{
	textureDetailLevel = MaxZero(Min(variable->GetIntegerValue(), 2));
	currentGraphicsState |= kGraphicsReactivateTextures;
	Texture::DeactivateAll();
}

void GraphicsMgr::HandlePaletteDetailLevelEvent(Variable *variable)
{
	paletteDetailLevel = MaxZero(Min(variable->GetIntegerValue(), 2));
	currentGraphicsState |= kGraphicsReactivateTextures;
	Texture::DeactivateAll();
}

void GraphicsMgr::HandleTextureAnisotropyEvent(Variable *variable)
{
	textureFilterAnisotropy = variable->GetIntegerValue();
	currentGraphicsState |= kGraphicsReactivateTextures;
	Texture::DeactivateAll();

	if (shadowFrameBuffer)
	{
		shadowFrameBuffer->SetTextureAnisotropy(textureFilterAnisotropy);
	}
}

void GraphicsMgr::HandleRenderNormalizeBumpsEvent(Variable *variable)
{
	unsigned_int32 flags = renderOptionFlags;

	if (variable->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionNormalizeBumps;
	}
	else
	{
		flags &= ~kRenderOptionNormalizeBumps;
	}

	renderOptionFlags = flags;
	ResetShaders();
}

void GraphicsMgr::HandleRenderParallaxMappingEvent(Variable *variable)
{
	unsigned_int32 flags = renderOptionFlags;

	if (variable->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionParallaxMapping;
	}
	else
	{
		flags &= ~kRenderOptionParallaxMapping;
	}

	renderOptionFlags = flags;
	ResetShaders();
}

void GraphicsMgr::HandleRenderHorizonMappingEvent(Variable *variable)
{
	unsigned_int32 flags = renderOptionFlags;

	if (variable->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionHorizonMapping;
	}
	else
	{
		flags &= ~kRenderOptionHorizonMapping;
	}

	renderOptionFlags = flags;
	ResetShaders();
}

void GraphicsMgr::HandleRenderTerrainBumpsEvent(Variable *variable)
{
	unsigned_int32 flags = renderOptionFlags;

	if (variable->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionTerrainBumps;
	}
	else
	{
		flags &= ~kRenderOptionTerrainBumps;
	}

	renderOptionFlags = flags;
	ResetShaders();
}

void GraphicsMgr::HandleRenderStructureEffectsEvent(Variable *variable)
{
	unsigned_int32 flags = renderOptionFlags;

	if (variable->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionStructureEffects;
	}
	else
	{
		flags &= ~kRenderOptionStructureEffects;
	}

	renderOptionFlags = flags;
	InitializeActiveFlags();
	ResetShaders();
}

void GraphicsMgr::HandleRenderAmbientOcclusionEvent(Variable *variable)
{
	unsigned_int32 flags = renderOptionFlags;

	if (variable->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionAmbientOcclusion;
	}
	else
	{
		flags &= ~kRenderOptionAmbientOcclusion;
	}

	renderOptionFlags = flags;
	InitializeActiveFlags();
	ResetShaders();
}

void GraphicsMgr::HandlePostBrightnessEvent(Variable *variable)
{
	brightnessMultiplier = variable->GetFloatValue();
}

void GraphicsMgr::HandlePostMotionBlurEvent(Variable *variable)
{
	unsigned_int32 flags = renderOptionFlags;

	if (variable->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionMotionBlur;
	}
	else
	{
		flags &= ~kRenderOptionMotionBlur;
	}

	renderOptionFlags = flags;
	InitializeActiveFlags();
}

void GraphicsMgr::HandlePostDistortionEvent(Variable *variable)
{
	unsigned_int32 flags = renderOptionFlags;

	if (variable->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionDistortion;
	}
	else
	{
		flags &= ~kRenderOptionDistortion;
	}

	renderOptionFlags = flags;
	InitializeActiveFlags();
}

void GraphicsMgr::HandlePostGlowBloomEvent(Variable *variable)
{
	unsigned_int32 flags = renderOptionFlags;

	if (variable->GetIntegerValue() != 0)
	{
		flags |= kRenderOptionGlowBloom;
	}
	else
	{
		flags &= ~kRenderOptionGlowBloom;
	}

	renderOptionFlags = flags;
	InitializeActiveFlags();
}

void GraphicsMgr::InitializeProcessGrid(void)
{
	volatile Point2D *restrict vertex = processGridVertexBuffer.BeginUpdate<Point2D>();

	float w = 2.0F / (float) kProcessGridWidth;
	float h = 2.0F / (float) kProcessGridHeight;

	for (machine j = 0; j < kProcessGridHeight; j++)
	{
		float y = (float) j * h - 1.0F;

		for (machine i = 0; i < kProcessGridWidth; i++)
		{
			vertex->Set((float) i * w - 1.0F, y);
			vertex++;
		}

		vertex->Set(1.0F, y);
		vertex++;
	}

	for (machine i = 0; i < kProcessGridWidth; i++)
	{
		vertex->Set((float) i * w - 1.0F, 1.0F);
		vertex++;
	}

	vertex->Set(1.0F, 1.0F);

	processGridVertexBuffer.EndUpdate();
}

void GraphicsMgr::InitializeActiveFlags(void)
{
	unsigned_int32 flags = 0;

	if ((diagnosticFlags & kDiagnosticTimer) && (capabilities->extensionFlag[kExtensionTimerQuery]))
	{
		flags |= kGraphicsActiveTimer;
	}

	if ((renderOptionFlags & (kRenderOptionStructureEffects | kRenderOptionAmbientOcclusion | kRenderOptionMotionBlur)) != 0)
	{
		unsigned_int32 mask = normalFrameBuffer->GetRenderTargetMask();
		if ((mask & (1 << kRenderTargetStructure)) != 0)
		{
			flags |= kGraphicsActiveStructureRendering;
			if (renderOptionFlags & kRenderOptionStructureEffects)
			{
				flags |= kGraphicsActiveStructureEffects;
			}

			if ((renderOptionFlags & kRenderOptionAmbientOcclusion) && (mask & (1 << kRenderTargetOcclusion1)))
			{
				flags |= kGraphicsActiveAmbientOcclusion;
			}
		}
	}

	graphicsActiveFlags = flags;
}

void GraphicsMgr::SetRenderOptionFlags(unsigned_int32 flags)
{
	renderOptionFlags = flags;
	InitializeActiveFlags();
}

void GraphicsMgr::SetDiagnosticFlags(unsigned_int32 flags)
{
	diagnosticFlags = flags;
	InitializeActiveFlags();
}

void GraphicsMgr::SetShaderTime(float time, float delta)
{
	Render::SetVertexShaderParameter(kVertexParamShaderTime, time, delta, 0.0F, 0.0F);
	Render::SetFragmentShaderParameter(kFragmentParamShaderTime, time * kInverseShaderTimePeriod, 0.0F, 0.0F, 0.0F);
}

void GraphicsMgr::SetImpostorDepthParams(float scale, float offset, float tangent)
{
	Render::SetVertexShaderParameter(kVertexParamImpostorDepth, scale, offset, tangent, 0.0F);
}

void GraphicsMgr::ResetShaders(void)
{
	ShaderData::Purge();
	ShaderProgram::Purge();
}

void GraphicsMgr::BeginRendering(void)
{
	Render::BeginRendering();

	#if C4ORBIS //[ 

			// -- Orbis code hidden --

	#endif //]

	if (graphicsActiveFlags & kGraphicsActiveTimer)
	{
		Render::QueryTimeStamp(&timerQuery[kGraphicsTimeIndexBeginRendering]);
	}

	unsigned_int32 graphicsState = currentGraphicsState;
	currentGraphicsState = graphicsState & ~kGraphicsReactivateTextures;

	if (graphicsState & kGraphicsReactivateTextures)
	{
		Texture::ReactivateAll();
	}

	for (machine a = 0; a < kGraphicsCounterCount; a++)
	{
		graphicsCounter[a] = 0;
	}

	#if C4OCULUS

		cameraLensMultiplier = 1.0F;

	#endif

	NullClass *object = syncRenderObject;
	if (object)
	{
		if (!syncLoadFlag)
		{
			syncRenderObject = nullptr;
			(object->*syncRenderFunction)(syncRenderData);
			syncRenderSignal->Trigger();
		}
		else
		{
			unsigned_int32	t;

			unsigned_int32 time = TheTimeMgr->GetMillisecondCount();
			do
			{
				if (object)
				{
					syncRenderObject = nullptr;
					(object->*syncRenderFunction)(syncRenderData);
					syncRenderSignal->Trigger();
				}
				else
				{
					Thread::Yield();
				}

				object = syncRenderObject;
				t = TheTimeMgr->GetMillisecondCount();
			} while (t - time < 16);
		}
	}
}

void GraphicsMgr::EndRendering(void)
{
	shadowFrameBuffer->Invalidate();

	if (graphicsActiveFlags & kGraphicsActiveTimer)
	{
		Render::QueryTimeStamp(&timerQuery[kGraphicsTimeIndexEndRendering]);
	}

	#if C4WINDOWS

		SwapBuffers(deviceContext);

	#elif C4MACOS

		[openglContext flushBuffer];

	#elif C4LINUX

		glXSwapBuffers(openglDisplay, openglWindow);

	#endif

	Render::EndRendering();

	#if C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]
}

void GraphicsMgr::SetSyncLoadFlag(bool flag)
{
	syncLoadFlag = flag;

	unsigned_int32 displayFlags = TheDisplayMgr->GetDisplayFlags();
	if (flag)
	{
		displayFlags &= ~kDisplayRefreshSync;
	}

	SyncRenderTask(&GraphicsMgr::SetDisplaySyncMode, this, &displayFlags);
}

void GraphicsMgr::SyncRenderTask(void (NullClass::*proc)(const void *), NullClass *object, const void *data)
{
	if (Thread::MainThread())
	{
		(object->*proc)(data);
	}
	else
	{
		GraphicsMgr *graphicsMgr = TheGraphicsMgr;
		graphicsMgr->syncRenderFunction = proc;
		graphicsMgr->syncRenderData = data;

		Thread::Fence();

		graphicsMgr->syncRenderObject = object;
		graphicsMgr->syncRenderSignal->Wait();
	}
}

void GraphicsMgr::GetRenderingTimeStamps(unsigned_int64 *stamp)
{
	if (graphicsActiveFlags & kGraphicsActiveTimer)
	{
		stamp[0] = Render::GetQueryTimeStamp(&timerQuery[0]);

		for (machine a = 1; a < kGraphicsTimeIndexCount; a++)
		{
			unsigned_int64 time = Render::GetQueryTimeStamp(&timerQuery[a]);
			stamp[a] = Max64(time, stamp[a - 1]);
		}
	}
	else
	{
		for (machine a = 0; a < kGraphicsTimeIndexCount; a++)
		{
			stamp[a] = 0;
		}
	}
}

void GraphicsMgr::SetFinalColorTransform(const ColorRGBA& scale, const ColorRGBA& bias)
{
	finalColorScale[0] = scale;
	finalColorBias = bias;
	colorTransformFlags = 0;
}

void GraphicsMgr::SetFinalColorTransform(const ColorRGBA& red, const ColorRGBA& green, const ColorRGBA& blue, const ColorRGBA& bias)
{
	finalColorScale[0] = red;
	finalColorScale[1] = green;
	finalColorScale[2] = blue;
	finalColorBias = bias;
	colorTransformFlags = kPostColorMatrix;
}

void GraphicsMgr::SetPostProcessingShader(unsigned_int32 postFlags, const VertexSnippet *snippet)
{
	static Link<ShaderProgram>		postShader[kPostShaderCount];

	postFlags |= colorTransformFlags;
	if (postFlags & kPostColorMatrix)
	{
		ColorRGBA red = finalColorScale[0] * brightnessMultiplier;
		ColorRGBA green = finalColorScale[1] * brightnessMultiplier;
		ColorRGBA blue = finalColorScale[2] * brightnessMultiplier;
		Render::SetFragmentShaderParameter(kFragmentParamConstant0, &red.red);
		Render::SetFragmentShaderParameter(kFragmentParamConstant1, &green.red);
		Render::SetFragmentShaderParameter(kFragmentParamConstant2, &blue.red);
	}
	else
	{
		ColorRGBA scale = finalColorScale[0] * brightnessMultiplier;
		Render::SetFragmentShaderParameter(kFragmentParamConstant0, &scale.red);
	}

	ColorRGBA bias = finalColorBias * brightnessMultiplier;
	Render::SetFragmentShaderParameter(kFragmentParamConstant3, &bias.red);

	ShaderProgram *shaderProgram = postShader[postFlags];
	if (!shaderProgram)
	{
		Process			*colorProcess;
		Process			*positionProcess;
		ShaderGraph		shaderGraph;

		Process *transformProcess = new TransformPostProcess((postFlags & kPostColorMatrix) != 0);
		shaderGraph.AddElement(transformProcess);

		if (postFlags & kPostMotionBlur)
		{
			colorProcess = new MotionBlurPostProcess((postFlags & kPostMotionBlurGradient) != 0);
		}
		else
		{
			colorProcess = new ColorPostProcess;
		}

		shaderGraph.AddElement(colorProcess);

		if (postFlags & kPostDistortion)
		{
			positionProcess = new DistortPostProcess;
		}
		else
		{
			positionProcess = new FragmentPositionProcess;
		}

		shaderGraph.AddElement(positionProcess);
		new Route(positionProcess, colorProcess, 0);

		if (postFlags & kPostGlowBloom)
		{
			Process *glowProcess = new GlowPostProcess;
			shaderGraph.AddElement(glowProcess);

			new Route(colorProcess, glowProcess, 0);
			new Route(glowProcess, transformProcess, 0);
		}
		else
		{
			new Route(colorProcess, transformProcess, 0);
		}

		VertexShader *vertexShader = GetLocalVertexShader(snippet);
		shaderProgram = ShaderAttribute::CompilePostShader(&shaderGraph, vertexShader);
		postShader[postFlags] = shaderProgram;
		vertexShader->Release();
	}

	Render::SetShaderProgram(shaderProgram);
}

void GraphicsMgr::SetDisplayWarpingShader(void)
{
	static Link<ShaderProgram>		warpShader;

	ShaderProgram *shaderProgram = warpShader;
	if (!shaderProgram)
	{
		ShaderGraph		shaderGraph;

		Process *displayWarpProcess = new DisplayWarpProcess(true);
		shaderGraph.AddElement(displayWarpProcess);

		VertexShader *vertexShader = GetLocalVertexShader(&VertexShader::nullTransform);
		shaderProgram = ShaderAttribute::CompilePostShader(&shaderGraph, vertexShader);
		warpShader = shaderProgram;
		vertexShader->Release();
	}

	Render::SetShaderProgram(shaderProgram);
}

void GraphicsMgr::SetRenderTarget(RenderTargetType type)
{
	RenderTargetType prev = currentRenderTargetType;
	if (prev != type)
	{
		currentRenderTargetType = type;

		if ((type == kRenderTargetPrimary) && (multisampleFrameBuffer))
		{
			Render::SetFrameBuffer(multisampleFrameBuffer);
		}
		else if (normalFrameBuffer->GetRenderTargetMask() & (1 << type))
		{
			Render::SetFrameBuffer(normalFrameBuffer);
			normalFrameBuffer->SetColorRenderTexture(normalFrameBuffer->GetRenderTargetTexture(type));
		}
	}
}

void GraphicsMgr::SetDisplayRenderTarget(void)
{
	if (graphicsActiveFlags & kGraphicsActiveTimer)
	{
		Render::QueryTimeStamp(&timerQuery[kGraphicsTimeIndexBeginPost]);
	}

	unsigned_int32 graphicsState = currentGraphicsState;
	currentGraphicsState = graphicsState & ~(kGraphicsMotionBlurAvail | kGraphicsDistortionAvail | kGraphicsGlowBloomAvail);
	if (!(renderOptionFlags & kRenderOptionGlowBloom))
	{
		graphicsState &= ~kGraphicsGlowBloomAvail;
	}

	if (multisampleFrameBuffer)
	{
		Render::SetDrawFrameBuffer(normalFrameBuffer);
		Render::SetReadFrameBuffer(multisampleFrameBuffer);

		normalFrameBuffer->SetColorRenderTexture(normalFrameBuffer->GetRenderTargetTexture(kRenderTargetPrimary));

		int32 left = cameraRect.left;
		int32 top = cameraRect.top;
		int32 right = cameraRect.right;
		int32 bottom = cameraRect.bottom;
		Render::ResolveMultisampleFrameBuffer(left, bottom, right, top);

		Render::InvalidateReadFrameBuffer();
		normalFrameBuffer->ResetColorRenderTexture();
	}

	Render::DisableBlend();
	SetRenderState(kRenderDepthInhibit);
	SetMaterialState(currentMaterialState & kMaterialTwoSided);

	ResetArrayState();

	const Render::TextureObject *mainTexture = normalFrameBuffer->GetRenderTargetTexture(kRenderTargetPrimary);
	Render::BindTexture(0, mainTexture);

	if (graphicsState & kGraphicsGlowBloomAvail)
	{
		static Link<ShaderProgram>		glowShaderProgram;

		volatile Point2D *restrict vertex = glowBloomVertexBuffer.BeginUpdate<Point2D>();

		float y = -4.0F / (float) viewportRect.Height();

		#if C4OPENGL

			vertex[0].Set(-1.0F, -1.0F);
			vertex[1].Set(0.0F, -1.0F);
			vertex[2].Set(-1.0F, y);
			vertex[3].Set(0.0F, y);

		#else

			vertex[0].Set(-1.0F, -y);
			vertex[1].Set(0.0F, -y);
			vertex[2].Set(-1.0F, 1.0F);
			vertex[3].Set(0.0F, 1.0F);

		#endif

		glowBloomVertexBuffer.EndUpdate();

		SetRenderTarget(kRenderTargetGlowBloom);

		ShaderProgram *shaderProgram = glowShaderProgram;
		if (!shaderProgram)
		{
			ShaderGraph		shaderGraph;

			shaderGraph.AddElement(new ExtractPostProcess);

			VertexShader *vertexShader = GetLocalVertexShader(&VertexShader::extractGlowTransform);
			shaderProgram = ShaderAttribute::CompilePostShader(&shaderGraph, vertexShader);
			glowShaderProgram = shaderProgram;
			vertexShader->Release();
		}

		Render::SetShaderProgram(shaderProgram);

		Render::SetVertexAttribArray(0, 2, Render::kVertexFloat, sizeof(Point2D), &glowBloomVertexBuffer, 0);
		Render::DrawPrimitives(Render::kPrimitiveTriangleStrip, 0, 4);
	}

	if (normalFrameBuffer->GetRenderTargetMask() & (1 << kRenderTargetDisplay))
	{
		Render::SetFrameBuffer(normalFrameBuffer);
		normalFrameBuffer->SetColorRenderTexture(normalFrameBuffer->GetRenderTargetTexture(kRenderTargetDisplay));
	}
	else
	{
		Render::ResetFrameBuffer();
	}

	if (graphicsState & (kGraphicsMotionBlurAvail | kGraphicsDistortionAvail | kGraphicsGlowBloomAvail))
	{
		unsigned_int32 postFlags = 0;

		if (graphicsState & kGraphicsMotionBlurAvail)
		{
			postFlags |= kPostMotionBlur;
		}

		if (graphicsState & kGraphicsDistortionAvail)
		{
			postFlags |= kPostDistortion;
		}

		if (graphicsState & kGraphicsGlowBloomAvail)
		{
			postFlags |= kPostGlowBloom;
		}

		const Render::TextureObject *structureTexture = normalFrameBuffer->GetRenderTargetTexture(kRenderTargetStructure);
		const Render::TextureObject *distortionTexture = normalFrameBuffer->GetRenderTargetTexture(kRenderTargetDistortion);
		const Render::TextureObject *glowBloomTexture = normalFrameBuffer->GetRenderTargetTexture(kRenderTargetGlowBloom);

		if (postFlags & kPostDistortion)
		{
			Render::BindTexture(3, distortionTexture);
		}

		if (postFlags & kPostGlowBloom)
		{
			Render::BindTexture(4, glowBloomTexture);
		}

		if (postFlags & kPostMotionBlur)
		{
			volatile Triangle *restrict basicTriangle = processGridIndexBuffer.BeginUpdate<Triangle>();
			volatile Triangle *restrict gradientTriangle = basicTriangle + kProcessGridWidth * kProcessGridHeight * 2;

			int32 basicCount = 0;
			int32 gradientCount = 0;

			const bool *flag = motionGridFlag;
			machine k = 0;

			for (machine j = 0; j < kProcessGridHeight; j++)
			{
				for (machine i = 0; i < kProcessGridWidth; i++)
				{
					if (!*flag)
					{
						basicTriangle[0].Set(k, k + 1, k + kProcessGridWidth + 2);
						basicTriangle[1].Set(k, k + kProcessGridWidth + 2, k + kProcessGridWidth + 1);
						basicTriangle += 2;
						basicCount++;
					}
					else
					{
						gradientTriangle -= 2;
						gradientTriangle[0].Set(k, k + 1, k + kProcessGridWidth + 2);
						gradientTriangle[1].Set(k, k + kProcessGridWidth + 2, k + kProcessGridWidth + 1);
						gradientCount++;
					}

					flag++;
					k++;
				}

				k++;
			}

			processGridIndexBuffer.EndUpdate();

			Render::SetVertexAttribArray(0, 2, Render::kVertexFloat, sizeof(Point2D), &processGridVertexBuffer, 0);
			Render::BindTexture(1, structureTexture);

			if (basicCount != 0)
			{
				SetPostProcessingShader(postFlags, &VertexShader::postProcessTransform);
				Render::DrawIndexedPrimitives(Render::kPrimitiveTriangles, (kProcessGridWidth + 1) * (kProcessGridHeight + 1), basicCount * 6, &processGridIndexBuffer, 0);
			}

			if (gradientCount != 0)
			{
				SetPostProcessingShader(postFlags | kPostMotionBlurGradient, &VertexShader::postProcessTransform);
				Render::DrawIndexedPrimitives(Render::kPrimitiveTriangles, (kProcessGridWidth + 1) * (kProcessGridHeight + 1), gradientCount * 6, &processGridIndexBuffer, (kProcessGridWidth * kProcessGridHeight - gradientCount) * (sizeof(Triangle) * 2));
			}

			structureTexture->Unbind(1);

			#if C4PS3 //[ 

			// -- PS3 code hidden --

			#endif //]
		}
		else
		{
			SetPostProcessingShader(postFlags, &VertexShader::postProcessTransform);

			Render::SetVertexAttribArray(0, 2, Render::kVertexFloat, sizeof(Point2D), &fullscreenVertexBuffer, 0);
			Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, 3);
		}

		if (postFlags & kPostDistortion)
		{
			distortionTexture->Unbind(3);
		}

		if (postFlags & kPostGlowBloom)
		{
			glowBloomTexture->Unbind(4);
		}
	}
	else
	{
		SetPostProcessingShader(0, &VertexShader::nullTransform);

		Render::SetVertexAttribArray(0, 2, Render::kVertexFloat, sizeof(Point2D), &fullscreenVertexBuffer, 0);
		Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, 3);
	}

	Render::EnableBlend();

	mainTexture->Unbind(0);
	currentRenderTargetType = kRenderTargetNone;

	if (graphicsActiveFlags & kGraphicsActiveTimer)
	{
		Render::QueryTimeStamp(&timerQuery[kGraphicsTimeIndexEndPost]);
	}
}

void GraphicsMgr::SetFullFrameRenderTarget(void)
{
	#if C4OCULUS

		if (normalFrameBuffer->GetRenderTargetMask() & (1 << kRenderTargetDisplay))
		{
			Render::DisableBlend();
			SetRenderState(kRenderDepthInhibit);
			SetMaterialState(currentMaterialState & kMaterialTwoSided);

			ResetArrayState();
			Render::ResetFrameBuffer();

			const Render::TextureObject *mainTexture = normalFrameBuffer->GetRenderTargetTexture(kRenderTargetDisplay);
			Render::BindTexture(0, mainTexture);

			SetDisplayWarpingShader();

			int32 index = (cameraLensMultiplier < 0.0F);
			Render::SetFragmentShaderParameter(0, Oculus::GetFullFrameParam(index));
			Render::SetFragmentShaderParameter(1, Oculus::GetDisplayParam(index));
			Render::SetFragmentShaderParameter(2, Oculus::GetDistortionParam());
			Render::SetFragmentShaderParameter(3, Oculus::GetChromaticParam());

			int32 fullFrameWidth = Oculus::GetFullFrameWidth() / 2;
			int32 fullFrameHeight = Oculus::GetFullFrameHeight();

			int32 i = fullFrameWidth * index;
			int32 j = Oculus::GetScissorInset();
			Render::SetViewport(i, 0, fullFrameWidth, fullFrameHeight);
			Render::SetScissor(i, j, fullFrameWidth, fullFrameHeight - j * 2);

			Render::SetVertexAttribArray(0, 2, Render::kVertexFloat, sizeof(Point2D), &fullscreenVertexBuffer, 0);
			Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, 3);

			Render::EnableBlend();

			mainTexture->Unbind(0);
			currentRenderTargetType = kRenderTargetNone;
		}

	#endif

	normalFrameBuffer->Invalidate();
}

void GraphicsMgr::CopyRenderTarget(Texture *texture, const Rect& rect)
{
	if ((multisampleFrameBuffer) && (currentRenderTargetType == kRenderTargetPrimary))
	{
		Render::SetDrawFrameBuffer(&genericFrameBuffer);
		genericFrameBuffer.SetColorRenderTexture(texture);

		int32 w = rect.Width();
		int32 h = rect.Height();
		int32 left = rect.left;
		int32 bottom = renderTargetHeight - rect.bottom;
		Render::CopyFrameBuffer(left, bottom, left + w, bottom + h, 0, 0, w, h, Render::kBlitFilterPoint);

		genericFrameBuffer.ResetColorRenderTexture();
		Render::SetDrawFrameBuffer(multisampleFrameBuffer);
	}
	else
	{
		texture->BlitImageRect(rect.left, renderTargetHeight - rect.bottom, 0, 0, rect.Width(), rect.Height());
	}
}

void GraphicsMgr::ActivateOrthoCamera(void)
{
	const OrthoCameraObject *orthoCameraObject = static_cast<const OrthoCameraObject *>(cameraObject);

	float nearDepth = orthoCameraObject->GetNearDepth();
	float farDepth = orthoCameraObject->GetFarDepth();

	currentNearDepth = nearDepth;
	currentFrustumFlags = 0;

	cameraPosition4D = -cameraTransformable->GetWorldTransform()[2];

	Matrix4D& matrix = cameraProjectionMatrix;
	matrix(0,1) = matrix(0,2) = matrix(1,0) = matrix(1,2) = matrix(2,0) = matrix(2,1) = matrix(3,0) = matrix(3,1) = matrix(3,2) = 0.0F;
	matrix(3,3) = 1.0F;

	float orthoLeft = orthoCameraObject->GetOrthoRectLeft();
	float orthoRight = orthoCameraObject->GetOrthoRectRight();
	float orthoTop = orthoCameraObject->GetOrthoRectTop();
	float orthoBottom = orthoCameraObject->GetOrthoRectBottom();

	float dx = 1.0F / (orthoRight - orthoLeft);
	float dy = 1.0F / (orthoBottom - orthoTop);
	float dz = 1.0F / (farDepth - nearDepth);

	matrix(0,0) = 2.0F * dx;
	matrix(0,3) = -(orthoRight + orthoLeft) * dx;
	matrix(1,1) = 2.0F * dy;
	matrix(1,3) = -(orthoBottom + orthoTop) * dy;
	matrix(2,2) = -2.0F * dz;
	matrix(2,3) = -(farDepth + nearDepth) * dz;

	#if C4OCULUS

		matrix(0,3) += orthoCameraObject->GetProjectionOffset() * cameraLensMultiplier;

	#endif

	currentProjectionMatrix = matrix;
	depthOffsetConstant = 0.0F;

	currentGraphicsState &= ~(kGraphicsScissorMask | kGraphicsDepthBoundsAvail | kGraphicsObliqueFrustum);
	currentRenderState &= ~kRenderDepthOffset;
	disabledRenderState = kRenderDepthOffset;

	const Rect& rect = orthoCameraObject->GetViewRect();
	int32 left = rect.left;
	int32 right = rect.right;
	int32 bottom = renderTargetHeight - rect.bottom;
	int32 top = renderTargetHeight - rect.top;
	int32 width = right - left;
	int32 height = top - bottom;

	viewportRect.Set(left, top, right, bottom);
	cameraRect.Set(left, top, right, bottom);

	Render::SetVertexShaderParameter(kVertexParamViewportTransform, (float) width, (float) height, (float) left, (float) bottom);
	Render::SetViewport(left, bottom, width, height);

	if (currentGraphicsState & kGraphicsClipEnabled)
	{
		left = Max(left, clipRect.left);
		right = Min(right, clipRect.right);
		bottom = Max(bottom, renderTargetHeight - clipRect.bottom);
		top = Min(top, renderTargetHeight - clipRect.top);

		right = Max(left, right);
		top = Max(bottom, top);

		width = right - left;
		height = top - bottom;

		scissorRect.Set(left, top, right, bottom);
	}
	else
	{
		scissorRect = cameraRect;
	}

	Render::SetScissor(left, bottom, width, height);
}

void GraphicsMgr::ActivateFrustumCamera(void)
{
	const FrustumCameraObject *frustumCameraObject = static_cast<const FrustumCameraObject *>(cameraObject);

	float nearDepth = frustumCameraObject->GetNearDepth();
	unsigned_int32 flags = frustumCameraObject->GetFrustumFlags();

	currentNearDepth = nearDepth;
	currentFrustumFlags = flags;

	cameraPosition4D = cameraTransformable->GetWorldPosition();

	Matrix4D& matrix = cameraProjectionMatrix;
	matrix(0,1) = matrix(0,2) = matrix(0,3) = matrix(1,0) = matrix(1,2) = matrix(1,3) = matrix(2,0) = matrix(2,1) = matrix(3,0) = matrix(3,1) = matrix(3,3) = 0.0F;
	matrix(3,2) = -1.0F;

	float focalLength = frustumCameraObject->GetFocalLength();
	matrix(0,0) = focalLength;
	matrix(1,1) = focalLength / frustumCameraObject->GetAspectRatio();

	#if C4OCULUS

		matrix(0,2) = -frustumCameraObject->GetProjectionOffset() * cameraLensMultiplier;

	#endif

	unsigned_int32 graphicsState = currentGraphicsState & ~(kGraphicsScissorMask | kGraphicsDepthBoundsAvail | kGraphicsObliqueFrustum);
	if (capabilities->extensionFlag[kExtensionDepthBoundsTest])
	{
		graphicsState |= kGraphicsDepthBoundsAvail;
	}

	currentGraphicsState = graphicsState;

	if (flags & kFrustumInfinite)
	{
		matrix(2,2) = kFrustumEpsilon - 1.0F;
		matrix(2,3) = nearDepth * (kFrustumEpsilon - 2.0F);
	}
	else
	{
		float farDepth = frustumCameraObject->GetFarDepth();
		float d = -1.0F / (farDepth - nearDepth);
		float k = 2.0F * farDepth * nearDepth;

		matrix(2,2) = (farDepth + nearDepth) * d;
		matrix(2,3) = k * d;
	}

	standardProjectionMatrix = matrix;
	currentProjectionMatrix = matrix;
	depthOffsetConstant = -matrix(2,3) / matrix(2,2);

	currentRenderState &= ~kRenderDepthOffset;
	disabledRenderState = 0;

	const Rect& rect = frustumCameraObject->GetViewRect();
	int32 left = rect.left;
	int32 right = rect.right;
	int32 bottom = renderTargetHeight - rect.bottom;
	int32 top = renderTargetHeight - rect.top;
	int32 width = right - left;
	int32 height = top - bottom;

	viewportRect.Set(left, top, right, bottom);
	cameraRect.Set(left, top, right, bottom);

	occlusionPlaneScale = 2.0F / ((float) width * focalLength);
	renderTargetOffsetSize = (float) width;

	Render::SetVertexShaderParameter(kVertexParamViewportTransform, (float) width, (float) height, (float) left, (float) bottom);
	Render::SetViewport(left, bottom, width, height);

	if (currentGraphicsState & kGraphicsClipEnabled)
	{
		left = Max(left, clipRect.left);
		right = Min(right, clipRect.right);
		bottom = Max(bottom, renderTargetHeight - clipRect.bottom);
		top = Min(top, renderTargetHeight - clipRect.top);

		right = Max(left, right);
		top = Max(bottom, top);

		width = right - left;
		height = top - bottom;

		scissorRect.Set(left, top, right, bottom);
	}
	else
	{
		scissorRect = cameraRect;
	}

	Render::SetScissor(left, bottom, width, height);
}

void GraphicsMgr::ActivateRemoteCamera(void)
{
	const RemoteCameraObject *remoteCameraObject = static_cast<const RemoteCameraObject *>(cameraObject);

	float nearDepth = remoteCameraObject->GetNearDepth();
	unsigned_int32 flags = remoteCameraObject->GetFrustumFlags();

	currentNearDepth = nearDepth;
	currentFrustumFlags = flags;

	cameraPosition4D = cameraTransformable->GetWorldPosition();

	Matrix4D& matrix = cameraProjectionMatrix;
	matrix(0,1) = matrix(0,2) = matrix(0,3) = matrix(1,0) = matrix(1,2) = matrix(1,3) = matrix(2,0) = matrix(2,1) = matrix(3,0) = matrix(3,1) = matrix(3,3) = 0.0F;
	matrix(3,2) = -1.0F;

	float focalLength = remoteCameraObject->GetFocalLength();
	matrix(0,0) = focalLength;
	matrix(1,1) = focalLength / remoteCameraObject->GetAspectRatio();

	#if C4OCULUS

		matrix(0,2) = -remoteCameraObject->GetProjectionOffset() * cameraLensMultiplier;

	#endif

	unsigned_int32 graphicsState = currentGraphicsState & ~(kGraphicsScissorMask | kGraphicsDepthBoundsAvail | kGraphicsObliqueFrustum);

	if (flags & kFrustumInfinite)
	{
		matrix(2,2) = kFrustumEpsilon - 1.0F;
		matrix(2,3) = nearDepth * (kFrustumEpsilon - 2.0F);
	}
	else
	{
		float farDepth = remoteCameraObject->GetFarDepth();
		float d = -1.0F / (farDepth - nearDepth);
		float k = 2.0F * farDepth * nearDepth;

		matrix(2,2) = (farDepth + nearDepth) * d;
		matrix(2,3) = k * d;
	}

	standardProjectionMatrix = matrix;

	if (flags & kFrustumOblique)
	{
		Antivector4D clipPlane = remoteCameraObject->GetRemoteClipPlane() * Inverse(cameraSpaceTransform);
		if (clipPlane.w < 0.0F)
		{
			float qpx = (clipPlane.x < 0.0F) ? -1.0F : 1.0F;
			float qpy = (clipPlane.y < 0.0F) ? -1.0F : 1.0F;

			float qx = (qpx + matrix(0,2)) / matrix(0,0);
			float qy = (qpy + matrix(1,2)) / matrix(1,1);
			float qw = (1.0F + matrix(2,2)) / matrix(2,3);

			float scale = 2.0F / (clipPlane.x * qx + clipPlane.y * qy - clipPlane.z + clipPlane.w * qw);

			matrix(2,0) = scale * clipPlane.x;
			matrix(2,1) = scale * clipPlane.y;
			matrix(2,2) = scale * clipPlane.z + 1.0F;
			matrix(2,3) = scale * clipPlane.w;

			graphicsState |= kGraphicsObliqueFrustum;
		}
	}
	else
	{
		if (capabilities->extensionFlag[kExtensionDepthBoundsTest])
		{
			graphicsState |= kGraphicsDepthBoundsAvail;
		}
	}

	currentGraphicsState = graphicsState;
	currentProjectionMatrix = matrix;
	depthOffsetConstant = -matrix(2,3) / matrix(2,2);

	currentRenderState &= ~kRenderDepthOffset;
	disabledRenderState = 0;

	const Rect& rect = remoteCameraObject->GetViewRect();
	int32 left = rect.left;
	int32 right = rect.right;
	int32 bottom = renderTargetHeight - rect.bottom;
	int32 top = renderTargetHeight - rect.top;
	int32 width = right - left;
	int32 height = top - bottom;

	viewportRect.Set(left, top, right, bottom);

	occlusionPlaneScale = 2.0F / ((float) width * focalLength);
	renderTargetOffsetSize = (float) width;

	Render::SetVertexShaderParameter(kVertexParamViewportTransform, (float) width, (float) height, (float) left, (float) bottom);
	Render::SetViewport(left, bottom, width, height);

	const ProjectionRect& frustumBoundary = remoteCameraObject->GetFrustumBoundary();
	float x1 = frustumBoundary.left * 0.5F + 0.5F;
	float x2 = frustumBoundary.right * 0.5F + 0.5F;
	float y1 = frustumBoundary.bottom * 0.5F + 0.5F;
	float y2 = frustumBoundary.top * 0.5F + 0.5F;

	float w = (float) width * 0.5F;
	float h = (float) height * 0.5F;

	int32 cameraLeft = left + (int32) (x1 * w * 2.0F);
	int32 cameraRight = left + (int32) (x2 * w * 2.0F + 0.5F);
	int32 cameraBottom = bottom + (int32) (y1 * h * 2.0F);
	int32 cameraTop = bottom + (int32) (y2 * h * 2.0F + 0.5F);

	cameraRect.Set(cameraLeft, cameraTop, cameraRight, cameraBottom);

	if (currentGraphicsState & kGraphicsClipEnabled)
	{
		left = Max(left, clipRect.left);
		right = Min(right, clipRect.right);
		bottom = Max(bottom, renderTargetHeight - clipRect.bottom);
		top = Min(top, renderTargetHeight - clipRect.top);

		right = Max(left, right);
		top = Max(bottom, top);

		scissorRect.Set(left, top, right, bottom);
		Render::SetScissor(left, bottom, right - left, top - bottom);
	}
	else
	{
		scissorRect = cameraRect;
		Render::SetScissor(cameraLeft, cameraBottom, cameraRight - cameraLeft, cameraTop - cameraBottom);
	}
}

void GraphicsMgr::SetCamera(const CameraObject *camera, const Transformable *transformable, unsigned_int32 clearMask, bool reset)
{
	cameraObject = camera;
	cameraTransformable = transformable;

	const Transform4D& worldTransform = (transformable) ? transformable->GetWorldTransform() : K::identity_4D;
	const Vector3D& rightDirection = worldTransform[0];
	const Vector3D& downDirection = worldTransform[1];
	const Vector3D& viewDirection = worldTransform[2];

	if (camera->GetCameraType() != kCameraOrtho)
	{
		const FrustumCameraObject *object = static_cast<const FrustumCameraObject *>(camera);
		const Rect& rect = object->GetViewRect();
		float viewWidth = (float) rect.Width();

		float focal = object->GetFocalLength();
		float factor = focal * viewWidth;
		Render::SetVertexShaderParameter(kVertexParamRadiusPointFactor, factor, 0.0F, 0.0F, 0.0F);

		factor = 1.0F / factor;
		float d = -(viewDirection * worldTransform.GetTranslation());
		Render::SetVertexShaderParameter(kVertexParamPointCameraPlane, viewDirection.x * factor, viewDirection.y * factor, viewDirection.z * factor, d * factor);

		focal = 1.0F / focal;
		distortionPlane.Set(viewDirection.x * focal, viewDirection.y * focal, viewDirection.z * focal, d * focal);

		occlusionAreaNormalizer = 1.0F / (viewWidth * (float) rect.Height());
	}

	cameraSpaceTransform(0,0) = rightDirection.x;
	cameraSpaceTransform(0,1) = rightDirection.y;
	cameraSpaceTransform(0,2) = rightDirection.z;
	cameraSpaceTransform(1,0) = -downDirection.x;
	cameraSpaceTransform(1,1) = -downDirection.y;
	cameraSpaceTransform(1,2) = -downDirection.z;
	cameraSpaceTransform(2,0) = -viewDirection.x;
	cameraSpaceTransform(2,1) = -viewDirection.y;
	cameraSpaceTransform(2,2) = -viewDirection.z;

	const Vector3D& worldOffset = worldTransform[3];
	cameraSpaceTransform(0,3) = -(cameraSpaceTransform.GetRow(0) ^ worldOffset);
	cameraSpaceTransform(1,3) = -(cameraSpaceTransform.GetRow(1) ^ worldOffset);
	cameraSpaceTransform(2,3) = -(cameraSpaceTransform.GetRow(2) ^ worldOffset);

	camera->Activate(this);
	unsigned_int32 graphicsState = currentGraphicsState;

	if (Determinant(worldTransform) > 0.0F)
	{
		if (!(graphicsState & kGraphicsFrontFaceCCW))
		{
			graphicsState |= kGraphicsFrontFaceCCW;
			Render::SetFrontFace(Render::kFrontCCW);
		}
	}
	else
	{
		if (graphicsState & kGraphicsFrontFaceCCW)
		{
			graphicsState &= ~kGraphicsFrontFaceCCW;
			Render::SetFrontFace(Render::kFrontCW);
		}
	}

	unsigned_int32 clearFlags = camera->GetClearFlags() & clearMask;
	if (clearFlags != 0)
	{
		unsigned_int32 renderState = currentRenderState;

		if (clearFlags & kClearColorBuffer)
		{
			const ColorRGBA& color = camera->GetClearColor();

			if (renderState & kRenderColorInhibit)
			{
				Render::SetColorMask(true, true, true, true);
				renderState &= ~kRenderColorInhibit;
			}

			if (clearFlags & kClearDepthStencilBuffer)
			{
				if (renderState & kRenderDepthInhibit)
				{
					Render::SetDepthMask(true);
					renderState &= ~kRenderDepthInhibit;
				}

				graphicsState |= kGraphicsStencilClear;
				graphicsCounter[kGraphicsCounterStencilClears]++;

				Render::ClearColorDepthStencilBuffers(color.red, color.green, color.blue, color.alpha);
			}
			else
			{
				Render::ClearColorBuffer(color.red, color.green, color.blue, color.alpha);
			}
		}
		else
		{
			if (renderState & kRenderDepthInhibit)
			{
				Render::SetDepthMask(true);
				renderState &= ~kRenderDepthInhibit;
			}

			graphicsState |= kGraphicsStencilClear;
			graphicsCounter[kGraphicsCounterStencilClears]++;

			Render::ClearDepthStencilBuffers();
		}

		currentRenderState = renderState;
	}

	currentGraphicsState = graphicsState;

	if (graphicsState & kGraphicsRenderShadowMap)
	{
		float cp = Magnitude(viewDirection.GetVector2D());
		float elevation = Atan(-viewDirection.z, cp) * K::degrees;

		float scale = 1.0F / (cp * (camera->GetFarDepth() - camera->GetNearDepth()));

		if (elevation < 30.5F)
		{
			float t = FmaxZero(elevation - 14.5F) * 0.0625F;
			Render::SetFragmentShaderParameter(kFragmentParamImpostorShadowBlend, 1.0F - t, t, 0.0F, 0.0F);
			Render::SetFragmentShaderParameter(kFragmentParamImpostorShadowScale, scale * (1.0F - t), scale * t, 0.0F, 0.0F);
		}
		else if (elevation < 45.5F)
		{
			float t = FmaxZero(elevation - 29.5F) * 0.0625F;
			Render::SetFragmentShaderParameter(kFragmentParamImpostorShadowBlend, 0.0F, 1.0F - t, t, 0.0F);
			Render::SetFragmentShaderParameter(kFragmentParamImpostorShadowScale, 0.0F, scale * (1.0F - t), scale * t, 0.0F);
		}
		else if (elevation < 60.5F)
		{
			float t = FmaxZero(elevation - 44.5F) * 0.0625F;
			Render::SetFragmentShaderParameter(kFragmentParamImpostorShadowBlend, 0.0F, 0.0F, 1.0F - t, t);
			Render::SetFragmentShaderParameter(kFragmentParamImpostorShadowScale, 0.0F, 0.0F, scale * (1.0F - t), scale * t);
		}
		else
		{
			Render::SetFragmentShaderParameter(kFragmentParamImpostorShadowBlend, 0.0F, 0.0F, 0.0F, 1.0F);
			Render::SetFragmentShaderParameter(kFragmentParamImpostorShadowScale, 0.0F, 0.0F, 0.0F, scale);
		}
	}
	else
	{
		const Point3D& position = worldTransform.GetTranslation();
		directCameraPosition = position;

		Render::SetVertexShaderParameter(kVertexParamImpostorCameraPosition, position.x, position.y, position.z, 1.0F);
	}

	if (reset)
	{
		fogSpaceObject = nullptr;
		fogSpaceTransformable = nullptr;

		currentShaderVariant = kShaderVariantNormal;
		SetAmbient();
	}
}

void GraphicsMgr::SetFogSpace(const FogSpaceObject *fogSpace, const Transformable *transformable)
{
	fogSpaceObject = fogSpace;
	fogSpaceTransformable = transformable;

	if (fogSpace)
	{
		float	f1, f2;

		const Transform4D& m = transformable->GetInverseWorldTransform();
		const MatrixRow4D& plane = m.GetRow(2);
		worldFogPlane = plane;

		float F_wedge_C = plane ^ cameraTransformable->GetWorldPosition();
		if (F_wedge_C > 0.0F)
		{
			f1 = 0.0F;
			f2 = 1.0F;
		}
		else
		{
			f1 = 1.0F;
			f2 = -1.0F;
		}

		float density = fogSpace->GetFogDensity();
		Render::SetVertexShaderParameter(kVertexParamFogParams, F_wedge_C, f1, 1.0F - f1, f2);
		Render::SetFragmentShaderParameter(kFragmentParamFogParams, F_wedge_C, -F_wedge_C * F_wedge_C, f1, density * K::one_over_ln_2);
		Render::SetFragmentShaderParameter(kFragmentParamFogColor, &fogSpace->GetFogColor().red);

		currentShaderVariant = (fogSpace->GetFogFunction() == kFogFunctionLinear) ? kShaderVariantLinearFog : kShaderVariantConstantFog;
	}
	else
	{
		currentShaderVariant = kShaderVariantNormal;
	}
}

void GraphicsMgr::SetAmbient(void)
{
	if (lightObject)
	{
		lightObject = nullptr;
		shadowFrameBuffer->GetRenderTargetTexture()->SetCompareMode(Render::kTextureCompareNone);
	}

	lightTransformable = nullptr;
	geometryTransformable = nullptr;

	unsigned_int32 oldGraphicsState = currentGraphicsState;
	unsigned_int32 newGraphicsState = oldGraphicsState & ~(kGraphicsAmbientLessEqual | kGraphicsScissorMask | kGraphicsDepthBoundsMask | kGraphicsRenderLight);

	currentGraphicsState = newGraphicsState;
	unsigned_int32 changed = (newGraphicsState ^ oldGraphicsState) & (kGraphicsRenderLight | kGraphicsScissorMask | kGraphicsDepthBoundsMask);
	if (changed != 0)
	{
		if (changed & kGraphicsRenderLight)
		{
			Render::DisableStencilTest();
		}

		if (changed & kGraphicsDepthBoundsMask)
		{
			Render::DisableDepthBoundsTest();
		}

		if (changed & kGraphicsScissorMask)
		{
			int32 left = scissorRect.left;
			int32 bottom = scissorRect.bottom;
			Render::SetScissor(left, bottom, scissorRect.right - left, scissorRect.top - bottom);
		}
	}

	currentShaderType = kShaderAmbient;
}

void GraphicsMgr::BeginClip(const Rect& rect)
{
	currentGraphicsState |= kGraphicsClipEnabled;
	clipRect = rect;

	int32 left = Max(cameraRect.left, rect.left);
	int32 bottom = Max(cameraRect.bottom, viewportRect.top - rect.bottom);
	int32 right = Min(cameraRect.right, rect.right);
	int32 top = Min(cameraRect.top, viewportRect.top - rect.top);

	scissorRect.Set(left, top, right, bottom);
	Render::SetScissor(left, bottom, right - left, top - bottom);
}

void GraphicsMgr::EndClip(void)
{
	currentGraphicsState &= ~kGraphicsClipEnabled;
	scissorRect = cameraRect;

	int32 left = cameraRect.left;
	int32 bottom = cameraRect.bottom;
	Render::SetScissor(left, bottom, cameraRect.right - left, cameraRect.top - bottom);
}

void GraphicsMgr::SetInfiniteLight(const InfiniteLightObject *light, const Transformable *transformable, const LightShadowData *shadowData)
{
	lightObject = light;
	lightTransformable = transformable;

	geometryTransformable = nullptr;

	const ColorRGB& lightColor = light->GetLightColor();
	Render::SetFragmentShaderParameter(kFragmentParamLightColor, lightColor.red, lightColor.green, lightColor.blue, 1.0F);

	ShaderType shaderType = kShaderInfiniteLight;
	LightType lightType = light->GetLightType();
	if (lightType == kLightDepth)
	{
		shaderType = kShaderDepthLight;
		lightShadowData = shadowData;

		Render::TextureObject *shadowTexture = shadowFrameBuffer->GetRenderTargetTexture();
		shadowTexture->SetCompareMode(Render::kTextureCompareReference);
		Render::BindTexture(kTextureUnitLightProjection, shadowTexture);

		float w = (float) dynamicShadowMapSize;
		float h = (float) (dynamicShadowMapSize * kMaxShadowCascadeCount);
		float dx = 1.5F / w;
		float dy = 1.5F / h;

		Render::SetFragmentShaderParameter(kFragmentParamShadowSample1, -0.125F * dx, -0.375F * dy, 0.375F * dx, -0.125F * dy);
		Render::SetFragmentShaderParameter(kFragmentParamShadowSample2, 0.125F * dx, 0.375F * dy, -0.375F * dx, 0.125F * dy);

		const Transform4D& shadowTransform = transformable->GetInverseWorldTransform();
		const Vector3D& cameraView = cameraTransformable->GetWorldTransform()[2];
		float f = InverseSqrt(cameraView.x * cameraView.x + cameraView.y * cameraView.y);
		Vector3D shadowView = shadowTransform[0] * (cameraView.x * f) + shadowTransform[1] * (cameraView.y * f);

		#if C4OPENGL

			Render::SetFragmentShaderParameter(kFragmentParamShadowViewDirection, shadowView.x * -shadowData->inverseShadowSize.x, shadowView.y * -kInverseMaxShadowCascadeCount * shadowData->inverseShadowSize.y, shadowView.z * -shadowData->inverseShadowSize.z, 0.0F);

		#elif C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

		#endif //]
	}
	else if (lightType == kLightLandscape)
	{
		shaderType = kShaderLandscapeLight;
		lightShadowData = shadowData;

		Render::TextureObject *shadowTexture = shadowFrameBuffer->GetRenderTargetTexture();
		shadowTexture->SetCompareMode(Render::kTextureCompareReference);
		Render::BindTexture(kTextureUnitLightProjection, shadowTexture);

		float w = (float) dynamicShadowMapSize;
		float h = (float) (dynamicShadowMapSize * kMaxShadowCascadeCount);
		float dx = 1.5F / w;
		float dy = 1.5F / h;

		Render::SetFragmentShaderParameter(kFragmentParamShadowSample1, -0.125F * dx, -0.375F * dy, 0.375F * dx, -0.125F * dy);
		Render::SetFragmentShaderParameter(kFragmentParamShadowSample2, 0.125F * dx, 0.375F * dy, -0.375F * dx, 0.125F * dy);

		const Transform4D& shadowTransform = transformable->GetInverseWorldTransform();
		const Vector3D& cameraView = cameraTransformable->GetWorldTransform()[2];
		float f = InverseSqrt(cameraView.x * cameraView.x + cameraView.y * cameraView.y);
		Vector3D shadowView = shadowTransform[0] * (cameraView.x * f) + shadowTransform[1] * (cameraView.y * f);

		#if C4OPENGL

			Render::SetFragmentShaderParameter(kFragmentParamShadowViewDirection, shadowView.x * -shadowData->inverseShadowSize.x, shadowView.y * -kInverseMaxShadowCascadeCount * shadowData->inverseShadowSize.y, shadowView.z * -shadowData->inverseShadowSize.z, 0.0F);

		#elif C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

		#endif //]

		Vector3D scale1(shadowData[1].inverseShadowSize.x * shadowData[0].shadowSize.x, shadowData[1].inverseShadowSize.y * shadowData[0].shadowSize.y, shadowData[1].inverseShadowSize.z * shadowData[0].shadowSize.z);
		Vector3D scale2(shadowData[2].inverseShadowSize.x * shadowData[0].shadowSize.x, shadowData[2].inverseShadowSize.y * shadowData[0].shadowSize.y, shadowData[2].inverseShadowSize.z * shadowData[0].shadowSize.z);
		Vector3D scale3(shadowData[3].inverseShadowSize.x * shadowData[0].shadowSize.x, shadowData[3].inverseShadowSize.y * shadowData[0].shadowSize.y, shadowData[3].inverseShadowSize.z * shadowData[0].shadowSize.z);

		Render::SetFragmentShaderParameter(kFragmentParamShadowMapScale1, scale1.x, scale1.y, scale1.z, 0.0F);
		Render::SetFragmentShaderParameter(kFragmentParamShadowMapScale2, scale2.x, scale2.y, scale2.z, 0.0F);
		Render::SetFragmentShaderParameter(kFragmentParamShadowMapScale3, scale3.x, scale3.y, scale3.z, 0.0F);

		#if C4OPENGL

			Render::SetFragmentShaderParameter(kFragmentParamShadowMapOffset1,
					shadowData[1].shadowPosition.x * shadowData[1].inverseShadowSize.x + 0.5F - scale1.x * (shadowData[0].shadowPosition.x * shadowData[0].inverseShadowSize.x + 0.5F),
					(shadowData[1].shadowPosition.y * shadowData[1].inverseShadowSize.y + 1.5F - scale1.y * (shadowData[0].shadowPosition.y * shadowData[0].inverseShadowSize.y + 0.5F)) * kInverseMaxShadowCascadeCount,
					shadowData[1].shadowPosition.z * shadowData[1].inverseShadowSize.z - scale1.z * (shadowData[0].shadowPosition.z * shadowData[0].inverseShadowSize.z), 0.0F);
			Render::SetFragmentShaderParameter(kFragmentParamShadowMapOffset2,
					shadowData[2].shadowPosition.x * shadowData[2].inverseShadowSize.x + 0.5F - scale2.x * (shadowData[0].shadowPosition.x * shadowData[0].inverseShadowSize.x + 0.5F),
					(shadowData[2].shadowPosition.y * shadowData[2].inverseShadowSize.y + 2.5F - scale2.y * (shadowData[0].shadowPosition.y * shadowData[0].inverseShadowSize.y + 0.5F)) * kInverseMaxShadowCascadeCount,
					shadowData[2].shadowPosition.z * shadowData[2].inverseShadowSize.z - scale2.z * (shadowData[0].shadowPosition.z * shadowData[0].inverseShadowSize.z), 0.0F);
			Render::SetFragmentShaderParameter(kFragmentParamShadowMapOffset3,
					shadowData[3].shadowPosition.x * shadowData[3].inverseShadowSize.x + 0.5F - scale3.x * (shadowData[0].shadowPosition.x * shadowData[0].inverseShadowSize.x + 0.5F),
					(shadowData[3].shadowPosition.y * shadowData[3].inverseShadowSize.y + 3.5F - scale3.y * (shadowData[0].shadowPosition.y * shadowData[0].inverseShadowSize.y + 0.5F)) * kInverseMaxShadowCascadeCount,
					shadowData[3].shadowPosition.z * shadowData[3].inverseShadowSize.z - scale3.z * (shadowData[0].shadowPosition.z * shadowData[0].inverseShadowSize.z), 0.0F);

		#elif C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

		#endif //]
	}

	currentShaderType = shaderType;

	lightRect = viewportRect;
	lightDepthBounds.Set(0.0F, 1.0F);

	unsigned_int32 oldGraphicsState = currentGraphicsState;
	unsigned_int32 newGraphicsState = (oldGraphicsState | kGraphicsRenderLight) & ~(kGraphicsScissorMask | kGraphicsDepthBoundsMask);
	currentGraphicsState = newGraphicsState;

	unsigned_int32 changed = (newGraphicsState ^ oldGraphicsState) & (kGraphicsRenderLight | kGraphicsScissorMask | kGraphicsDepthBoundsMask);
	if (changed != 0)
	{
		if (changed & kGraphicsRenderLight)
		{
			Render::EnableStencilTest();
		}

		if (changed & kGraphicsDepthBoundsMask)
		{
			Render::DisableDepthBoundsTest();
		}

		if (changed & kGraphicsScissorMask)
		{
			int32 left = scissorRect.left;
			int32 bottom = scissorRect.bottom;
			Render::SetScissor(left, bottom, scissorRect.right - left, scissorRect.top - bottom);
		}
	}
}

bool GraphicsMgr::SetPointLight(const PointLightObject *light, const Transformable *transformable)
{
	unsigned_int32 oldGraphicsState = currentGraphicsState;
	unsigned_int32 newGraphicsState = (oldGraphicsState | kGraphicsRenderLight) & ~(kGraphicsScissorMask | kGraphicsDepthBoundsMask);

	float r = light->GetLightRange();

	if (cameraObject->GetCameraType() != kCameraOrtho)
	{
		ProjectionRect		projectionRect;

		const FrustumCameraObject *frustumCamera = static_cast<const FrustumCameraObject *>(cameraObject);
		Point3D center = cameraSpaceTransform * transformable->GetWorldPosition();

		ProjectionResult result = frustumCamera->ProjectSphere(center, r, standardProjectionMatrix(0,2), &projectionRect);
		if (result == kProjectionEmpty)
		{
			return (false);
		}

		if (result == kProjectionPartial)
		{
			const Rect& viewRect = frustumCamera->GetViewRect();

			float viewLeft = (float) viewRect.left;
			float viewBottom = (float) (renderTargetHeight - viewRect.bottom);
			float viewWidth = (float) viewRect.Width() * 0.5F;
			float viewHeight = (float) viewRect.Height() * 0.5F;

			int32 scissorLeft = Max(scissorRect.left, (int32) (viewLeft + viewWidth * (projectionRect.left + 1.0F)));
			int32 scissorRight = Min(scissorRect.right, (int32) (viewLeft + viewWidth * (projectionRect.right + 1.0F)));
			int32 scissorBottom = Max(scissorRect.bottom, (int32) (viewBottom + viewHeight * (projectionRect.bottom + 1.0F)));
			int32 scissorTop = Min(scissorRect.top, (int32) (viewBottom + viewHeight * (projectionRect.top + 1.0F)));

			if ((scissorRight <= scissorLeft) || (scissorTop <= scissorBottom))
			{
				return (false);
			}

			newGraphicsState |= kGraphicsLightScissor;
			lightRect.Set(scissorLeft, scissorTop, scissorRight, scissorBottom);
			Render::SetScissor(scissorLeft, scissorBottom, scissorRight - scissorLeft, scissorTop - scissorBottom);
		}
		else
		{
			lightRect = viewportRect;
		}

		float n = -currentNearDepth;
		float z1 = Fmin(center.z + r, n);
		float z2 = Fmin(center.z - r, n);

		float p33 = standardProjectionMatrix(2,2);
		float p34 = standardProjectionMatrix(2,3);
		float dmin = -0.5F * (p33 * z1 + p34) / z1 + 0.5F;
		float dmax = -0.5F * (p33 * z2 + p34) / z2 + 0.5F;

		lightDepthBounds.Set(dmin, dmax);

		if (newGraphicsState & kGraphicsDepthBoundsAvail)
		{
			Render::SetDepthBounds(dmin, dmax);
			newGraphicsState |= kGraphicsLightDepthBounds;
		}
	}

	lightObject = light;
	lightTransformable = transformable;

	geometryTransformable = nullptr;

	const ColorRGB& lightColor = light->GetLightColor();
	Render::SetFragmentShaderParameter(kFragmentParamLightColor, lightColor.red, lightColor.green, lightColor.blue, 1.0F);
	Render::SetVertexShaderParameter(kVertexParamLightRange, r, 0.0F, 0.0F, 1.0F / r);

	ShaderType shaderType = kShaderPointLight;
	LightType lightType = light->GetLightType();
	if (lightType == kLightCube)
	{
		shaderType = kShaderCubeLight;
		Render::BindTexture(kTextureUnitLightProjection, static_cast<const CubeLightObject *>(light)->GetShadowMap());
	}
	else if (lightType == kLightSpot)
	{
		shaderType = kShaderSpotLight;
		Render::BindTexture(kTextureUnitLightProjection, static_cast<const SpotLightObject *>(light)->GetShadowMap());
	}

	currentShaderType = shaderType;
	currentGraphicsState = newGraphicsState;

	unsigned_int32 changed = (newGraphicsState ^ oldGraphicsState) & (kGraphicsRenderLight | kGraphicsScissorMask | kGraphicsDepthBoundsMask);
	if (changed != 0)
	{
		if (changed & kGraphicsRenderLight)
		{
			Render::EnableStencilTest();
		}

		if (changed & kGraphicsDepthBoundsMask)
		{
			if (newGraphicsState & kGraphicsDepthBoundsMask)
			{
				Render::EnableDepthBoundsTest();
			}
			else
			{
				Render::DisableDepthBoundsTest();
			}
		}

		if (changed & kGraphicsScissorMask)
		{
			if (!(newGraphicsState & kGraphicsScissorMask))
			{
				int32 left = scissorRect.left;
				int32 bottom = scissorRect.bottom;
				Render::SetScissor(left, bottom, scissorRect.right - left, scissorRect.top - bottom);
			}
		}
	}

	return (true);
}

const Vector4D& GraphicsMgr::SetGeometryTransformable(const Transformable *transformable)
{
	geometryTransformable = transformable;

	if (lightObject->GetBaseLightType() == kLightInfinite)
	{
		const Vector3D& direction = lightTransformable->GetWorldTransform()[2];
		geometryLightPosition = (transformable) ? transformable->GetInverseWorldTransform() * direction : direction;
	}
	else
	{
		const Point3D& position = lightTransformable->GetWorldPosition();
		geometryLightPosition = (transformable) ? transformable->GetInverseWorldTransform() * position : position;
	}

	Render::SetVertexShaderParameter(kVertexParamLightPosition, &geometryLightPosition.x);
	return (geometryLightPosition);
}

template <int32 index> void GraphicsMgr::GroupRenderSublist(List<Renderable> *list, unsigned_int32 bit, List<Renderable> *finalList)
{
	List<Renderable>	oddList;

	machine_address evenAccum = 0;

	Renderable *renderable = list->First();
	while (renderable)
	{
		Renderable *next = renderable->Next();

		machine_address key = renderable->GetGroupKey(index);
		if (key & bit)
		{
			oddList.Append(renderable);
		}
		else
		{
			evenAccum |= key;
		}

		renderable = next;
	}

	if ((evenAccum & ~(bit - 1)) == 0)
	{
		if (list != finalList)
		{
			for (;;)
			{
				renderable = list->First();
				if (!renderable)
				{
					break;
				}

				finalList->Append(renderable);
			}
		}
	}
	else
	{
		GroupRenderSublist<index>(list, bit << 1, finalList);
	}

	if (oddList.First())
	{
		GroupRenderSublist<index>(&oddList, bit << 1, finalList);
	}
}

void GraphicsMgr::GroupAmbientRenderList(List<Renderable> *renderList)
{
	GroupRenderSublist<kGroupKeyAmbient>(renderList, 0x10, renderList);
}

void GraphicsMgr::GroupLightRenderList(List<Renderable> *renderList)
{
	GroupRenderSublist<kGroupKeyLight>(renderList, 0x10, renderList);
}

void GraphicsMgr::SortRenderSublist(List<Renderable> *list, float zmin, float zmax, List<Renderable> *finalList)
{
	float avg = (zmin + zmax) * 0.5F;

	Renderable *renderable = list->Last();
	if ((list->First() == renderable) || (!(zmax - zmin > Fabs(avg) * 0.001F)))
	{
		while (renderable)
		{
			Renderable *prev = renderable->Previous();
			finalList->Prepend(renderable);
			renderable = prev;
		}
	}
	else
	{
		List<Renderable>	farList;

		float zminFar = zmax;
		float zmaxNear = zmin;

		renderable = list->First();
		do
		{
			Renderable *next = renderable->Next();
			float z = renderable->GetTransparentDepth();
			if (z < avg)
			{
				zmaxNear = Fmax(zmaxNear, z);
			}
			else
			{
				zminFar = Fmin(zminFar, z);
				farList.Append(renderable);
			}

			renderable = next;
		} while (renderable);

		SortRenderSublist(list, zmin, zmaxNear, finalList);
		SortRenderSublist(&farList, zminFar, zmax, finalList);
	}
}

void GraphicsMgr::SortRenderList(List<Renderable> *renderList)
{
	Renderable *renderable = renderList->First();
	if (renderable)
	{
		List<Renderable>	transparentList;
		List<Renderable>	attachedList;

		const Vector3D& direction = cameraTransformable->GetWorldTransform()[2];

		float zmin = K::infinity;
		float zmax = K::minus_infinity;

		do
		{
			Renderable *next = renderable->Next();

			if (renderable->GetTransparentAttachment())
			{
				attachedList.Append(renderable);
			}
			else
			{
				const Point3D *position = renderable->GetTransparentPosition();
				if (position)
				{
					float z = direction * *position;
					zmin = Fmin(zmin, z);
					zmax = Fmax(zmax, z);

					renderable->SetTransparentDepth(z);
					transparentList.Append(renderable);
				}
			}

			renderable = next;
		} while (renderable);

		if (!transparentList.Empty())
		{
			SortRenderSublist(&transparentList, zmin, zmax, renderList);
		}

		renderable = attachedList.First();
		while (renderable)
		{
			Renderable *next = renderable->Next();

			Renderable *attachment = renderable->GetTransparentAttachment();
			if (attachment->GetOwningList() == renderList)
			{
				renderList->InsertBefore(renderable, attachment);
			}
			else
			{
				renderList->Append(renderable);
			}

			renderable = next;
		}
	}
}

void GraphicsMgr::SetModelviewMatrix(const Transform4D& matrix)
{
	Matrix4D& mvp = currentMVPMatrix;
	mvp = currentProjectionMatrix * matrix;

	Render::SetVertexShaderParameter(kVertexParamMatrixMVP, mvp(0,0), mvp(0,1), mvp(0,2), mvp(0,3));
	Render::SetVertexShaderParameter(kVertexParamMatrixMVP + 1, mvp(1,0), mvp(1,1), mvp(1,2), mvp(1,3));
	Render::SetVertexShaderParameter(kVertexParamMatrixMVP + 2, mvp(2,0), mvp(2,1), mvp(2,2), mvp(2,3));
	Render::SetVertexShaderParameter(kVertexParamMatrixMVP + 3, mvp(3,0), mvp(3,1), mvp(3,2), mvp(3,3));
}

void GraphicsMgr::SetGeometryModelviewMatrix(const Transform4D& matrix)
{
	Matrix4D& mvp = currentMVPMatrix;
	mvp = currentProjectionMatrix * matrix;

	Render::SetGeometryShaderParameter(kGeometryParamMatrixMVP, mvp(0,0), mvp(0,1), mvp(0,2), mvp(0,3));
	Render::SetGeometryShaderParameter(kGeometryParamMatrixMVP + 1, mvp(1,0), mvp(1,1), mvp(1,2), mvp(1,3));
	Render::SetGeometryShaderParameter(kGeometryParamMatrixMVP + 2, mvp(2,0), mvp(2,1), mvp(2,2), mvp(2,3));
	Render::SetGeometryShaderParameter(kGeometryParamMatrixMVP + 3, mvp(3,0), mvp(3,1), mvp(3,2), mvp(3,3));
}

VertexShader *GraphicsMgr::GetLocalVertexShader(const VertexSnippet *snippet)
{
	VertexAssembly assembly(ShaderAttribute::signatureStorage);
	assembly.AddSnippet(snippet);
	return (VertexShader::Get(&assembly));
}

VertexShader *GraphicsMgr::GetLocalVertexShader(int32 snippetCount, const VertexSnippet *const *snippet)
{
	VertexAssembly assembly(ShaderAttribute::signatureStorage);
	for (machine a = 0; a < snippetCount; a++)
	{
		assembly.AddSnippet(snippet[a]);
	}

	return (VertexShader::Get(&assembly));
}

FragmentShader *GraphicsMgr::GetLocalFragmentShader(int32 shaderIndex)
{
	static const unsigned_int32 shaderSignature[kLocalFragmentShaderCount][2] =
	{
		{1, '%CPZ'}, {1, '%CPC'}
	};

	const unsigned_int32 *signature = shaderSignature[shaderIndex];
	FragmentShader *fragmentShader = FragmentShader::Find(signature);
	if (!fragmentShader)
	{
		static const char *const shaderSource[kLocalFragmentShaderCount] =
		{
			FragmentShader::copyZero, FragmentShader::copyConstant
		};

		const char *source = shaderSource[shaderIndex];
		fragmentShader = FragmentShader::New(source, Text::GetTextLength(source), signature);
	}

	return (fragmentShader);
}

GeometryShader *GraphicsMgr::GetLocalGeometryShader(int32 shaderIndex)
{
	static const unsigned_int32 shaderSignature[kLocalGeometryShaderCount][2] =
	{
		{1, '%ENL'}
	};

	const unsigned_int32 *signature = shaderSignature[shaderIndex];
	GeometryShader *geometryShader = GeometryShader::Find(signature);
	if (!geometryShader)
	{
		static const char *const shaderSource[kLocalGeometryShaderCount] =
		{
			GeometryShader::extrudeNormalLine
		};

		const char *source = shaderSource[shaderIndex];
		geometryShader = GeometryShader::New(source, Text::GetTextLength(source), signature);
	}

	return (geometryShader);
}

void GraphicsMgr::SetBlendState(unsigned_int32 newBlendState)
{
	unsigned_int32 oldBlendState = currentBlendState;
	if (newBlendState != oldBlendState)
	{
		static const unsigned_int32 blendFactor[kBlendFactorCount] =
		{
			Render::kBlendZero, Render::kBlendOne,
			Render::kBlendSrcColor, Render::kBlendDstColor, Render::kBlendConstColor,
			Render::kBlendSrcAlpha, Render::kBlendDstAlpha, Render::kBlendConstAlpha,
			Render::kBlendInvSrcColor, Render::kBlendInvDstColor, Render::kBlendInvConstColor,
			Render::kBlendInvSrcAlpha, Render::kBlendInvDstAlpha, Render::kBlendInvConstAlpha
		};

		currentBlendState = newBlendState;

		unsigned_int32 sourceRGB = blendFactor[GetBlendSource(newBlendState)];
		unsigned_int32 destRGB = blendFactor[GetBlendDest(newBlendState)];
		unsigned_int32 sourceAlpha = blendFactor[GetBlendSourceAlpha(newBlendState)];
		unsigned_int32 destAlpha = blendFactor[GetBlendDestAlpha(newBlendState)];

		Render::SetBlendFunc(sourceRGB, destRGB, sourceAlpha, destAlpha);
	}
}

void GraphicsMgr::SetRenderState(unsigned_int32 newRenderState)
{
	unsigned_int32 oldRenderState = currentRenderState;
	unsigned_int32 changed = newRenderState ^ oldRenderState;
	if (changed != 0)
	{
		currentRenderState = newRenderState;

		if (changed & kRenderDepthTest)
		{
			if (newRenderState & kRenderDepthTest)
			{
				Render::EnableDepthTest();
			}
			else
			{
				Render::DisableDepthTest();
			}
		}

		if (changed & kRenderColorInhibit)
		{
			if (newRenderState & kRenderColorInhibit)
			{
				Render::SetColorMask(false, false, false, false);
			}
			else
			{
				Render::SetColorMask(true, true, true, true);
			}
		}

		if (changed & kRenderDepthInhibit)
		{
			if (newRenderState & kRenderDepthInhibit)
			{
				Render::SetDepthMask(false);
			}
			else
			{
				Render::SetDepthMask(true);
			}
		}

		if ((changed & kRenderDepthOffset) && (!(newRenderState & kRenderDepthOffset)))
		{
			currentProjectionMatrix = cameraProjectionMatrix;
		}

		if (changed & kRenderLineSmooth)
		{
			if (newRenderState & kRenderLineSmooth)
			{
				Render::EnableLineSmooth();
			}
			else
			{
				Render::DisableLineSmooth();
			}
		}

		if (changed & kRenderWireframe)
		{
			if (newRenderState & kRenderWireframe)
			{
				Render::SetLinePolygonMode();
			}
			else
			{
				Render::SetFillPolygonMode();
			}
		}

		if (changed & kRenderStencilTest)
		{
			if (newRenderState & kRenderStencilTest)
			{
				Render::SetStencilFunc(Render::kStencilEqual, 0, ~0);
			}
			else
			{
				Render::SetStencilFunc(Render::kStencilAlways, 0, ~0);
			}
		}
	}
}

void GraphicsMgr::SetMaterialState(unsigned_int32 newMaterialState)
{
	newMaterialState &= kMaterialShaderStateMask;
	unsigned_int32 oldMaterialState = currentMaterialState;
	unsigned_int32 changed = newMaterialState ^ oldMaterialState;
	if (changed != 0)
	{
		currentMaterialState = newMaterialState;

		if (changed & kMaterialTwoSided)
		{
			if (newMaterialState & kMaterialTwoSided)
			{
				Render::DisableCullFace();
			}
			else
			{
				Render::EnableCullFace();
			}
		}

		if (changed & kMaterialAlphaCoverage)
		{
			if (newMaterialState & kMaterialAlphaCoverage)
			{
				Render::EnableAlphaCoverage();
			}
			else
			{
				Render::DisableAlphaCoverage();
			}
		}

		if (changed & kMaterialSampleShading)
		{
			if (newMaterialState & kMaterialSampleShading)
			{
				Render::EnableSampleShading();
			}
			else
			{
				Render::DisableSampleShading();
			}
		}
	}

	if (newMaterialState & (kMaterialEmissionGlow | kMaterialSpecularBloom))
	{
		currentGraphicsState |= kGraphicsGlowBloomAvail;
	}
}

void GraphicsMgr::ResetArrayState(void)
{
	unsigned_int32 arrayState = currentArrayState;

	if (arrayState & (1 << kShaderArrayPosition1))
	{
		Render::DisableVertexAttribArray(1);
	}

	if (arrayState & (1 << kShaderArrayPrevious))
	{
		Render::DisableVertexAttribArray(7);
	}

	if (arrayState & (1 << kShaderArrayNormal))
	{
		Render::DisableVertexAttribArray(2);
	}

	if (arrayState & (1 << kShaderArrayTangent))
	{
		Render::DisableVertexAttribArray(6);
	}

	if (arrayState & (1 << kShaderArrayColor0))
	{
		Render::DisableVertexAttribArray(3);
	}

	if (arrayState & (1 << kShaderArrayColor1))
	{
		Render::DisableVertexAttribArray(4);
	}

	if (arrayState & (1 << kShaderArrayColor2))
	{
		Render::DisableVertexAttribArray(5);
	}

	arrayState &= ~((1 << kShaderArrayPosition1) | (1 << kShaderArrayPrevious) | (1 << kShaderArrayNormal) | (1 << kShaderArrayTangent) | (1 << kShaderArrayColor0) | (1 << kShaderArrayColor1) | (1 << kShaderArrayColor2));

	unsigned_int32 bit = 1 << kShaderArrayTexture0;
	for (machine a = 0; a < kMaxShaderArrayCount - kShaderArrayTexture0; a++)
	{
		if (arrayState & bit)
		{
			arrayState &= ~bit;
			Render::DisableVertexAttribArray(a + 8);
		}

		bit <<= 1;
	}

	currentArrayState = arrayState;
}

void GraphicsMgr::SetVertexArray(const ShaderData *shaderData)
{
	const Render::VertexBufferObject *vertexBuffer = shaderData->vertexBuffer[kShaderArrayPosition0];
	unsigned_int32 offset = *shaderData->attributeOffset[kShaderArrayPosition0];
	Render::SetVertexAttribArray(0, shaderData->componentCount[kShaderArrayPosition0], Render::kVertexFloat, shaderData->vertexStride[kShaderArrayPosition0], vertexBuffer, offset);
}

void GraphicsMgr::SetPosition1Array(const ShaderData *shaderData)
{
	unsigned_int32 arrayState = currentArrayState;

	const Render::VertexBufferObject *vertexBuffer = shaderData->vertexBuffer[kShaderArrayPosition1];
	if (vertexBuffer)
	{
		if (!(arrayState & (1 << kShaderArrayPosition1)))
		{
			currentArrayState = arrayState | (1 << kShaderArrayPosition1);
			Render::EnableVertexAttribArray(1);
		}

		unsigned_int32 offset = *shaderData->attributeOffset[kShaderArrayPosition1];
		Render::SetVertexAttribArray(1, shaderData->componentCount[kShaderArrayPosition1], Render::kVertexFloat, shaderData->vertexStride[kShaderArrayPosition1], vertexBuffer, offset);
	}
	else if (arrayState & (1 << kShaderArrayPosition1))
	{
		currentArrayState = arrayState & ~(1 << kShaderArrayPosition1);
		Render::DisableVertexAttribArray(1);
	}
}

void GraphicsMgr::SetPreviousArray(const ShaderData *shaderData)
{
	unsigned_int32 arrayState = currentArrayState;

	const Render::VertexBufferObject *vertexBuffer = shaderData->vertexBuffer[kShaderArrayPrevious];
	if (vertexBuffer)
	{
		if (!(arrayState & (1 << kShaderArrayPrevious)))
		{
			currentArrayState = arrayState | (1 << kShaderArrayPrevious);
			Render::EnableVertexAttribArray(7);
		}

		unsigned_int32 offset = *shaderData->attributeOffset[kShaderArrayPrevious];
		Render::SetVertexAttribArray(7, shaderData->componentCount[kShaderArrayPrevious], Render::kVertexFloat, shaderData->vertexStride[kShaderArrayPrevious], vertexBuffer, offset);
	}
	else if (arrayState & (1 << kShaderArrayPrevious))
	{
		currentArrayState = arrayState & ~(1 << kShaderArrayPrevious);
		Render::DisableVertexAttribArray(7);
	}
}

void GraphicsMgr::SetNormalArray(const ShaderData *shaderData)
{
	unsigned_int32 arrayState = currentArrayState;

	const Render::VertexBufferObject *vertexBuffer = shaderData->vertexBuffer[kShaderArrayNormal];
	if (vertexBuffer)
	{
		if (!(arrayState & (1 << kShaderArrayNormal)))
		{
			currentArrayState = arrayState | (1 << kShaderArrayNormal);
			Render::EnableVertexAttribArray(2);
		}

		unsigned_int32 offset = *shaderData->attributeOffset[kShaderArrayNormal];
		Render::SetVertexAttribArray(2, shaderData->componentCount[kShaderArrayNormal], Render::kVertexFloat, shaderData->vertexStride[kShaderArrayNormal], vertexBuffer, offset);
	}
	else if (arrayState & (1 << kShaderArrayNormal))
	{
		currentArrayState = arrayState & ~(1 << kShaderArrayNormal);
		Render::DisableVertexAttribArray(2);
	}
}

void GraphicsMgr::SetTangentArray(const ShaderData *shaderData)
{
	unsigned_int32 arrayState = currentArrayState;

	const Render::VertexBufferObject *vertexBuffer = shaderData->vertexBuffer[kShaderArrayTangent];
	if (vertexBuffer)
	{
		if (!(arrayState & (1 << kShaderArrayTangent)))
		{
			currentArrayState = arrayState | (1 << kShaderArrayTangent);
			Render::EnableVertexAttribArray(6);
		}

		unsigned_int32 offset = *shaderData->attributeOffset[kShaderArrayTangent];
		Render::SetVertexAttribArray(6, shaderData->componentCount[kShaderArrayTangent], Render::kVertexFloat, shaderData->vertexStride[kShaderArrayTangent], vertexBuffer, offset);
	}
	else if (arrayState & (1 << kShaderArrayTangent))
	{
		currentArrayState = arrayState & ~(1 << kShaderArrayTangent);
		Render::DisableVertexAttribArray(6);
	}
}

void GraphicsMgr::SetOffsetArray(const ShaderData *shaderData)
{
	unsigned_int32 arrayState = currentArrayState;

	const Render::VertexBufferObject *vertexBuffer = shaderData->vertexBuffer[kShaderArrayOffset];
	if (vertexBuffer)
	{
		if (!(arrayState & (1 << kShaderArrayOffset)))
		{
			currentArrayState = arrayState | (1 << kShaderArrayOffset);
			Render::EnableVertexAttribArray(6);
		}

		unsigned_int32 offset = *shaderData->attributeOffset[kShaderArrayOffset];
		Render::SetVertexAttribArray(6, shaderData->componentCount[kShaderArrayOffset], Render::kVertexFloat, shaderData->vertexStride[kShaderArrayOffset], vertexBuffer, offset);
	}
	else if (arrayState & (1 << kShaderArrayOffset))
	{
		currentArrayState = arrayState & ~(1 << kShaderArrayOffset);
		Render::DisableVertexAttribArray(6);
	}
}

void GraphicsMgr::SetColorArray(const ShaderData *shaderData)
{
	unsigned_int32 arrayState = currentArrayState;

	const Render::VertexBufferObject *vertexBuffer = shaderData->vertexBuffer[kShaderArrayColor0];
	if (vertexBuffer)
	{
		if (!(arrayState & (1 << kShaderArrayColor0)))
		{
			currentArrayState = arrayState | (1 << kShaderArrayColor0);
			Render::EnableVertexAttribArray(3);
		}

		unsigned_int32 offset = *shaderData->attributeOffset[kShaderArrayColor0];
		int32 size = shaderData->componentCount[kShaderArrayColor0];
		if (size > 1)
		{
			Render::SetVertexAttribArray(3, size, Render::kVertexFloat, shaderData->vertexStride[kShaderArrayColor0], vertexBuffer, offset);
		}
		else
		{
			Render::SetVertexAttribArray(3, 4, Render::kVertexUnsignedByte, true, shaderData->vertexStride[kShaderArrayColor0], vertexBuffer, offset);
		}
	}
	else if (arrayState & (1 << kShaderArrayColor0))
	{
		currentArrayState = arrayState & ~(1 << kShaderArrayColor0);
		Render::DisableVertexAttribArray(3);
	}
}

void GraphicsMgr::SetAuxColorArray(const ShaderData *shaderData, int32 index)
{
	unsigned_int32 color = index - kShaderArrayColor0;

	unsigned_int32 arrayBit = 1 << index;
	unsigned_int32 arrayState = currentArrayState;

	const Render::VertexBufferObject *vertexBuffer = shaderData->vertexBuffer[index];
	if (vertexBuffer)
	{
		if (!(arrayState & arrayBit))
		{
			currentArrayState = arrayState | arrayBit;
			Render::EnableVertexAttribArray(color + 3);
		}

		unsigned_int32 offset = *shaderData->attributeOffset[index];
		Render::SetVertexAttribArray(color + 3, 4, Render::kVertexUnsignedByte, true, shaderData->vertexStride[index], vertexBuffer, offset);
	}
	else if (arrayState & arrayBit)
	{
		currentArrayState = arrayState & ~arrayBit;
		Render::DisableVertexAttribArray(color + 3);
	}
}

void GraphicsMgr::SetTexcoordArray(const ShaderData *shaderData, int32 index)
{
	unsigned_int32 coord = index - kShaderArrayTexture0;

	unsigned_int32 arrayBit = 1 << index;
	unsigned_int32 arrayState = currentArrayState;

	const Render::VertexBufferObject *vertexBuffer = shaderData->vertexBuffer[index];
	if (vertexBuffer)
	{
		if (!(arrayState & arrayBit))
		{
			currentArrayState = arrayState | arrayBit;
			Render::EnableVertexAttribArray(coord + 8);
		}

		unsigned_int32 offset = *shaderData->attributeOffset[index];
		Render::SetVertexAttribArray(coord + 8, shaderData->componentCount[index], Render::kVertexFloat, shaderData->vertexStride[index], vertexBuffer, offset);
	}
	else if (arrayState & arrayBit)
	{
		currentArrayState = arrayState & ~arrayBit;
		Render::DisableVertexAttribArray(coord + 8);
	}
}

void GraphicsMgr::DrawRenderList(const List<Renderable> *renderList)
{
	Renderable *renderable = renderList->First();
	if (!renderable)
	{
		return;
	}

	unsigned_int32 graphicsState = currentGraphicsState;

	unsigned_int32 extraState = 0;
	if (lightObject)
	{
		extraState = kRenderDepthInhibit;
		unsigned_int32 lightFlags = lightObject->GetLightFlags();
		if ((!(lightFlags & kLightShadowInhibit)) && (graphicsState & kGraphicsStencilValid))
		{
			extraState |= kRenderStencilTest;
		}

		if (graphicsState & kGraphicsDepthTestLess)
		{
			currentGraphicsState = graphicsState & ~kGraphicsDepthTestLess;
			Render::SetDepthFunc(Render::kDepthLessEqual);
		}
	}
	else
	{
		if (!(graphicsState & kGraphicsAmbientLessEqual))
		{
			if (!(graphicsState & kGraphicsDepthTestLess))
			{
				currentGraphicsState = graphicsState | kGraphicsDepthTestLess;
				Render::SetDepthFunc(Render::kDepthLess);
			}
		}
		else if (graphicsState & kGraphicsDepthTestLess)
		{
			currentGraphicsState = graphicsState & ~kGraphicsDepthTestLess;
			Render::SetDepthFunc(Render::kDepthLessEqual);
		}
	}

	do
	{
		unsigned_int32 newRenderState = renderable->GetRenderState() & ~disabledRenderState;
		SetRenderState(newRenderState | extraState);

		unsigned_int32 renderableFlags = renderable->GetRenderableFlags();
		const Transformable *transformable = renderable->GetTransformable();
		if (!(renderableFlags & kRenderableCameraTransformInhibit))
		{
			if (newRenderState & kRenderDepthOffset)
			{
				float z = Fmin(cameraSpaceTransform.GetRow(2) ^ renderable->GetDepthOffsetPoint(), -cameraObject->GetNearDepth());
				float delta = renderable->GetDepthOffsetDelta();
				float epsilon = depthOffsetConstant * delta / (z * (z + delta));
				epsilon = Fmax(Fabs(epsilon), 4.8e-7F) * NonzeroFsgn(epsilon);

				currentProjectionMatrix = cameraProjectionMatrix;
				currentProjectionMatrix(2,2) *= 1.0F + epsilon;
			}

			if (transformable)
			{
				SetModelviewMatrix(cameraSpaceTransform * transformable->GetWorldTransform());
			}
			else
			{
				SetModelviewMatrix(cameraSpaceTransform);
			}
		}
		else
		{
			if (transformable)
			{
				SetModelviewMatrix(transformable->GetWorldTransform());
			}
			else
			{
				SetModelviewMatrix(K::identity_4D);
			}
		}

		ShaderType shaderType = currentShaderType;
		if (shaderType == kShaderAmbient)
		{
			shaderType = renderable->GetAmbientEnvironment()->ambientShaderType;
		}

		ShaderVariant variant = (renderableFlags & (kRenderableFogInhibit | kRenderableUnfog)) ? kShaderVariantNormal : currentShaderVariant;

		RenderSegment *segment = renderable->GetFirstRenderSegment();
		do
		{
			const ShaderData *shaderData = segment->GetShaderData(shaderType, renderable->GetShaderDetailLevel());
			if ((!shaderData) || ((shaderData->variantMask & (1 << variant)) == 0))
			{
				shaderData = segment->InitShaderData(renderable, shaderType, variant);
			}

			if (!shaderData->shaderProgram[variant])
			{
				continue;
			}

			SetBlendState(shaderData->blendState);
			SetMaterialState(shaderData->materialState);

			Render::SetShaderProgram(shaderData->shaderProgram[variant]);

			SetVertexArray(shaderData);
			SetPosition1Array(shaderData);
			SetNormalArray(shaderData);
			SetTangentArray(shaderData);
			SetColorArray(shaderData);
			SetAuxColorArray(shaderData, kShaderArrayColor1);
			SetAuxColorArray(shaderData, kShaderArrayColor2);

			for (machine a = kShaderArrayTexture0; a < kMaxShaderArrayCount; a++)
			{
				SetTexcoordArray(shaderData, a);
			}

			int32 stateProcCount = shaderData->shaderStateDataCount;
			for (machine a = 0; a < stateProcCount; a++)
			{
				(*shaderData->shaderStateData[a].stateProc)(renderable, shaderData->shaderStateData[a].stateCookie);
			}

			int32 unitCount = shaderData->textureUnitCount;
			const Render::TextureObject *const *textureObject = shaderData->textureObject;
			for (machine unit = 0; unit < unitCount; unit++)
			{
				Render::BindTexture(unit, textureObject[unit]);
			}

			int32 vertexCount = renderable->GetVertexCount();
			graphicsCounter[kGraphicsCounterDirectVertices] += vertexCount;
			graphicsCounter[kGraphicsCounterDirectCommands]++;

			switch (renderable->GetRenderType())
			{
				case kRenderPointSprites:

					Render::EnablePointSprite();
					Render::DrawPrimitives(Render::kPrimitivePoints, 0, vertexCount);
					Render::DisablePointSprite();
					break;

				case kRenderPoints:

					Render::DrawPrimitives(Render::kPrimitivePoints, 0, vertexCount);
					break;

				case kRenderLines:

					Render::DrawPrimitives(Render::kPrimitiveLines, 0, vertexCount);
					break;

				case kRenderLineStrip:

					Render::DrawPrimitives(Render::kPrimitiveLineStrip, 0, vertexCount);
					break;

				case kRenderLineLoop:

					Render::DrawPrimitives(Render::kPrimitiveLineLoop, 0, vertexCount);
					break;

				case kRenderIndexedLines:

					Render::DrawIndexedPrimitives(Render::kPrimitiveLines, vertexCount, segment->GetPrimitiveCount() * 2, shaderData->indexBuffer, renderable->GetPrimitiveIndexOffset() + segment->GetPrimitiveStart() * sizeof(Line));
					break;

				case kRenderTriangles:

					graphicsCounter[kGraphicsCounterDirectPrimitives] += vertexCount / 3;
					Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, vertexCount);
					break;

				case kRenderTriangleStrip:

					graphicsCounter[kGraphicsCounterDirectPrimitives] += vertexCount - 2;
					Render::DrawPrimitives(Render::kPrimitiveTriangleStrip, 0, vertexCount);
					break;

				case kRenderIndexedTriangles:
				{
					int32 triangleCount = segment->GetPrimitiveCount();
					graphicsCounter[kGraphicsCounterDirectPrimitives] += triangleCount;

					Render::DrawIndexedPrimitives(Render::kPrimitiveTriangles, vertexCount, triangleCount * 3, shaderData->indexBuffer, renderable->GetPrimitiveIndexOffset() + segment->GetPrimitiveStart() * sizeof(Triangle));
					break;
				}

				case kRenderQuads:

					graphicsCounter[kGraphicsCounterDirectPrimitives] += vertexCount >> 2;
					Render::DrawQuads(0, vertexCount);
					break;

				case kRenderMultiIndexedTriangles:

					Render::MultiDrawIndexedPrimitives(Render::kPrimitiveTriangles, segment->GetMultiCountArray(), shaderData->indexBuffer, segment->GetMultiOffsetArray(), segment->GetMultiRenderCount());
					break;

				case kRenderMaskedMultiIndexedTriangles:
				{
					static machine_address	index[33];
					static unsigned_int32	count[33];

					unsigned_int32 size = 0;
					unsigned_int32 offset = renderable->GetPrimitiveIndexOffset();

					int32 triangleCount = segment->GetPrimitiveCount();
					if (triangleCount != 0)
					{
						graphicsCounter[kGraphicsCounterDirectPrimitives] += triangleCount;

						index[0] = offset;
						count[0] = triangleCount * 3;
						size = 1;
					}

					unsigned_int32 mask = segment->GetMultiRenderMask();
					const int32 *data = segment->GetMultiRenderData();
					while (mask != 0)
					{
						if (mask & 1)
						{
							triangleCount = data[1];
							graphicsCounter[kGraphicsCounterDirectPrimitives] += triangleCount;

							index[size] = offset + data[0] * sizeof(Triangle);
							count[size] = triangleCount * 3;
							size++;
						}

						mask >>= 1;
						data += 2;
					}

					Render::MultiDrawIndexedPrimitives(Render::kPrimitiveTriangles, count, shaderData->indexBuffer, index, size);
					break;
				}
			}
		} while ((segment = segment->GetNextRenderSegment()) != nullptr);

		OcclusionQuery *query = currentOcclusionQuery;
		if (query)
		{
			currentOcclusionQuery = nullptr;
			occlusionQueryList.Append(query);
			Render::EndOcclusionQuery(query);
		}

		renderable = renderable->Next();
	} while (renderable);

	#if C4DIAGNOSTICS

		if ((diagnosticFlags & (kDiagnosticWireframe | kDiagnosticNormals | kDiagnosticTangents)) && (!lightObject))
		{
			if (diagnosticFlags & kDiagnosticWireframe)
			{
				DrawWireframe((diagnosticFlags & kDiagnosticDepthTest) ? kWireframeDepthTest : 0, renderList);
			}

			if (diagnosticFlags & kDiagnosticNormals)
			{
				DrawVectors(kArrayNormal, renderList);
			}

			if (diagnosticFlags & kDiagnosticTangents)
			{
				DrawVectors(kArrayTangent, renderList);
			}
		}

	#endif
}

bool GraphicsMgr::BeginStructureRendering(const Transform4D& previousCameraWorldTransform, unsigned_int32 structureFlags, float velocityScale)
{
	unsigned_int32 activeFlags = graphicsActiveFlags;
	if ((!(activeFlags & kGraphicsActiveStructureRendering)) || (currentRenderTargetType != kRenderTargetPrimary))
	{
		return (false);
	}

	if (activeFlags & kGraphicsActiveTimer)
	{
		Render::QueryTimeStamp(&timerQuery[kGraphicsTimeIndexBeginStructure]);
	}

	if (!(renderOptionFlags & kRenderOptionMotionBlur))
	{
		structureFlags &= ~kStructureRenderVelocity;
	}

	previousCameraSpaceTransform(3,0) = previousCameraSpaceTransform(3,1) = previousCameraSpaceTransform(3,2) = 0.0F;
	previousCameraSpaceTransform(3,3) = 1.0F;

	const Vector3D& rightDirection = previousCameraWorldTransform[0];
	const Vector3D& downDirection = previousCameraWorldTransform[1];
	const Vector3D& viewDirection = previousCameraWorldTransform[2];

	previousCameraSpaceTransform(0,0) = rightDirection.x;
	previousCameraSpaceTransform(0,1) = rightDirection.y;
	previousCameraSpaceTransform(0,2) = rightDirection.z;
	previousCameraSpaceTransform(1,0) = -downDirection.x;
	previousCameraSpaceTransform(1,1) = -downDirection.y;
	previousCameraSpaceTransform(1,2) = -downDirection.z;
	previousCameraSpaceTransform(2,0) = -viewDirection.x;
	previousCameraSpaceTransform(2,1) = -viewDirection.y;
	previousCameraSpaceTransform(2,2) = -viewDirection.z;

	const Vector3D& previousCameraWorldOffset = previousCameraWorldTransform[3];
	previousCameraSpaceTransform(0,3) = -(previousCameraSpaceTransform.GetRow(0) ^ previousCameraWorldOffset);
	previousCameraSpaceTransform(1,3) = -(previousCameraSpaceTransform.GetRow(1) ^ previousCameraWorldOffset);
	previousCameraSpaceTransform(2,3) = -(previousCameraSpaceTransform.GetRow(2) ^ previousCameraWorldOffset);

	float red = 0.0F;
	float green = 0.0F;
	unsigned_int32 graphicsState = currentGraphicsState;

	if ((structureFlags & (kStructureZeroBackgroundVelocity | kStructureRenderVelocity)) == kStructureRenderVelocity)
	{
		graphicsState |= kGraphicsMotionBlurAvail;

		Vector4D v = currentProjectionMatrix * (cameraSpaceTransform * viewDirection);
		float w = (float) (viewportRect.right - viewportRect.left) * 0.5F;
		float h = (float) (viewportRect.top - viewportRect.bottom) * 0.5F;
		float f = kVelocityMultiplier / v.w;
		red = v.x * f * w;
		green = v.y * f * h;

		float m = 1.0F / Fmax(Fabs(red), Fabs(green), 1.0F);
		red = Clamp(red * m, -1.0F, 1.0F);
		green = Clamp(green * m, -1.0F, 1.0F);
	}

	normalFrameBuffer->GetRenderTargetTexture(kRenderTargetStructure)->UnbindAll();
	SetRenderTarget(kRenderTargetStructure);

	ResetArrayState();
	Render::DisableBlend();

	if (multisampleFrameBuffer)
	{
		if (structureFlags & kStructureClearBuffer)
		{
			#if C4OPENGL || C4ORBIS

				SetRenderState(kRenderDepthTest);
				Render::ClearColorDepthStencilBuffers(red, green, 65504.0F, 0.0F);

			#elif C4PS3 //[ 

			// -- PS3 code hidden --

			#endif //]
		}
		else
		{
			SetRenderState(kRenderDepthTest);
			Render::ClearDepthStencilBuffers();
		}
	}
	else
	{
		if (structureFlags & kStructureClearBuffer)
		{
			#if C4OPENGL || C4ORBIS

				Render::ClearColorBuffer(red, green, 65504.0F, 0.0F);

			#elif C4PS3 //[ 

			// -- PS3 code hidden --

			#endif //]
		}

		graphicsState |= kGraphicsAmbientLessEqual;
	}

	if (!(graphicsState & kGraphicsDepthTestLess))
	{
		graphicsState |= kGraphicsDepthTestLess;
		Render::SetDepthFunc(Render::kDepthLess);
	}

	currentGraphicsState = graphicsState;
	currentStructureFlags = structureFlags;

	float scale = velocityScale * kVelocityMultiplier;

	#if C4OPENGL

		Render::SetFragmentShaderParameter(kFragmentParamVelocityScale, scale, scale, 0.0F, 0.0F);

	#elif C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]

	MemoryMgr::ClearMemory(motionGridFlag, kProcessGridWidth * kProcessGridHeight);
	return (true);
}

void GraphicsMgr::EndStructureRendering(void)
{
	if (graphicsActiveFlags & kGraphicsActiveTimer)
	{
		Render::QueryTimeStamp(&timerQuery[kGraphicsTimeIndexEndStructure]);
	}

	if (graphicsActiveFlags & kGraphicsActiveAmbientOcclusion)
	{
		static Link<ShaderProgram>		occlusionShaderProgram;
		static Link<ShaderProgram>		occlusionBlurShaderProgram;

		normalFrameBuffer->GetRenderTargetTexture(kRenderTargetOcclusion1)->UnbindAll();
		SetRenderTarget(kRenderTargetOcclusion1);

		SetRenderState(0);
		SetMaterialState(currentMaterialState & kMaterialTwoSided);

		ResetArrayState();

		const Render::TextureObject *structureTexture = normalFrameBuffer->GetRenderTargetTexture(kRenderTargetStructure);
		Render::BindTexture(1, structureTexture);
		Render::BindTexture(5, OcclusionPostProcess::GetVectorNoiseTexture());

		VertexShader *vertexShader = nullptr;
		ShaderProgram *shaderProgram = occlusionShaderProgram;
		if (!shaderProgram)
		{
			ShaderGraph		shaderGraph;

			shaderGraph.AddElement(new OcclusionPostProcess);

			vertexShader = GetLocalVertexShader(&VertexShader::nullTransform);
			shaderProgram = ShaderAttribute::CompilePostShader(&shaderGraph, vertexShader);
			occlusionShaderProgram = shaderProgram;
			vertexShader->Release();
		}

		Render::SetShaderProgram(shaderProgram);
		Render::SetFragmentShaderParameter(0, occlusionPlaneScale, 2.0F, 0.1F, 0.0F);

		Render::SetVertexAttribArray(0, 2, Render::kVertexFloat, sizeof(Point2D), &fullscreenVertexBuffer, 0);
		Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, 3);

		normalFrameBuffer->GetRenderTargetTexture(kRenderTargetOcclusion2)->UnbindAll();
		SetRenderTarget(kRenderTargetOcclusion2);

		const Render::TextureObject *occlusionTexture = normalFrameBuffer->GetRenderTargetTexture(kRenderTargetOcclusion1);
		Render::BindTexture(2, occlusionTexture);

		shaderProgram = occlusionBlurShaderProgram;
		if (!shaderProgram)
		{
			ShaderGraph		shaderGraph;

			shaderGraph.AddElement(new OcclusionBlurPostProcess);
			shaderProgram = ShaderAttribute::CompilePostShader(&shaderGraph, vertexShader);
			occlusionBlurShaderProgram = shaderProgram;
		}

		Render::SetShaderProgram(shaderProgram);
		Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, 3);

		occlusionTexture->Unbind(2);
		structureTexture->Unbind(1);

		#if C4PS3 //[ 

			// -- PS3 code hidden --

		#endif //]

		if (graphicsActiveFlags & kGraphicsActiveTimer)
		{
			Render::QueryTimeStamp(&timerQuery[kGraphicsTimeIndexEndOcclusion]);
		}
	}
	else
	{
		unsigned_int32 arrayState = currentArrayState;
		if (arrayState & (1 << kShaderArrayPrevious))
		{
			currentArrayState = arrayState & ~(1 << kShaderArrayPrevious);
			Render::DisableVertexAttribArray(7);
		}
	}

	Render::EnableBlend();
	SetRenderTarget(kRenderTargetPrimary);
}

void GraphicsMgr::DrawStructureList(const List<Renderable> *renderList)
{
	if (!(currentStructureFlags & kStructureRenderVelocity))
	{
		DrawStructureDepthList(renderList);
		return;
	}

	for (Renderable *renderable = renderList->First(); renderable; renderable = renderable->Next())
	{
		Transform4D		mv, *modelview;
		Matrix4D		mvp1;

		unsigned_int32 flags = renderable->GetRenderableFlags();
		if (flags & kRenderableStructureBufferInhibit)
		{
			continue;
		}

		const Transformable *transformable = renderable->GetTransformable();
		if (transformable)
		{
			mv = cameraSpaceTransform * transformable->GetWorldTransform();
			modelview = &mv;
		}
		else
		{
			modelview = &cameraSpaceTransform;
		}

		if (flags & kRenderableMotionBlurGradient)
		{
			static const char edgeVertexIndex[24] =
			{
				0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 1, 5, 2, 6, 3, 7
			};

			Vector4D	vertex[8];
			float 		xmin, xmax, ymin, ymax;

			Matrix4D transform = standardProjectionMatrix * *modelview;
			const Box3D *box = renderable->GetMotionBlurBox();

			vertex[0] = transform * Point3D(box->min.x, box->min.y, box->min.z);
			vertex[1] = transform * Point3D(box->max.x, box->min.y, box->min.z);
			vertex[2] = transform * Point3D(box->max.x, box->max.y, box->min.z);
			vertex[3] = transform * Point3D(box->min.x, box->max.y, box->min.z);
			vertex[4] = transform * Point3D(box->min.x, box->min.y, box->max.z);
			vertex[5] = transform * Point3D(box->max.x, box->min.y, box->max.z);
			vertex[6] = transform * Point3D(box->max.x, box->max.y, box->max.z);
			vertex[7] = transform * Point3D(box->min.x, box->max.y, box->max.z);

			bool visible = false;
			const char *edge = edgeVertexIndex;
			for (machine a = 0; a < 12; a++, edge += 2)
			{
				Vector4D p1 = vertex[edge[0]];
				Vector4D p2 = vertex[edge[1]];

				if (p1.z < -p1.w)
				{
					if (p2.z < -p2.w)
					{
						continue;
					}

					Vector4D dp = p1 - p2;
					p1 -= dp * ((p1.z + p1.w) / (dp.w + dp.z));
				}
				else if (p2.z < -p2.w)
				{
					Vector4D dp = p2 - p1;
					p2 -= dp * ((p2.z + p2.w) / (dp.w + dp.z));
				}

				float f1 = 1.0F / p1.w;
				float f2 = 1.0F / p2.w;
				float x1 = p1.x * f1;
				float x2 = p2.x * f2;
				float y1 = p1.y * f1;
				float y2 = p2.y * f2;

				if (!visible)
				{
					visible = true;

					if (x1 < x2)
					{
						xmin = x1;
						xmax = x2;
					}
					else
					{
						xmin = x2;
						xmax = x1;
					}

					if (y1 < y2)
					{
						ymin = y1;
						ymax = y2;
					}
					else
					{
						ymin = y2;
						ymax = y1;
					}
				}
				else
				{
					if (x1 < xmin)
					{
						xmin = x1;
					}
					else if (x1 > xmax)
					{
						xmax = x1;
					}

					if (x2 < xmin)
					{
						xmin = x2;
					}
					else if (x2 > xmax)
					{
						xmax = x2;
					}

					if (y1 < ymin)
					{
						ymin = y1;
					}
					else if (y1 > ymax)
					{
						ymax = y1;
					}

					if (y2 < ymin)
					{
						ymin = y2;
					}
					else if (y2 > ymax)
					{
						ymax = y2;
					}
				}
			}

			if (!visible)
			{
				continue;
			}

			int32 left = MaxZero((int32) ((xmin * 0.5F + motionBlurBoxLeftOffset) * (float) kProcessGridWidth));
			int32 right = Min((int32) ((xmax * 0.5F + motionBlurBoxRightOffset) * (float) kProcessGridWidth), kProcessGridWidth - 1);
			int32 bottom = MaxZero((int32) ((ymin * 0.5F + motionBlurBoxBottomOffset) * (float) kProcessGridHeight));
			int32 top = Min((int32) ((ymax * 0.5F + motionBlurBoxTopOffset) * (float) kProcessGridHeight), kProcessGridHeight - 1);

			for (machine j = bottom; j <= top; j++)
			{
				machine k = j * kProcessGridWidth;
				for (machine i = left; i <= right; i++)
				{
					motionGridFlag[k + i] = true;
				}
			}
		}

		unsigned_int32 newRenderState = kRenderDepthTest | (renderable->GetRenderState() & (kRenderDepthInhibit | kRenderDepthOffset));
		SetRenderState(newRenderState);

		if (newRenderState & kRenderDepthOffset)
		{
			float z = Fmin(cameraSpaceTransform.GetRow(2) ^ renderable->GetDepthOffsetPoint(), -cameraObject->GetNearDepth());
			float delta = renderable->GetDepthOffsetDelta();
			float epsilon = depthOffsetConstant * delta / (z * (z + delta));
			epsilon = Fmax(Fabs(epsilon), 4.8e-7F) * NonzeroFsgn(epsilon);

			currentProjectionMatrix = cameraProjectionMatrix;
			currentProjectionMatrix(2,2) *= 1.0F + epsilon;
		}

		SetModelviewMatrix(*modelview);

		if (transformable)
		{
			const Transform4D *previousWorldTransform = renderable->GetPreviousWorldTransformPointer();
			if (previousWorldTransform)
			{
				mvp1 = currentProjectionMatrix * (previousCameraSpaceTransform * *previousWorldTransform);
			}
			else
			{
				mvp1 = currentProjectionMatrix * (previousCameraSpaceTransform * transformable->GetWorldTransform());
			}
		}
		else
		{
			mvp1 = currentProjectionMatrix * previousCameraSpaceTransform;
		}

		float l = (float) viewportRect.left;
		float b = (float) viewportRect.bottom;
		float w = ((float) viewportRect.right - l) * 0.5F;
		float h = ((float) viewportRect.top - b) * 0.5F;
		l += w;
		b += h;

		Render::SetVertexShaderParameter(kVertexParamMatrixVelocityA, mvp1(0,0) * w + mvp1(3,0) * l, mvp1(0,1) * w + mvp1(3,1) * l, mvp1(0,2) * w + mvp1(3,2) * l, mvp1(0,3) * w + mvp1(3,3) * l);
		Render::SetVertexShaderParameter(kVertexParamMatrixVelocityA + 1, mvp1(1,0) * h + mvp1(3,0) * b, mvp1(1,1) * h + mvp1(3,1) * b, mvp1(1,2) * h + mvp1(3,2) * b, mvp1(1,3) * h + mvp1(3,3) * b);
		Render::SetVertexShaderParameter(kVertexParamMatrixVelocityA + 2, (mvp1(2,0) + mvp1(3,0)) * 0.5F, (mvp1(2,1) + mvp1(3,1)) * 0.5F, (mvp1(2,2) + mvp1(3,2)) * 0.5F, (mvp1(2,3) + mvp1(3,3)) * 0.5F);
		Render::SetVertexShaderParameter(kVertexParamMatrixVelocityA + 3, mvp1(3,0), mvp1(3,1), mvp1(3,2), mvp1(3,3));

		const Matrix4D& mvp2 = currentMVPMatrix;
		Render::SetVertexShaderParameter(kVertexParamMatrixVelocityB, mvp2(0,0) * w + mvp2(3,0) * l, mvp2(0,1) * w + mvp2(3,1) * l, mvp2(0,2) * w + mvp2(3,2) * l, mvp2(0,3) * w + mvp2(3,3) * l);
		Render::SetVertexShaderParameter(kVertexParamMatrixVelocityB + 1, mvp2(1,0) * h + mvp2(3,0) * b, mvp2(1,1) * h + mvp2(3,1) * b, mvp2(1,2) * h + mvp2(3,2) * b, mvp2(1,3) * h + mvp2(3,3) * b);
		Render::SetVertexShaderParameter(kVertexParamMatrixVelocityB + 2, (mvp2(2,0) + mvp2(3,0)) * 0.5F, (mvp2(2,1) + mvp2(3,1)) * 0.5F, (mvp2(2,2) + mvp2(3,2)) * 0.5F, (mvp2(2,3) + mvp2(3,3)) * 0.5F);
		Render::SetVertexShaderParameter(kVertexParamMatrixVelocityB + 3, mvp2(3,0), mvp2(3,1), mvp2(3,2), mvp2(3,3));

		RenderSegment *segment = renderable->GetFirstRenderSegment();
		do
		{
			const ShaderData *shaderData = segment->GetShaderData(kShaderStructure, renderable->GetShaderDetailLevel());
			if (!shaderData)
			{
				shaderData = segment->InitShaderData(renderable, kShaderStructure);
			}

			if (!shaderData->shaderProgram[kShaderVariantNormal])
			{
				continue;
			}

			SetMaterialState(shaderData->materialState);

			Render::SetShaderProgram(shaderData->shaderProgram[kShaderVariantNormal]);

			SetVertexArray(shaderData);
			SetPosition1Array(shaderData);
			SetPreviousArray(shaderData);
			SetNormalArray(shaderData);
			SetOffsetArray(shaderData);
			SetColorArray(shaderData);
			SetAuxColorArray(shaderData, kShaderArrayColor1);
			SetAuxColorArray(shaderData, kShaderArrayColor2);

			for (machine a = kShaderArrayTexture0; a < kMaxShaderArrayCount; a++)
			{
				SetTexcoordArray(shaderData, a);
			}

			int32 stateProcCount = shaderData->shaderStateDataCount;
			for (machine a = 0; a < stateProcCount; a++)
			{
				(*shaderData->shaderStateData[a].stateProc)(renderable, shaderData->shaderStateData[a].stateCookie);
			}

			int32 unitCount = shaderData->textureUnitCount;
			const Render::TextureObject *const *textureObject = shaderData->textureObject;
			for (machine unit = 0; unit < unitCount; unit++)
			{
				Render::BindTexture(unit, textureObject[unit]);
			}

			int32 vertexCount = renderable->GetVertexCount();
			graphicsCounter[kGraphicsCounterVelocityVertices] += vertexCount;
			graphicsCounter[kGraphicsCounterVelocityCommands]++;

			switch (renderable->GetRenderType())
			{
				case kRenderTriangles:

					graphicsCounter[kGraphicsCounterVelocityPrimitives] += vertexCount / 3;
					Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, vertexCount);
					break;

				case kRenderTriangleStrip:

					graphicsCounter[kGraphicsCounterVelocityPrimitives] += vertexCount - 2;
					Render::DrawPrimitives(Render::kPrimitiveTriangleStrip, 0, vertexCount);
					break;

				case kRenderIndexedTriangles:
				{
					int32 triangleCount = segment->GetPrimitiveCount();
					graphicsCounter[kGraphicsCounterVelocityPrimitives] += triangleCount;

					Render::DrawIndexedPrimitives(Render::kPrimitiveTriangles, vertexCount, triangleCount * 3, shaderData->indexBuffer, renderable->GetPrimitiveIndexOffset() + segment->GetPrimitiveStart() * sizeof(Triangle));
					break;
				}

				case kRenderQuads:

					graphicsCounter[kGraphicsCounterVelocityPrimitives] += vertexCount >> 2;
					Render::DrawQuads(0, vertexCount);
					break;

				case kRenderMultiIndexedTriangles:

					Render::MultiDrawIndexedPrimitives(Render::kPrimitiveTriangles, segment->GetMultiCountArray(), shaderData->indexBuffer, segment->GetMultiOffsetArray(), segment->GetMultiRenderCount());
					break;

				case kRenderMaskedMultiIndexedTriangles:
				{
					static machine_address	index[33];
					static unsigned_int32	count[33];

					unsigned_int32 size = 0;
					unsigned_int32 offset = renderable->GetPrimitiveIndexOffset();

					int32 triangleCount = segment->GetPrimitiveCount();
					if (triangleCount != 0)
					{
						graphicsCounter[kGraphicsCounterVelocityPrimitives] += triangleCount;

						index[0] = offset;
						count[0] = triangleCount * 3;
						size = 1;
					}

					unsigned_int32 mask = segment->GetMultiRenderMask();
					const int32 *data = segment->GetMultiRenderData();
					while (mask != 0)
					{
						if (mask & 1)
						{
							triangleCount = data[1];
							graphicsCounter[kGraphicsCounterVelocityPrimitives] += triangleCount;

							index[size] = offset + data[0] * sizeof(Triangle);
							count[size] = triangleCount * 3;
							size++;
						}

						mask >>= 1;
						data += 2;
					}

					Render::MultiDrawIndexedPrimitives(Render::kPrimitiveTriangles, count, shaderData->indexBuffer, index, size);
					break;
				}
			}
		} while ((segment = segment->GetNextRenderSegment()) != nullptr);
	}
}

void GraphicsMgr::DrawStructureDepthList(const List<Renderable> *renderList)
{
	for (Renderable *renderable = renderList->First(); renderable; renderable = renderable->Next())
	{
		Transform4D		mv;
		Transform4D		*modelview;

		if (renderable->GetRenderableFlags() & kRenderableStructureBufferInhibit)
		{
			continue;
		}

		const Transformable *transformable = renderable->GetTransformable();
		if (transformable)
		{
			mv = cameraSpaceTransform * transformable->GetWorldTransform();
			modelview = &mv;
		}
		else
		{
			modelview = &cameraSpaceTransform;
		}

		unsigned_int32 newRenderState = kRenderDepthTest | (renderable->GetRenderState() & (kRenderDepthInhibit | kRenderDepthOffset));
		SetRenderState(newRenderState);

		if (newRenderState & kRenderDepthOffset)
		{
			float z = Fmin(cameraSpaceTransform.GetRow(2) ^ renderable->GetDepthOffsetPoint(), -cameraObject->GetNearDepth());
			float delta = renderable->GetDepthOffsetDelta();
			float epsilon = depthOffsetConstant * delta / (z * (z + delta));
			epsilon = Fmax(Fabs(epsilon), 4.8e-7F) * NonzeroFsgn(epsilon);

			currentProjectionMatrix = cameraProjectionMatrix;
			currentProjectionMatrix(2,2) *= 1.0F + epsilon;
		}

		SetModelviewMatrix(*modelview);

		RenderSegment *segment = renderable->GetFirstRenderSegment();
		do
		{
			const ShaderData *shaderData = segment->GetShaderData(kShaderStructure, renderable->GetShaderDetailLevel());
			if (!shaderData)
			{
				shaderData = segment->InitShaderData(renderable, kShaderStructure);
			}

			if (!shaderData->shaderProgram[kShaderVariantNormal])
			{
				continue;
			}

			SetMaterialState(shaderData->materialState);

			Render::SetShaderProgram(shaderData->shaderProgram[kShaderVariantNormal]);

			SetVertexArray(shaderData);
			SetPosition1Array(shaderData);
			SetPreviousArray(shaderData);
			SetNormalArray(shaderData);
			SetOffsetArray(shaderData);
			SetColorArray(shaderData);
			SetAuxColorArray(shaderData, kShaderArrayColor1);
			SetAuxColorArray(shaderData, kShaderArrayColor2);

			for (machine a = kShaderArrayTexture0; a < kMaxShaderArrayCount; a++)
			{
				SetTexcoordArray(shaderData, a);
			}

			int32 stateProcCount = shaderData->shaderStateDataCount;
			for (machine a = 0; a < stateProcCount; a++)
			{
				(*shaderData->shaderStateData[a].stateProc)(renderable, shaderData->shaderStateData[a].stateCookie);
			}

			int32 unitCount = shaderData->textureUnitCount;
			const Render::TextureObject *const *textureObject = shaderData->textureObject;
			for (machine unit = 0; unit < unitCount; unit++)
			{
				Render::BindTexture(unit, textureObject[unit]);
			}

			int32 vertexCount = renderable->GetVertexCount();
			graphicsCounter[kGraphicsCounterVelocityVertices] += vertexCount;
			graphicsCounter[kGraphicsCounterVelocityCommands]++;

			switch (renderable->GetRenderType())
			{
				case kRenderTriangles:

					graphicsCounter[kGraphicsCounterVelocityPrimitives] += vertexCount / 3;
					Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, vertexCount);
					break;

				case kRenderTriangleStrip:

					graphicsCounter[kGraphicsCounterVelocityPrimitives] += vertexCount - 2;
					Render::DrawPrimitives(Render::kPrimitiveTriangleStrip, 0, vertexCount);
					break;

				case kRenderIndexedTriangles:
				{
					int32 triangleCount = segment->GetPrimitiveCount();
					graphicsCounter[kGraphicsCounterVelocityPrimitives] += triangleCount;

					Render::DrawIndexedPrimitives(Render::kPrimitiveTriangles, vertexCount, triangleCount * 3, shaderData->indexBuffer, renderable->GetPrimitiveIndexOffset() + segment->GetPrimitiveStart() * sizeof(Triangle));
					break;
				}

				case kRenderQuads:

					graphicsCounter[kGraphicsCounterVelocityPrimitives] += vertexCount >> 2;
					Render::DrawQuads(0, vertexCount);
					break;

				case kRenderMultiIndexedTriangles:

					Render::MultiDrawIndexedPrimitives(Render::kPrimitiveTriangles, segment->GetMultiCountArray(), shaderData->indexBuffer, segment->GetMultiOffsetArray(), segment->GetMultiRenderCount());
					break;

				case kRenderMaskedMultiIndexedTriangles:
				{
					static machine_address	index[33];
					static unsigned_int32	count[33];

					unsigned_int32 size = 0;
					unsigned_int32 offset = renderable->GetPrimitiveIndexOffset();

					int32 triangleCount = segment->GetPrimitiveCount();
					if (triangleCount != 0)
					{
						graphicsCounter[kGraphicsCounterVelocityPrimitives] += triangleCount;

						index[0] = offset;
						count[0] = triangleCount * 3;
						size = 1;
					}

					unsigned_int32 mask = segment->GetMultiRenderMask();
					const int32 *data = segment->GetMultiRenderData();
					while (mask != 0)
					{
						if (mask & 1)
						{
							triangleCount = data[1];
							graphicsCounter[kGraphicsCounterVelocityPrimitives] += triangleCount;

							index[size] = offset + data[0] * sizeof(Triangle);
							count[size] = triangleCount * 3;
							size++;
						}

						mask >>= 1;
						data += 2;
					}

					Render::MultiDrawIndexedPrimitives(Render::kPrimitiveTriangles, count, shaderData->indexBuffer, index, size);
					break;
				}
			}
		} while ((segment = segment->GetNextRenderSegment()) != nullptr);
	}
}

bool GraphicsMgr::BeginDistortionRendering(void)
{
	if ((renderOptionFlags & kRenderOptionDistortion) && (currentRenderTargetType == kRenderTargetPrimary))
	{
		SetRenderTarget(kRenderTargetDistortion);
		SetRenderState(kRenderDepthTest | kRenderDepthInhibit);

		Render::ClearColorBuffer(0.0F, 0.0F, 0.0F, 0.0F);

		SetBlendState(kBlendAccumulate | kBlendAlphaAccumulate);

		unsigned_int32 graphicsState = currentGraphicsState;
		if (!(graphicsState & kGraphicsDepthTestLess))
		{
			currentGraphicsState = graphicsState | kGraphicsDepthTestLess;
			Render::SetDepthFunc(Render::kDepthLess);
		}

		ResetArrayState();

		currentGraphicsState |= kGraphicsDistortionAvail;
		return (true);
	}

	return (false);
}

void GraphicsMgr::EndDistortionRendering(void)
{
	SetRenderTarget(kRenderTargetPrimary);
}

void GraphicsMgr::DrawDistortionList(const List<Renderable> *renderList)
{
	for (Renderable *renderable = renderList->First(); renderable; renderable = renderable->Next())
	{
		unsigned_int32 newRenderState = renderable->GetRenderState();
		SetRenderState(newRenderState);

		unsigned_int32 renderableFlags = renderable->GetRenderableFlags();
		const Transformable *transformable = renderable->GetTransformable();
		if (!(renderableFlags & kRenderableCameraTransformInhibit))
		{
			if (newRenderState & kRenderDepthOffset)
			{
				float z = Fmin(cameraSpaceTransform.GetRow(2) ^ renderable->GetDepthOffsetPoint(), -cameraObject->GetNearDepth());
				float delta = renderable->GetDepthOffsetDelta();
				float epsilon = depthOffsetConstant * delta / (z * (z + delta));
				epsilon = Fmax(Fabs(epsilon), 4.8e-7F) * NonzeroFsgn(epsilon);

				currentProjectionMatrix = cameraProjectionMatrix;
				currentProjectionMatrix(2,2) *= 1.0F + epsilon;
			}

			if (transformable)
			{
				SetModelviewMatrix(cameraSpaceTransform * transformable->GetWorldTransform());
			}
			else
			{
				SetModelviewMatrix(cameraSpaceTransform);
			}
		}
		else
		{
			if (transformable)
			{
				SetModelviewMatrix(transformable->GetWorldTransform());
			}
			else
			{
				SetModelviewMatrix(K::identity_4D);
			}
		}

		RenderSegment *segment = renderable->GetFirstRenderSegment();
		do
		{
			const ShaderData *shaderData = segment->GetShaderData(kShaderAmbient, renderable->GetShaderDetailLevel());
			if ((!shaderData) || ((shaderData->variantMask & (1 << kShaderVariantNormal)) == 0))
			{
				shaderData = segment->InitShaderData(renderable, kShaderAmbient, kShaderVariantNormal);
			}

			if (!shaderData->shaderProgram[kShaderVariantNormal])
			{
				continue;
			}

			SetMaterialState(shaderData->materialState & kMaterialTwoSided);

			Render::SetShaderProgram(shaderData->shaderProgram[kShaderVariantNormal]);

			SetVertexArray(shaderData);
			SetNormalArray(shaderData);
			SetTangentArray(shaderData);
			SetColorArray(shaderData);

			for (machine a = kShaderArrayTexture0; a < kMaxShaderArrayCount; a++)
			{
				SetTexcoordArray(shaderData, a);
			}

			int32 stateProcCount = shaderData->shaderStateDataCount;
			for (machine a = 0; a < stateProcCount; a++)
			{
				(*shaderData->shaderStateData[a].stateProc)(renderable, shaderData->shaderStateData[a].stateCookie);
			}

			int32 unitCount = shaderData->textureUnitCount;
			const Render::TextureObject *const *textureObject = shaderData->textureObject;
			for (machine unit = 0; unit < unitCount; unit++)
			{
				Render::BindTexture(unit, textureObject[unit]);
			}

			int32 vertexCount = renderable->GetVertexCount();
			graphicsCounter[kGraphicsCounterDistortionVertices] += vertexCount;
			graphicsCounter[kGraphicsCounterDistortionCommands]++;

			switch (renderable->GetRenderType())
			{
				case kRenderTriangles:

					graphicsCounter[kGraphicsCounterDistortionPrimitives] += vertexCount / 3;
					Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, vertexCount);
					break;

				case kRenderTriangleStrip:

					graphicsCounter[kGraphicsCounterDistortionPrimitives] += vertexCount - 2;
					Render::DrawPrimitives(Render::kPrimitiveTriangleStrip, 0, vertexCount);
					break;

				case kRenderIndexedTriangles:
				{
					int32 triangleCount = segment->GetPrimitiveCount();
					graphicsCounter[kGraphicsCounterDistortionPrimitives] += triangleCount;

					Render::DrawIndexedPrimitives(Render::kPrimitiveTriangles, vertexCount, triangleCount * 3, shaderData->indexBuffer, renderable->GetPrimitiveIndexOffset() + segment->GetPrimitiveStart() * sizeof(Triangle));
					break;
				}

				case kRenderQuads:

					graphicsCounter[kGraphicsCounterDistortionPrimitives] += vertexCount >> 2;
					Render::DrawQuads(0, vertexCount);
					break;
			}
		} while ((segment = segment->GetNextRenderSegment()) != nullptr);
	}
}

bool GraphicsMgr::ActivateShadowBounds(const StencilShadow *shadow)
{
	const Polyhedron *polyhedron = shadow->GetShadowPolyhedron();
	int32 edgeCount = polyhedron->edgeCount;
	if (edgeCount != 0)
	{
		float 	xmin, xmax, ymin, ymax, zmin, zmax;

		Matrix4D transform = standardProjectionMatrix * cameraSpaceTransform;

		const Point3D *vertex = polyhedron->vertex;
		const Edge *edge = polyhedron->edge;

		bool visible = false;
		for (machine a = 0; a < edgeCount; a++, edge++)
		{
			Vector4D p1 = transform * vertex[edge->vertexIndex[0]];
			Vector4D p2 = transform * vertex[edge->vertexIndex[1]];

			if (p1.z < -p1.w)
			{
				if (p2.z < -p2.w)
				{
					continue;
				}

				Vector4D dp = p1 - p2;
				p1 -= dp * ((p1.z + p1.w) / (dp.w + dp.z));
			}
			else if (p2.z < -p2.w)
			{
				Vector4D dp = p2 - p1;
				p2 -= dp * ((p2.z + p2.w) / (dp.w + dp.z));
			}

			float f1 = 0.5F / p1.w;
			float f2 = 0.5F / p2.w;
			float x1 = p1.x * f1;
			float x2 = p2.x * f2;
			float y1 = p1.y * f1;
			float y2 = p2.y * f2;
			float z1 = p1.z * f1;
			float z2 = p2.z * f2;

			if (!visible)
			{
				visible = true;

				if (x1 < x2)
				{
					xmin = x1;
					xmax = x2;
				}
				else
				{
					xmin = x2;
					xmax = x1;
				}

				if (y1 < y2)
				{
					ymin = y1;
					ymax = y2;
				}
				else
				{
					ymin = y2;
					ymax = y1;
				}

				if (z1 < z2)
				{
					zmin = z1;
					zmax = z2;
				}
				else
				{
					zmin = z2;
					zmax = z1;
				}
			}
			else
			{
				if (x1 < xmin)
				{
					xmin = x1;
				}
				else if (x1 > xmax)
				{
					xmax = x1;
				}

				if (x2 < xmin)
				{
					xmin = x2;
				}
				else if (x2 > xmax)
				{
					xmax = x2;
				}

				if (y1 < ymin)
				{
					ymin = y1;
				}
				else if (y1 > ymax)
				{
					ymax = y1;
				}

				if (y2 < ymin)
				{
					ymin = y2;
				}
				else if (y2 > ymax)
				{
					ymax = y2;
				}

				if (z1 < zmin)
				{
					zmin = z1;
				}
				else if (z1 > zmax)
				{
					zmax = z1;
				}

				if (z2 < zmin)
				{
					zmin = z2;
				}
				else if (z2 > zmax)
				{
					zmax = z2;
				}
			}
		}

		if (visible)
		{
			float viewLeft = (float) viewportRect.left;
			float viewWidth = (float) viewportRect.Width();
			shadowRect.left = Max((int32) (viewLeft + viewWidth * (xmin + 0.5F)), lightRect.left);
			shadowRect.right = Min((int32) (viewLeft + viewWidth * (xmax + 0.5F)), lightRect.right);

			float viewBottom = (float) viewportRect.bottom;
			float viewHeight = (float) viewportRect.Height();
			shadowRect.bottom = Max((int32) (viewBottom - viewHeight * (ymin + 0.5F)), lightRect.bottom);
			shadowRect.top = Min((int32) (viewBottom - viewHeight * (ymax + 0.5F)), lightRect.top);

			if ((shadowRect.left < shadowRect.right) && (shadowRect.bottom < shadowRect.top))
			{
				unsigned_int32 graphicsState = currentGraphicsState;
				if (graphicsState & kGraphicsObliqueFrustum)
				{
					currentGraphicsState = graphicsState | kGraphicsUpdateShadowScissor;

					#if C4DIAGNOSTICS

						if (diagnosticFlags & kDiagnosticShadowBounds)
						{
							DrawShadowBoundsDiagnostic(shadow);
						}

					#endif

					return (true);
				}

				zmin = Fmax(zmin + 0.5F, lightDepthBounds.min);
				zmax = Fmin(zmax + 0.5F, lightDepthBounds.max);

				if (zmin < zmax)
				{
					graphicsState |= kGraphicsUpdateShadowScissor;

					if (graphicsState & kGraphicsDepthBoundsAvail)
					{
						if (!(graphicsState & kGraphicsDepthBoundsMask))
						{
							Render::EnableDepthBoundsTest();
						}

						graphicsState |= kGraphicsShadowDepthBounds;
						Render::SetDepthBounds(zmin, zmax);
					}

					currentGraphicsState = graphicsState;

					#if C4DIAGNOSTICS

						if (diagnosticFlags & kDiagnosticShadowBounds)
						{
							DrawShadowBoundsDiagnostic(shadow);
						}

					#endif

					return (true);
				}
			}
		}
	}
	else
	{
		DeactivateShadowBounds();
		return (true);
	}

	return (false);
}

void GraphicsMgr::DeactivateShadowBounds(void)
{
	unsigned_int32 graphicsState = currentGraphicsState;

	if (graphicsState & kGraphicsShadowScissor)
	{
		graphicsState &= ~kGraphicsShadowScissor;
		const Rect& rect = (graphicsState & kGraphicsLightScissor) ? lightRect : scissorRect;
		Render::SetScissor(rect.left, rect.bottom, rect.Width(), -rect.Height());
	}

	if (graphicsState & kGraphicsShadowDepthBounds)
	{
		float	dmin, dmax;

		graphicsState &= ~kGraphicsShadowDepthBounds;

		if (graphicsState & kGraphicsLightDepthBounds)
		{
			dmin = lightDepthBounds.min;
			dmax = lightDepthBounds.max;
		}
		else
		{
			dmin = 0.0F;
			dmax = 1.0F;
		}

		Render::SetDepthBounds(dmin, dmax);
	}

	currentGraphicsState = graphicsState;
}

#if C4DIAGNOSTICS

	void GraphicsMgr::DrawShadowBoundsDiagnostic(const StencilShadow *shadow)
	{
		static Link<ShaderProgram>		diagnosticProgram;

		#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

		#endif //]

		ShaderProgram *program = diagnosticProgram;
		if (!program)
		{
			ProgramStageTable	stageTable;

			VertexShader *vertexShader = GetLocalVertexShader(&VertexShader::modelviewProjectTransform);
			FragmentShader *fragmentShader = GetLocalFragmentShader(kLocalFragmentShaderCopyConstant);

			stageTable.vertexShader = vertexShader;
			stageTable.fragmentShader = fragmentShader;

			program = ShaderProgram::Get(stageTable);
			diagnosticProgram = program;

			fragmentShader->Release();
			vertexShader->Release();
		}

		Render::SetShaderProgram(program);

		SetBlendState(kBlendReplace | kBlendAlphaPreserve);
		SetModelviewMatrix(cameraSpaceTransform);

		int32 left = scissorRect.left;
		int32 bottom = scissorRect.bottom;
		Render::SetScissor(left, bottom, scissorRect.right - left, scissorRect.top - bottom);

		Render::DisableDepthTest();
		if (currentGraphicsState & kGraphicsDepthBoundsMask)
		{
			Render::DisableDepthBoundsTest();
		}

		Render::SetLinePolygonMode();
		Render::SetColorMask(true, true, true, true);
		Render::SetFragmentShaderParameter(kFragmentParamConstant7, 1.0F, 0.0F, 0.0F, 0.0F);

		const Polyhedron *polyhedron = shadow->GetShadowPolyhedron();
		int32 edgeCount = polyhedron->edgeCount;

		VertexBuffer vertexBuffer(kVertexBufferAttribute | kVertexBufferDynamic);
		vertexBuffer.Establish(sizeof(Point3D) * 2 * edgeCount);
		volatile Point3D *restrict vertex = vertexBuffer.BeginUpdate<Point3D>();

		const Point3D *polyhedronVertex = polyhedron->vertex;
		const Edge *polyhedronEdge = polyhedron->edge;

		for (machine a = 0; a < edgeCount; a++)
		{
			vertex[0] = polyhedronVertex[polyhedronEdge->vertexIndex[0]];
			vertex[1] = polyhedronVertex[polyhedronEdge->vertexIndex[1]];

			polyhedronEdge++;
			vertex += 2;
		}

		vertexBuffer.EndUpdate();

		Render::SetVertexAttribArray(0, 3, Render::kVertexFloat, sizeof(Point3D), &vertexBuffer, 0);
		Render::DrawPrimitives(Render::kPrimitiveLines, 0, edgeCount * 2);

		Render::EnableDepthTest();
		if (currentGraphicsState & kGraphicsDepthBoundsMask)
		{
			Render::EnableDepthBoundsTest();
		}

		Render::SetFillPolygonMode();
		Render::SetColorMask(false, false, false, false);

		#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

		#endif //]
	}

#endif

void GraphicsMgr::BeginStencilShadow(void)
{
	unsigned_int32 graphicsState = (currentGraphicsState | kGraphicsRenderStencil) & ~kGraphicsStencilValid;
	if (graphicsState & kGraphicsStencilClear)
	{
		graphicsState |= kGraphicsStencilValid;
	}

	if (!(graphicsState & kGraphicsDepthTestLess))
	{
		graphicsState |= kGraphicsDepthTestLess;
		Render::SetDepthFunc(Render::kDepthLess);
	}

	currentGraphicsState = graphicsState;

	SetMaterialState(kMaterialTwoSided);
	SetRenderState(kRenderDepthTest | kRenderColorInhibit | kRenderDepthInhibit);

	ResetArrayState();
}

void GraphicsMgr::EndStencilShadow(void)
{
	DeactivateShadowBounds();

	if (currentStencilMode != kStencilNone)
	{
		currentStencilMode = kStencilNone;
		Render::SetStencilOp(Render::kStencilKeep, Render::kStencilKeep, Render::kStencilKeep);
	}

	currentGraphicsState &= ~(kGraphicsUpdateShadowScissor | kGraphicsRenderStencil);
}

void GraphicsMgr::DrawStencilShadow(const StencilShadow *shadow, StencilType type, StencilMode mode)
{
	static Link<ShaderProgram>		stencilProgram[kStencilTypeCount];

	unsigned_int32 graphicsState = currentGraphicsState;
	if (!(graphicsState & kGraphicsStencilValid))
	{
		graphicsState |= kGraphicsStencilValid;
		graphicsCounter[kGraphicsCounterStencilClears]++;

		Render::ClearStencilBuffer();
	}

	if (graphicsState & kGraphicsUpdateShadowScissor)
	{
		graphicsState |= kGraphicsShadowScissor;
		Render::SetScissor(shadowRect.left, shadowRect.bottom, shadowRect.Width(), -shadowRect.Height());
	}

	const Transformable *transformable = geometryTransformable;
	if (transformable)
	{
		SetModelviewMatrix(cameraSpaceTransform * transformable->GetWorldTransform());
	}
	else
	{
		SetModelviewMatrix(cameraSpaceTransform);
	}

	ShaderProgram *program = stencilProgram[type];
	if (!program)
	{
		static const VertexSnippet *shadowSnippet[kStencilTypeCount] =
		{
			&VertexShader::shadowInfiniteExtrusionTransform,
			&VertexShader::shadowPointExtrusionTransform,
			&VertexShader::shadowEndcapProjectionTransform,
			&VertexShader::modelviewProjectTransform
		};

		ProgramStageTable	stageTable;

		VertexShader *vertexShader = GetLocalVertexShader(shadowSnippet[type]);
		FragmentShader *fragmentShader = GetLocalFragmentShader(kLocalFragmentShaderCopyZero);

		stageTable.vertexShader = vertexShader;
		stageTable.fragmentShader = fragmentShader;

		program = ShaderProgram::Get(stageTable);
		stencilProgram[type] = program;

		fragmentShader->Release();
		vertexShader->Release();
	}

	Render::SetShaderProgram(program);

	if (mode == kStencilPass)
	{
		if (currentStencilMode != kStencilPass)
		{
			currentStencilMode = kStencilPass;
			Render::SetBackStencilOp(Render::kStencilKeep, Render::kStencilKeep, Render::kStencilDecrWrap);
			Render::SetFrontStencilOp(Render::kStencilKeep, Render::kStencilKeep, Render::kStencilIncrWrap);
		}

		graphicsCounter[kGraphicsCounterStencilCommands]++;

		switch (type)
		{
			case kStencilInfiniteExtrusion:
			{
				int32 triangleCount = shadow->GetExtrusionEdgeCount();
				int32 vertexCount = triangleCount * 3;
				graphicsCounter[kGraphicsCounterStencilVertices] += vertexCount;
				graphicsCounter[kGraphicsCounterStencilPrimitives] += triangleCount;

				Render::SetVertexAttribArray(0, 4, Render::kVertexFloat, sizeof(Vector4D), shadow->GetExtrusionVertexBuffer(), 0);
				Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, vertexCount);
				break;
			}

			case kStencilPointExtrusion:
			{
				int32 quadCount = shadow->GetExtrusionEdgeCount();
				int32 vertexCount = quadCount * 4;
				graphicsCounter[kGraphicsCounterStencilVertices] += vertexCount;
				graphicsCounter[kGraphicsCounterStencilPrimitives] += quadCount;

				Render::SetVertexAttribArray(0, 4, Render::kVertexFloat, sizeof(Vector4D), shadow->GetExtrusionVertexBuffer(), 0);
				Render::DrawQuads(0, vertexCount);
				break;
			}
		}
	}
	else if (mode == kStencilFail)
	{
		if (currentStencilMode != kStencilFail)
		{
			currentStencilMode = kStencilFail;
			Render::SetBackStencilOp(Render::kStencilKeep, Render::kStencilIncrWrap, Render::kStencilKeep);
			Render::SetFrontStencilOp(Render::kStencilKeep, Render::kStencilDecrWrap, Render::kStencilKeep);
		}

		graphicsCounter[kGraphicsCounterStencilCommands]++;

		switch (type)
		{
			case kStencilInfiniteExtrusion:
			{
				int32 triangleCount = shadow->GetExtrusionEdgeCount();
				int32 vertexCount = triangleCount * 3;
				graphicsCounter[kGraphicsCounterStencilVertices] += vertexCount;
				graphicsCounter[kGraphicsCounterStencilPrimitives] += triangleCount;

				Render::SetVertexAttribArray(0, 4, Render::kVertexFloat, sizeof(Vector4D), shadow->GetExtrusionVertexBuffer(), 0);
				Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, vertexCount);
				break;
			}

			case kStencilPointExtrusion:
			{
				int32 quadCount = shadow->GetExtrusionEdgeCount();
				int32 vertexCount = quadCount * 4;
				graphicsCounter[kGraphicsCounterStencilVertices] += vertexCount;
				graphicsCounter[kGraphicsCounterStencilPrimitives] += quadCount;

				Render::SetVertexAttribArray(0, 4, Render::kVertexFloat, sizeof(Vector4D), shadow->GetExtrusionVertexBuffer(), 0);
				Render::DrawQuads(0, vertexCount);
				break;
			}

			case kStencilEndcapProjection:
			{
				int32 triangleCount = shadow->GetBackEndcapTriangleCount();
				int32 vertexCount = shadow->GetGeometryVertexCount();
				graphicsCounter[kGraphicsCounterStencilVertices] += vertexCount;
				graphicsCounter[kGraphicsCounterStencilPrimitives] += triangleCount;

				Render::SetVertexAttribArray(0, 3, Render::kVertexFloat, shadow->GetGeometryVertexStride(), shadow->GetGeometryVertexBuffer(), shadow->GetGeometryVertexOffset());
				Render::DrawIndexedPrimitives(Render::kPrimitiveTriangles, vertexCount, triangleCount * 3, shadow->GetEndcapIndexBuffer(), shadow->GetFrontEndcapTriangleCount() * sizeof(Triangle));
				break;
			}

			case kStencilEndcapIdentity:
			{
				int32 triangleCount = shadow->GetFrontEndcapTriangleCount();
				int32 vertexCount = shadow->GetGeometryVertexCount();
				graphicsCounter[kGraphicsCounterStencilVertices] += vertexCount;
				graphicsCounter[kGraphicsCounterStencilPrimitives] += triangleCount;

				Render::SetVertexAttribArray(0, 3, Render::kVertexFloat, shadow->GetGeometryVertexStride(), shadow->GetGeometryVertexBuffer(), shadow->GetGeometryVertexOffset());
				Render::DrawIndexedPrimitives(Render::kPrimitiveTriangles, vertexCount, triangleCount * 3, shadow->GetEndcapIndexBuffer(), 0);
				break;
			}
		}
	}

	currentGraphicsState = graphicsState & ~(kGraphicsStencilClear | kGraphicsUpdateShadowScissor);

	#if C4DIAGNOSTICS

		if (diagnosticFlags & kDiagnosticShadows)
		{
			DrawStencilDiagnostic(shadow, type, mode);
		}

	#endif
}

#if C4DIAGNOSTICS

	void GraphicsMgr::DrawStencilDiagnostic(const StencilShadow *shadow, StencilType type, StencilMode mode)
	{
		static Link<ShaderProgram>		diagnosticProgram[kStencilTypeCount];

		ShaderProgram *program = diagnosticProgram[type];
		if (!program)
		{
			static const VertexSnippet *diagnosticSnippet[kStencilTypeCount] =
			{
				&VertexShader::shadowInfiniteExtrusionTransform, &VertexShader::shadowPointExtrusionTransform,
				&VertexShader::shadowEndcapProjectionTransform, &VertexShader::modelviewProjectTransform
			};

			ProgramStageTable	stageTable;

			VertexShader *vertexShader = GetLocalVertexShader(diagnosticSnippet[type]);
			FragmentShader *fragmentShader = GetLocalFragmentShader(kLocalFragmentShaderCopyConstant);

			stageTable.vertexShader = vertexShader;
			stageTable.fragmentShader = fragmentShader;

			program = ShaderProgram::Get(stageTable);
			diagnosticProgram[type] = program;

			fragmentShader->Release();
			vertexShader->Release();
		}

		Render::SetShaderProgram(program);

		SetBlendState(kBlendAccumulate | kBlendAlphaPreserve);

		Render::DisableDepthTest();
		Render::SetLinePolygonMode();
		Render::SetColorMask(true, true, true, true);

		currentStencilMode = kStencilNone;
		Render::SetStencilOp(Render::kStencilKeep, Render::kStencilKeep, Render::kStencilKeep);

		switch (type)
		{
			case kStencilInfiniteExtrusion:

				Render::SetFragmentShaderParameter(kFragmentParamConstant7, 0.0625F, 0.125F, 0.015625F, 0.0F);
				Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, shadow->GetExtrusionEdgeCount() * 3);
				break;

			case kStencilPointExtrusion:

				Render::SetFragmentShaderParameter(kFragmentParamConstant7, 0.0625F, 0.125F, 0.015625F, 0.0F);
				Render::DrawQuads(0, shadow->GetExtrusionEdgeCount() * 4);
				break;

			case kStencilEndcapProjection:

				Render::SetFragmentShaderParameter(kFragmentParamConstant7, 0.125F, 0.0625F, 0.015625F, 0.0F);
				Render::DrawIndexedPrimitives(Render::kPrimitiveTriangles, shadow->GetGeometryVertexCount(), shadow->GetBackEndcapTriangleCount() * 3, shadow->GetEndcapIndexBuffer(), shadow->GetFrontEndcapTriangleCount() * sizeof(Triangle));
				break;

			case kStencilEndcapIdentity:

				Render::SetFragmentShaderParameter(kFragmentParamConstant7, 0.09375F, 0.015625F, 0.15625F, 0.0F);
				Render::DrawIndexedPrimitives(Render::kPrimitiveTriangles, shadow->GetGeometryVertexCount(), shadow->GetFrontEndcapTriangleCount() * 3, shadow->GetEndcapIndexBuffer(), 0);
				break;
		}

		Render::EnableDepthTest();
		Render::SetFillPolygonMode();
		Render::SetColorMask(false, false, false, false);
	}

#endif

void GraphicsMgr::BeginShadowMap(void)
{
	savedCameraObject = cameraObject;
	savedCameraTransformable = cameraTransformable;

	unsigned_int32 graphicsState = currentGraphicsState;
	if (!(graphicsState & kGraphicsDepthTestLess))
	{
		Render::SetDepthFunc(Render::kDepthLess);
	}

	currentGraphicsState = (graphicsState | (kGraphicsDepthTestLess | kGraphicsRenderShadowMap)) & ~kGraphicsScissorMask;

	SetAmbient();

	shadowFrameBuffer->GetRenderTargetTexture()->Unbind(kTextureUnitLightProjection);
	Render::SetFrameBuffer(shadowFrameBuffer);

	if (graphicsActiveFlags & kGraphicsActiveTimer)
	{
		Render::QueryTimeStamp(&timerQuery[kGraphicsTimeIndexBeginShadow]);
	}

	SetBlendState(kBlendReplace);
	SetRenderState((currentRenderState & kRenderDepthTest) | kRenderColorInhibit);

	unsigned_int32 newMaterialState = currentMaterialState & kMaterialTwoSided;
	SetMaterialState(newMaterialState);

	ResetArrayState();

	renderTargetHeight = dynamicShadowMapSize * kMaxShadowCascadeCount;
	Render::SetScissor(0, 0, dynamicShadowMapSize, renderTargetHeight);
	Render::ClearDepthBuffer();

	Render::SetPolygonOffset(1.0F, 1.0F);
	Render::EnablePolygonFillOffset();

	if (capabilities->extensionFlag[kExtensionDepthClamp])
	{
		Render::EnableDepthClamp();
	}
}

void GraphicsMgr::EndShadowMap(void)
{
	if (graphicsActiveFlags & kGraphicsActiveTimer)
	{
		Render::QueryTimeStamp(&timerQuery[kGraphicsTimeIndexEndShadow]);
	}

	if ((currentRenderTargetType == kRenderTargetPrimary) && (multisampleFrameBuffer))
	{
		Render::SetFrameBuffer(multisampleFrameBuffer);
	}
	else
	{
		Render::SetFrameBuffer(normalFrameBuffer);
	}

	Render::DisablePolygonFillOffset();

	if (capabilities->extensionFlag[kExtensionDepthClamp])
	{
		Render::DisableDepthClamp();
	}

	currentGraphicsState &= ~kGraphicsRenderShadowMap;
	renderTargetHeight = TheDisplayMgr->GetDisplayHeight();

	SetCamera(savedCameraObject, savedCameraTransformable, 0, false);
}

void GraphicsMgr::DrawShadowMapList(const List<Renderable> *renderList)
{
	for (Renderable *renderable = renderList->First(); renderable; renderable = renderable->Next())
	{
		SetRenderState((renderable->GetRenderState() & kRenderDepthTest) | (kRenderColorInhibit | kRenderDepthOffset));

		const Transformable *transformable = renderable->GetTransformable();
		if (transformable)
		{
			SetModelviewMatrix(cameraSpaceTransform * transformable->GetWorldTransform());
		}
		else
		{
			SetModelviewMatrix(cameraSpaceTransform);
		}

		RenderSegment *segment = renderable->GetFirstRenderSegment();
		do
		{
			const ShaderData *shaderData = segment->GetShaderData(kShaderShadow, renderable->GetShaderDetailLevel());
			if (!shaderData)
			{
				shaderData = segment->InitShaderData(renderable, kShaderShadow);
			}

			if (!shaderData->shaderProgram[kShaderVariantNormal])
			{
				continue;
			}

			SetMaterialState(shaderData->materialState & kMaterialTwoSided);

			Render::SetShaderProgram(shaderData->shaderProgram[kShaderVariantNormal]);

			SetVertexArray(shaderData);
			SetPosition1Array(shaderData);
			SetOffsetArray(shaderData);
			SetColorArray(shaderData);
			SetAuxColorArray(shaderData, kShaderArrayColor1);
			SetAuxColorArray(shaderData, kShaderArrayColor2);

			for (machine a = kShaderArrayTexture0; a < kMaxShaderArrayCount; a++)
			{
				SetTexcoordArray(shaderData, a);
			}

			int32 stateProcCount = shaderData->shaderStateDataCount;
			for (machine a = 0; a < stateProcCount; a++)
			{
				(*shaderData->shaderStateData[a].stateProc)(renderable, shaderData->shaderStateData[a].stateCookie);
			}

			int32 unitCount = shaderData->textureUnitCount;
			const Render::TextureObject *const *textureObject = shaderData->textureObject;
			for (machine unit = 0; unit < unitCount; unit++)
			{
				Render::BindTexture(unit, textureObject[unit]);
			}

			int32 vertexCount = renderable->GetVertexCount();
			graphicsCounter[kGraphicsCounterShadowVertices] += vertexCount;
			graphicsCounter[kGraphicsCounterShadowCommands]++;

			switch (renderable->GetRenderType())
			{
				case kRenderTriangles:

					graphicsCounter[kGraphicsCounterShadowPrimitives] += vertexCount / 3;
					Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, vertexCount);
					break;

				case kRenderTriangleStrip:

					graphicsCounter[kGraphicsCounterShadowPrimitives] += vertexCount - 2;
					Render::DrawPrimitives(Render::kPrimitiveTriangleStrip, 0, vertexCount);
					break;

				case kRenderIndexedTriangles:
				{
					int32 triangleCount = segment->GetPrimitiveCount();
					graphicsCounter[kGraphicsCounterShadowPrimitives] += triangleCount;

					Render::DrawIndexedPrimitives(Render::kPrimitiveTriangles, vertexCount, triangleCount * 3, shaderData->indexBuffer, renderable->GetPrimitiveIndexOffset() + segment->GetPrimitiveStart() * sizeof(Triangle));
					break;
				}

				case kRenderQuads:

					graphicsCounter[kGraphicsCounterShadowPrimitives] += vertexCount >> 2;
					Render::DrawQuads(0, vertexCount);
					break;

				case kRenderMultiIndexedTriangles:

					Render::MultiDrawIndexedPrimitives(Render::kPrimitiveTriangles, segment->GetMultiCountArray(), shaderData->indexBuffer, segment->GetMultiOffsetArray(), segment->GetMultiRenderCount());
					break;

				case kRenderMaskedMultiIndexedTriangles:
				{
					static machine_address	index[33];
					static unsigned_int32	count[33];

					unsigned_int32 size = 0;
					unsigned_int32 offset = renderable->GetPrimitiveIndexOffset();

					int32 triangleCount = segment->GetPrimitiveCount();
					if (triangleCount != 0)
					{
						graphicsCounter[kGraphicsCounterShadowPrimitives] += triangleCount;

						index[0] = offset;
						count[0] = triangleCount * 3;
						size = 1;
					}

					unsigned_int32 mask = segment->GetMultiRenderMask();
					const int32 *data = segment->GetMultiRenderData();
					while (mask != 0)
					{
						if (mask & 1)
						{
							triangleCount = data[1];
							graphicsCounter[kGraphicsCounterShadowPrimitives] += triangleCount;

							index[size] = offset + data[0] * sizeof(Triangle);
							count[size] = triangleCount * 3;
							size++;
						}

						mask >>= 1;
						data += 2;
					}

					Render::MultiDrawIndexedPrimitives(Render::kPrimitiveTriangles, count, shaderData->indexBuffer, index, size);
					break;
				}
			}
		} while ((segment = segment->GetNextRenderSegment()) != nullptr);
	}
}

void GraphicsMgr::DrawWireframe(unsigned_int32 flags, const List<Renderable> *renderList)
{
	unsigned_int32 state = (currentRenderState & kRenderDepthInhibit) | kRenderWireframe;

	if (flags & kWireframeDepthTest)
	{
		state |= kRenderDepthTest;
		Render::EnablePolygonLineOffset();
		Render::SetPolygonOffset(0.0F, -2.0F);
	}

	SetRenderState(state);
	SetBlendState(kBlendReplace);

	unsigned_int32 newMaterialState = currentMaterialState & kMaterialTwoSided;
	SetMaterialState(newMaterialState);

	ResetArrayState();

	Render::SetFragmentShaderParameter(kFragmentParamConstant7, &K::white.red);

	for (Renderable *renderable = renderList->First(); renderable; renderable = renderable->Next())
	{
		if (renderable->GetRenderType() >= kRenderTriangles)
		{
			const Transformable *transformable = renderable->GetTransformable();
			if (!(renderable->GetRenderableFlags() & kRenderableCameraTransformInhibit))
			{
				if (transformable)
				{
					SetModelviewMatrix(cameraSpaceTransform * transformable->GetWorldTransform());
				}
				else
				{
					SetModelviewMatrix(cameraSpaceTransform);
				}
			}
			else
			{
				if (transformable)
				{
					SetModelviewMatrix(transformable->GetWorldTransform());
				}
				else
				{
					SetModelviewMatrix(K::identity_4D);
				}
			}

			if (flags & kWireframeColor)
			{
				const ColorRGBA *wireColor = renderable->GetWireframeColorPointer();
				Render::SetFragmentShaderParameter(kFragmentParamConstant7, (wireColor) ? &wireColor->red : &K::white.red);
			}

			RenderSegment *segment = renderable->GetFirstRenderSegment();
			do
			{
				const ShaderData *shaderData = segment->GetShaderData(kShaderShadow, renderable->GetShaderDetailLevel());
				if ((!shaderData) || ((shaderData->variantMask & (1 << kShaderVariantNormal)) == 0))
				{
					shaderData = segment->InitShaderData(renderable, kShaderShadow);
				}

				if (!shaderData->shaderProgram[kShaderVariantNormal])
				{
					continue;
				}

				if ((shaderData->materialState & kMaterialTwoSided) || (flags & kWireframeTwoSided))
				{
					if (!(newMaterialState & kMaterialTwoSided))
					{
						newMaterialState |= kMaterialTwoSided;
						Render::DisableCullFace();
					}
				}
				else
				{
					if (newMaterialState & kMaterialTwoSided)
					{
						newMaterialState &= ~kMaterialTwoSided;
						Render::EnableCullFace();
					}
				}

				if (shaderData->vertexBuffer[kShaderArrayPosition0])
				{
					ProgramStageTable	stageTable;

					FragmentShader *fragmentShader = GetLocalFragmentShader(kLocalFragmentShaderCopyConstant);

					stageTable.vertexShader = shaderData->shaderProgram[kShaderVariantNormal]->GetVertexShader();
					stageTable.fragmentShader = fragmentShader;

					ShaderProgram *program = ShaderProgram::Get(stageTable);
					Render::SetShaderProgram(program);
					program->Release();

					fragmentShader->Release();

					SetVertexArray(shaderData);
					SetPosition1Array(shaderData);
					SetNormalArray(shaderData);
					SetAuxColorArray(shaderData, kShaderArrayColor2);
					SetOffsetArray(shaderData);

					int32 stateProcCount = shaderData->shaderStateDataCount;
					for (machine a = 0; a < stateProcCount; a++)
					{
						(*shaderData->shaderStateData[a].stateProc)(renderable, shaderData->shaderStateData[a].stateCookie);
					}

					int32 vertexCount = renderable->GetVertexCount();
					switch (renderable->GetRenderType())
					{
						case kRenderTriangles:

							Render::DrawPrimitives(Render::kPrimitiveTriangles, 0, vertexCount);
							break;

						case kRenderTriangleStrip:

							Render::DrawPrimitives(Render::kPrimitiveTriangleStrip, 0, vertexCount);
							break;

						case kRenderIndexedTriangles:

							Render::DrawIndexedPrimitives(Render::kPrimitiveTriangles, vertexCount, segment->GetPrimitiveCount() * 3, shaderData->indexBuffer, renderable->GetPrimitiveIndexOffset() + segment->GetPrimitiveStart() * sizeof(Triangle));
							break;

						case kRenderQuads:

							Render::DrawQuads(0, vertexCount);
							break;

						case kRenderMultiIndexedTriangles:

							Render::MultiDrawIndexedPrimitives(Render::kPrimitiveTriangles, segment->GetMultiCountArray(), shaderData->indexBuffer, segment->GetMultiOffsetArray(), segment->GetMultiRenderCount());
							break;

						case kRenderMaskedMultiIndexedTriangles:
						{
							static machine_address	index[33];
							static unsigned_int32	count[33];

							unsigned_int32 size = 0;
							unsigned_int32 offset = renderable->GetPrimitiveIndexOffset();

							int32 triangleCount = segment->GetPrimitiveCount();
							if (triangleCount != 0)
							{
								index[0] = offset;
								count[0] = triangleCount * 3;
								size = 1;
							}

							unsigned_int32 mask = segment->GetMultiRenderMask();
							const int32 *data = segment->GetMultiRenderData();
							while (mask != 0)
							{
								if (mask & 1)
								{
									triangleCount = data[1];

									index[size] = offset + data[0] * sizeof(Triangle);
									count[size] = triangleCount * 3;
									size++;
								}

								mask >>= 1;
								data += 2;
							}

							Render::MultiDrawIndexedPrimitives(Render::kPrimitiveTriangles, count, shaderData->indexBuffer, index, size);
							break;
						}
					}
				}
			} while ((segment = segment->GetNextRenderSegment()) != nullptr);
		}
	}

	currentMaterialState = newMaterialState;

	if (flags & kWireframeDepthTest)
	{
		Render::DisablePolygonLineOffset();
	}
}

void GraphicsMgr::DrawVectors(int32 arrayIndex, const List<Renderable> *renderList)
{
	static Link<ShaderProgram>		vectorShaderProgram;

	SetBlendState(kBlendReplace);
	SetRenderState(currentRenderState & kRenderDepthInhibit);
	SetMaterialState(currentMaterialState & kMaterialTwoSided);

	currentArrayState |= (1 << kShaderArrayNormal);
	Render::EnableVertexAttribArray(2);

	Render::SetFragmentShaderParameter(kFragmentParamConstant7, &K::white.red);

	ShaderProgram *program = vectorShaderProgram;
	if (!program)
	{
		static const VertexSnippet *vectorSnippet[2] =
		{
			&VertexShader::nullTransform, &VertexShader::outputNormalTexcoord
		};

		ProgramStageTable	stageTable;

		VertexShader *vertexShader = GetLocalVertexShader(2, vectorSnippet);
		FragmentShader *fragmentShader = GetLocalFragmentShader(kLocalFragmentShaderCopyConstant);
		GeometryShader *geometryShader = GetLocalGeometryShader(kLocalGeometryShaderExtrudeNormalLine);

		stageTable.vertexShader = vertexShader;
		stageTable.fragmentShader = fragmentShader;
		stageTable.geometryShader = geometryShader;

		program = ShaderProgram::Get(stageTable);
		vectorShaderProgram = program;

		geometryShader->Release();
		fragmentShader->Release();
		vertexShader->Release();
	}

	Render::SetShaderProgram(program);

	Renderable *renderable = renderList->First();
	while (renderable)
	{
		if (renderable->GetRenderType() >= kRenderTriangles)
		{
			int32 positionComponentCount = renderable->GetVertexAttributeComponentCount(kArrayPosition);
			int32 vectorComponentCount = renderable->GetVertexAttributeComponentCount(arrayIndex);
			if ((positionComponentCount >= 2) && (vectorComponentCount >= 3))
			{
				const Transformable *transformable = renderable->GetTransformable();
				if (!(renderable->GetRenderableFlags() & kRenderableCameraTransformInhibit))
				{
					if (transformable)
					{
						SetGeometryModelviewMatrix(cameraSpaceTransform * transformable->GetWorldTransform());
					}
					else
					{
						SetGeometryModelviewMatrix(cameraSpaceTransform);
					}
				}
				else
				{
					if (transformable)
					{
						SetGeometryModelviewMatrix(transformable->GetWorldTransform());
					}
					else
					{
						SetGeometryModelviewMatrix(K::identity_4D);
					}
				}

				unsigned_int32 bufferIndex = ((renderable->GetVertexBufferArrayFlags() & (1 << kArrayPosition)) != 0);
				Render::SetVertexAttribArray(0, positionComponentCount, Render::kVertexFloat, renderable->GetVertexBufferStride(bufferIndex), renderable->GetVertexBuffer(bufferIndex), renderable->GetVertexAttributeOffset(kArrayPosition));

				bufferIndex = ((renderable->GetVertexBufferArrayFlags() & (1 << arrayIndex)) != 0);
				Render::SetVertexAttribArray(2, vectorComponentCount, Render::kVertexFloat, renderable->GetVertexBufferStride(bufferIndex), renderable->GetVertexBuffer(bufferIndex), renderable->GetVertexAttributeOffset(arrayIndex));

				Render::DrawPrimitives(Render::kPrimitivePoints, 0, renderable->GetVertexCount());
			}
		}

		renderable = renderable->Next();
	}
}

void GraphicsMgr::ProcessOcclusionQueries(void)
{
	List<Renderable>	renderList;

	float normalizer = occlusionAreaNormalizer;
	if ((multisampleFrameBuffer) && (currentRenderTargetType == kRenderTargetPrimary))
	{
		normalizer *= multisampleFrameBuffer->GetSampleDivider();
	}

	for (;;)
	{
		OcclusionQuery *query = occlusionQueryList.First();
		if (!query)
		{
			break;
		}

		OcclusionQuery::occlusionQueryList.Append(query);
		query->Process(&renderList, normalizer);
	}

	if (!renderList.Empty())
	{
		DrawRenderList(&renderList);
		renderList.RemoveAll();
	}
}

void GraphicsMgr::ReadImageBuffer(const Rect& rect, Color4C *image, int32 rowPixels)
{
	#if C4OPENGL

		glPixelStorei(GL_PACK_ROW_LENGTH, rowPixels);
		glReadPixels(rect.left, TheDisplayMgr->GetDisplayHeight() - rect.bottom, rect.Width(), rect.Height(), GL_RGBA, GL_UNSIGNED_BYTE, image);

	#endif
}

// ZYUTNLM
