//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4Variables.h"


using namespace C4;


Variable::Variable(const char *name, unsigned_int32 flags, ObserverType *observer)
{
	variableFlags = flags;
	variableName = name;
	variableValue[0] = 0;

	if (observer)
	{
		AddObserver(observer);
	}
}

Variable::~Variable()
{
}

void Variable::SetValue(const char *value)
{
	variableValue = value;
	PostEvent();
}

void Variable::SetIntegerValue(int32 value)
{
	variableValue = value;
	PostEvent();
}

void Variable::SetFloatValue(float value)
{
	variableValue = value;
	PostEvent();
}

// ZYUTNLM
