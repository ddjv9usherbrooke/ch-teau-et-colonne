//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Universit� de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4VertexShaders_h
#define C4VertexShaders_h


#include "C4Programs.h"


namespace C4
{
	enum
	{
		kVertexParamMatrixMVP					= 0,		// Model-view-projection matrix
		kVertexParamMatrixVelocityA				= 4,		// Object to viewport transform, previous frame
		kVertexParamMatrixVelocityB				= 8,		// Object to viewport transform, current frame
		kVertexParamMatrixWorld					= 12,		// Object to world transform
		kVertexParamMatrixCamera				= 15,		// Object to camera transform
		kVertexParamMatrixLight					= 18,		// Object to light transform
		kVertexParamMatrixSpace					= 21,		// Object to space transform
		kVertexParamMatrixShadow				= 24,		// Object to shadow transform A

		kVertexParamCameraPosition				= 27,		// Object-space camera position
		kVertexParamCameraRight					= 28,		// Object-space camera right direction
		kVertexParamCameraDown					= 29,		// Object-space camera down direction

		kVertexParamViewportTransform			= 30,		// Viewport (w/2, h/2, w/2 + l, h/2 + b)

		kVertexParamLightPosition				= 31,		// Object-space light position
		kVertexParamLightRange					= 32,		// Light range (r, 0.0, 0.0, 1 / r)

		kVertexParamFogPlane					= 33,		// Object-space fog plane
		kVertexParamFogParams					= 34,		// (F ∧ C, F ∧ C <= 0, F ∧ C > 0, sgn(F ∧ C))

		kVertexParamShaderTime					= 35,		// (absolute time, delta time, unused, 0.0)
		kVertexParamFireParams					= 36,		// Fire params (intensity, 0.0, 0.0, 0.0)

		kVertexParamTexcoordGenerate			= 37,		// (x scale, y scale, 0.0, 0.0)
		kVertexParamTexcoordTransform0			= 38,		// (x scale, y scale, x offset, y offset)
		kVertexParamTexcoordTransform1			= 39,		// (x scale, y scale, x offset, y offset)
		kVertexParamTexcoordVelocity0			= 40,		// (v1.x, v1.y, 0.0, 0.0) or (v1.x, v1.y, v2.x, v2.y)
		kVertexParamTexcoordVelocity1			= 41,		// (v2.x, v2.y, 0.0, 0.0) or (v3.x, v3.y, 0.0, 0.0)

		kVertexParamTerrainTexcoordScale		= 42,		// (scale, 0.0, 0.0, 0.0)
		kVertexParamTerrainParameter0			= 43,		// Terrain border parameters, positive faces
		kVertexParamTerrainParameter1			= 44,		// Terrain border parameters, negative faces

		kVertexParamShadowCascadePlane1			= 45,		// Shadow cascade 0-1 blend plane
		kVertexParamShadowCascadePlane2			= 46,		// Shadow cascade 1-2 blend plane
		kVertexParamShadowCascadePlane3			= 47,		// Shadow cascade 2-3 blend plane

		kVertexParamSpaceScale					= 48,		// Reciprocal ambient space size (x, y, z, 0.0)
		kVertexParamVertexScaleOffset			= 49,		// (scale.x, scale.y, scale.z, offset)
		kVertexParamRadiusPointFactor			= 50,		// Radius-to-point-size factor
		kVertexParamPointCameraPlane			= 51,		// World-space camera plane over radius-to-point-size factor
		kVertexParamDistortCameraPlane			= 52,		// Object-space camera plane over focal length
		kVertexParamReflectionScale				= 53,		// (reflection offset scale, 0.0, 0.0, 0.0)
		kVertexParamRefractionScale				= 54,		// (refraction offset scale, 0.0, 0.0, 0.0)

		kVertexParamImpostorCameraPosition		= 55,		// Camera position used for impostor transitions
		kVertexParamImpostorTransition			= 56,		// (transition scale, transition bias, 0.0, 0.0)
		kVertexParamImpostorDepth				= 57,		// (impostor depth scale, impostor depth offset, tan(elevation), 0.0)
		kVertexParamImpostorPlaneS				= 58,		// Geometry noisy blend impostor s-ccordinate generation plane
		kVertexParamImpostorPlaneT				= 59,		// Geometry noisy blend impostor t-ccordinate generation plane

		kVertexParamPaintPlaneS					= 60,		// Paint space s-ccordinate generation plane
		kVertexParamPaintPlaneT					= 61		// Paint space t-ccordinate generation plane

		// Render::kMaxVertexParamCount
	};


	#define VERTEX_PARAM_MATRIX_MVP0				"0"
	#define VERTEX_PARAM_MATRIX_MVP1				"1"
	#define VERTEX_PARAM_MATRIX_MVP2				"2"
	#define VERTEX_PARAM_MATRIX_MVP3				"3"

	#define VERTEX_PARAM_MATRIX_VELOCITY_A0			"4"
	#define VERTEX_PARAM_MATRIX_VELOCITY_A1			"5"
	#define VERTEX_PARAM_MATRIX_VELOCITY_A2			"6"
	#define VERTEX_PARAM_MATRIX_VELOCITY_A3			"7"

	#define VERTEX_PARAM_MATRIX_VELOCITY_B0			"8"
	#define VERTEX_PARAM_MATRIX_VELOCITY_B1			"9"
	#define VERTEX_PARAM_MATRIX_VELOCITY_B2			"10"
	#define VERTEX_PARAM_MATRIX_VELOCITY_B3			"11"

	#define VERTEX_PARAM_MATRIX_WORLD0				"12"
	#define VERTEX_PARAM_MATRIX_WORLD1				"13"
	#define VERTEX_PARAM_MATRIX_WORLD2				"14"

	#define VERTEX_PARAM_MATRIX_CAMERA0				"15"
	#define VERTEX_PARAM_MATRIX_CAMERA1				"16"
	#define VERTEX_PARAM_MATRIX_CAMERA2				"17"

	#define VERTEX_PARAM_MATRIX_LIGHT0				"18"
	#define VERTEX_PARAM_MATRIX_LIGHT1				"19"
	#define VERTEX_PARAM_MATRIX_LIGHT2				"20"

	#define VERTEX_PARAM_MATRIX_SPACE0				"21"
	#define VERTEX_PARAM_MATRIX_SPACE1				"22"
	#define VERTEX_PARAM_MATRIX_SPACE2				"23"
 
	#define VERTEX_PARAM_MATRIX_SHADOW0				"24"
	#define VERTEX_PARAM_MATRIX_SHADOW1				"25"
	#define VERTEX_PARAM_MATRIX_SHADOW2				"26" 

	#define VERTEX_PARAM_CAMERA_POSITION			"27" 
	#define VERTEX_PARAM_CAMERA_RIGHT				"28" 
	#define VERTEX_PARAM_CAMERA_DOWN				"29"

	#define VERTEX_PARAM_VIEWPORT_TRANSFORM			"30"
 
	#define VERTEX_PARAM_LIGHT_POSITION				"31"
	#define VERTEX_PARAM_LIGHT_RANGE				"32"

	#define VERTEX_PARAM_FOG_PLANE					"33"
	#define VERTEX_PARAM_FOG_PARAMS					"34" 

	#define VERTEX_PARAM_SHADER_TIME				"35"
	#define VERTEX_PARAM_FIRE_PARAMS				"36"

	#define VERTEX_PARAM_TEXCOORD_GENERATE			"37"
	#define VERTEX_PARAM_TEXCOORD_TRANSFORM0		"38"
	#define VERTEX_PARAM_TEXCOORD_TRANSFORM1		"39"
	#define VERTEX_PARAM_TEXCOORD_VELOCITY0			"40"
	#define VERTEX_PARAM_TEXCOORD_VELOCITY1			"41"

	#define VERTEX_PARAM_TERRAIN_TEXCOORD_SCALE		"42"
	#define VERTEX_PARAM_TERRAIN_PARAMETER0			"43"
	#define VERTEX_PARAM_TERRAIN_PARAMETER1			"44"

	#define VERTEX_PARAM_SHADOW_CASCADE_PLANE1		"45"
	#define VERTEX_PARAM_SHADOW_CASCADE_PLANE2		"46"
	#define VERTEX_PARAM_SHADOW_CASCADE_PLANE3		"47"

	#define VERTEX_PARAM_SPACE_SCALE				"48"
	#define VERTEX_PARAM_VERTEX_SCALE_OFFSET		"49"
	#define VERTEX_PARAM_RADIUS_POINT_FACTOR		"50"
	#define VERTEX_PARAM_POINT_CAMERA_PLANE			"51"
	#define VERTEX_PARAM_DISTORT_CAMERA_PLANE		"52"
	#define VERTEX_PARAM_REFLECTION_SCALE			"53"
	#define VERTEX_PARAM_REFRACTION_SCALE			"54"

	#define VERTEX_PARAM_IMPOSTOR_CAMERA_POSITION	"55"
	#define VERTEX_PARAM_IMPOSTOR_TRANSITION		"56"
	#define VERTEX_PARAM_IMPOSTOR_DEPTH				"57"
	#define VERTEX_PARAM_IMPOSTOR_PLANE_S			"58"
	#define VERTEX_PARAM_IMPOSTOR_PLANE_T			"59"

	#define VERTEX_PARAM_PAINT_PLANE_S				"60"
	#define VERTEX_PARAM_PAINT_PLANE_T				"61"

	#define VERTEX_PARAM_COUNT						"62"


	enum
	{
		kMaxVertexSnippetCount		= 32
	};


	enum
	{
		kVertexSnippetOutputObjectPosition,
		kVertexSnippetOutputObjectNormal,
		kVertexSnippetOutputObjectTangent,
		kVertexSnippetOutputObjectBitangent,
		kVertexSnippetOutputWorldPosition,
		kVertexSnippetOutputWorldNormal,
		kVertexSnippetOutputWorldTangent,
		kVertexSnippetOutputWorldBitangent,
		kVertexSnippetOutputCameraNormal,
		kVertexSnippetOutputVertexGeometry,

		kVertexSnippetOutputObjectInfiniteLightDirection,
		kVertexSnippetCalculateObjectPointLightDirection,
		kVertexSnippetOutputObjectPointLightDirection,
		kVertexSnippetOutputTangentInfiniteLightDirection,
		kVertexSnippetOutputTangentPointLightDirection,

		kVertexSnippetCalculateObjectViewDirection,
		kVertexSnippetOutputObjectViewDirection,
		kVertexSnippetOutputTangentViewDirection,
		kVertexSnippetOutputAlternateViewDirection,

		kVertexSnippetOutputBillboardInfiniteLightDirection,
		kVertexSnippetOutputBillboardPointLightDirection,

		kVertexSnippetCalculateTerrainTangentData,
		kVertexSnippetOutputTerrainInfiniteLightDirection,
		kVertexSnippetOutputTerrainPointLightDirection,
		kVertexSnippetOutputTerrainViewDirection,
		kVertexSnippetOutputTerrainWorldTangentFrame,

		kVertexSnippetOutputRawTexcoords,
		kVertexSnippetOutputTerrainTexcoords,
		kVertexSnippetOutputImpostorTexcoords,
		kVertexSnippetOutputImpostorTransitionBlend,
		kVertexSnippetOutputGeometryImpostorTexcoords,
		kVertexSnippetOutputPaintTexcoords,

		kVertexSnippetOutputFireTexcoords,
		kVertexSnippetOutputFireArrayTexcoords,

		kVertexSnippetCalculateCameraDistance,
		kVertexSnippetOutputCameraWarpFunction,
		kVertexSnippetOutputCameraBumpWarpFunction,

		kVertexSnippetOutputDistortionDepth,

		kVertexSnippetOutputImpostorDepth,
		kVertexSnippetOutputImpostorRadius,
		kVertexSnippetOutputImpostorShadowRadius,

		kVertexSnippetOutputPointLightAttenuation,
		kVertexSnippetOutputSpotLightAttenuation,
		kVertexSnippetOutputDepthProjectTexcoord,
		kVertexSnippetOutputLandscapeProjectTexcoord,
		kVertexSnippetOutputCubeProjectTexcoord,
		kVertexSnippetOutputSpotProjectTexcoord,

		kVertexSnippetOutputAmbientSpaceVector,

		kVertexSnippetOutputFiniteConstantFogFactors,
		kVertexSnippetOutputInfiniteConstantFogFactors,
		kVertexSnippetOutputFiniteLinearFogFactors,
		kVertexSnippetOutputInfiniteLinearFogFactors,

		kVertexSnippetMotionBlurTransform,
		kVertexSnippetDeformMotionBlurTransform,
		kVertexSnippetVelocityMotionBlurTransform,
		kVertexSnippetInfiniteMotionBlurTransform,

		kVertexSnippetCount
	};


	enum
	{
		kVertexSnippetPositionFlag		= 1 << 0,
		kVertexSnippetNormalFlag		= 1 << 1,
		kVertexSnippetTangentFlag		= 1 << 2
	};


	struct VertexSnippet
	{
		Type				signature;
		unsigned_int32		flags;

		const char			*shaderCode;
	};


	struct VertexAssembly
	{
		unsigned_int32			*signatureStorage;
		const VertexSnippet		*vertexSnippet[kMaxVertexSnippetCount];

		VertexAssembly(unsigned_int32 *storage)
		{
			signatureStorage = storage;
			storage[0] = 0;
		}

		void AddSnippet(const VertexSnippet *snippet)
		{
			unsigned_int32 count = signatureStorage[0];
			Assert(count < kMaxVertexSnippetCount, "Vertex snippet table overflow.\n");

			vertexSnippet[count] = snippet;
			signatureStorage[++count] = snippet->signature;
			signatureStorage[0] = count;
		}
	};


	class VertexShader : public Render::VertexShaderObject, public Shared, public HashTableElement<VertexShader>
	{
		public:

			typedef ShaderSignature KeyType;

		private:

			static Storage<HashTable<VertexShader>>		hashTable;

			#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

			#endif //]

			unsigned_int32		shaderSignature[1];

			VertexShader(const char *source, unsigned_int32 size, const unsigned_int32 *signature);

			#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

			#endif //]

			~VertexShader();

		public:

			static const char				prologText[];
			static const char				epilogText[];

			static const VertexSnippet		nullTransform;
			static const VertexSnippet		modelviewProjectTransform;
			static const VertexSnippet		modelviewProjectTransformInfinite;
			static const VertexSnippet		modelviewProjectTransformHomogeneous;

			static const VertexSnippet		calculateCameraDirection;
			static const VertexSnippet		calculateCameraDirection4D;
			static const VertexSnippet		scaleVertexCalculateCameraDirection;
			static const VertexSnippet		scaleVertexCalculateCameraDirection4D;

			static const VertexSnippet		calculateBillboardPosition;
			static const VertexSnippet		calculateBillboardScalePosition;
			static const VertexSnippet		calculateVertexBillboardPosition;
			static const VertexSnippet		calculateVertexBillboardScalePosition;
			static const VertexSnippet		calculateLightedBillboardPosition;
			static const VertexSnippet		calculatePostboardPosition;
			static const VertexSnippet		calculatePostboardScalePosition;
			static const VertexSnippet		calculatePolyboardNormal;
			static const VertexSnippet		calculateLinearPolyboardNormal;
			static const VertexSnippet		calculatePolyboardPosition;
			static const VertexSnippet		calculatePolyboardScalePosition;

			static const VertexSnippet		calculateScalePosition;
			static const VertexSnippet		calculateScaleOffsetPosition;
			static const VertexSnippet		calculateExpandNormalPosition;

			static const VertexSnippet		calculateTerrainBorderPosition;
			static const VertexSnippet		calculateWaterHeightPosition;

			static const VertexSnippet		texcoordVertexTransform;
			static const VertexSnippet		extractGlowTransform;
			static const VertexSnippet		postProcessTransform;

			static const VertexSnippet		shadowInfiniteExtrusionTransform;
			static const VertexSnippet		shadowPointExtrusionTransform;
			static const VertexSnippet		shadowEndcapProjectionTransform;

			static const VertexSnippet		outputNormalTexcoord;
			static const VertexSnippet		outputPrimaryColor;
			static const VertexSnippet		outputSecondaryColor;
			static const VertexSnippet		outputPointSize;
			static const VertexSnippet		outputInfinitePointSize;

			static const VertexSnippet		copyPrimaryTexcoord0;
			static const VertexSnippet		copyPrimaryTexcoord1;
			static const VertexSnippet		copySecondaryTexcoord1;
			static const VertexSnippet		transformPrimaryTexcoord0;
			static const VertexSnippet		transformPrimaryTexcoord1;
			static const VertexSnippet		transformSecondaryTexcoord1;
			static const VertexSnippet		animatePrimaryTexcoord0;
			static const VertexSnippet		animatePrimaryTexcoord1;
			static const VertexSnippet		animateSecondaryTexcoord1;
			static const VertexSnippet		transformAnimatePrimaryTexcoord0;
			static const VertexSnippet		transformAnimatePrimaryTexcoord1;
			static const VertexSnippet		transformAnimateSecondaryTexcoord1;
			static const VertexSnippet		generateTexcoord0;
			static const VertexSnippet		generateTexcoord1;
			static const VertexSnippet		generateTransformTexcoord0;
			static const VertexSnippet		generateTransformTexcoord1;
			static const VertexSnippet		generateBaseTexcoord;
			static const VertexSnippet		generateAnimateTexcoord0;
			static const VertexSnippet		generateAnimateTexcoord1;
			static const VertexSnippet		generateTransformAnimateTexcoord0;
			static const VertexSnippet		generateTransformAnimateTexcoord1;

			static const VertexSnippet		normalizeNormal;
			static const VertexSnippet		normalizeTangent;
			static const VertexSnippet		orthonormalizeTangent;
			static const VertexSnippet		generateTangent;
			static const VertexSnippet		generateImpostorFrame;
			static const VertexSnippet		calculateBitangent;
			static const VertexSnippet		adjustBitangent;

			static const VertexSnippet		vertexSnippet[kVertexSnippetCount];

			KeyType GetKey(void) const
			{
				return (ShaderSignature(shaderSignature));
			}

			static void Initialize(void);
			static void Terminate(void);

			static unsigned_int32 Hash(const KeyType& key);

			static VertexShader *Find(const unsigned_int32 *signature);
			static VertexShader *Get(const VertexAssembly *assembly);
			static VertexShader *New(const char *source, unsigned_int32 size, const unsigned_int32 *signature);

			static void ReleaseCache(void);

			#if C4ORBIS //[ 

			// -- Orbis code hidden --

			#elif C4PS3 //[ 

			// -- PS3 code hidden --

			#endif //]
	};
}


#endif

// ZYUTNLM
