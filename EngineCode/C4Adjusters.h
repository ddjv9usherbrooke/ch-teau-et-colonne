//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4Adjusters_h
#define C4Adjusters_h


#include "C4Types.h"


namespace C4
{
	typedef Type	AdjusterType;


	enum
	{
		kAdjusterShake		= 'SHAK'
	};


	class FrustumCamera;


	class Adjuster
	{
		friend class FrustumCamera;

		private:

			AdjusterType		adjusterType;

			FrustumCamera		*targetCamera;

			virtual Adjuster *Replicate(void) const = 0;

		protected:

			C4API Adjuster(AdjusterType type);
			C4API Adjuster(const Adjuster& adjuster);

		public:

			C4API virtual ~Adjuster();

			AdjusterType GetAdjusterType(void) const
			{
				return (adjusterType);
			}

			Adjuster *Clone(void) const
			{
				return (Replicate());
			}

			FrustumCamera *GetTargetCamera(void) const
			{
				return (targetCamera);
			}

			virtual void CalculateAdjustmentTransform(Transform4D *transform) = 0;
	};


	class ShakeAdjuster : public Adjuster, public Completable<ShakeAdjuster>
	{
		private:

			float			maxShakeIntensity;
			float			shakeIntensity;

			float			shakeDuration;
			float			shakeTime;

			Antivector3D	shakeAxis;
			float			shakeFrequency;
			float			shakePhase;

			ShakeAdjuster();
			ShakeAdjuster(const ShakeAdjuster& shakeAdjuster);

			Adjuster *Replicate(void) const override;

			void NewShakeAxis(void);

		public:

			C4API ShakeAdjuster(float maxIntensity);
			C4API ~ShakeAdjuster();

			void CalculateAdjustmentTransform(Transform4D *transform) override;

			C4API void Shake(float intensity, int32 duration);
			C4API void Reset(void);
	};
}


#endif

// ZYUTNLM
