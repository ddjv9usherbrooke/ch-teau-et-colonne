//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4GeometryShaders.h"
#include "C4Engine.h"


#define C4LOG_GEOMETRY_SHADERS		0


using namespace C4;


Storage<HashTable<GeometryShader>> GeometryShader::hashTable;


const char GeometryShader::extrudeNormalLine[] =
{
	#if C4OPENGL

		"#version 150\n"
		"layout(points) in;\n"
		"layout(line_strip, max_vertices = 2) out;\n"

		"uniform vec4 gparam[" GEOMETRY_PARAM_COUNT "];\n"
		"in vresult\n"
		"{\n"
			"vec4 vcolor[2];\n"
			"vec4 texcoord[8];\n"
		"} gattrib[];\n"

		"void main()\n"
		"{\n"
			"gl_Position.x = dot(gl_in[0].gl_Position, gparam[" GEOMETRY_PARAM_MATRIX_MVP0 "]);\n"
			"gl_Position.y = dot(gl_in[0].gl_Position, gparam[" GEOMETRY_PARAM_MATRIX_MVP1 "]);\n"
			"gl_Position.z = dot(gl_in[0].gl_Position, gparam[" GEOMETRY_PARAM_MATRIX_MVP2 "]);\n"
			"gl_Position.w = dot(gl_in[0].gl_Position, gparam[" GEOMETRY_PARAM_MATRIX_MVP3 "]);\n"
			"EmitVertex();\n"

			"vec3 p = gl_in[0].gl_Position.xyz + normalize(gattrib[0].texcoord[0].xyz) * 0.0625;\n"
			"gl_Position.x = dot(p, gparam[" GEOMETRY_PARAM_MATRIX_MVP0 "].xyz) + gparam[" GEOMETRY_PARAM_MATRIX_MVP0 "].w;\n"
			"gl_Position.y = dot(p, gparam[" GEOMETRY_PARAM_MATRIX_MVP1 "].xyz) + gparam[" GEOMETRY_PARAM_MATRIX_MVP1 "].w;\n"
			"gl_Position.z = dot(p, gparam[" GEOMETRY_PARAM_MATRIX_MVP2 "].xyz) + gparam[" GEOMETRY_PARAM_MATRIX_MVP2 "].w;\n"
			"gl_Position.w = dot(p, gparam[" GEOMETRY_PARAM_MATRIX_MVP3 "].xyz) + gparam[" GEOMETRY_PARAM_MATRIX_MVP3 "].w;\n"
			"EmitVertex();\n"
		"}\n"

	#else

		""

	#endif
};


GeometryShader::GeometryShader(const char *source, unsigned_int32 size, const unsigned_int32 *signature) : GeometryShaderObject(source, size)
{
	MemoryMgr::CopyMemory(signature, shaderSignature, signature[0] * 4 + 4);

	#if C4LOG_GEOMETRY_SHADERS

		Engine::LogSource(source);

	#endif
}

GeometryShader::~GeometryShader()
{
}

void GeometryShader::Initialize(void)
{
	new(hashTable) HashTable<GeometryShader>(16, 16);
}

void GeometryShader::Terminate(void)
{
	hashTable->~HashTable();
}

unsigned_int32 GeometryShader::Hash(const KeyType& key)
{
	unsigned_int32 hash = 0;

	int32 count = key[0];
	for (machine a = 1; a <= count; a++)
	{
		hash += key[a];
		hash = (hash << 5) | (hash >> 27);
	}

	return (hash);
}

GeometryShader *GeometryShader::Find(const unsigned_int32 *signature)
{
	GeometryShader *shader = hashTable->Find(ShaderSignature(signature));
	if (shader)
	{
		shader->Retain();
	}

	return (shader);
} 

GeometryShader *GeometryShader::New(const char *source, unsigned_int32 size, const unsigned_int32 *signature)
{ 
	GeometryShader *shader = MemoryMgr::GetMainHeap()->New<GeometryShader>(sizeof(GeometryShader) + signature[0] * 4);
	new(shader) GeometryShader(source, size, signature); 
	hashTable->Insert(shader); 

	shader->Retain();
	return (shader);
} 

void GeometryShader::ReleaseCache(void)
{
	int32 bucketCount = hashTable->GetBucketCount();
	for (machine a = 0; a < bucketCount; a++) 
	{
		GeometryShader *shader = hashTable->GetFirstBucketElement(a);
		while (shader)
		{
			GeometryShader *next = shader->Next();
			if (shader->GetReferenceCount() == 1)
			{
				shader->Release();
			}

			shader = next;
		}
	}
}

// ZYUTNLM
