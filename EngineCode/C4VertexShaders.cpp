//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4Shaders.h"
#include "C4Engine.h"


#define C4LOG_VERTEX_SHADERS		0


using namespace C4;


#if C4OPENGL

	#define RESULT_POSITION			"gl_Position"
	#define RESULT_POINTSIZE		"gl_PointSize"
	#define RESULT_COLOR0			"vcolor[0]"
	#define RESULT_COLOR1			"vcolor[1]"
	#define RESULT_TEXCOORD0		"texcoord[0]"

#elif C4PSSL //[ 

			// -- Orbis code hidden --

#elif C4CG //[ 

			// -- PS3 code hidden --

#endif //]


Storage<HashTable<VertexShader>> VertexShader::hashTable;


#if C4OPENGL

	const char VertexShader::prologText[] =
	{
		"#version 150\n"

		#if !C4MACOS

			"invariant gl_Position;\n"

		#endif

		"uniform vec4 vparam[" VERTEX_PARAM_COUNT "];\n"
		"in vec4 attrib[16];\n"
		"out vresult\n"
		"{\n"
			"vec4 vcolor[2];\n"
			"vec4 texcoord[8];\n"
		"};\n"

		"void main()\n"
		"{\n"
			"vec4 temp;\n"
	};

	const char VertexShader::epilogText[] =
	{
		"}\n"
	};

#elif C4PSSL //[ 

			// -- Orbis code hidden --

#elif C4CG //[ 

			// -- PS3 code hidden --

#endif //]


const VertexSnippet VertexShader::nullTransform =
{
	'NULL', 0,

	RESULT_POSITION " = attrib[0];\n"
};

const VertexSnippet VertexShader::modelviewProjectTransform =
{
	'MODL', 0,

	RESULT_POSITION ".x = dot(attrib[0], vparam[" VERTEX_PARAM_MATRIX_MVP0 "]);\n"
	RESULT_POSITION ".y = dot(attrib[0], vparam[" VERTEX_PARAM_MATRIX_MVP1 "]);\n"
	RESULT_POSITION ".z = dot(attrib[0], vparam[" VERTEX_PARAM_MATRIX_MVP2 "]);\n"
	RESULT_POSITION ".w = dot(attrib[0], vparam[" VERTEX_PARAM_MATRIX_MVP3 "]);\n"
};

const VertexSnippet VertexShader::modelviewProjectTransformInfinite =
{
	'MDLI', 0,

	RESULT_POSITION ".x = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_MVP0 "].xyz);\n"
	RESULT_POSITION ".y = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_MVP1 "].xyz);\n"
	RESULT_POSITION ".z = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_MVP2 "].xyz);\n"
	RESULT_POSITION ".w = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_MVP3 "].xyz);\n"
};

const VertexSnippet VertexShader::modelviewProjectTransformHomogeneous = 
{
	'MDLH', 0,
 
	RESULT_POSITION ".x = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_MVP0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_MVP0 "].w;\n"
	RESULT_POSITION ".y = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_MVP1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_MVP1 "].w;\n" 
	RESULT_POSITION ".z = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_MVP2 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_MVP2 "].w;\n" 
	RESULT_POSITION ".w = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_MVP3 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_MVP3 "].w;\n"
};

const VertexSnippet VertexShader::calculateCameraDirection = 
{
	'CDIR', 0,

	FLOAT3 " cdir = attrib[0].xyz - vparam[" VERTEX_PARAM_CAMERA_POSITION "].xyz;\n"
}; 

const VertexSnippet VertexShader::calculateCameraDirection4D =
{
	'CDR4', 0,

	FLOAT3 " cdir = attrib[0].xyz * vparam[" VERTEX_PARAM_CAMERA_POSITION "].w - vparam[" VERTEX_PARAM_CAMERA_POSITION "].xyz;\n"
};

const VertexSnippet VertexShader::scaleVertexCalculateCameraDirection =
{
	'SCDR', 0,

	FLOAT4 " vrtx;\n"

	"vrtx.xyz = attrib[0].xyz * vparam[" VERTEX_PARAM_VERTEX_SCALE_OFFSET "].xyz;\n"
	"vrtx.w = attrib[6].w * vparam[" VERTEX_PARAM_VERTEX_SCALE_OFFSET "].w;\n"
	FLOAT3 " cdir = vrtx.xyz - vparam[" VERTEX_PARAM_CAMERA_POSITION "].xyz;\n"

};

const VertexSnippet VertexShader::scaleVertexCalculateCameraDirection4D =
{
	'SCD4', 0,

	FLOAT4 " vrtx;\n"

	"vrtx.xyz = attrib[0].xyz * vparam[" VERTEX_PARAM_VERTEX_SCALE_OFFSET "].xyz;\n"
	"vrtx.w = attrib[6].w * vparam[" VERTEX_PARAM_VERTEX_SCALE_OFFSET "].w;\n"
	FLOAT3 " cdir = vrtx.xyz * vparam[" VERTEX_PARAM_CAMERA_POSITION "].w - vparam[" VERTEX_PARAM_CAMERA_POSITION "].xyz;\n"
};

const VertexSnippet VertexShader::calculateBillboardPosition =
{
	'BILL', kVertexSnippetPositionFlag,

	FLOAT3 " opos = attrib[0].xyz + vparam[" VERTEX_PARAM_CAMERA_RIGHT "].xyz * attrib[6].x + vparam[" VERTEX_PARAM_CAMERA_DOWN "].xyz * attrib[6].y;\n"
};

const VertexSnippet VertexShader::calculateBillboardScalePosition =
{
	'SBLL', kVertexSnippetPositionFlag,

	"temp.xy = attrib[6].xy * vparam[" VERTEX_PARAM_VERTEX_SCALE_OFFSET "].xy;\n"
	FLOAT3 " opos = attrib[0].xyz + vparam[" VERTEX_PARAM_CAMERA_RIGHT "].xyz * temp.x + vparam[" VERTEX_PARAM_CAMERA_DOWN "].xyz * temp.y;\n"
};

const VertexSnippet VertexShader::calculateVertexBillboardPosition =
{
	'VBLL', kVertexSnippetPositionFlag,

	FLOAT3 " opos = vparam[" VERTEX_PARAM_CAMERA_RIGHT "].xyz * attrib[0].x + vparam[" VERTEX_PARAM_CAMERA_DOWN "].xyz * attrib[0].y;\n"
};

const VertexSnippet VertexShader::calculateVertexBillboardScalePosition =
{
	'VSBL', kVertexSnippetPositionFlag,

	"temp.xy = attrib[0].xy * vparam[" VERTEX_PARAM_VERTEX_SCALE_OFFSET "].xy;\n"
	FLOAT3 " opos = vparam[" VERTEX_PARAM_CAMERA_RIGHT "].xyz * temp.x + vparam[" VERTEX_PARAM_CAMERA_DOWN "].xyz * temp.y;\n"
};

const VertexSnippet VertexShader::calculateLightedBillboardPosition =
{
	'LBLL', kVertexSnippetPositionFlag,

	FLOAT3 " tang = vparam[" VERTEX_PARAM_CAMERA_RIGHT "].xyz * attrib[6].x + vparam[" VERTEX_PARAM_CAMERA_DOWN "].xyz * attrib[6].y;\n"
	FLOAT3 " btng = vparam[" VERTEX_PARAM_CAMERA_DOWN "].xyz * attrib[6].x - vparam[" VERTEX_PARAM_CAMERA_RIGHT "].xyz * attrib[6].y;\n"
	FLOAT3 " opos = attrib[0].xyz + tang * attrib[6].z + btng * attrib[6].w;\n"
};

const VertexSnippet VertexShader::calculatePostboardPosition =
{
	'POST', kVertexSnippetPositionFlag,

	FLOAT3 " opos;\n"

	"temp.xy = attrib[0].xy * vparam[" VERTEX_PARAM_CAMERA_POSITION "].w - vparam[" VERTEX_PARAM_CAMERA_POSITION "].xy;\n"
	"temp.w = " RSQRT "(temp.x * temp.x + temp.y * temp.y);\n"
	"opos.xy = " FLOAT2 "(temp.y * temp.w, -temp.x * temp.w) * attrib[6].x + attrib[0].xy;\n"
	"opos.z = attrib[0].z;\n"
};

const VertexSnippet VertexShader::calculatePostboardScalePosition =
{
	'SPST', kVertexSnippetPositionFlag,

	FLOAT3 " opos;\n"

	"temp.xy = attrib[0].xy * vparam[" VERTEX_PARAM_CAMERA_POSITION "].w - vparam[" VERTEX_PARAM_CAMERA_POSITION "].xy;\n"
	"temp.w = " RSQRT "(temp.x * temp.x + temp.y * temp.y);\n"
	"opos.xy = " FLOAT2 "(temp.y * temp.w, -temp.x * temp.w) * (attrib[6].x * vparam[" VERTEX_PARAM_VERTEX_SCALE_OFFSET "].x) + attrib[0].xy;\n"
	"opos.z = attrib[0].z * vparam[" VERTEX_PARAM_VERTEX_SCALE_OFFSET "].z;\n"
};

const VertexSnippet VertexShader::calculatePolyboardNormal =
{
	'POLY', kVertexSnippetPositionFlag,

	FLOAT3 " opos = cross(normalize(cdir), attrib[6].xyz);\n"
	"temp.w = dot(opos, opos);\n"
	"temp.y = " RSQRT "(temp.w);\n"
	"temp.x = temp.w * temp.y;\n"
	"temp.z = temp.x * (temp.x * (temp.x * 132.741 - 130.37) + 34.6667) + 1.0;\n"
	"opos *= min(temp.y, temp.z);\n"
};

const VertexSnippet VertexShader::calculateLinearPolyboardNormal =
{
	'LPOL', kVertexSnippetPositionFlag,

	FLOAT3 " opos = normalize(cross(cdir, attrib[6].xyz));\n"
};

const VertexSnippet VertexShader::calculatePolyboardPosition =
{
	'CPBP', 0,

	"opos = opos * attrib[6].w + attrib[0].xyz;\n"
};

const VertexSnippet VertexShader::calculatePolyboardScalePosition =
{
	'CPSP', 0,

	"opos = opos * vrtx.w + vrtx.xyz;\n"
};

const VertexSnippet VertexShader::calculateScalePosition =
{
	'SPOS', kVertexSnippetPositionFlag,

	FLOAT3 " opos = attrib[0].xyz * vparam[" VERTEX_PARAM_VERTEX_SCALE_OFFSET "].xyz;\n"
};

const VertexSnippet VertexShader::calculateScaleOffsetPosition =
{
	'SOPS', kVertexSnippetPositionFlag,

	FLOAT3 " opos = attrib[6].xyz * vparam[" VERTEX_PARAM_VERTEX_SCALE_OFFSET "].w + attrib[0].xyz * vparam[" VERTEX_PARAM_VERTEX_SCALE_OFFSET "].xyz;\n"
};

const VertexSnippet VertexShader::calculateExpandNormalPosition =
{
	'NEPT', kVertexSnippetPositionFlag,

	FLOAT3 " opos = %NRML * vparam[" VERTEX_PARAM_VERTEX_SCALE_OFFSET "].x + attrib[0].xyz;\n"
};

const VertexSnippet VertexShader::calculateTerrainBorderPosition =
{
	'TRBP', kVertexSnippetPositionFlag,

	FLOAT3 " prim = " FLOAT3 "(attrib[5].x >= 0.75, attrib[5].y >= 0.75, attrib[5].z >= 0.75);\n"
	"temp.xyz = " FLOAT3 "(attrib[5].x < 0.25, attrib[5].y < 0.25, attrib[5].z < 0.25);\n"

	"prim = prim * vparam[" VERTEX_PARAM_TERRAIN_PARAMETER0 "].xyz + temp.xyz * vparam[" VERTEX_PARAM_TERRAIN_PARAMETER1 "].xyz;\n"
	"prim.x = max(max(prim.x, prim.y), prim.z);\n"
	FLOAT3 " opos = attrib[0].xyz * prim.x + attrib[1].xyz * (1.0 - prim.x);\n"
};

const VertexSnippet VertexShader::calculateWaterHeightPosition =
{
	'WHTP', kVertexSnippetPositionFlag,

	FLOAT3 " opos = " FLOAT3 "(attrib[0].xy, attrib[2].w);\n"
};

const VertexSnippet VertexShader::texcoordVertexTransform =
{
	'TXVT', 0,

	RESULT_POSITION ".x = dot(attrib[8].xyz, vparam[" VERTEX_PARAM_MATRIX_MVP0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_MVP0 "].w;\n"
	RESULT_POSITION ".y = dot(attrib[8].xyz, vparam[" VERTEX_PARAM_MATRIX_MVP1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_MVP1 "].w;\n"
	RESULT_POSITION ".z = dot(attrib[8].xyz, vparam[" VERTEX_PARAM_MATRIX_MVP2 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_MVP2 "].w;\n"
	RESULT_POSITION ".w = dot(attrib[8].xyz, vparam[" VERTEX_PARAM_MATRIX_MVP3 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_MVP3 "].w;\n"
};

const VertexSnippet VertexShader::extractGlowTransform =
{
	'EXGT', 0,

	#if C4OPENGL

		RESULT_POSITION " = attrib[0];\n"
		RESULT_TEXCOORD0 ".xy = " FLOAT2 "(attrib[0].x + 1.0, attrib[0].y + 1.0) * vparam[" VERTEX_PARAM_VIEWPORT_TRANSFORM "].xy + vparam[" VERTEX_PARAM_VIEWPORT_TRANSFORM "].zw;\n"

	#elif C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]
};

const VertexSnippet VertexShader::postProcessTransform =
{
	'PSTP', 0,

	RESULT_POSITION " = attrib[0];\n"
	RESULT_TEXCOORD0 ".xy = " FLOAT2 "(attrib[0].x + 1.0, attrib[0].y + 1.0) * (vparam[" VERTEX_PARAM_VIEWPORT_TRANSFORM "].xy * 0.25) + vparam[" VERTEX_PARAM_VIEWPORT_TRANSFORM "].zw;\n"
};

const VertexSnippet VertexShader::shadowInfiniteExtrusionTransform =
{
	'SIET', 0,

	#if C4OPENGL

		"temp.w = float(attrib[0].w < 0.5);\n"

	#elif C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]

	FLOAT4 " position = attrib[0] - (attrib[0] + vparam[" VERTEX_PARAM_LIGHT_POSITION "]) * temp.w;\n"

	RESULT_POSITION ".x = dot(position, vparam[" VERTEX_PARAM_MATRIX_MVP0 "]);\n"
	RESULT_POSITION ".y = dot(position, vparam[" VERTEX_PARAM_MATRIX_MVP1 "]);\n"
	RESULT_POSITION ".z = dot(position, vparam[" VERTEX_PARAM_MATRIX_MVP2 "]);\n"
	RESULT_POSITION ".w = dot(position, vparam[" VERTEX_PARAM_MATRIX_MVP3 "]);\n"
};

const VertexSnippet VertexShader::shadowPointExtrusionTransform =
{
	'SPET', 0,

	FLOAT4 " position;\n"

	#if C4OPENGL

		"temp.w = float(attrib[0].w < 0.5);\n"

	#elif C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]

	"position.xyz = attrib[0].xyz - vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz * temp.w;\n"
	"position.w = attrib[0].w;\n"

	RESULT_POSITION ".x = dot(position, vparam[" VERTEX_PARAM_MATRIX_MVP0 "]);\n"
	RESULT_POSITION ".y = dot(position, vparam[" VERTEX_PARAM_MATRIX_MVP1 "]);\n"
	RESULT_POSITION ".z = dot(position, vparam[" VERTEX_PARAM_MATRIX_MVP2 "]);\n"
	RESULT_POSITION ".w = dot(position, vparam[" VERTEX_PARAM_MATRIX_MVP3 "]);\n"
};

const VertexSnippet VertexShader::shadowEndcapProjectionTransform =
{
	'SBCT', 0,

	FLOAT3 " position = attrib[0].xyz - vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz;\n"
	RESULT_POSITION ".x = dot(position, vparam[" VERTEX_PARAM_MATRIX_MVP0 "].xyz);\n"
	RESULT_POSITION ".y = dot(position, vparam[" VERTEX_PARAM_MATRIX_MVP1 "].xyz);\n"
	RESULT_POSITION ".z = dot(position, vparam[" VERTEX_PARAM_MATRIX_MVP2 "].xyz);\n"
	RESULT_POSITION ".w = dot(position, vparam[" VERTEX_PARAM_MATRIX_MVP3 "].xyz);\n"
};

const VertexSnippet VertexShader::outputNormalTexcoord =
{
	'NMTX', 0,

	RESULT_TEXCOORD0 ".xyz = attrib[2].xyz;\n"
};

const VertexSnippet VertexShader::outputPrimaryColor =
{
	'PCOL', 0,

	RESULT_COLOR0 " = attrib[3];\n"
};

const VertexSnippet VertexShader::outputSecondaryColor =
{
	'SCOL', 0,

	RESULT_COLOR1 " = attrib[4];\n"
};

const VertexSnippet VertexShader::outputPointSize =
{
	'PSIZ', 0,

	#if C4OPENGL || C4ORBIS

		RESULT_POINTSIZE " = attrib[6].x / (dot(%OPOS, vparam[" VERTEX_PARAM_POINT_CAMERA_PLANE "].xyz) + vparam[" VERTEX_PARAM_POINT_CAMERA_PLANE "].w);\n"

	#elif C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]
};

const VertexSnippet VertexShader::outputInfinitePointSize =
{
	'IPSZ', 0,

	RESULT_POINTSIZE " = attrib[6].x * vparam[" VERTEX_PARAM_RADIUS_POINT_FACTOR "].x;\n"
};

const VertexSnippet VertexShader::copyPrimaryTexcoord0 =
{
	'CPT0', 0,

	"$TEX0 = attrib[8].xy;\n"
};

const VertexSnippet VertexShader::copyPrimaryTexcoord1 =
{
	'CPT1', 0,

	"$TEX1 = attrib[8].xy;\n"
};

const VertexSnippet VertexShader::copySecondaryTexcoord1 =
{
	'CST1', 0,

	"$TEX1 = attrib[9].xy;\n"
};

const VertexSnippet VertexShader::transformPrimaryTexcoord0 =
{
	'TPT0', 0,

	"$TEX0 = attrib[8].xy * vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM0 "].xy + vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM0 "].zw;\n"
};

const VertexSnippet VertexShader::transformPrimaryTexcoord1 =
{
	'TPT1', 0,

	"$TEX1 = attrib[8].xy * vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM1 "].xy + vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM1 "].zw;\n"
};

const VertexSnippet VertexShader::transformSecondaryTexcoord1 =
{
	'TST1', 0,

	"$TEX1 = attrib[9].xy * vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM1 "].xy + vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM1 "].zw;\n"
};

const VertexSnippet VertexShader::animatePrimaryTexcoord0 =
{
	'APT0', 0,

	"$TEX0 = vparam[" VERTEX_PARAM_TEXCOORD_VELOCITY0 "].xy * vparam[" VERTEX_PARAM_SHADER_TIME "].x + attrib[8].xy;\n"
};

const VertexSnippet VertexShader::animatePrimaryTexcoord1 =
{
	'APT1', 0,

	"$TEX1 = vparam[" VERTEX_PARAM_TEXCOORD_VELOCITY1 "].xy * vparam[" VERTEX_PARAM_SHADER_TIME "].x + attrib[8].xy;\n"
};

const VertexSnippet VertexShader::animateSecondaryTexcoord1 =
{
	'AST1', 0,

	"$TEX1 = vparam[" VERTEX_PARAM_TEXCOORD_VELOCITY1 "].xy * vparam[" VERTEX_PARAM_SHADER_TIME "].x + attrib[9].xy;\n"
};

const VertexSnippet VertexShader::transformAnimatePrimaryTexcoord0 =
{
	'XPT0', 0,

	"temp.xy = attrib[8].xy * vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM0 "].xy + vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM0 "].zw;\n"
	"$TEX0 = vparam[" VERTEX_PARAM_TEXCOORD_VELOCITY0 "].xy * vparam[" VERTEX_PARAM_SHADER_TIME "].x + temp.xy;\n"
};

const VertexSnippet VertexShader::transformAnimatePrimaryTexcoord1 =
{
	'XPT1', 0,

	"temp.xy = attrib[8].xy * vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM1 "].xy + vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM1 "].zw;\n"
	"$TEX1 = vparam[" VERTEX_PARAM_TEXCOORD_VELOCITY1 "].xy * vparam[" VERTEX_PARAM_SHADER_TIME "].x + temp.xy;\n"
};

const VertexSnippet VertexShader::transformAnimateSecondaryTexcoord1 =
{
	'XST1', 0,

	"temp.xy = attrib[9].xy * vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM1 "].xy + vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM1 "].zw;\n"
	"$TEX1 = vparam[" VERTEX_PARAM_TEXCOORD_VELOCITY1 "].xy * vparam[" VERTEX_PARAM_SHADER_TIME "].x + temp.xy;\n"
};

const VertexSnippet VertexShader::generateTexcoord0 =
{
	'GTX0', 0,

	"$TEX0 = attrib[0].xy * vparam[" VERTEX_PARAM_TEXCOORD_GENERATE "].xy;\n"
};

const VertexSnippet VertexShader::generateTexcoord1 =
{
	'GTX1', 0,

	"$TEX1 = attrib[0].xy * vparam[" VERTEX_PARAM_TEXCOORD_GENERATE "].xy;\n"
};

const VertexSnippet VertexShader::generateTransformTexcoord0 =
{
	'GTT0', 0,

	"$TEX0 = attrib[0].xy * vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM0 "].xy + vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM0 "].zw;\n"
};

const VertexSnippet VertexShader::generateTransformTexcoord1 =
{
	'GTT1', 0,

	"$TEX1 = attrib[0].xy * vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM1 "].xy + vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM1 "].zw;\n"
};

const VertexSnippet VertexShader::generateBaseTexcoord =
{
	'GBTC', 0,

	FLOAT2 " btex = attrib[0].xy * vparam[" VERTEX_PARAM_TEXCOORD_GENERATE "].xy;\n"
};

const VertexSnippet VertexShader::generateAnimateTexcoord0 =
{
	'GAT0', 0,

	"$TEX0 = vparam[" VERTEX_PARAM_TEXCOORD_VELOCITY0 "].xy * vparam[" VERTEX_PARAM_SHADER_TIME "].x + btex;\n"
};

const VertexSnippet VertexShader::generateAnimateTexcoord1 =
{
	'GAT1', 0,

	"$TEX1 = vparam[" VERTEX_PARAM_TEXCOORD_VELOCITY1 "].xy * vparam[" VERTEX_PARAM_SHADER_TIME "].x + btex;\n"
};

const VertexSnippet VertexShader::generateTransformAnimateTexcoord0 =
{
	'GXT0', 0,

	"temp.xy = attrib[0].xy * vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM0 "].xy + vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM0 "].zw;\n"
	"$TEX0 = vparam[" VERTEX_PARAM_TEXCOORD_VELOCITY0 "].xy * vparam[" VERTEX_PARAM_SHADER_TIME "].x + temp.xy;\n"
};

const VertexSnippet VertexShader::generateTransformAnimateTexcoord1 =
{
	'GXT1', 0,

	"temp.xy = attrib[0].xy * vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM1 "].xy + vparam[" VERTEX_PARAM_TEXCOORD_TRANSFORM1 "].zw;\n"
	"$TEX1 = vparam[" VERTEX_PARAM_TEXCOORD_VELOCITY1 "].xy * vparam[" VERTEX_PARAM_SHADER_TIME "].x + temp.xy;\n"
};

const VertexSnippet VertexShader::normalizeNormal =
{
	'NMZN', kVertexSnippetNormalFlag,

	FLOAT3 " nrml = normalize(attrib[2].xyz);\n"
};

const VertexSnippet VertexShader::normalizeTangent =
{
	'NMZT', kVertexSnippetTangentFlag,

	FLOAT3 " tang = normalize(attrib[6].xyz);\n"
};

const VertexSnippet VertexShader::orthonormalizeTangent =
{
	'ONMT', kVertexSnippetTangentFlag,

	FLOAT3 " tang = normalize(attrib[6].xyz - %NRML * dot(%NRML, attrib[6].xyz));\n"
};

const VertexSnippet VertexShader::generateTangent =
{
	'NMTG', kVertexSnippetTangentFlag,

	"temp.w = " RSQRT "(%NRML.x * %NRML.x + %NRML.z * %NRML.z);\n"
	FLOAT3 " tang = " FLOAT3 "(%NRML.z * temp.w, 0.0, %NRML.x * -temp.w);\n"
};

const VertexSnippet VertexShader::generateImpostorFrame =
{
	'IFRM', kVertexSnippetNormalFlag | kVertexSnippetTangentFlag,

	FLOAT3 " nrml = " FLOAT3 "(normalize(vparam[" VERTEX_PARAM_CAMERA_POSITION "].xy - attrib[0].xy), 0.0);\n"
	FLOAT3 " tang = " FLOAT3 "(-nrml.y, nrml.x, 0.0);\n"
	FLOAT3 " btng = " FLOAT3 "(0.0, 0.0, 1.0);\n"
};

const VertexSnippet VertexShader::calculateBitangent =
{
	'CBTN', 0,

	FLOAT3 " btng = cross(%NRML, %TANG);\n"
};

const VertexSnippet VertexShader::adjustBitangent =
{
	'ABTN', 0,

	"btng *= attrib[6].w;\n"
};

const VertexSnippet VertexShader::vertexSnippet[kVertexSnippetCount] =
{
	// kVertexSnippetOutputObjectPosition
	{
		'POSI', 0,

		"$POSI.xyz = %OPOS;\n"
	},

	// kVertexSnippetOutputObjectNormal
	{
		'NRML', 0,

		"$NRML.xyz = %NRML;\n"
	},

	// kVertexSnippetOutputObjectTangent
	{
		'TANG', 0,

		"$TANG.xyz = %TANG;\n"
	},

	// kVertexSnippetOutputObjectBitangent
	{
		'BTNG', 0,

		"$BTNG.xyz = btng;\n"
	},

	// kVertexSnippetOutputWorldPosition
	{
		'WPOS', 0,

		"$WPOS.x = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD0 "].xyz, %OPOS) + vparam[" VERTEX_PARAM_MATRIX_WORLD0 "].w;\n"
		"$WPOS.y = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD1 "].xyz, %OPOS) + vparam[" VERTEX_PARAM_MATRIX_WORLD1 "].w;\n"
		"$WPOS.z = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD2 "].xyz, %OPOS) + vparam[" VERTEX_PARAM_MATRIX_WORLD2 "].w;\n"
	},

	// kVertexSnippetOutputWorldNormal
	{
		'WNRM', 0,

		"$WNRM.x = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD0 "].xyz, %NRML);\n"
		"$WNRM.y = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD1 "].xyz, %NRML);\n"
		"$WNRM.z = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD2 "].xyz, %NRML);\n"
	},

	// kVertexSnippetOutputWorldTangent
	{
		'WTAN', 0,

		"$WTAN.x = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD0 "].xyz, %TANG);\n"
		"$WTAN.y = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD1 "].xyz, %TANG);\n"
		"$WTAN.z = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD2 "].xyz, %TANG);\n"
	},

	// kVertexSnippetOutputWorldBitangent
	{
		'WBTN', 0,

		"$WBTN.x = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD0 "].xyz, btng);\n"
		"$WBTN.y = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD1 "].xyz, btng);\n"
		"$WBTN.z = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD2 "].xyz, btng);\n"
	},

	// kVertexSnippetOutputCameraNormal
	{
		'NRMC', 0,

		"$NRMC.x = dot(vparam[" VERTEX_PARAM_MATRIX_CAMERA0 "].xyz, %NRML);\n"
		"$NRMC.y = dot(vparam[" VERTEX_PARAM_MATRIX_CAMERA1 "].xyz, %NRML);\n"
		"$NRMC.z = dot(vparam[" VERTEX_PARAM_MATRIX_CAMERA2 "].xyz, %NRML);\n"
	},

	// kVertexSnippetOutputVertexGeometry
	{
		'GEOM', 0,

		"$GEOM.xy = attrib[1].xy;\n"
		"$GEOM.z = attrib[1].z - %OPOS.z;\n"
	},

	// kVertexSnippetOutputObjectInfiniteLightDirection
	{
		'OOIL', 0,

		"$OLDR.xyz = vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz;\n"
	},

	// kVertexSnippetCalculateObjectPointLightDirection
	{
		'COPL', 0,

		FLOAT3 " ldir = vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz - %OPOS;\n"
	},

	// kVertexSnippetOutputObjectPointLightDirection
	{
		'OOPL', 0,

		"$OLDR.xyz = ldir;\n"
	},

	// kVertexSnippetOutputTangentInfiniteLightDirection
	{
		'OTIL', 0,

		"$LDIR.x = dot(%TANG, vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz);\n"
		"$LDIR.y = dot(btng, vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz);\n"
		"$LDIR.z = dot(%NRML, vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz);\n"
	},

	// kVertexSnippetOutputTangentPointLightDirection
	{
		'OTPL', 0,

		"$LDIR.x = dot(%TANG, ldir);\n"
		"$LDIR.y = dot(btng, ldir);\n"
		"$LDIR.z = dot(%NRML, ldir);\n"
	},

	// kVertexSnippetCalculateObjectViewDirection
	{
		'COVD', 0,

		FLOAT3 " vdir = vparam[" VERTEX_PARAM_CAMERA_POSITION "].xyz - %OPOS;\n"
	},

	// kVertexSnippetOutputObjectViewDirection
	{
		'OOVD', 0,

		"$OVDR.xyz = vdir;\n"
	},

	// kVertexSnippetOutputTangentViewDirection
	{
		'OTVD', 0,

		"$VDIR.x = dot(%TANG, vdir);\n"
		"$VDIR.y = dot(btng, vdir);\n"
		"$VDIR.z = dot(%NRML, vdir);\n"
	},

	// kVertexSnippetOutputAlternateViewDirection
	{
		'OAVF', 0,

		"$VDIR.xyz = vdir;\n"
	},

	// kVertexSnippetOutputBillboardInfiniteLightDirection
	{
		'OBIL', 0,

		"temp.xyz = normalize(vdir);\n"
		"$LDIR.x = dot(tang, vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz);\n"
		"$LDIR.y = dot(btng, vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz);\n"
		"$LDIR.z = dot(temp, vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz);\n"
	},

	// kVertexSnippetOutputBillboardPointLightDirection
	{
		'OBPL', 0,

		"temp.xyz = normalize(vdir);\n"
		"$LDIR.x = dot(tang, ldir);\n"
		"$LDIR.y = dot(btng, ldir);\n"
		"$LDIR.z = dot(temp, ldir);\n"
	},

	// kVertexSnippetCalculateTerrainTangentData
	{
		'CTTD', 0,

		FLOAT4 " ttnd;\n"

		"ttnd.xyz = %NRML * %NRML;\n"
		"ttnd = ttnd.x + ttnd.yzyz;\n"
		"ttnd.xy = max(ttnd.xy, 0.03125);\n"
		"ttnd.x = " RSQRT "(ttnd.x);\n"
		"ttnd.y = " RSQRT "(ttnd.y);\n"
	},

	// kVertexSnippetOutputTerrainInfiniteLightDirection
	{
		'TLDI', 0,

		FLOAT3 " ltan = cross(vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz, %NRML);\n"
		"temp.xy = %NRML.xz * vparam[" VERTEX_PARAM_LIGHT_POSITION "].yx - %NRML.yx * vparam[" VERTEX_PARAM_LIGHT_POSITION "].xz;\n"
		"temp.zw = %NRML.xz * ltan.yx - %NRML.yx * ltan.xz;\n"
		"$TLDR.xy = temp.xz * ttnd.x;\n"
		"$TLD2 = temp.yw * ttnd.y;\n"
		"$TLDR.z = dot(%NRML, vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz);\n"
	},

	// kVertexSnippetOutputTerrainPointLightDirection
	{
		'TLDP', 0,

		FLOAT3 " tldp = vparam[" VERTEX_PARAM_LIGHT_POSITION "].xyz - %OPOS;\n"
		FLOAT3 " ltan = cross(tldp, %NRML);\n"
		"temp.xy = %NRML.xz * tldp.yx - %NRML.yx * tldp.xz;\n"
		"temp.zw = %NRML.xz * ltan.yx - %NRML.yx * ltan.xz;\n"
		"$TLDR.xy = temp.xz * ttnd.x;\n"
		"$TLD2 = temp.yw * ttnd.y;\n"
		"$TLDR.z = dot(%NRML, tldp);\n"
	},

	// kVertexSnippetOutputTerrainViewDirection
	{
		'TVDR', 0,

		FLOAT3 " tvdp = vparam[" VERTEX_PARAM_CAMERA_POSITION "].xyz - %OPOS;\n"
		FLOAT3 " vtan = cross(tvdp, %NRML);\n"
		"temp.xy = %NRML.xz * tvdp.yx - %NRML.yx * tvdp.xz;\n"
		"temp.zw = %NRML.xz * vtan.yx - %NRML.yx * vtan.xz;\n"
		"$TVDR.xy = temp.xz * ttnd.x;\n"
		"$TVDX = temp.y * ttnd.y;\n"
		"$TVDY = temp.w * ttnd.y;\n"
		"$TVDR.z = dot(%NRML, tvdp);\n"
	},

	// kVertexSnippetOutputTerrainWorldTangentFrame
	{
		'TWTF', 0,

		FLOAT3 " wtan;\n"

		"$TWNM.x = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD0 "].xyz, %NRML);\n"
		"$TWNM.y = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD1 "].xyz, %NRML);\n"
		"$TWNM.z = dot(vparam[" VERTEX_PARAM_MATRIX_WORLD2 "].xyz, %NRML);\n"

		"wtan.xw = -%NRML.yx * ttnd.xy;\n"
		"wtan.yz = %NRML.xz * ttnd.xy;\n"

		"$TWT1.x = vparam[" VERTEX_PARAM_MATRIX_WORLD0 "].x * wtan.x + vparam[" VERTEX_PARAM_MATRIX_WORLD0 "].y * wtan.y;\n"
		"$TWT1.y = vparam[" VERTEX_PARAM_MATRIX_WORLD1 "].x * wtan.x + vparam[" VERTEX_PARAM_MATRIX_WORLD1 "].y * wtan.y;\n"
		"$TWT1.z = vparam[" VERTEX_PARAM_MATRIX_WORLD2 "].x * wtan.x + vparam[" VERTEX_PARAM_MATRIX_WORLD2 "].y * wtan.y;\n"

		"$TWT2.x = vparam[" VERTEX_PARAM_MATRIX_WORLD0 "].x * wtan.z + vparam[" VERTEX_PARAM_MATRIX_WORLD0 "].z * wtan.w;\n"
		"$TWT2.y = vparam[" VERTEX_PARAM_MATRIX_WORLD1 "].x * wtan.z + vparam[" VERTEX_PARAM_MATRIX_WORLD1 "].z * wtan.w;\n"
		"$TWT2.z = vparam[" VERTEX_PARAM_MATRIX_WORLD2 "].x * wtan.z + vparam[" VERTEX_PARAM_MATRIX_WORLD2 "].z * wtan.w;\n"
	},

	// kVertexSnippetOutputRawTexcoords
	{
		'RTXC', 0,

		"$RTXC.xyz = attrib[8].xyz;\n"
	},

	// kVertexSnippetOutputTerrainTexcoords
	{
		'TERA', 0,

		"$TERA.xyz = %OPOS * vparam[" VERTEX_PARAM_TERRAIN_TEXCOORD_SCALE "].x;\n"
	},

	// kVertexSnippetOutputImpostorTexcoords
	{
		'CITX', 0,

		FLOAT2 " itmp;\n"

		FLOAT2 " idir = vparam[" VERTEX_PARAM_CAMERA_POSITION "].xy - attrib[0].xy * vparam[" VERTEX_PARAM_CAMERA_POSITION "].w;\n"

		"temp.x = (attrib[6].y * idir.x + attrib[6].z * idir.y) * " RSQRT "(dot(idir, idir));\n"
		"temp.x = 2.0 - temp.x - temp.x * temp.x * temp.x;\n"

		"itmp.x = attrib[6].y * idir.y - attrib[6].z * idir.x;\n"

		#if C4OPENGL

			"itmp.y = float(itmp.x < 0.0);\n"

		#elif C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

		#endif //]

		"temp.x += (8.0 - temp.x * 2.0) * itmp.y;\n"

		"temp.w = " FRAC "(temp.x);\n"
		"$IBLD = temp.w;\n"
		"temp.x = temp.x - temp.w + attrib[8].x;\n"
		"$IMPT.xy = attrib[8].xy;\n"
		"$IMPT.z = temp.x * 0.125;\n"
		"$IMPT.w = temp.x * 0.125 + 0.125;\n"
	},

	// kVertexSnippetOutputImpostorTransitionBlend
	{
		'IXBL', 0,

		"temp.xy = attrib[0].xy - vparam[" VERTEX_PARAM_IMPOSTOR_CAMERA_POSITION "].xy;\n"
		"$IXBL = length(temp.xy) * vparam[" VERTEX_PARAM_IMPOSTOR_TRANSITION "].x + vparam[" VERTEX_PARAM_IMPOSTOR_TRANSITION "].y;\n"
	},

	// kVertexSnippetOutputGeometryImpostorTexcoords
	{
		'GITX', 0,

		"$GITX:x = dot(attrib[0].xyz, vparam[" VERTEX_PARAM_IMPOSTOR_PLANE_S "].xyz) + vparam[" VERTEX_PARAM_IMPOSTOR_PLANE_S "].w;\n"
		"$GITX:y = dot(attrib[0].xyz, vparam[" VERTEX_PARAM_IMPOSTOR_PLANE_T "].xyz) + vparam[" VERTEX_PARAM_IMPOSTOR_PLANE_T "].w;\n"
	},

	// kVertexSnippetOutputPaintTexcoords
	{
		'PTXC', 0,

		"$PTXC:x = dot(attrib[0].xyz, vparam[" VERTEX_PARAM_PAINT_PLANE_S "].xyz) + vparam[" VERTEX_PARAM_PAINT_PLANE_S "].w;\n"
		"$PTXC:y = dot(attrib[0].xyz, vparam[" VERTEX_PARAM_PAINT_PLANE_T "].xyz) + vparam[" VERTEX_PARAM_PAINT_PLANE_T "].w;\n"
	},

	// kVertexSnippetOutputFireTexcoords
	{
		'FIRE', 0,

		"$FIRE.xy = attrib[8].xy;\n"
		"$FIRE.z = attrib[8].y * vparam[" VERTEX_PARAM_FIRE_PARAMS "].x;\n"

		"temp.xy = attrib[8].xy + attrib[8].zw;\n"
		"$FIR1 = vparam[" VERTEX_PARAM_TEXCOORD_VELOCITY0 "] * vparam[" VERTEX_PARAM_SHADER_TIME "].x + temp.xyxy;\n"
		"$FIR2 = vparam[" VERTEX_PARAM_TEXCOORD_VELOCITY1 "].xy * vparam[" VERTEX_PARAM_SHADER_TIME "].x + temp.xy;\n"
	},

	// kVertexSnippetOutputFireArrayTexcoords
	{
		'FIRA', 0,

		"$FIRE.xy = attrib[8].xy;\n"
		"$FIRE.z = attrib[8].y * attrib[6].y;\n"

		"temp.xy = attrib[8].xy + attrib[8].zw;\n"
		"$FIR1 = attrib[9] * vparam[" VERTEX_PARAM_SHADER_TIME "].x + temp.xyxy;\n"
		"$FIR2 = attrib[6].zw * vparam[" VERTEX_PARAM_SHADER_TIME "].x + temp.xy;\n"
	},

	// kVertexSnippetCalculateCameraDistance
	{
		'CAMD', 0,

		"temp.w = dot(vdir, vdir);\n"
		"float dist = " RSQRT "(temp.w) * temp.w;\n"
	},

	// kVertexSnippetOutputCameraWarpFunction
	{
		'WARP', 0,

		"$WARP.x = dot(vparam[" VERTEX_PARAM_CAMERA_RIGHT "].xyz, %NRML);\n"
		"$WARP.y = dot(vparam[" VERTEX_PARAM_CAMERA_DOWN "].xyz, %NRML);\n"

		"temp.x = dist * 8.0;\n"
		"temp.w = temp.x / (temp.x * dist + 4.0);\n"
		"$WARP.z = temp.w * vparam[" VERTEX_PARAM_REFLECTION_SCALE "].x;\n"
		"$WARP.w = temp.w * vparam[" VERTEX_PARAM_REFRACTION_SCALE "].x;\n"
	},

	// kVertexSnippetOutputCameraBumpWarpFunction
	{
		'BWRP', 0,

		"$RGHT.x = dot(vparam[" VERTEX_PARAM_CAMERA_RIGHT "].xyz, %TANG);\n"
		"$RGHT.y = dot(vparam[" VERTEX_PARAM_CAMERA_RIGHT "].xyz, btng);\n"
		"$RGHT.z = dot(vparam[" VERTEX_PARAM_CAMERA_RIGHT "].xyz, %NRML);\n"
		"$DOWN.x = dot(vparam[" VERTEX_PARAM_CAMERA_DOWN "].xyz, %TANG);\n"
		"$DOWN.y = dot(vparam[" VERTEX_PARAM_CAMERA_DOWN "].xyz, btng);\n"
		"$DOWN.z = dot(vparam[" VERTEX_PARAM_CAMERA_DOWN "].xyz, %NRML);\n"

		"temp.x = dist * 8.0;\n"
		"temp.w = temp.x / (temp.x * dist + 4.0);\n"
		"$RGHT.w = temp.w * vparam[" VERTEX_PARAM_REFLECTION_SCALE "].x;\n"
		"$DOWN.w = temp.w * vparam[" VERTEX_PARAM_REFRACTION_SCALE "].x;\n"
	},

	// kVertexSnippetOutputDistortionDepth
	{
		'DDEP', 0,

		"$DDEP = dot(%OPOS, vparam[" VERTEX_PARAM_DISTORT_CAMERA_PLANE "].xyz) + vparam[" VERTEX_PARAM_DISTORT_CAMERA_PLANE "].w;\n"
	},

	// kVertexSnippetOutputImpostorDepth
	{
		'IDEP', 0,

		"temp.y = dot(vparam[" VERTEX_PARAM_MATRIX_CAMERA1 "].xyz, %OPOS) + vparam[" VERTEX_PARAM_MATRIX_CAMERA1 "].w;\n"
		"temp.z = dot(vparam[" VERTEX_PARAM_MATRIX_CAMERA2 "].xyz, %OPOS) + vparam[" VERTEX_PARAM_MATRIX_CAMERA2 "].w - temp.y * vparam[" VERTEX_PARAM_IMPOSTOR_DEPTH "].z;\n"
		"$IDEP = temp.z * vparam[" VERTEX_PARAM_IMPOSTOR_DEPTH "].x + vparam[" VERTEX_PARAM_IMPOSTOR_DEPTH "].y;\n"
	},

	// kVertexSnippetOutputImpostorRadius
	{
		'IRAD', 0,

		"$IRAD = abs(attrib[6].x) * " FLOAT2 "(2.0, -1.0);\n"
	},

	// kVertexSnippetOutputImpostorShadowRadius
	{
		'ISRD', 0,

		"$ISRD = abs(attrib[6].x) * " FLOAT2 "(4.0, -1.0);\n"
	},

	// kVertexSnippetOutputPointLightAttenuation
	{
		'CPLA', 0,

		"$ATTN.xyz = ldir * vparam[" VERTEX_PARAM_LIGHT_RANGE "].w;\n"
	},

	// kVertexSnippetOutputSpotLightAttenuation
	{
		'CSLA', 0,

		"temp.x = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_LIGHT0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_LIGHT0 "].w;\n"
		"temp.y = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_LIGHT1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_LIGHT1 "].w;\n"
		"temp.z = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_LIGHT2 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_LIGHT2 "].w;\n"
		"$ATTN.xyz = temp.xyz * vparam[" VERTEX_PARAM_LIGHT_RANGE "].w;\n"
	},

	// kVertexSnippetOutputDepthProjectTexcoord
	{
		'DPPT', 0,

		"$SHAD:x = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_SHADOW0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_SHADOW0 "].w;\n"
		"$SHAD:y = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_SHADOW1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_SHADOW1 "].w;\n"
		"$SHDZ = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_SHADOW2 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_SHADOW2 "].w;\n"
	},

	// kVertexSnippetOutputLandscapeProjectTexcoord
	{
		'LSPT', 0,

		"$LAND.x = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_SHADOW0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_SHADOW0 "].w;\n"
		"$LAND.y = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_SHADOW1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_SHADOW1 "].w;\n"
		"$LAND.z = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_SHADOW2 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_SHADOW2 "].w;\n"

		"$CSCD.x = dot(%OPOS, vparam[" VERTEX_PARAM_SHADOW_CASCADE_PLANE1 "].xyz) + vparam[" VERTEX_PARAM_SHADOW_CASCADE_PLANE1 "].w;\n"
		"$CSCD.y = dot(%OPOS, vparam[" VERTEX_PARAM_SHADOW_CASCADE_PLANE2 "].xyz) + vparam[" VERTEX_PARAM_SHADOW_CASCADE_PLANE2 "].w;\n"
		"$CSCD.z = dot(%OPOS, vparam[" VERTEX_PARAM_SHADOW_CASCADE_PLANE3 "].xyz) + vparam[" VERTEX_PARAM_SHADOW_CASCADE_PLANE3 "].w;\n"
	},

	// kVertexSnippetOutputCubeProjectTexcoord
	{
		'CPPT', 0,

		"$PROJ.x = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_LIGHT0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_LIGHT0 "].w;\n"
		"$PROJ.y = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_LIGHT1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_LIGHT1 "].w;\n"
		"$PROJ.z = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_LIGHT2 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_LIGHT2 "].w;\n"
	},

	// kVertexSnippetOutputSpotProjectTexcoord
	{
		'SPPT', 0,

		"$PROJ.x = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_SHADOW0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_SHADOW0 "].w;\n"
		"$PROJ.y = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_SHADOW1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_SHADOW1 "].w;\n"
		"$PROJ.z = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_LIGHT2 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_LIGHT2 "].w;\n"
	},

	// kVertexSnippetOutputAmbientSpaceVector
	{
		'AMSV', 0,

		"$AMBT.x = dot(vparam[" VERTEX_PARAM_MATRIX_SPACE0 "].xyz, %NRML);\n"
		"$AMBT.y = dot(vparam[" VERTEX_PARAM_MATRIX_SPACE1 "].xyz, %NRML);\n"
		"$AMBT.z = dot(vparam[" VERTEX_PARAM_MATRIX_SPACE2 "].xyz, %NRML);\n"

		"temp.x = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_SPACE0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_SPACE0 "].w;\n"
		"temp.y = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_SPACE1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_SPACE1 "].w;\n"
		"temp.z = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_SPACE2 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_SPACE2 "].w;\n"
		"$APOS.xyz = temp.xyz * vparam[" VERTEX_PARAM_SPACE_SCALE "].xyz;\n"
	},

	// kVertexSnippetOutputFiniteConstantFogFactors
	{
		'FCFF', 0,

		"$FDTP = dot(%OPOS, vparam[" VERTEX_PARAM_FOG_PLANE "].xyz) + vparam[" VERTEX_PARAM_FOG_PLANE "].w;\n"
		"$FDTV = dot(vdir, vparam[" VERTEX_PARAM_FOG_PLANE "].xyz);\n"
	},

	// kVertexSnippetOutputInfiniteConstantFogFactors
	{
		'ICFF', 0,

		"$FDTV = dot(%OPOS, vparam[" VERTEX_PARAM_FOG_PLANE "].xyz);\n"
	},

	// kVertexSnippetOutputFiniteLinearFogFactors
	{
		'FLFF', 0,

		"temp.z = dot(%OPOS, vparam[" VERTEX_PARAM_FOG_PLANE "].xyz) + vparam[" VERTEX_PARAM_FOG_PLANE "].w;\n"
		"$FOGK = (temp.z + vparam[" VERTEX_PARAM_FOG_PARAMS "].x) * vparam[" VERTEX_PARAM_FOG_PARAMS "].y;\n"
		"$FDTV = dot(vdir, vparam[" VERTEX_PARAM_FOG_PLANE "].xyz);\n"
		"$FDTP = temp.z * vparam[" VERTEX_PARAM_FOG_PARAMS "].w;\n"
	},

	// kVertexSnippetOutputInfiniteLinearFogFactors
	{
		'ILFF', 0,

		"temp.x = dot(%OPOS, vparam[" VERTEX_PARAM_FOG_PLANE "].xyz);\n"
		"$FOGK = temp.x * vparam[" VERTEX_PARAM_FOG_PARAMS "].z;\n"
		"$FDTV = temp.x;\n"
	},

	// kVertexSnippetMotionBlurTransform
	{
		'BLUR', 0,

		"$VELA.x = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A0 "].w;\n"
		"$VELA.y = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A1 "].w;\n"
		"$VELA.w = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A3 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A3 "].w;\n"

		"$VELB.x = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B0 "].w;\n"
		"$VELB.y = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B1 "].w;\n"
		"$VELB.w = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B3 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B3 "].w;\n"
	},

	// kVertexSnippetDeformMotionBlurTransform
	{
		'DBLR', 0,

		"$VELA.x = dot(attrib[7].xyz, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A0 "].w;\n"
		"$VELA.y = dot(attrib[7].xyz, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A1 "].w;\n"
		"$VELA.w = dot(attrib[7].xyz, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A3 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A3 "].w;\n"

		"$VELB.x = dot(attrib[0].xyz, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B0 "].w;\n"
		"$VELB.y = dot(attrib[0].xyz, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B1 "].w;\n"
		"$VELB.w = dot(attrib[0].xyz, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B3 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B3 "].w;\n"
	},

	// kVertexSnippetVelocityMotionBlurTransform
	{
		'VBLR', 0,

		"temp.xyz = attrib[0].xyz - attrib[7].xyz * vparam[" VERTEX_PARAM_SHADER_TIME "].y;\n"

		"$VELA.x = dot(temp.xyz, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A0 "].w;\n"
		"$VELA.y = dot(temp.xyz, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A1 "].w;\n"
		"$VELA.w = dot(temp.xyz, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A3 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A3 "].w;\n"

		"$VELB.x = dot(attrib[0].xyz, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B0 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B0 "].w;\n"
		"$VELB.y = dot(attrib[0].xyz, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B1 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B1 "].w;\n"
		"$VELB.w = dot(attrib[0].xyz, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B3 "].xyz) + vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B3 "].w;\n"
	},

	// kVertexSnippetInfiniteMotionBlurTransform
	{
		'IBLR', 0,

		"$VELA.x = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A0 "].xyz);\n"
		"$VELA.y = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A1 "].xyz);\n"
		"$VELA.w = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_A3 "].xyz);\n"

		"$VELB.x = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B0 "].xyz);\n"
		"$VELB.y = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B1 "].xyz);\n"
		"$VELB.w = dot(%OPOS, vparam[" VERTEX_PARAM_MATRIX_VELOCITY_B3 "].xyz);\n"
	}
};


VertexShader::VertexShader(const char *source, unsigned_int32 size, const unsigned_int32 *signature) : VertexShaderObject(source, size)
{
	MemoryMgr::CopyMemory(signature, shaderSignature, signature[0] * 4 + 4);

	#if C4LOG_VERTEX_SHADERS

		Engine::LogSource(source);

	#endif

	#if C4ORBIS //[ 

			// -- Orbis code hidden --

	#endif //]

	#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]
}

#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

#endif //]

VertexShader::~VertexShader()
{
	#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]
}

void VertexShader::Initialize(void)
{
	new(hashTable) HashTable<VertexShader>(16, 16);

	#if C4ORBIS //[ 

			// -- Orbis code hidden --

	#endif //]
}

void VertexShader::Terminate(void)
{
	hashTable->~HashTable();
}

unsigned_int32 VertexShader::Hash(const KeyType& key)
{
	unsigned_int32 hash = 0;

	int32 count = key[0];
	for (machine a = 1; a <= count; a++)
	{
		hash += key[a];
		hash = (hash << 5) | (hash >> 27);
	}

	return (hash);
}

VertexShader *VertexShader::Find(const unsigned_int32 *signature)
{
	VertexShader *shader = hashTable->Find(ShaderSignature(signature));
	if (shader)
	{
		shader->Retain();
	}

	return (shader);
}

VertexShader *VertexShader::Get(const VertexAssembly *assembly)
{
	const unsigned_int32 *signature = assembly->signatureStorage;
	VertexShader *shader = hashTable->Find(ShaderSignature(signature));
	if (!shader)
	{
		char *source = ShaderAttribute::sourceStorage;
		int32 size = Text::CopyText(prologText, source);

		int32 count = signature[0];
		for (machine a = 0; a < count; a++)
		{
			size += Text::CopyText(assembly->vertexSnippet[a]->shaderCode, &source[size]);
		}

		size += Text::CopyText(epilogText, &source[size]);

		shader = MemoryMgr::GetMainHeap()->New<VertexShader>(sizeof(VertexShader) + signature[0] * 4);
		new(shader) VertexShader(source, size, signature);
		hashTable->Insert(shader);
	}

	shader->Retain();
	return (shader);
}

VertexShader *VertexShader::New(const char *source, unsigned_int32 size, const unsigned_int32 *signature)
{
	VertexShader *shader = MemoryMgr::GetMainHeap()->New<VertexShader>(sizeof(VertexShader) + signature[0] * 4);
	new(shader) VertexShader(source, size, signature);
	hashTable->Insert(shader);

	shader->Retain();
	return (shader);
}

void VertexShader::ReleaseCache(void)
{
	int32 bucketCount = hashTable->GetBucketCount();
	for (machine a = 0; a < bucketCount; a++)
	{
		VertexShader *shader = hashTable->GetFirstBucketElement(a);
		while (shader)
		{
			VertexShader *next = shader->Next();

			if (shader->GetReferenceCount() == 1)
			{
				shader->Release();
			}

			shader = next;
		}
	}
}

#if C4ORBIS //[ 

			// -- Orbis code hidden --

#elif C4PS3 //[ 

			// -- PS3 code hidden --

#endif //]

// ZYUTNLM
