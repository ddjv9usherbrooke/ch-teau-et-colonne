//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4Wintab.h"


using namespace C4;


namespace C4
{
	UINT (WINAPI *WTInfoA)(UINT, UINT, void *) = nullptr;
	HCTX (WINAPI *WTOpenA)(HWND, TabletLogContext *, BOOL) = nullptr;
	BOOL (WINAPI *WTClose)(HCTX) = nullptr;
	int (WINAPI *WTPacketsGet)(HCTX, int, void *) = nullptr;
}

// ZYUTNLM
