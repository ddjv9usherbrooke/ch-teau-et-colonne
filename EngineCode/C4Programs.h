//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4Programs_h
#define C4Programs_h


#include "C4Render.h"
#include "C4Resources.h"


namespace C4
{
	class VertexShader;
	class FragmentShader;
	class GeometryShader;


	struct ProgramStageTable
	{
		VertexShader		*vertexShader;
		FragmentShader		*fragmentShader;
		GeometryShader		*geometryShader;

		ProgramStageTable()
		{
			geometryShader = nullptr;
		}

		bool operator ==(const ProgramStageTable& x) const;
	};


	class ShaderSignature
	{
		private:

			const unsigned_int32	*signature;

		public:

			explicit ShaderSignature(const unsigned_int32 *sig)
			{
				signature = sig;
			}

			operator bool(void) const
			{
				return (signature != nullptr);
			}

			const unsigned_int32& operator [](machine k) const
			{
				return (signature[k]);
			}

			bool operator ==(const ShaderSignature& x) const;
	};


	class ProgramSignature
	{
		private:

			ShaderSignature		vertexSignature;
			ShaderSignature		fragmentSignature;
			ShaderSignature		geometrySignature;

		public:

			ProgramSignature(const ProgramStageTable& table);
			ProgramSignature(const unsigned_int32 *vertexSig, const unsigned_int32 *fragmentSig, const unsigned_int32 *geometrySig);

			const ShaderSignature& GetVertexSignature(void) const
			{
				return (vertexSignature);
			}

			const ShaderSignature& GetFragmentSignature(void) const
			{
				return (fragmentSignature);
			}

			const ShaderSignature& GetGeometrySignature(void) const
			{
				return (geometrySignature);
			}

			bool operator ==(const ProgramSignature& x) const;
	};


	class ProgramBinary : public HashTableElement<ProgramBinary>
	{
		public:

			typedef ProgramSignature KeyType;

		private:

			static Storage<HashTable<ProgramBinary>>	hashTable;

			unsigned_int32				binaryFormat;
			unsigned_int32				binarySize; 

			unsigned_int32				signatureSize;
			Storage<ProgramSignature>	programSignature; 

			ProgramBinary(const ProgramStageTable& table, unsigned_int32 format, const void *data, unsigned_int32 size); 
 
		public:

			~ProgramBinary();
 
			const KeyType& GetKey(void) const
			{
				return (*programSignature);
			}
 
			unsigned_int32 GetBinaryFormat(void) const
			{
				return (binaryFormat);
			}

			unsigned_int32 GetBinarySize(void) const
			{
				return (binarySize);
			}

			const void *GetBinaryData(void) const
			{
				return (reinterpret_cast<const char *>(this + 1) + signatureSize);
			}

			static ProgramBinary *Find(const ProgramStageTable& table)
			{
				return (hashTable->Find(ProgramSignature(table)));
			}

			static void Initialize(void);
			static void Terminate(void);

			static unsigned_int32 Hash(const KeyType& key);

			static ProgramBinary *New(const ProgramStageTable& table, unsigned_int32 binaryFormat, const void *binaryData, unsigned_int32 binarySize);
	};


	class ShaderProgram : public Render::ShaderProgramObject, public Shared, public HashTableElement<ShaderProgram>, public LinkTarget<ShaderProgram>
	{
		public:

			typedef ProgramStageTable KeyType;

		private:

			static Storage<HashTable<ShaderProgram>>	hashTable;

			#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

			#endif //]

			ProgramStageTable		stageTable;

		public:

			ShaderProgram(const ProgramStageTable& table);
			~ShaderProgram();

			#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

			#endif //]

			const KeyType& GetKey(void) const
			{
				return (stageTable);
			}

			VertexShader *GetVertexShader(void) const
			{
				return (stageTable.vertexShader);
			}

			FragmentShader *GetFragmentShader(void) const
			{
				return (stageTable.fragmentShader);
			}

			GeometryShader *GetGeometryShader(void) const
			{
				return (stageTable.geometryShader);
			}

			static void Initialize(void);
			static void Terminate(void);

			static unsigned_int32 Hash(const KeyType& key);

			static ShaderProgram *Get(const ProgramStageTable& table);

			static void ReleaseCache(void);
			static void Purge(void);
	};
}


#endif

// ZYUTNLM
