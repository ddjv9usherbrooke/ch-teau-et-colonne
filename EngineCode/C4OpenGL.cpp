//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#define C4OpenGL_cpp


#include "C4Graphics.h"

#if C4MACOS

	#include "C4Engine.h"

#endif


using namespace C4;


#if C4WINDOWS

	void *C4::GetCoreExtFuncAddress(bool core, const char *coreName, const char *extName)
	{
		if (core)
		{
			void *address = wglGetProcAddress(coreName);
			if (address)
			{
				return (address);
			}
		}

		return (wglGetProcAddress(extName));
	}

#elif C4LINUX

	void *C4::GetCoreExtFuncAddress(bool core, const char *coreName, const char *extName)
	{
		if (core)
		{
			void *address = (void *) glXGetProcAddress(reinterpret_cast<const GLubyte *>(coreName));
			if (address)
			{
				return (address);
			}
		}

		return ((void *) glXGetProcAddress(reinterpret_cast<const GLubyte *>(extName)));
	}

#elif C4MACOS

	void *C4::GetCoreExtFuncAddress(CFBundleRef bundle, bool core, const char *coreName, const char *extName)
	{
		if (core)
		{
			void *address = Engine::GetBundleFunctionAddress(bundle, coreName);
			if (address)
			{
				return (address);
			}
		}

		return (Engine::GetBundleFunctionAddress(bundle, extName));
	}

#endif


#if C4WINDOWS || C4LINUX

	void C4::InitializeOpenglExtensions(GraphicsCapabilities *capabilities)

#elif C4MACOS || C4IOS

	void C4::InitializeOpenglExtensions(GraphicsCapabilities *capabilities, CFBundleRef openglBundle)

#endif

{
	const bool *extensionFlag = capabilities->extensionFlag;
	unsigned_int32 version = capabilities->openglVersion;

	GLGETCOREFUNC(glBlendColor);
	GLGETCOREFUNC(glBlendEquation);
	GLGETCOREFUNC(glDrawRangeElements);
	GLGETCOREFUNC(glTexImage3D);
	GLGETCOREFUNC(glTexSubImage3D);
	GLGETCOREFUNC(glCopyTexSubImage3D);
	GLGETCOREFUNC(glSampleCoverage);
	GLGETCOREFUNC(glActiveTexture);
	GLGETCOREFUNC(glClientActiveTexture);
	GLGETCOREFUNC(glCompressedTexImage3D);
	GLGETCOREFUNC(glCompressedTexImage2D);
	GLGETCOREFUNC(glCompressedTexImage1D);
	GLGETCOREFUNC(glCompressedTexSubImage3D);
	GLGETCOREFUNC(glCompressedTexSubImage2D);
	GLGETCOREFUNC(glCompressedTexSubImage1D);
	GLGETCOREFUNC(glBlendFuncSeparate);
	GLGETCOREFUNC(glMultiDrawArrays);
	GLGETCOREFUNC(glMultiDrawElements);
	GLGETCOREFUNC(glGenQueries);
	GLGETCOREFUNC(glDeleteQueries);
	GLGETCOREFUNC(glBeginQuery); 
	GLGETCOREFUNC(glEndQuery);
	GLGETCOREFUNC(glGetQueryiv);
	GLGETCOREFUNC(glGetQueryObjectiv); 
	GLGETCOREFUNC(glGetQueryObjectuiv);
	GLGETCOREFUNC(glBindBuffer); 
	GLGETCOREFUNC(glDeleteBuffers); 
	GLGETCOREFUNC(glGenBuffers);
	GLGETCOREFUNC(glBufferData);
	GLGETCOREFUNC(glBufferSubData);
	GLGETCOREFUNC(glMapBuffer); 
	GLGETCOREFUNC(glUnmapBuffer);
	GLGETCOREFUNC(glGetBufferSubData);
	GLGETCOREFUNC(glPointParameterf);
	GLGETCOREFUNC(glPointParameterfv);
	GLGETCOREFUNC(glPointParameteri); 
	GLGETCOREFUNC(glPointParameteriv);
	GLGETCOREFUNC(glStencilFuncSeparate);
	GLGETCOREFUNC(glStencilOpSeparate);
	GLGETCOREFUNC(glCreateShader);
	GLGETCOREFUNC(glDeleteShader);
	GLGETCOREFUNC(glShaderSource);
	GLGETCOREFUNC(glCompileShader);
	GLGETCOREFUNC(glAttachShader);
	GLGETCOREFUNC(glDetachShader);
	GLGETCOREFUNC(glCreateProgram);
	GLGETCOREFUNC(glDeleteProgram);
	GLGETCOREFUNC(glLinkProgram);
	GLGETCOREFUNC(glUseProgram);
	GLGETCOREFUNC(glValidateProgram);
	GLGETCOREFUNC(glGetShaderiv);
	GLGETCOREFUNC(glGetProgramiv);
	GLGETCOREFUNC(glGetShaderInfoLog);
	GLGETCOREFUNC(glGetProgramInfoLog);
	GLGETCOREFUNC(glGetShaderSource);
	GLGETCOREFUNC(glGetActiveUniform);
	GLGETCOREFUNC(glGetActiveAttrib);
	GLGETCOREFUNC(glGetUniformLocation);
	GLGETCOREFUNC(glGetAttribLocation);
	GLGETCOREFUNC(glBindAttribLocation);
	GLGETCOREFUNC(glUniform1f);
	GLGETCOREFUNC(glUniform2f);
	GLGETCOREFUNC(glUniform3f);
	GLGETCOREFUNC(glUniform4f);
	GLGETCOREFUNC(glUniform1i);
	GLGETCOREFUNC(glUniform2i);
	GLGETCOREFUNC(glUniform3i);
	GLGETCOREFUNC(glUniform4i);
	GLGETCOREFUNC(glUniform1fv);
	GLGETCOREFUNC(glUniform2fv);
	GLGETCOREFUNC(glUniform3fv);
	GLGETCOREFUNC(glUniform4fv);
	GLGETCOREFUNC(glUniform1iv);
	GLGETCOREFUNC(glUniform2iv);
	GLGETCOREFUNC(glUniform3iv);
	GLGETCOREFUNC(glUniform4iv);
	GLGETCOREFUNC(glUniformMatrix2fv);
	GLGETCOREFUNC(glUniformMatrix3fv);
	GLGETCOREFUNC(glUniformMatrix4fv);
	GLGETCOREFUNC(glVertexAttribPointer);
	GLGETCOREFUNC(glEnableVertexAttribArray);
	GLGETCOREFUNC(glDisableVertexAttribArray);
	GLGETCOREFUNC(glBindFragDataLocation);

	if (extensionFlag[kExtensionConditionalRender])
	{
		GLGETEXTFUNC(glBeginConditionalRender);
		GLGETEXTFUNC(glEndConditionalRender);
	}

	if (extensionFlag[kExtensionFramebufferBlit])
	{
		GLGETCOREEXTFUNC(glBlitFramebuffer);
	}

	if (extensionFlag[kExtensionFramebufferObject])
	{
		GLGETCOREEXTFUNC(glBindRenderbuffer);
		GLGETCOREEXTFUNC(glDeleteRenderbuffers);
		GLGETCOREEXTFUNC(glGenRenderbuffers);
		GLGETCOREEXTFUNC(glRenderbufferStorage);
		GLGETCOREEXTFUNC(glGetRenderbufferParameteriv);
		GLGETCOREEXTFUNC(glBindFramebuffer);
		GLGETCOREEXTFUNC(glDeleteFramebuffers);
		GLGETCOREEXTFUNC(glGenFramebuffers);
		GLGETCOREEXTFUNC(glCheckFramebufferStatus);
		GLGETCOREEXTFUNC(glFramebufferTexture1D);
		GLGETCOREEXTFUNC(glFramebufferTexture2D);
		GLGETCOREEXTFUNC(glFramebufferTexture3D);
		GLGETCOREEXTFUNC(glFramebufferRenderbuffer);
		GLGETCOREEXTFUNC(glGenerateMipmap);
	}

	if (extensionFlag[kExtensionFramebufferMultisample])
	{
		GLGETCOREEXTFUNC(glRenderbufferStorageMultisample);
	}

	if (extensionFlag[kExtensionMapBufferRange])
	{
		GLGETCOREEXTFUNC(glMapBufferRange);
	}
	else
	{
		glMapBufferRange = &Render::MapBufferRange;
	}

	if (extensionFlag[kExtensionTextureArray])
	{
		GLGETCOREEXTFUNC(glFramebufferTexture);
		GLGETCOREEXTFUNC(glFramebufferTextureLayer);
	}

	if (extensionFlag[kExtensionUniformBufferObject])
	{
		GLGETCOREEXTFUNC(glGetUniformIndices);
		GLGETCOREEXTFUNC(glGetActiveUniformsiv);
		GLGETCOREEXTFUNC(glGetActiveUniformName);
		GLGETCOREEXTFUNC(glGetUniformBlockIndex);
		GLGETCOREEXTFUNC(glGetActiveUniformBlockiv);
		GLGETCOREEXTFUNC(glGetActiveUniformBlockName);
		GLGETCOREEXTFUNC(glBindBufferRange);
		GLGETCOREEXTFUNC(glBindBufferBase);
		GLGETCOREEXTFUNC(glUniformBlockBinding);
	}

	if (extensionFlag[kExtensionVertexArrayObject])
	{
		GLGETCOREEXTFUNC(glBindVertexArray);
		GLGETCOREEXTFUNC(glDeleteVertexArrays);
		GLGETCOREEXTFUNC(glGenVertexArrays);
	}

	if (extensionFlag[kExtensionGeometryShader4])
	{
		GLGETCOREEXTFUNC(glProgramParameteri);
	}

	if (extensionFlag[kExtensionInstancedArrays])
	{
		GLGETCOREEXTFUNC(glDrawElementsInstanced);
		GLGETCOREEXTFUNC(glVertexAttribDivisor);
	}

	if (extensionFlag[kExtensionInvalidateSubdata])
	{
		GLGETEXTFUNC(glInvalidateTexImage);
		GLGETEXTFUNC(glInvalidateBufferData);
		GLGETEXTFUNC(glInvalidateFramebuffer);
	}
	else
	{
		glInvalidateTexImage = &Render::InvalidateTexImage;
		glInvalidateBufferData = &Render::InvalidateBufferData;
		glInvalidateFramebuffer = &Render::InvalidateFramebuffer;
	}

	if (extensionFlag[kExtensionTimerQuery])
	{
		GLGETEXTFUNC(glQueryCounter);
		GLGETEXTFUNC(glGetQueryObjecti64v);
		GLGETEXTFUNC(glGetQueryObjectui64v);
	}

	if (extensionFlag[kExtensionSampleShading])
	{
		GLGETCOREEXTFUNC(glMinSampleShading);
	}

	if (extensionFlag[kExtensionTessellationShader])
	{
		GLGETCOREEXTFUNC(glPatchParameteri);
		GLGETCOREEXTFUNC(glPatchParameterfv);
	}

	if (extensionFlag[kExtensionGetProgramBinary])
	{
		GLGETEXTFUNC(glGetProgramBinary);
		GLGETEXTFUNC(glProgramBinary);
		GLGETCOREEXTFUNC(glProgramParameteri);
	}

	#if C4DEBUG

		if (extensionFlag[kExtensionDebugOutput])
		{
			GLGETEXTFUNC(glDebugMessageControlARB);
			GLGETEXTFUNC(glDebugMessageInsertARB);
			GLGETEXTFUNC(glDebugMessageCallbackARB);
			GLGETEXTFUNC(glGetDebugMessageLogARB);
			GLGETEXTFUNC(glGetPointerv);
		}

	#endif

	if (extensionFlag[kExtensionDepthBoundsTest])
	{
		GLGETEXTFUNC(glDepthBoundsEXT);
	}

	if (extensionFlag[kExtensionDirectStateAccess])
	{
		GLGETEXTFUNC(glBindMultiTextureEXT);
		GLGETEXTFUNC(glTextureParameteriEXT);
		GLGETEXTFUNC(glTextureParameterivEXT);
		GLGETEXTFUNC(glTextureParameterfEXT);
		GLGETEXTFUNC(glTextureParameterfvEXT);
		GLGETEXTFUNC(glTextureImage2DEXT);
		GLGETEXTFUNC(glTextureSubImage2DEXT);
		GLGETEXTFUNC(glTextureImage3DEXT);
		GLGETEXTFUNC(glTextureSubImage3DEXT);
		GLGETEXTFUNC(glCompressedTextureImage2DEXT);
		GLGETEXTFUNC(glCompressedTextureImage3DEXT);
		GLGETEXTFUNC(glCopyTextureSubImage2DEXT);
		GLGETEXTFUNC(glNamedBufferDataEXT);
		GLGETEXTFUNC(glNamedBufferSubDataEXT);
		GLGETEXTFUNC(glGetNamedBufferSubDataEXT);
		GLGETEXTFUNC(glMapNamedBufferEXT);
		GLGETEXTFUNC(glMapNamedBufferRangeEXT);
		GLGETEXTFUNC(glUnmapNamedBufferEXT);
		GLGETEXTFUNC(glProgramUniform4fvEXT);
		GLGETEXTFUNC(glVertexArrayVertexAttribOffsetEXT);
		GLGETEXTFUNC(glNamedRenderbufferStorageEXT);
		GLGETEXTFUNC(glNamedRenderbufferStorageMultisampleEXT);
		GLGETEXTFUNC(glNamedRenderbufferStorageMultisampleCoverageEXT);
		GLGETEXTFUNC(glCheckNamedFramebufferStatusEXT);
		GLGETEXTFUNC(glNamedFramebufferTexture2DEXT);
		GLGETEXTFUNC(glNamedFramebufferTexture3DEXT);
		GLGETEXTFUNC(glNamedFramebufferRenderbufferEXT);
		GLGETEXTFUNC(glFramebufferDrawBufferEXT);
		GLGETEXTFUNC(glFramebufferDrawBuffersEXT);
		GLGETEXTFUNC(glFramebufferReadBufferEXT);
		GLGETEXTFUNC(glNamedFramebufferTextureEXT);
		GLGETEXTFUNC(glNamedFramebufferTextureLayerEXT);
		GLGETEXTFUNC(glTextureRenderbufferEXT);

		if (!glMapNamedBufferRangeEXT)
		{
			glMapNamedBufferRangeEXT = &Render::MapNamedBufferRange;
		}
	}
	else
	{
		glBindMultiTextureEXT = &Render::BindMultiTexture;
		glTextureParameteriEXT = &Render::TextureParameteri;
		glTextureParameterivEXT = &Render::TextureParameteriv;
		glTextureParameterfEXT = &Render::TextureParameterf;
		glTextureParameterfvEXT = &Render::TextureParameterfv;
		glTextureImage2DEXT = &Render::TextureImage2D;
		glTextureSubImage2DEXT = &Render::TextureSubImage2D;
		glTextureImage3DEXT = &Render::TextureImage3D;
		glTextureSubImage3DEXT = &Render::TextureSubImage3D;
		glCompressedTextureImage2DEXT = &Render::CompressedTextureImage2D;
		glCompressedTextureImage3DEXT = &Render::CompressedTextureImage3D;
		glCopyTextureSubImage2DEXT = &Render::CopyTextureSubImage2D;
		glNamedBufferDataEXT = &Render::NamedBufferData;
		glNamedBufferSubDataEXT = &Render::NamedBufferSubData;
		glGetNamedBufferSubDataEXT = &Render::GetNamedBufferSubData;
		glMapNamedBufferEXT = &Render::MapNamedBuffer;
		glMapNamedBufferRangeEXT = &Render::MapNamedBufferRange;
		glUnmapNamedBufferEXT = &Render::UnmapNamedBuffer;
		glProgramUniform4fvEXT = &Render::ProgramUniform4fv;
		glVertexArrayVertexAttribOffsetEXT = &Render::VertexArrayVertexAttribOffset;
	}

	if (extensionFlag[kExtensionExplicitMultisample])
	{
		GLGETEXTFUNC(glGetBooleanIndexedvEXT);
		GLGETEXTFUNC(glGetIntegerIndexedvEXT);
		GLGETEXTFUNC(glGetMultisamplefvNV);
		GLGETEXTFUNC(glSampleMaskIndexedNV);
		GLGETEXTFUNC(glTexRenderbufferNV);
	}

	if (extensionFlag[kExtensionFramebufferMultisampleCoverage])
	{
		GLGETEXTFUNC(glRenderbufferStorageMultisampleCoverageNV);
	}

	if (extensionFlag[kExtensionShaderBufferLoad])
	{
		GLGETEXTFUNC(glMakeNamedBufferResidentNV);
		GLGETEXTFUNC(glMakeNamedBufferNonResidentNV);
		GLGETEXTFUNC(glGetNamedBufferParameterui64vNV);
		GLGETEXTFUNC(glProgramUniformui64NV);
		GLGETEXTFUNC(glProgramUniformui64vNV);
	}

	if (extensionFlag[kExtensionTransformFeedback])
	{
		GLGETEXTFUNC(glTransformFeedbackAttribsNV);
		GLGETEXTFUNC(glTransformFeedbackVaryingsNV);
		GLGETEXTFUNC(glBeginTransformFeedbackNV);
		GLGETEXTFUNC(glEndTransformFeedbackNV);
		GLGETEXTFUNC(glGetVaryingLocationNV);
		GLGETEXTFUNC(glGetActiveVaryingNV);
		GLGETEXTFUNC(glActiveVaryingNV);
	}

	if (extensionFlag[kExtensionVertexBufferUnifiedMemory])
	{
		GLGETEXTFUNC(glBufferAddressRangeNV);
		GLGETEXTFUNC(glVertexAttribFormatNV);
		GLGETEXTFUNC(glVertexAttribIFormatNV);
		GLGETEXTFUNC(glGetIntegerui64i_vNV);
	}
}

// ZYUTNLM
